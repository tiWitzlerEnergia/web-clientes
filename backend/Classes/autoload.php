<?php
include "Funcoes.php";
function incluirClasses($nomeClasse)
{
    if (file_exists($nomeClasse . ".php")) {
        require_once($nomeClasse . ".php");
    }
}

function procurarDiretorio($nomeClasses)
{
    $procuraFront = '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'backend' . DIRECTORY_SEPARATOR;
    $array_paths = array(
        //Caso for chamado dentro de backend, classes ou outro dentro de classes
        '',
        '' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR,
        'controller' . DIRECTORY_SEPARATOR,
        'view' . DIRECTORY_SEPARATOR,
        'model' . DIRECTORY_SEPARATOR,
        'model' . DIRECTORY_SEPARATOR . 'Card' . DIRECTORY_SEPARATOR,
        'model' . DIRECTORY_SEPARATOR . 'Relatorio' . DIRECTORY_SEPARATOR,
        'model' . DIRECTORY_SEPARATOR . 'Migracao' . DIRECTORY_SEPARATOR,
        'model' . DIRECTORY_SEPARATOR . 'Graficos' . DIRECTORY_SEPARATOR,

        //Caso for chamado desde os update:
        '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Card' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Relatorio' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Migracao' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Graficos' . DIRECTORY_SEPARATOR,

        //Caso for procurado desde as div do front
        '..' . DIRECTORY_SEPARATOR . $procuraFront . 'Classes' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Card' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Relatorio' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Migracao' . DIRECTORY_SEPARATOR,
        '..' . DIRECTORY_SEPARATOR . $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Graficos' . DIRECTORY_SEPARATOR,
        //Caso for chamado desde la do front:
        $procuraFront . 'Classes' . DIRECTORY_SEPARATOR,
        $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR,
        $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR,
        $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR,
        $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Card' . DIRECTORY_SEPARATOR,
        $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Relatorio' . DIRECTORY_SEPARATOR,
        $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Migracao' . DIRECTORY_SEPARATOR,
        $procuraFront . 'Classes' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'Graficos' . DIRECTORY_SEPARATOR,

    );

    foreach ($array_paths as $dir) {
        $arquivo = $dir . $nomeClasses . ".php";
        if (file_exists($arquivo)) {
            require_once($arquivo);
        }
    }
}

//spl_autoload_register("incluirClasses");
spl_autoload_register("procurarDiretorio");
