<?php
class RegistrosView
{
    // Function que verifica que deu certo insert:
    public function responseRequest($values)
    {
        $msg = new MessagesController();
        if (isset($values["idstatus"]) && $values["idstatus"] = 200) {
            return $msg->message($values["idstatus"], $values["msg"]);
        } else {
            return $msg->message($values["iderror"], $values["msg"]);
        }
    }

    // Function que verifica se select retornou dados ou se eles estão vazios:
    public function validateSelect($values)
    {
        if (isset($values) && !empty($values)) {
            return $values;
        } else {
            $msg = new MessagesController();

            $error = array("iderror" => 404, "msg" => "Não tem nenhum registro feito.");
            return $msg->message($error["iderror"], $error["msg"]);
        }
    }
}
