<?php
class ClienteView
{
    // Function que vai receber o status da api ao fazer uma acao
    public function responseApi($values)
    {
        if (isset($values) && !empty($values)) {
            if (isset($values->status) && !empty($values->status)) {
                // Quer dizer que a resposta tem um status, ou seja, é um registro correto:
                return $values->status;
            } else {
                // Não tem resposta correta do servidor:
                return 400;
            }
        } else {
            // Nem tem dados de resposta:
            return 500;
        }
    }

    // Recolhe os values retornados de dados de info usuario
    public function valuesInfoUsuarioCliente($values)
    {
        $finalvalues = array();
        if (isset($values) && !empty($values)) {
            // Caso onde é verdadeiro, tem dados de resposta:
            $totValues = count($values) ? count($values) : 0;
            if ($totValues > 0) {
                for ($i = 0; $i < $totValues; $i++) {
                    // Recolhe valores:
                    $id_cliente = isset($values[$i]->id_cliente) ? $values[$i]->id_cliente : 0;
                    $nome = isset($values[$i]->nome) ? $values[$i]->nome : "";
                    $email = isset($values[$i]->email) ? $values[$i]->email : "";
                    $username = isset($values[$i]->username) ? $values[$i]->username : "";
                    $nome_fantasia = isset($values[$i]->nome_fantasia) ? $values[$i]->nome_fantasia : "";
                    $colaborador_tec_id = isset($values[$i]->colaborador_tec_id) ? $values[$i]->colaborador_tec_id : 0;
                    $colaborador_comercial_id = isset($values[$i]->colaborador_comercial_id) ? $values[$i]->colaborador_comercial_id : 0;
                    $cow_client_id = isset($values[$i]->cow_client_id) ? $values[$i]->cow_client_id : 0;
                    $email_recp = isset($values[$i]->email_recp) ? $values[$i]->email_recp : "";

                    $finalvalues[] = array("id_cliente" => $id_cliente, "nome" => $nome, "email" => $email, "username" => $username, "nome_fantasia" => $nome_fantasia, "colaborador_tec_id" => $colaborador_tec_id, "colaborador_comercial_id" => $colaborador_comercial_id, "cow_client_id" => $cow_client_id, "email_recp" => $email_recp);
                }
            }
        }

        return $finalvalues;
    }
}
