<?php
class ColaboradoresView
{
    // Function que vai receber o status da api ao fazer uma acao
    public function responseApi($values)
    {
        if (isset($values) && !empty($values)) {
            if (isset($values->status) && !empty($values->status)) {
                // Quer dizer que a resposta tem um status, ou seja, é um registro correto:
                return $values->status;
            } else {
                // Não tem resposta correta do servidor:
                return 400;
            }
        } else {
            // Nem tem dados de resposta:
            return 500;
        }
    }

    // Recolhe os values retornados de lista clientes e os retorna como array para facil manipulacao
    public function valuesListaClientesUsuario($values)
    {
        $finalvalues = array();
        if (isset($values) && !empty($values)) {
            // Caso onde é verdadeiro, tem dados de resposta:
            $totValues = count($values) ? count($values) : 0;
            if ($totValues > 0) {
                for ($i = 0; $i < $totValues; $i++) {
                    // Recolhe valores:
                    $id_cliente = isset($values[$i]->id_cliente) ? $values[$i]->id_cliente : 0;
                    $nome = isset($values[$i]->nome) ? $values[$i]->nome : "";
                    $nome_fantasia = isset($values[$i]->nome_fantasia) ? $values[$i]->nome_fantasia : "";
                    $colaborador_tec_id = isset($values[$i]->colaborador_tec_id) ? $values[$i]->colaborador_tec_id : 0;
                    $colaborador_comercial_id = isset($values[$i]->colaborador_comercial_id) ? $values[$i]->colaborador_comercial_id : 0;
                    $cow_client_id = isset($values[$i]->cow_client_id) ? $values[$i]->cow_client_id : 0;
                    $email = isset($values[$i]->email) ? $values[$i]->email : "";

                    $finalvalues[] = array("id_cliente" => $id_cliente, "nome" => $nome, "nome_fantasia" => $nome_fantasia, "colaborador_tec_id" => $colaborador_tec_id, "colaborador_comercial_id" => $colaborador_comercial_id, "cow_client_id" => $cow_client_id, "email" => $email);
                }
            }
        }

        return $finalvalues;
    }

    // Recolhe os values retornados das informacoes do usuaio colaborador e retorna como array para facil manipulacao
    public function valuesInfoUsuarioColaborador($values)
    {
        $finalvalues = array();
        if (isset($values) && !empty($values)) {
            // Caso onde é verdadeiro, tem dados de resposta:
            $totValues = count($values) ? count($values) : 0;
            if ($totValues > 0) {
                for ($i = 0; $i < $totValues; $i++) {
                    // Recolhe valores:
                    $id_colaborador = isset($values[$i]->id_colaborador) ? $values[$i]->id_colaborador : 0;
                    $nome_colaborador = isset($values[$i]->nome_colaborador) ? $values[$i]->nome_colaborador : "";
                    $email = isset($values[$i]->email) ? $values[$i]->email : "";
                    $telefone_1 = isset($values[$i]->telefone_1) ? $values[$i]->telefone_1 : "";
                    $telefone_2 = isset($values[$i]->telefone_2) ? $values[$i]->telefone_2 : "";
                    $path_img = isset($values[$i]->path_img) ? $values[$i]->path_img : "";
                    $username = isset($values[$i]->username) ? $values[$i]->username : "";
                    $password_colaborador = isset($values[$i]->password_colaborador) ? $values[$i]->password_colaborador : "";
                    $administrador = isset($values[$i]->administrador) ? $values[$i]->administrador : false;

                    $finalvalues[] = array("id_colaborador" => $id_colaborador, "nome_colaborador" => $nome_colaborador, "email" => $email, "telefone_1" => $telefone_1, "telefone_2" => $telefone_2, "path_img" => $path_img, "username" => $username, "password_colaborador" => $password_colaborador, "administrador" => $administrador);
                }
            }
        }
        //var_dump($finalvalues);
        return $finalvalues;
    }
}
