<?php

class MenuDashboard extends Usuario
{
    // Atributos:

    // Metodos especiais:
    public function __construct()
    {
        $this->definirUsuario();
    }

    // Metodos publicos:
    public function criarElementoMenu($dir, $url, $imagem, $idAnimacao, $nome, $manutencao = false)
    {
        //Recolher diretorios da URL atual:
        $ativo = file_exists($dir) ? "c-active" : "";
        $cManutencao = ($manutencao) ? "<span class=\"badge badge-danger\"> Manutenção</span>" : "";
        echo "
        <li class=\"c-sidebar-nav-item\">
            <a rel=\"nofollow\" class=\"c-sidebar-nav-link $ativo\" href=\"$url\">
                <img src=\"$imagem\" class=\"c-icon\">
                <h1 class=\"ml11 $idAnimacao\">
                    <span class=\"text-wrapper\">
                        <span class=\"line line1\"></span>
                        <span class=\"letters\">$nome</span>
                        $cManutencao
                    </span>
                </h1>
            </a>
        </li>
        ";
    }

    public function criarAnimacaoElemento($idAnimacao){
        echo "
            // Wrap every letter in a span
            var textWrapper = document.querySelector('.$idAnimacao .letters');
            textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, \"<span class='letter'>$&</span>\");

            anime.timeline({
                loop: false
             })
             .add({
                targets: '.$idAnimacao .line',
                scaleY: [0, 1],
                opacity: [0.5, 1],
                easing: \"easeOutExpo\",
                duration: 700
             })
             .add({
                targets: '.$idAnimacao .line',
                translateX: [0, document.querySelector('.$idAnimacao .letters').getBoundingClientRect().width + 10],
                easing: \"easeOutExpo\",
                duration: 700,
                delay: 0
             }).add({
                targets: '.$idAnimacao .letter',
                opacity: [0, 1],
                easing: \"easeOutExpo\",
                duration: 600,
                offset: '-=775',
                delay: (el, i) => 34 * (i + 1)
             }).add({
                targets: '.$idAnimacao',
                //opacity: 0,
                duration: 1000,
                easing: \"easeOutExpo\",
                delay: 1000
             }).add({
                targets: '.$idAnimacao .line',
                opacity: 0,
                duration: 1000,
                easing: \"easeOutExpo\",
                delay: 1000
             });
        ";
    }
}
