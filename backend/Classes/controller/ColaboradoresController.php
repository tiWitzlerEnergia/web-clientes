<?php
class ColaboradoresController
{
    // Request get para recolher dados de listaClientesUsuarioApi
    public function listaClientesUsuario($id_colaborador)
    {
        // Inicializa as classes necessarias
        $view = new ColaboradoresView();
        $model = new Colaboradores();

        $model->constructListaClientesUsuario($id_colaborador);
        $values = $model->listaClientesUsuarioApi();
        return $view->valuesListaClientesUsuario($values);
    }

    // Request get para recolher informacoes de infoUsuarioColaboradorApi
    public function infoUsuarioColaborador($username)
    {
        // Inicializa as classes necessarias
        $view = new ColaboradoresView();
        $model = new Colaboradores();

        $model->constructInfoUsuarioColaborador($username);
        $values = $model->infoUsuarioColaboradorApi();
        $valuesArray = $view->valuesInfoUsuarioColaborador($values);
        $info = isset($values) && !empty($values) ? $valuesArray[0] : array();
        
        return $info;
    }

    // Request get para recolher dados de listaClientesAdminApi
    public function listaClientesAdmin() {
        // Inicializa as classes necessarias
        $view = new ColaboradoresView();
        $model = new Colaboradores();

        $values = $model->listaClientesAdminApi();
        return $view->valuesListaClientesUsuario($values);
    }
}
