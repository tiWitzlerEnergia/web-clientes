<?php
class ConnectController
{
    public function selectQuery($newQuery, $params = array())
    {
        $model = new Connect();
        $view = new ConnectView();

        $stmt = $model->newQuery($newQuery, $params);
        $validate = $view->validateQuery($stmt);

        if (isset($validate["idstatus"]) && $validate["idstatus"] == 200) {
            return $view->validateFetchAll($stmt->fetchAll(PDO::FETCH_ASSOC));
        } else {
            // Aconteceu um erro ao executar a query, entao retorna como msg de erro e seu status:
            var_dump($validate);
        }
    }

    public function insertQuery($newQuery, $params = array())
    {
        $model = new Connect();
        $view = new ConnectView();

        $stmt = $model->newQuery($newQuery, $params);

        $validate = $view->validateQuery($stmt);

        /*if (isset($validate["idstatus"]) && $validate["idstatus"] == 200) {
            //return $stmt;
            return $validate;
        } else {
            var_dump($validate);
        }*/
        // Validate pode retornar "iderror" em caso de erro ou idstatus em caso de dar certo
        return $validate;
    }

    public function updateQuery($newQuery, $params = array())
    {
        $model = new Connect();
        $view = new ConnectView();

        $stmt = $model->newQuery($newQuery, $params);
        $validate = $view->validateRowsAffected($stmt);

        /*if($validate == 200) {
            return array("idstatus" => 200, "msg" => "Foi atualizado");
        } else {
            return $validate;
        }*/

        return $validate;
    }
}
