<?php
class RegistrosController
{
    // Function que vai fazer o insert dos dados de registro das informacoes do visitante no postgres:
    public function salvarInfoRegistroPg()
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();
        $timezone = new DateTimeZone("America/Sao_Paulo");
        $dateTime = new DateTime('now', $timezone);
        $uri = new Uri();

        // Constroi dados no model e executa query dentro do model
        $data = $dateTime->format('Y-m-d H:i:s');
        $dados = getUserBrowser();
        $rota = $uri->scriptName();

        $system = isset($dados["systemDetect"]) && isset($dados["systemDetect"]) != "" ? $dados["systemDetect"] : "N/D";
        $browser = isset($dados["browserDetect"]) && isset($dados["browserDetect"]) != "" ? $dados["browserDetect"] : "N/D";
        $system_name = isset($dados["systemNameDetected"]) && isset($dados["systemNameDetected"]) != "" ? $dados["systemNameDetected"] : "N/D";
        $dispositivo = isset($dados["dispositivo"]) && isset($dados["dispositivo"]) != "" ? $dados["dispositivo"] : "N/D";

        $model->constructSalvarInfoRegistro($rota, $dispositivo, $system, $system_name, $browser, $data);
        $values = $model->salvarInfoRegistroRequestPg();

        $message = $view->responseRequest($values);
        return $message;
    }

    // Function que vai fazer o insert dos dados de registro das informacoes do visitante no postgres:
    public function salvarInfoLoginRegistroPg()
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();
        $timezone = new DateTimeZone("America/Sao_Paulo");
        $dateTime = new DateTime('now', $timezone);
        $uri = new Uri();

        // Constroi dados no model e executa query dentro do model
        $data = $dateTime->format('Y-m-d H:i:s');
        $dados = getUserBrowser();
        $rota = $uri->scriptName();

        $system = isset($dados["systemDetect"]) && isset($dados["systemDetect"]) != "" ? $dados["systemDetect"] : "N/D";
        $browser = isset($dados["browserDetect"]) && isset($dados["browserDetect"]) != "" ? $dados["browserDetect"] : "N/D";
        $system_name = isset($dados["systemNameDetected"]) && isset($dados["systemNameDetected"]) != "" ? $dados["systemNameDetected"] : "N/D";
        $dispositivo = isset($dados["dispositivo"]) && isset($dados["dispositivo"]) != "" ? $dados["dispositivo"] : "N/D";

        $model->constructSalvarInfoLogin($dispositivo, $system, $system_name, $browser, $data);
        $values = $model->salvarInfoLoginRequestPg();

        $message = $view->responseRequest($values);
        return $message;
    }

    // Function que vai fazer o insert dos dados de registro das informacoes do visitante no postgres:
        public function salvarInfoRotasRegistroPg()
        {
            // Call de classes necessarias:
            $view = new RegistrosView();
            $model = new Registros();
            $timezone = new DateTimeZone("America/Sao_Paulo");
            $dateTime = new DateTime('now', $timezone);
            $uri = new Uri();
    
            // Constroi dados no model e executa query dentro do model
            $data = $dateTime->format('Y-m-d H:i:s');
            $dados = getUserBrowser();
            $rota = $uri->scriptName();
    
            $system = isset($dados["systemDetect"]) && isset($dados["systemDetect"]) != "" ? $dados["systemDetect"] : "N/D";
            $browser = isset($dados["browserDetect"]) && isset($dados["browserDetect"]) != "" ? $dados["browserDetect"] : "N/D";
            $system_name = isset($dados["systemNameDetected"]) && isset($dados["systemNameDetected"]) != "" ? $dados["systemNameDetected"] : "N/D";
            $dispositivo = isset($dados["dispositivo"]) && isset($dados["dispositivo"]) != "" ? $dados["dispositivo"] : "N/D";
    
            $model->constructSalvarRotas($rota, $data);
            $values = $model->salvarInfoRotasRequestPg();
    
            $message = $view->responseRequest($values);
            return $message;
        }

    // Function para listar os dados salvos de registro das informacoes do visitantes no Postgres:
    public function selectInfoRegistroPg()
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();

        $values = $model->listarRegistrosRequestPg();

        return $view->validateSelect($values);
    }
}

