<?php
class ClienteController
{
    // Request get para recolher dados de infoUsuarioClienteByIdApi
    public function infoUsuarioClienteById($id_cliente)
    {
        // Inicializa as classes necessarias
        $view = new ClienteView();
        $model = new Cliente();

        $model->constructInfoUsuarioClienteById($id_cliente);
        $values = $model->infoUsuarioClienteByIdApi();
        //$info = isset($values) && !empty($values) ? $values[0] : array();
        //return $view->valuesInfoUsuarioCliente($values);
        $info = $view->valuesInfoUsuarioCliente($values);
        $final = isset($info) && !empty($info) ? $info[0] : array();
        return $final;
    }
}
