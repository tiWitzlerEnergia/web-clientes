<?php

class PostoTarifario extends Grafico implements GerarGraficoInterface
{
    // Atributos:
    protected $dataGerada;
    protected $idUnidadeGrafico;

    // Métodos especiais:
    public function __construct($idGrafico, $dataGerada, $idUnidadeGrafico, $parametrosApi = array("ponta" => array(), "foraPonta" => array()))
    {
        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setDataGerada($dataGerada);
        $this->setIdUnidadeGrafico($idUnidadeGrafico);
        //Recolhe data, pega o final do mês, definir url e pegar dados.
        $dataInicial = date("Y-m-d", strtotime($this->getDataGerada()));
        $dataFinal = date("Y-m-t", strtotime($this->getDataGerada()));
        $id_unidade = $this->getIdUnidadeGrafico();
        //medidas/periodo/fora/ponta/diario?dataFinal=".$dataFinalPostoTarifario."&dataInicial=".$dataInicialPostoTarifario."&unidade_id=".$id_unidade
        $this->setUrlApi("medidas/periodo/fora/ponta/diario?dataFinal=" . $dataFinal . "&dataInicial=" . $dataInicial . "&unidade_id=" . $id_unidade);
        $this->setParametrosApi($parametrosApi);
        $this->recolherDados();                     // Define valoresGrafico
    }

    /**
     * Get the value of dataGerada
     */
    public function getDataGerada()
    {
        return $this->dataGerada;
    }

    /**
     * Set the value of dataGerada
     *
     * @return  self
     */
    public function setDataGerada($dataGerada)
    {
        $dataGerada = date("Y-m-d", strtotime($dataGerada));
        $this->dataGerada = $dataGerada;

        return $this;
    }

    /**
     * Get the value of idUnidadeGrafico
     */
    public function getIdUnidadeGrafico()
    {
        return $this->idUnidadeGrafico;
    }

    /**
     * Set the value of idUnidadeGrafico
     *
     * @return  self
     */
    public function setIdUnidadeGrafico($idUnidadeGrafico)
    {
        $this->idUnidadeGrafico = $idUnidadeGrafico;

        return $this;
    }

    // Métodos publicos:
    public function updateValues()
    {
        if ($this->getValoresGrafico()) {
            $values = $this->getValoresGrafico();
            $somaPonta = array_sum($values["ponta"]);
            $somaForaPonta = array_sum($values["foraPonta"]);
            $newValues = array("ponta" => $somaPonta, "foraPonta" => $somaForaPonta);
        } else {
            $newValues = array("ponta" => 1, "foraPonta" => 1);
        }
        return $newValues;
    }

    public function varChartData($data, $labelsName = array(), $labelsId = array(), $backgroundColor = array(), $borderColor = array())
    {
        $values = $this->updateValues();
        $somaPonta = $values["ponta"];
        $somaForaPonta = $values["foraPonta"];
        echo "
        var $data = {
            labels: [
        ";
        for ($i = 0; $i < count($labelsName); $i++) {
            echo "'" . $labelsName[$i] . "', ";
        }
        echo "
            ], datasets: [
                {
                    label: 'Dataset 1',
                    backgroundColor: [
                        '$backgroundColor[0]',
                        '$backgroundColor[1]',
                    ],
                    data: [
                        $somaPonta,
                        $somaForaPonta
                    ]
                }
            ],
        };
        ";
    }

    public function configChartData($data, $tipo = "", $titleText)
    {
        if ($this->getIdGrafico()) {
            $id = $this->getIdGrafico();
            echo "
            var ctx = document.getElementById('$id').getContext('2d');
			window.graficoPostoTarifario = new Chart(ctx, {
				type: '$tipo',
				data: $data,
				options: {
					responsive: true,
                    maintainAspectRatio: false,
					title: {
						display: false,
						text: '$titleText'
					},
					legend: {
                        position: 'top',
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true,
                        duration: 2000,
                    },
                    tooltips: {
						mode: 'index',
						intersect: false,
                        callbacks: {
                            label: function(tooltipItem, data) {
                                var labelText = data.labels[tooltipItem.index];
                                var valueData = data.datasets[0].data[tooltipItem.index];
                                return labelText + \": \" + formatadorDeNumeroInglesParaNumeroBrasileiro(valueData);
                            },
                        }
					},
				}
			});
            ";
        }
    }
}
