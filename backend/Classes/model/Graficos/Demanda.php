<?php
class Demanda extends Grafico implements GerarGraficoInterface
{
    // Atributos:
    protected $dataGerada;
    protected $idUnidadeAtiva;

    // Metodos especiais:
    public function __construct($idGrafico = "", $dataGerada, $idUnidadeAtiva, $parametrosApi = array("mes_ref" => array(), "c_demanda_fponta" => array(), "c_demanda_ponta" => array(), "demanda_fponta" => array(), "demanda_ponta" => array()))
    {
        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setDataGerada($dataGerada);
        $inicioData = $this->getDataGerada();
        $finalData = date("Y-m-01", strtotime("-1 year", strtotime($inicioData)));
        $this->setIdUnidadeAtiva($idUnidadeAtiva);
        $this->setUrlApi("demanda/periodo?dataFinal=" . $inicioData . "&dataInicial=" . $finalData . "&unidade_id=" . $this->getIdUnidadeAtiva());
        $this->setParametrosApi($parametrosApi);
        $this->recolherDados();                     // Define valoresGrafico
        // Atualiza valores do grafico:
        //var_dump($this->getValoresGrafico());
        $this->updateDados();
    }

    /**
     * Get the value of dataGerada
     */
    public function getDataGerada()
    {
        return $this->dataGerada;
    }

    /**
     * Set the value of dataGerada
     */
    public function setDataGerada($dataGerada): self
    {
        $dataGerada = date("Y-m-d", strtotime($dataGerada));
        $this->dataGerada = $dataGerada;

        return $this;
    }

    /**
     * Get the value of idUnidadeAtiva
     */
    public function getIdUnidadeAtiva()
    {
        return $this->idUnidadeAtiva;
    }

    /**
     * Set the value of idUnidadeAtiva
     */
    public function setIdUnidadeAtiva($idUnidadeAtiva): self
    {
        $this->idUnidadeAtiva = $idUnidadeAtiva;

        return $this;
    }

    public function updateDados()
    {
        $arrmes_ref = array();
        $arrc_demanda_fponta = array();
        $arrc_demanda_ponta = array();
        $arrdemanda_fponta = array();
        $arrdemanda_ponta = array();

        if ($this->getValoresGrafico()) {
            $values = $this->getValoresGrafico();

            $dataInicial = $this->getDataGerada();
            $finalData = date("Y-m-01", strtotime("-1 year", strtotime($dataInicial)));

            for ($i = 0; $i <= 12; $i++) {
                // Procura mes
                $dataAtual = date("01-m-Y", strtotime("+$i months", strtotime($finalData)));
                $search = array_search($dataAtual, $values["mes_ref"]);

                if ($search) {
                    //echo "existe";
                    // Existe data no array
                    $value_c_demanda_fponta = isset($values["c_demanda_fponta"][$search]) ? $values["c_demanda_fponta"][$search] : 0;
                    $value_c_demanda_ponta = isset($values["c_demanda_ponta"][$search]) ? $values["c_demanda_ponta"][$search] : 0;
                    $value_demanda_fponta = isset($values["demanda_fponta"][$search]) ? $values["demanda_fponta"][$search] : 0;
                    $value_demanda_ponta = isset($values["demanda_ponta"][$search]) ? $values["demanda_ponta"][$search] : 0;
                } else {
                    // Não existe data no array.
                    $value_c_demanda_fponta = 0;
                    $value_c_demanda_ponta = 0;
                    $value_demanda_fponta = 0;
                    $value_demanda_ponta = 0;
                }

                array_push($arrmes_ref, $dataAtual);
                array_push($arrc_demanda_fponta, $value_c_demanda_fponta);
                array_push($arrc_demanda_ponta, $value_c_demanda_ponta);
                array_push($arrdemanda_fponta, $value_demanda_fponta);
                array_push($arrdemanda_ponta, $value_demanda_ponta);
            }

            /*echo "<pre>";
            var_dump($arrdemanda_fponta);
            echo "</pre>";*/

            $final = array("mes_ref" => $arrmes_ref, "c_demanda_fponta" => $arrc_demanda_fponta, "c_demanda_ponta" => $arrc_demanda_ponta, "demanda_fponta" => $arrdemanda_fponta, "demanda_ponta" => $arrdemanda_ponta);
            $this->setValoresGrafico($final);
        } else {
            $dataInicial = $this->getDataGerada();
            $finalData = date("Y-m-01", strtotime("-1 year", strtotime($dataInicial)));
            for ($i = 0; $i <= 12; $i++) {
                $dataAtual = date("Y-m-d", strtotime("+$i months", strtotime($finalData)));
                $value_c_demanda_fponta = 0;
                $value_c_demanda_ponta = 0;
                $value_demanda_fponta = 0;
                $value_demanda_ponta = 0;

                array_push($arrmes_ref, $dataAtual);
                array_push($arrc_demanda_fponta, $value_c_demanda_fponta);
                array_push($arrc_demanda_ponta, $value_c_demanda_ponta);
                array_push($arrdemanda_fponta, $value_demanda_fponta);
                array_push($arrdemanda_ponta, $value_demanda_ponta);
            }

            $final = array("mes_ref" => $arrmes_ref, "c_demanda_fponta" => $arrc_demanda_fponta, "c_demanda_ponta" => $arrc_demanda_ponta, "demanda_fponta" => $arrdemanda_fponta, "demanda_ponta" => $arrdemanda_ponta);
            $this->setValoresGrafico($final);
        }
    }

    public function varChartData($data, $labelsName = array(), $labelsId = array(), $backgroundColor = array(), $borderColor = array())
    {
        $values = $this->getValoresGrafico();
        $unique1 = true;
        $unique2 = true;

        foreach ($values["c_demanda_ponta"] as $value) {
            if (0 != $value) {
                $unique1 = false;
                break;
            }
        }
        foreach ($values["demanda_ponta"] as $value) {
            if (0 != $value) {
                $unique2 = false;
                break;
            }
        }

        // SE OS 2 UNIQUE FOREM TRUE, ENTAO CLIENTE E VERDE E NAO PRECISA MOSTRAR PONTA
        echo "
        var $data = {
            labels: [
        ";
        // Listagem datas:
        for ($i = 12; $i >= 0; $i--) {
            $data = date("d/m/Y", strtotime($values["mes_ref"][$i]));
            echo "'$data', ";
        }
        echo "
            ],
            datasets: [
                {
                    type: 'bar',
                    label: 'Demanda Fora Ponta Registrada',
                    backgroundColor: '".$_SESSION['cor-custom-8']."',
                    borderColor: '".$_SESSION['cor-custom-8']."',
                    data: [
        ";
        for ($i = 12; $i >= 0; $i--) {
            $value_demanda_registrada = isset($values["demanda_fponta"][$i]) ? $values["demanda_fponta"][$i] : 0;
            echo "$value_demanda_registrada, ";
        }
        echo "
                    ]
                },
                {
                    type: 'line',
                    label: 'Demanda contratada',
                    fill: false,
                    borderWidth: 1,
                    borderColor: '".$_SESSION['cor-custom-3']."',
                    borderDash: [5, 4],
                    lineTension: 0,
                    steppedLine: true,
                    data: [
        ";
        for ($i = 12; $i >= 0; $i--) {
            $value_c_demanda_fponta = isset($values["c_demanda_fponta"][$i]) ? $values["c_demanda_fponta"][$i] : 0;
            echo "$value_c_demanda_fponta, ";
        }
        echo "
                    ]
                },
        ";
        if (!$unique1 && !$unique2) {
            echo "
                {
                    type: 'bar',
                    label: 'Demanda Ponta Registrada',
                    backgroundColor: '#868686',
                    borderColor: '#868686',
                    data: [
        ";
            for ($i = 12; $i >= 0; $i--) {
                $value_demanda_ponta_registrada = isset($values["demanda_ponta"][$i]) ? $values["demanda_ponta"][$i] : 0;
                echo "$value_demanda_ponta_registrada, ";
            }
            echo "
                    ]
                }, 
                {
                    type: 'line',
                    label: 'Demanda Ponta contratada',
                    fill: false,
                    borderWidth: 1,
                    borderColor: '".$_SESSION['cor-custom-3']."',
                    borderDash: [5, 4],
                    lineTension: 0,
                    steppedLine: true,
                    data: [
        ";
            for ($i = 12; $i >= 0; $i--) {
                $value_c_demanda_ponta = isset($values["c_demanda_ponta"][$i]) ? $values["c_demanda_ponta"][$i] : 0;
                echo "$value_c_demanda_ponta, ";
            }
            echo "
                    ]
                },
            ";
        }
        echo "
            ],
        };
        ";
    }

    public function configChartData($data, $tipo = "", $titleText)
    {
        if ($this->getIdGrafico()) {
            $id = $this->getIdGrafico();
            echo "
            var ctx = document.getElementById('$id').getContext('2d');
			window.graficoDemanda = new Chart(ctx, {
				type: '$tipo',
				data: $data,
				options: {
                    responsive: false,
                    maintainAspectRatio: true,
                    legend: {
                        fullWidth: false,
                        position: 'bottom',
                        labels: {
                            padding: 2
                        },
                    },
                    title: {
                        display: false,
                        text: '$titleText'
                    },
                    tooltips: {
						mode: 'index',
						intersect: false,
                        callbacks: {
                            label: function(tooltipItem, data) {
                                if(tooltipItem.datasetIndex == 0) {
                                    var consumoForaPonta = data.datasets[0].data[tooltipItem.index];
                                    return \"Demanda Ponta contratada: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(consumoForaPonta);
                                } else if(tooltipItem.datasetIndex == 1) {
                                    var consumoPonta = data.datasets[1].data[tooltipItem.index];
                                    return \"Demanda contratada: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(consumoPonta);
                                }
                                
                            },
                        }
					},
                    scales: {
                        yAxes: [
                            {
                                display: true,
                                scaleLabel: {
                                    display: true
                                },
                                stacked: false,
                                ticks: {
                                    min: 0,
                                    stepSize: 0.5,
                                    callback: function(label, index, labels){
                                        return formatadorDeNumeroInglesParaNumeroBrasileiro(label);
                                    }   
                                }
                            }
                        ],
                        xAxes: [
                            {
                                display: true,
                                scaleLabel: {
                                    display: true
                                },
                                stacked: false,
                            }
                        ],
                    },
                    animation: {
                        duration: 4000,
                    },
                    chartArea: {
                        width: '100%',
                        height: '100%',
                        padding: {
                            top: 0,
                            bottom: 0,
                        },
                    },
                }
			});
            ";
        }
    }

    public function obterDados() {
        return $this->getValoresGrafico();
    }
}
