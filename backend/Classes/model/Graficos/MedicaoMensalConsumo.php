<?php
class MedicaoMensalConsumo extends Grafico implements GerarGraficoInterface
{
    // atributos:
    protected $dataGerada;

    // metodos especiais:
    public function __construct($idGrafico = "", $urlApi = "", $parametrosApi = array(), $dataGerada)
    {
        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setUrlApi($urlApi);
        // Por padrao, definir sempre como primeiro parametro o indice que define os label.
        $this->setParametrosApi($parametrosApi);
        $this->recolherDados();                     // Define valoresGrafico
        $this->setDataGerada($dataGerada);
    }

    /**
     * Get the value of dataGerada
     */
    public function getDataGerada()
    {
        return $this->dataGerada;
    }

    /**
     * Set the value of dataGerada
     *
     * @return  self
     */
    protected function setDataGerada($dataGerada)
    {
        $dataGerada = date("Y-m-d", strtotime($dataGerada));
        $this->dataGerada = $dataGerada;

        return $this;
    }

    // metodos publicos:
    public function varChartData($data, $labelsName = array(), $labelsId = array(), $backgroundColor = array(), $borderColor = array())
    {
        //Pega array de valores:
        $arrayValores = $this->getValoresGrafico();
        $totVazio = 0;
        $dataGeradaInicio = date("d", strtotime($this->getDataGerada()));
        $dataGeradaFinal = date("t", strtotime($this->getDataGerada()));
        echo "
        var $data = {
            labels: [
        ";
        for ($i = 1; $i <= $dataGeradaFinal; $i++) {
            echo "'" . $dataGeradaInicio . "', ";
            $dataGeradaInicio++;
        }
        echo "
            ], datasets: [
        ";
        for ($i = 0; $i < count($labelsName); $i++) {
            $labelAtual = $i + 1;
            echo "
                {
                    label: '$labelsName[$i]',
                    backgroundColor: '$backgroundColor[$i]',
                    borderColor: '$borderColor[$i]',
                    data: [
            ";

            for ($j = 0; $j < $dataGeradaFinal; $j++) {
                $id = $j + 1;
                $data = isset($arrayValores["data_medicao"][$j - $totVazio]) ? $arrayValores["data_medicao"][$j - $totVazio] : $id . "-07";
                $data_arr = explode("-", $data);
                $dia = $data_arr[0];

                if ($dia == $id) {
                    $valor = isset($arrayValores[$labelsId[$labelAtual]][$j - $totVazio]) ? $arrayValores[$labelsId[$labelAtual]][$j - $totVazio] : 0;
                } else {
                    $valor = 0;
                    $totVazio++;
                }

                echo "$valor, ";
            }
            echo "
                    ],
                },
            ";
            $totVazio = 0;
        }
        echo "
            ]
        };
        ";
    }

    public function configChartData($data, $tipo = "", $titleText)
    {
        if ($this->getIdGrafico()) {
            $id = $this->getIdGrafico();
            echo "
            var ctx = document.getElementById('$id').getContext('2d');
			window.myGraph = new Chart(ctx, {
				type: '$tipo',
				data: $data,
				options: {
					responsive: false,
					maintainAspectRatio: true,
					title: {
						display: false,
						text: '$titleText'
					},
					tooltips: {
						mode: 'index',
						intersect: false,
                        callbacks: {
                            label: function(tooltipItem, data) {
                                if(tooltipItem.datasetIndex == 0) {
                                    var consumoForaPonta = data.datasets[0].data[tooltipItem.index];
                                    return \"Fora Ponta: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(consumoForaPonta);
                                } else if(tooltipItem.datasetIndex == 1) {
                                    var consumoPonta = data.datasets[1].data[tooltipItem.index];
                                    return \"Ponta: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(consumoPonta);
                                }
                            }
                        }
					},
					scales: {
						xAxes: [{
							stacked: true
						}],
						yAxes: [{
							stacked: true,
                            ticks:{
                                callback: function(label, index, labels){
                                    return formatadorDeNumeroInglesParaNumeroBrasileiro(label);
                                }    
                            }
						}]
					},
					animation: {
						duration: 4000,
                    }
				}
			});
            ";
        }
    }

    public function gerarValoresDownload($mes, $ano)
    {
        $arrayValores = $this->getValoresGrafico();
        $totVazio = 0;
        $dataGeradaInicio = date("d", strtotime($this->getDataGerada()));
        $dataGeradaFinal = date("t", strtotime($this->getDataGerada()));
        $index = array("data_medicao", "foraPonta", "ponta");

        echo "
        <table>
            <tr>
                <th>Data</th>
                <th>Ponta</th>
                <th>Fora Ponta</th>
            </tr>
        ";


        for ($j = 0; $j < $dataGeradaFinal; $j++) {
            $id = $j + 1;
            $data = isset($arrayValores["data_medicao"][$j - $totVazio]) ? $arrayValores["data_medicao"][$j - $totVazio] : $id . "-$mes-$ano";
            $data_arr = explode("-", $data);
            $dia = $data_arr[0];

            if ($dia == $id) {
                $valorPonta = isset($arrayValores["ponta"][$j - $totVazio]) ? $arrayValores["ponta"][$j - $totVazio] : 0;
                $valorFPonta = isset($arrayValores["foraPonta"][$j - $totVazio]) ? $arrayValores["foraPonta"][$j - $totVazio] : 0;
                
            } else {
                $valorPonta = 0;
                $valorFPonta = 0;
                $totVazio++;
            }

            echo "
                <tr>
                    <td>$data</td>
                    <td>$valorPonta</td>
                    <td>$valorFPonta</td>
                </tr>
            ";
        }
        echo "</table>";
    }
}
