<?php
class Pld extends Montantes implements GerarGraficoInterface
{
    // Atributos

    // Metodos especiais:
    // Metodos especiais:
    public function __construct($idGrafico = "", $urlApi = "", $parametrosApi = array())
    {
        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setUrlApi($urlApi);
        // Por padrao, definir sempre como primeiro parametro o indice que define os label.
        $this->setParametrosApi($parametrosApi);
        $this->recolherDados();                     // Define valoresGrafico
    }

    // Metodos publicos:
    public function varChartData($data, $labelsName = array(), $labelsId = array(), $backgroundColor = array(), $borderColor = array())
    {
        $arrayValores = $this->getValoresGrafico();
        echo "
        var $data = {
            labels: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
            datasets: [
        ";
        for ($i = 0; $i < count($labelsName); $i++) {
            echo "
                {
                    label: '$labelsName[$i]',
                    backgroundColor: '$backgroundColor[$i]',
                    borderColor: '$borderColor[$i]',
                    pointRadius: 0,
                    fill: false,
                    data: [
            ";
            for ($j = 0; $j < 24; $j++) {
                $valor = isset($arrayValores[$labelsId[$i]][$j]) ? $arrayValores[$labelsId[$i]][$j] : 0;
                echo "$valor, ";
            }
            echo "
                    ]
                },
        ";
        }
        echo "
            ]
        };
        ";
    }

    public function configChartData($data, $tipo = "", $titleText)
    {
        if ($this->getIdGrafico()) {
            $id = $this->getIdGrafico();
            echo "
            var ctx = document.getElementById('$id').getContext('2d');
			window.graficoPdl = new Chart(ctx, {
				type: '$tipo',
				data: $data,
				options: {
					responsive: true,
					maintainAspectRatio: false,
					title: {
						display: false,
						text: '$titleText'
					},
					tooltips: {
						mode: 'index',
						intersect: false,
                        callbacks: {
                            label: function(tooltipItem, data) {
                                var valueData = data.datasets[0].data[tooltipItem.index];
                                return \"Consumo PLD: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(valueData);
                            },
                        }
					},
					scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Hora'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                            },
                            ticks:{
                                callback: function(label, index, labels){
                                    return formatadorDeNumeroInglesParaNumeroBrasileiro(label);
                                }    
                            }
                        }]
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true,
                        duration: 4000,
                    }
				}
			});
            ";
        }
    }
}
