<?php
setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_get('America/Sao_Paulo');

class Calendario extends Grafico
{
    // Atributos:
    protected $idUnidadeUrl;
    protected $dataUrl;
    protected $finalDataUrl;

    // Metodos especiais:
    public function __construct($idGrafico, $idUnidadeUrl, $dataUrl)
    {
        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setIdUnidadeUrl($idUnidadeUrl);
        $this->setDataUrl($dataUrl);
        $this->setFinalDataUrl($this->getDataUrl());
        $this->setUrlApi("medidas/periodo?data_final=" . $this->getFinalDataUrl() . "&data_inicial=" . $this->getDataUrl() . "&unidade_id=" . $this->getIdUnidadeUrl());
        $this->setParametrosApi(array("id_medidas" => array(), "ativo_c" => array(), "hora_do_dia" => array(), "data_medicao" => array(), "dia_da_semana" => array()));
        $this->recolherDados();                     // Define valoresGrafico
    }

    /**
     * Get the value of idUnidadeUrl
     */
    public function getIdUnidadeUrl()
    {
        return $this->idUnidadeUrl;
    }

    /**
     * Set the value of idUnidadeUrl
     *
     * @return  self
     */
    public function setIdUnidadeUrl($idUnidadeUrl)
    {
        $this->idUnidadeUrl = $idUnidadeUrl;

        return $this;
    }

    /**
     * Get the value of dataUrl
     */
    public function getDataUrl()
    {
        return $this->dataUrl;
    }

    /**
     * Set the value of dataUrl
     *
     * @return  self
     */
    public function setDataUrl($dataUrl)
    {
        $dataUrl = date("Y-m-d", strtotime($dataUrl));
        $this->dataUrl = $dataUrl;

        return $this;
    }

    /**
     * Get the value of finalDataUrl
     */
    public function getFinalDataUrl()
    {
        return $this->finalDataUrl;
    }

    /**
     * Set the value of finalDataUrl
     *
     * @return  self
     */
    public function setFinalDataUrl($finalDataUrl)
    {
        $finalDataUrl = date("Y-m-t", strtotime($finalDataUrl));
        $this->finalDataUrl = $finalDataUrl;

        return $this;
    }

    // Metodos publicos
    public function date_sort($a, $b)
    {
        return strcmp($a, $b); //matriz b mapping
    }

    public function updateDados()
    {
        $dataAtual = $this->getDataUrl();
        // Recolher valores de data:
        $ano = date("Y", strtotime($dataAtual));
        $mes = date("m", strtotime($dataAtual));
        $diaFinal = date("t", strtotime($dataAtual));
        $list = array();
        $arrayDatas = array();
        $arrayDia = array();
        $arrayHora = array();
        $arrayAtivoC = array();
        if ($this->getValoresGrafico()) {
            $values = $this->getValoresGrafico();
            //Criar array segundo o total de dias:
            for ($i = 0; $i < count($values["id_medidas"]); $i++) {
                //Recolhe dados necessarios
                $hora = $values["hora_do_dia"][$i];
                $dataMedicao = $values["data_medicao"][$i];
                $ativoC = $values["ativo_c"][$i];
                //Formateia
                $dia = date("d", strtotime($dataMedicao));
                if ($hora == 24) {
                    $hora = 0;
                }
                //Monta em array
                array_push($arrayAtivoC, $ativoC);
                array_push($arrayDia, $dia);
                array_push($arrayHora, $hora);
            }
            $newValues = array("ativo_c" => $arrayAtivoC, "dia" => $arrayDia, "hora" => $arrayHora);
        } else {
            //Preenchendo a lista de dias com valores null:
            for ($i = 1; $i <= $diaFinal; $i++) {
                for ($j = 0; $j < 24; $j++) {
                    array_push($arrayDia, $i);  //Como tem 24hrs no dia, ele deve ser armazenado 24 vezes.
                    array_push($arrayHora, $j); //Armazena cada hora por cada dia começando das 00:00.
                    array_push($arrayAtivoC, 0);    // Armazena valor 0 para todos.
                }
            }
            $newValues = array("ativo_c" => $arrayAtivoC, "dia" => $arrayDia, "hora" => $arrayHora);
        }

        array_multisort($newValues["dia"], SORT_NUMERIC, SORT_ASC, $newValues["hora"], SORT_NUMERIC, SORT_ASC, $newValues["ativo_c"]);
        return $newValues;
    }

    public function gerarGrafico()
    {
        $dataAtual = $this->getDataUrl();
        $finalDoMes = date("t", strtotime($dataAtual));
        $ano = date("Y", strtotime($dataAtual));
        $mes = date("m", strtotime($dataAtual));
        $values = $this->updateDados();
        echo "
        <script>
            var listaData = [];
        </script>
        ";
        //Gerar todos os chartjs para o codigo
        for ($j = 0; $j < $finalDoMes; $j++) {
            //Config para pegar os dias e agrupar eles por id nas <td>
            $diaChartjs = $j + 01;
            $totVazio = 0;
            $dia = $j + 1;
            if ($diaChartjs < 10) {
                $diaChartjs = "0" . $diaChartjs;
            }
            $day = $ano . "-" . $mes . "-" . $diaChartjs;
            $dayStr = strftime('%A', strtotime($day));
            echo "
            <script>
                var data$diaChartjs = {
                    labels: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
                    datasets: [{
                        label: 'Consumo (KWh): ',
                        backgroundColor: '".$_SESSION['cor-custom-3']."',
                        borderColor: '".$_SESSION['cor-custom-3']."',
                        lineTension: 0.1,
                        borderWidth: 2,
                        pointBorderWidth: 1,
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        fill: false,
                        data: [
            ";
            /*for ($i = 0; $i < 24; $i++) {
                $totRepeticao = 24 * $j;
                $indice = $totRepeticao + $i - $totVazio * 24;
                $diaatual = isset($values['dia'][$indice]) ? $values["dia"][$indice] : $dia;
                //$valorAtual = isset($values["ativo_c"][$indice]) ? $values["ativo_c"][$indice] : 0;
                //echo "$valorAtual, ";
                if ($diaatual != $dia) {
                    $valorAtual = 0;
                    $totVazio++;
                } else {
                    $valorAtual = isset($values["ativo_c"][$indice]) ? $values["ativo_c"][$indice] : 0;
                }

                echo "$valorAtual, ";

                if (!empty($values["ativo_c"])) {
                    if (count($values["ativo_c"]) >= 1) {
                        $maiorValor = max($values['ativo_c']);
                    } else {
                        $maiorValor = 0;
                    }
                } else {
                    $maiorValor = 0;
                }
            }*/
            $valorc = 0;
            $existe = false;
            $meusvalores[$j] = array();
            foreach ($values["dia"] as $key => $value) {
                if ($value == $dia) {
                    $valorc = isset($values["ativo_c"][$key]) ? $values["ativo_c"][$key] : 0;
                    $hora = isset($values["hora"][$key]) ? $values["ativo_c"][$key] : 0;
                    //$meusvalores[$j][$hora] = $valorc;
                    array_push($meusvalores[$j], $valorc);
                    $existe = true;
                    //echo "$valorc, ";
                }
            }
            for($n = 0; $n < 24; $n++){
                //$valor_final = isset($meusvalores[$j][$n]) ? $meusvalores[$j][$n] : 0;
                $valor_final = isset($meusvalores[$j][$n]) ? $meusvalores[$j][$n] : 0;
                echo "$valor_final, ";
            }

            if(!$existe){
                for($i = 0; $i < 24; $i++){
                    echo "0, ";
                }
            }

            if (!empty($values["ativo_c"])) {
                if (count($values["ativo_c"]) >= 1) {
                    $maiorValor = max($values['ativo_c']);
                } else {
                    $maiorValor = 0;
                }
            } else {
                $maiorValor = 0;
            }

            $totVazio = 0;
            $maiorValor = 0;
            echo "
                        ]
                    }]
                }

                listaData.push(data$diaChartjs);

                var config$diaChartjs = {
                    type: 'line',
                    data: data$diaChartjs,
                    responsive: true,
                    maintainAspectRatio: false,
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            fontSize: 15,
                            text: '$diaChartjs - $dayStr'
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        legend: {
                            display: false,
                            padding: 0,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                visible: false,
                                display: false,
                                gridLines: {
                                    drawBorder: false,
                                    display: false,
                                },
                            }],
                            yAxes: [{
                                visible: false,
                                display: false,
                                ticks: {
                                    /*max: $maiorValor,*/
                                    min: 0
                                },
                                gridLines: {
                                    drawBorder: false,
                                    display: false,
                                },
                            }]
                        },
                        animation: {
                            duration: 4000,
                        }
                    }
                };

                var ctx$diaChartjs = document.getElementById('chart$diaChartjs-$mes-$ano').getContext('2d');
                var myLine$diaChartjs = new Chart(ctx$diaChartjs, config$diaChartjs);
            </script>
            ";
        }
    }
}
