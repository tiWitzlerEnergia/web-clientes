<?php
class ComparacaoCalendario extends Grafico
{
    // Atributos:
    protected $anoComparacao;
    protected $mesComparacao;
    protected $listaComparacao;
    protected $datasset;

    // Metodos especiais:
    public function __construct($idGrafico, $ano, $mes, $listaComparacao = array(), $datasset = array())
    {
        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setAnoComparacao($ano);
        $this->setMesComparacao($mes);
        $this->setListaComparacao($listaComparacao);
        $this->setDatasset($datasset);
    }
    /**
     * Get the value of anoComparacao
     */
    public function getAnoComparacao()
    {
        return $this->anoComparacao;
    }

    /**
     * Set the value of anoComparacao
     *
     * @return  self
     */
    public function setAnoComparacao($anoComparacao)
    {
        $this->anoComparacao = $anoComparacao;

        return $this;
    }

    /**
     * Get the value of mesComparacao
     */
    public function getMesComparacao()
    {
        return $this->mesComparacao;
    }

    /**
     * Set the value of mesComparacao
     *
     * @return  self
     */
    public function setMesComparacao($mesComparacao)
    {
        $this->mesComparacao = $mesComparacao;

        return $this;
    }

    /**
     * Get the value of listaComparacao
     */
    public function getListaComparacao()
    {
        return $this->listaComparacao;
    }

    /**
     * Set the value of listaComparacao
     *
     * @return  self
     */
    public function setListaComparacao($listaComparacao)
    {
        $this->listaComparacao = $listaComparacao;

        return $this;
    }

    /**
     * Get the value of datasset
     */
    public function getDatasset()
    {
        return $this->datasset;
    }

    /**
     * Set the value of datasset
     *
     * @return  self
     */
    public function setDatasset($datasset)
    {
        $this->datasset = $datasset;

        return $this;
    }

    // Metodos publicos:
    public function calcMedia()
    {
        $arrayMedia = [];
        if ($this->getDatasset()) {
            $values = $this->getDatasset();
            $tamanhoItems = count($values);
            for ($i = 0; $i < 24; $i++) {
                $unidade = 0;
                for ($j = 0; $j < $tamanhoItems; $j++) {
                    $unidade += $values[$j][$i];
                }
                $media = $unidade / $tamanhoItems;
                array_push($arrayMedia, round($media, 2));
            }
        } else {
            for ($i = 0; $i < 24; $i++) {
                array_push($arrayMedia, 0);
            }
        }

        return $arrayMedia;
    }

    public function gerarGrafico()
    {
        $id = $this->getIdGrafico();
        $medias = $this->calcMedia();
        $mes = $this->getMesComparacao();
        $ano = $this->getAnoComparacao();
        $listaComparacao = $this->getListaComparacao();
        $dataset = $this->getDatasset();
        echo "
        var configComparacao = {
			type: 'line',
			data: {
				labels: [
                    '00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'
                ],
				datasets: [
                    {
                        label: 'Media',
                        backgroundColor: window.chartColors.red,
                        borderColor: window.chartColors.red,
                        lineTension: 0.1,
                        borderWidth: 2,
                        pointBorderWidth: 1,
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        fill: false,
                        data: [
        ";
        for ($i = 0; $i < 24; $i++) {
            $valorMedia = $medias[$i];
            echo "$valorMedia, ";
        }
        echo "
                        ],
                    },
        ";
        if ($this->getDatasset()) {
            for ($i = 0; $i < count($listaComparacao); $i++) {
                $dia = $listaComparacao[$i];
                echo "
                {
                    label: '$dia',
                    backgroundColor: dynamicColors(),
                    borderColor: dynamicColors(),
                    lineTension: 0.1,
                    borderWidth: 2,
                    pointBorderWidth: 1,
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [
                ";
                for ($j = 0; $j < 24; $j++) {
                    $valueDataset = isset($dataset[$i][$j]) ? $dataset[$i][$j] : 0;
                    echo "$valueDataset, ";
                }
                echo "
                    ],
                    fill: false,
                },
                ";
            }
        }
        echo "
                ]
            },
			options: {
				tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: false,
                maintainAspectRatio: true,
                legend: {
                    position: 'bottom',
                },
                animation: {
					duration: 4000,
                }
			}
        };

        var ctxComparacao = document.getElementById('$id').getContext('2d');
	    window.myLine = new Chart(ctxComparacao, configComparacao);
        ";
    }
}
