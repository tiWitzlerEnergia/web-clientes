<?php
class HistoricoEconomia extends Grafico implements GerarGraficoInterface
{
    // Atributos:
    protected $porcentagemEconomia;
    protected $valorEconomizado;
    protected $dataReferencia;
    protected $unidadeACR;
    protected $unidadeACL;

    // Metodos especiais:
    public function __construct($idGrafico = "", $urlApi = "")
    {
        $parametrosApi = array("data_referencia" => array(), "porcentagem_economia" => array(), "valor_economizado" => array());
        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setUrlApi($urlApi);
        // Por padrao, definir sempre como primeiro parametro o indice que define os label.
        $this->setParametrosApi($parametrosApi);
        $this->recolherDados();                     // Define valoresGrafico
    }

    // Metodos publicos:
    public function montarValores()
    {
        $valores = $this->getValoresGrafico();
        $newArray = array("porcentagemEconomia" => array(), "valorEconomizado" => array(), "dataReferencia" => array(), "valorACR" => array(), "valorACL" => array());
        for ($i = 0; $i < count($valores["data_referencia"]); $i++) {
            $unidadeACR = ($valores["valor_economizado"][$i] / $valores["porcentagem_economia"][$i]);
            $unidadeACL = $valores["valor_economizado"][$i] / $valores["porcentagem_economia"][$i] - $valores["valor_economizado"][$i];
            $porcentagemEconomia = $valores["porcentagem_economia"][$i] * 100;
            $valorEconomizado = $valores["valor_economizado"][$i];
            $dataReferencia = $valores["data_referencia"][$i];
            //Formatacao:
            $unidadeACL = $this->formatarNumber($unidadeACL);
            $unidadeACR = $this->formatarNumber($unidadeACR);
            $porcentagemEconomia = $this->formatarNumber($porcentagemEconomia);
            $valorEconomizado = $this->formatarNumber($valorEconomizado);
            $dataReferencia = date("m-Y", strtotime($dataReferencia));
            //Push:
            array_push($newArray["porcentagemEconomia"], $porcentagemEconomia);
            array_push($newArray["valorEconomizado"], $valorEconomizado);
            array_push($newArray["dataReferencia"], $dataReferencia);
            array_push($newArray["valorACR"], $unidadeACR);
            array_push($newArray["valorACL"], $unidadeACL);
        }
        $this->setValoresGrafico($newArray);
    }

    public function varChartData($data, $labelsName = array(), $labelsId = array(), $backgroundColor = array(), $borderColor = array())
    {
        $arrayYAxisID = array("A", "A", "A", "B");
        //Pega array de valores:
        $arrayValores = $this->getValoresGrafico();
        echo "
        var $data = {
            labels: [
        ";
        for ($i = 0; $i < 12; $i++) {
            $dataInicial = $arrayValores[$labelsId[0]][0];
            $dataAtual = isset($arrayValores[$labelsId[0]][$i]) ? $arrayValores[$labelsId[0]][$i] : "----";
            echo "'" . $dataAtual . "', ";
        }
        echo "
            ], datasets: [
        ";
        for ($i = 0; $i < count($labelsName); $i++) {
            $labelAtual = $i + 1;
            echo "
                {
                    label: '$labelsName[$i]',
                    backgroundColor: '$backgroundColor[$i]',
                    borderColor: '$borderColor[$i]',
                    data: [
            ";
            for ($j = 0; $j < 12; $j++) {
                $valor = isset($arrayValores[$labelsId[$labelAtual]][$j]) ? $arrayValores[$labelsId[$labelAtual]][$j] : 0;
                echo "$valor, ";
            }
            echo "
                    ],
                    yAxisID: '$arrayYAxisID[$i]',
            ";
            if ($i == 3) {
                echo "
                    borderWidth: 2,
                    type: 'line',
                    fill: false
                ";
            }
            echo "
                },
            ";
        }
        echo "
            ]
        };
        ";
    }

    public function configChartData($data, $tipo = "", $titleText)
    {
        if ($this->getIdGrafico()) {
            $id = $this->getIdGrafico();
            echo "
            var ctx = document.getElementById('$id').getContext('2d');
			window.historicoEconomia = new Chart(ctx, {
				type: '$tipo',
				data: $data,
				options: {
					responsive: false,
                    maintainAspectRatio: true,
                    legend:{
                        position: 'bottom'
                    },
					title: {
						display: false,
						text: '$titleText'
					},
					tooltips: {
						mode: 'index',
						intersect: false,
                        callbacks: {
                            label: function(tooltipItem, data) {
                                /*var labelText = data.labels[tooltipItem.index];
                                var valueData = data.datasets[0].data[tooltipItem.index];
                                return labelText + \": \" + formatadorDeNumeroInglesParaNumeroBrasileiro(valueData);*/
                                if(tooltipItem.datasetIndex == 0) {
                                    var valACR = data.datasets[0].data[tooltipItem.index];
                                    return \"Valor ACR: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(valACR);
                                } else if(tooltipItem.datasetIndex == 1) {
                                    var valACL = data.datasets[1].data[tooltipItem.index];
                                    return \"Valor ACL: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(valACL);
                                } else if(tooltipItem.datasetIndex == 2) {
                                    var economiaReais = data.datasets[2].data[tooltipItem.index];
                                    return \"Economia [R$]: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(economiaReais);
                                } else if(tooltipItem.datasetIndex == 3) {
                                    var economiaPer = data.datasets[3].data[tooltipItem.index];
                                    return \"Economia [%]: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(economiaPer);
                                }
                            },
                        }
					},
					scales: {
                        yAxes: [{
                            id: 'A',
                            position: 'left',
                            stacked: false,
                            ticks: {
                                callback: function(label, index, labels){
                                    return formatadorDeNumeroInglesParaNumeroBrasileiro(label);
                                }   
                            }
                        },
                        {
                            id: 'B',
                            position: 'left',
                            type: 'linear',
                            ticks: {
                                min: 0,
                                callback: function(label, index, labels){
                                    return formatadorDeNumeroInglesParaNumeroBrasileiro(label);
                                }   
                            },
                            stacked: false,
                        },
                        ],
                        xAxes: [{
                            stacked: false,
                        }]
                    },
					animation: {
						duration: 4000,
                    }
				}
			});
            ";
        }
    }
}
