<?php
class Montantes extends Grafico implements GerarGraficoInterface
{
    // Atributos:
    protected $idUnidadeUrl;
    protected $anoBusqueda;
    protected $somaTotalComercializadora;

    // Metodos especiais:
    public function __construct($anoBusqueda, $idGrafico = "", $idUnidadeUrl = 0)
    {
        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setIdUnidadeUrl($idUnidadeUrl);
        $this->setUrlApi("contrato2?id_unidades=" . $this->getIdUnidadeUrl());
        $this->setAnoBusqueda($anoBusqueda);
        $this->montantesApi();
    }

    /**
     * Get the value of idUnidadeUrl
     */
    public function getIdUnidadeUrl()
    {
        return $this->idUnidadeUrl;
    }

    /**
     * Set the value of idUnidadeUrl
     *
     * @return  self
     */
    public function setIdUnidadeUrl($idUnidadeUrl)
    {
        $this->idUnidadeUrl = $idUnidadeUrl;

        return $this;
    }

    /**
     * Get the value of anoBusqueda
     */
    public function getAnoBusqueda()
    {
        return $this->anoBusqueda;
    }

    /**
     * Set the value of anoBusqueda
     *
     * @return  self
     */
    public function setAnoBusqueda($anoBusqueda)
    {
        ///$anoBusqueda = date("Y", strtotime($anoBusqueda));
        $this->anoBusqueda = $anoBusqueda;

        return $this;
    }

    /**
     * Get the value of somaTotalComercializadora
     */
    public function getSomaTotalComercializadora()
    {
        return $this->somaTotalComercializadora;
    }

    /**
     * Set the value of somaTotalComercializadora
     *
     * @return  self
     */
    public function setSomaTotalComercializadora($somaTotalComercializadora)
    {
        $this->somaTotalComercializadora = $somaTotalComercializadora;

        return $this;
    }

    // Metodos publicos:
    public function montantesApi()
    {
        //Arrays utilizados:
        $u = 0;
        $arrAno = [];
        $arrId = [];
        $arrFlexMax = [];
        $arrFlexMin = [];
        $arrVValues = [];
        //Dados api
        $url = $this->getUrlApi();
        $recolhe = file_get_contents($url, false, $this->getContextToken());
        $result = json_decode($recolhe);
        if ($result) {
            foreach ($result as $contratos) {
                $arrV[$u] = [];
                $ano = $contratos->ano;
                $id = $contratos->id;
                $flex_max = $contratos->flex_max;
                $flex_min = $contratos->flex_min;
                $v1 = $contratos->v1;
                $v2 = $contratos->v2;
                $v3 = $contratos->v3;
                $v4 = $contratos->v4;
                $v5 = $contratos->v5;
                $v6 = $contratos->v6;
                $v7 = $contratos->v7;
                $v8 = $contratos->v8;
                $v9 = $contratos->v9;
                $v10 = $contratos->v10;
                $v11 = $contratos->v11;
                $v12 = $contratos->v12;

                array_push($arrAno, $ano);
                array_push($arrId, $id);
                array_push($arrFlexMax, $flex_max);
                array_push($arrFlexMin, $flex_min);
                array_push($arrV[$u], $v1);
                array_push($arrV[$u], $v2);
                array_push($arrV[$u], $v3);
                array_push($arrV[$u], $v4);
                array_push($arrV[$u], $v5);
                array_push($arrV[$u], $v6);
                array_push($arrV[$u], $v7);
                array_push($arrV[$u], $v8);
                array_push($arrV[$u], $v9);
                array_push($arrV[$u], $v10);
                array_push($arrV[$u], $v11);
                array_push($arrV[$u], $v12);
                array_push($arrVValues, $arrV[$u]);
                $u++;
            }
            $newArray = array("id" => $arrId, "ano" => $arrAno, "arrV" => $arrVValues, "flexMax" => $arrFlexMax, "flexMin" => $arrFlexMin);
            $this->setValoresGrafico($newArray);
        }
    }

    public function separarAno()
    {
        //Recolhe e cria dados necessarios.
        if ($this->getAnoBusqueda()) {
            $values = $this->getValoresGrafico();
            $ano_contrato = $this->getAnoBusqueda();
            $totalMontante = count($values["id"]);
            $arrVAno = [];
            //Pegamos os índices que tem nosso ano instanciado:
            for ($i = 0; $i < $totalMontante; $i++) {
                if ($values["ano"][$i] == $ano_contrato) {
                    $ano_montante_id = $i;
                    array_push($arrVAno, $ano_montante_id);
                }
            }
            return $arrVAno;
        }
    }

    public function varChartData($data, $labelsName = array(), $labelsId = array(), $backgroundColor = array(), $borderColor = array())
    {
        if ($this->getIdGrafico()) {
            $varPorAno = $this->separarAno();
            $values = $this->getValoresGrafico();
            $totalAnos = count($varPorAno);
            echo "
            var $data = {
                labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'], 
                datasets: [
                    {
                        label: '$labelsName[0]',
                        type: 'line',
                        fill: false,
                        borderWidth: 2,
                        lineTension: 0,
                        borderDash: [5, 5],
                        backgroundColor: '$backgroundColor[0]',
                        borderColor: '$borderColor[0]',
                        data: [
            ";
            if ($totalAnos) {
                if ($totalAnos == 1) {
                    for ($i = 0; $i < $totalAnos; $i++) {
                        $o = $varPorAno[$i];
                        for ($j = 0; $j < 12; $j++) {
                            echo round(($values["arrV"][$o][$j]) * (1 + $values["flexMax"][$o] / 100), 2) . ", ";
                        }
                    }
                } else {
                    for ($i = 0; $i < 12; $i++) {
                        $v_mes_atual = 0;
                        for ($j = 0; $j < $totalAnos; $j++) {
                            $o = $varPorAno[$j];
                            $v_mes_atual += ($values["arrV"][$o][$i]) * (1 + ($values["flexMax"][$o] / 100));
                        }
                        echo round($v_mes_atual, 2) . ", ";
                    }
                }
            } else {
                for ($i = 0; $i < 12; $i++) {
                    echo "0, ";
                }
            }
            echo "
                        ]
                    },
                    {
                        label: '$labelsName[1]',
                        type: 'line',
                        fill: false,
                        borderWidth: 2,
                        lineTension: 0,
                        borderDash: [5, 5],
                        backgroundColor: '$backgroundColor[1]',
                        borderColor: '$borderColor[1]',
                        data: [
            ";
            if ($totalAnos) {
                if ($totalAnos == 1) {
                    for ($i = 0; $i < $totalAnos; $i++) {
                        $o = $varPorAno[$i];
                        for ($j = 0; $j < 12; $j++) {
                            echo round(($values["arrV"][$o][$j]), 2) . ", ";
                        }
                    }
                } else {
                    for ($i = 0; $i < 12; $i++) {
                        $v_mes_atual = 0;
                        for ($j = 0; $j < $totalAnos; $j++) {
                            $o = $varPorAno[$j];
                            $v_mes_atual += ($values["arrV"][$o][$i]);
                        }
                        echo round($v_mes_atual, 2) . ", ";
                    }
                }
            } else {
                for ($i = 0; $i < 12; $i++) {
                    echo "0, ";
                }
            }
            echo "
                        ]
                    },
                    {
                        label: '$labelsName[2]',
                        type: 'line',
                        fill: false,
                        borderWidth: 2,
                        lineTension: 0,
                        borderDash: [5, 5],
                        backgroundColor: '$backgroundColor[2]',
                        borderColor: '$borderColor[2]',
                        data: [
            ";
            if ($totalAnos) {
                if ($totalAnos == 1) {
                    for ($i = 0; $i < $totalAnos; $i++) {
                        $o = $varPorAno[$i];
                        for ($j = 0; $j < 12; $j++) {
                            echo round(($values["arrV"][$o][$j]) * (1 - ($values["flexMin"][$o] / 100)), 2) . ", ";
                        }
                    }
                } else {
                    for ($i = 0; $i < 12; $i++) {
                        $v_mes_atual = 0;
                        for ($j = 0; $j < $totalAnos; $j++) {
                            $o = $varPorAno[$j];
                            $v_mes_atual += ($values["arrV"][$o][$i]) * (1 - ($values["flexMin"][$o] / 100));
                        }
                        echo round($v_mes_atual, 2) . ", ";
                    }
                }
            } else {
                for ($i = 0; $i < 12; $i++) {
                    echo "0, ";
                }
            }
            echo "
                        ]
                    }
                ]
            };
            ";
        }
    }

    public function configChartData($data, $tipo = "", $titleText)
    {
        if ($this->getIdGrafico()) {
            $id = $this->getIdGrafico();
            echo "
                var ctx = document.getElementById('$id').getContext('2d');
                window.myBar = new Chart(ctx, {
                    type: 'line',
                    data: $data,
                    options: {
                        responsive: false,
                        maintainAspectRatio: true,
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: false,
                            text: '$titleText'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true,
                            duration: 2000,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    if(tooltipItem.datasetIndex == 0) {
                                        var flexMax = data.datasets[0].data[tooltipItem.index];
                                        return \"Flex máx: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(flexMax);
                                    } else if(tooltipItem.datasetIndex == 1) {
                                        var valMedio = data.datasets[1].data[tooltipItem.index];
                                        return \"Valor médio: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(valMedio);
                                    } else if(tooltipItem.datasetIndex == 2) {
                                        var flexMin = data.datasets[2].data[tooltipItem.index];
                                        return \"Flex min: \" + formatadorDeNumeroInglesParaNumeroBrasileiro(flexMin);
                                    }
                                },
                            }
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    min: 0,
                                    callback: function(label, index, labels){
                                        return formatadorDeNumeroInglesParaNumeroBrasileiro(label);
                                    }   
                                },
                            }, ]
                        }
                    }
                });
            ";
        }
    }
}
