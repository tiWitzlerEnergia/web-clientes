<?php
class Grafico extends Usuario
{
    // atributos:
    protected $idGrafico;
    protected $urlApi;
    protected $parametrosApi;
    protected $valoresGrafico;

    // metodos especiais:
    public function __construct($idGrafico = "", $urlApi = "", $parametrosApi = array())
    {
        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setUrlApi($urlApi);
        // Por padrao, definir sempre como primeiro parametro o indice que define os label.
        $this->setParametrosApi($parametrosApi);
        $this->recolherDados();                     // Define valoresGrafico
    }

    /**
     * Get the value of idGrafico
     */
    protected function getIdGrafico()
    {
        return $this->idGrafico;
    }

    /**
     * Set the value of idGrafico
     *
     * @return  self
     */
    protected function setIdGrafico($idGrafico)
    {
        $this->idGrafico = $idGrafico;

        return $this;
    }

    /**
     * Get the value of url
     */
    protected function getUrlApi()
    {
        return $this->urlApi;
    }

    /**
     * Set the value of url
     *
     * @return  self
     */
    protected function setUrlApi($urlApi)
    {
        $this->urlApi = "https://api.develop.clientes.witzler.com.br/api/" . $urlApi;

        return $this;
    }

    /**
     * Get the value of parametrosApi
     */
    protected function getParametrosApi()
    {
        return $this->parametrosApi;
    }

    /**
     * Set the value of parametrosApi
     *
     * @return  self
     */
    protected function setParametrosApi($parametrosApi = array())
    {
        $this->parametrosApi = $parametrosApi;

        return $this;
    }

    /**
     * Get the value of valoresGrafico
     */
    public function getValoresGrafico()
    {
        return $this->valoresGrafico;
    }

    /**
     * Set the value of valoresGrafico
     *
     * @return  self
     */
    protected function setValoresGrafico($valoresGrafico = array())
    {
        $this->valoresGrafico = $valoresGrafico;

        return $this;
    }

    // Metodos publicos:
    public function validateDate($date, $format = 'Y-m-d\TH:i:s\.\0\0\0\+\0\0\0\0')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function verificarNumber($valor)
    {
        if (is_numeric($valor)) {
            return true;
        } else {
            return false;
        }
    }

    public function formatarNumber($valor)
    {
        $valor = number_format($valor, 2);
        $valor = str_replace(',', '', $valor);
        return $valor;
    }

    public function get_duplicates($array)
    {
        return array_unique(array_diff_assoc($array, array_unique($array)));
    }

    public function date_sort($a, $b)
    {
        return strcmp($a, $b); //matriz b mapping
    }

    public function recolherDados()
    {
        if ($this->getUrlApi() && $this->getParametrosApi() && $this->getContextToken()) {
            $valores = $this->getParametrosApi();                                   // recolhe parametros da api que irao ser utilizados em var valores
            $url = $this->getUrlApi();                                              // recolhe url da api para procurar dados
            @$recolhe = file_get_contents($url, false, $this->getContextToken());    // recolhe dados da api
            @$result = json_decode($recolhe);                                        // Transforma em array.
            if ($result) {
                foreach ($result as $value) {
                    $newArray = get_object_vars($value);                            // Os object viram array para trabalhar com eles em foreach pegando key e value
                    foreach ($newArray as $key => $values) {
                        if (array_key_exists($key, $valores)) {                     // pegar somente valores onde a key esta em nosso array valores.
                            $values = $values;
                            if ($this->validateDate($values)) {
                                $values = date("d-m-Y", strtotime($values));
                            } else if ($this->verificarNumber($values)) {
                                //$values = number_format($values, 2);
                                //$values = str_replace(',', '', $values);
                                $values = $this->formatarNumber($values);
                            }
                            array_push($valores[$key], $values);                    // adicona o value no seu respectivo parametro dentro de valores.
                        }
                    }
                }
                // quando terminou de montar array valores, setar em valoresGrafico:
                $this->setValoresGrafico($valores);
            }
        }
    }
}
