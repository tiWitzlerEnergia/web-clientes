<?php

class Comercializadoras extends Grafico implements GerarGraficoInterface
{
    // Atributos
    protected $idUnidadeUrl;
    protected $anoBusqueda;
    protected $somaTotalComercializadora;

    // Metodos especiais
    public function __construct($anoBusqueda, $idGrafico = "", $idUnidadeUrl = 0)
    {
        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setIdUnidadeUrl($idUnidadeUrl);
        $this->setUrlApi("contrato2?id_unidades=" . $this->getIdUnidadeUrl());
        $this->setAnoBusqueda($anoBusqueda);
    }

    /**
     * Get the value of idUnidadeUrl
     */
    public function getIdUnidadeUrl()
    {
        return $this->idUnidadeUrl;
    }

    /**
     * Set the value of idUnidadeUrl
     *
     * @return  self
     */
    public function setIdUnidadeUrl($idUnidadeUrl)
    {
        $this->idUnidadeUrl = $idUnidadeUrl;

        return $this;
    }

    /**
     * Get the value of anoBusqueda
     */
    public function getAnoBusqueda()
    {
        return $this->anoBusqueda;
    }

    /**
     * Set the value of anoBusqueda
     *
     * @return  self
     */
    public function setAnoBusqueda($anoBusqueda)
    {
        //$anoBusqueda = date("Y", strtotime($anoBusqueda));
        $this->anoBusqueda = $anoBusqueda;

        return $this;
    }

    /**
     * Get the value of somaTotalComercializadora
     */
    public function getSomaTotalComercializadora()
    {
        return $this->somaTotalComercializadora;
    }

    /**
     * Set the value of somaTotalComercializadora
     *
     * @return  self
     */
    public function setSomaTotalComercializadora($somaTotalComercializadora)
    {
        $this->somaTotalComercializadora = $somaTotalComercializadora;

        return $this;
    }

    // Metodos pubicos:
    public function contratosApi()
    {
        //Arrays utilizados:
        $u = 0;
        $arrId = [];
        $arrIdDistribuidora = [];
        $arrAno = [];
        $arrVValues = [];
        //Dados api
        $url = $this->getUrlApi();
        $recolhe = file_get_contents($url, false, $this->getContextToken());
        $result = json_decode($recolhe);
        if ($result) {
            foreach ($result as $contratos) {
                $arrV[$u] = [];

                $id = $contratos->id;
                $id_distribuidora = $contratos->id_distribuidora;
                $ano = $contratos->ano;
                $v1 = $contratos->v1;
                $v2 = $contratos->v2;
                $v3 = $contratos->v3;
                $v4 = $contratos->v4;
                $v5 = $contratos->v5;
                $v6 = $contratos->v6;
                $v7 = $contratos->v7;
                $v8 = $contratos->v8;
                $v9 = $contratos->v9;
                $v10 = $contratos->v10;
                $v11 = $contratos->v11;
                $v12 = $contratos->v12;

                array_push($arrId, $id);
                array_push($arrIdDistribuidora, $id_distribuidora);
                array_push($arrAno, $ano);
                array_push($arrV[$u], $v1);
                array_push($arrV[$u], $v2);
                array_push($arrV[$u], $v3);
                array_push($arrV[$u], $v4);
                array_push($arrV[$u], $v5);
                array_push($arrV[$u], $v6);
                array_push($arrV[$u], $v7);
                array_push($arrV[$u], $v8);
                array_push($arrV[$u], $v9);
                array_push($arrV[$u], $v10);
                array_push($arrV[$u], $v11);
                array_push($arrV[$u], $v12);
                array_push($arrVValues, $arrV[$u]);
                $u++;
            }
            $newArray = array("id" => $arrId, "idDistribuidora" => $arrIdDistribuidora, "ano" => $arrAno, "arrV" => $arrVValues);
            return $newArray;
        }
    }

    public function separarAno()
    {
        //Recolhe e cria dados necessarios.
        if ($this->getAnoBusqueda() && $this->contratosApi()) {
            $values = $this->contratosApi();
            $ano_contrato = $this->getAnoBusqueda();
            $totalMontante = count($values["id"]);
            $arrVAno = [];
            $arrMontante = [];
            $arrMontanteId = [];
            //Pegamos os índices que tem nosso ano instanciado:
            for ($i = 0; $i < $totalMontante; $i++) {
                if ($values["ano"][$i] == $ano_contrato) {
                    $ano_montante_id = $i;
                    array_push($arrVAno, $ano_montante_id);
                }
            }
            $totalAnos = count($arrVAno);
            //Fazendo soma de v1 a v12 separado por ano:
            for ($i = 0; $i < $totalAnos; $i++) {
                //v_unidade é a soma total de todos os v1 dada uma comercializadora
                $o = $arrVAno[$i];
                $v_unidade[$i] = 0;
                for ($j = 0; $j < 12; $j++) {
                    $v_unidade[$i] += $values["arrV"][$o][$j];
                }
                //Agora criamos uma variável que vai ter $id_comercializadora + soma total dos v
                $valor_montante = $values["idDistribuidora"][$o] . "/" . $v_unidade[$i];

                array_push($arrMontanteId, $values["idDistribuidora"][$o]);
                array_push($arrMontante, $valor_montante);
            }
            //Montamos tudo num array so e damos return:
            $newArray = array("montanteId" => $arrMontanteId, "montante" => $arrMontante);
            return $newArray;
        }
    }

    public function semRepeticoes()
    {
        if ($this->separarAno()) {
            //Recupera dados necessarios, fragmenta e cria variaveis necessarias:
            $values = $this->separarAno();
            $limiteMontante = count($values["montante"]);
            $arrSemRepeticoes = [];
            //Comeca a montar arrays com os id unicos:
            for ($i = 0; $i < $limiteMontante; $i++) {
                if (isset($values["montante"][$i])) {
                    $arr_valor_atual = explode("/", $values["montante"][$i]);
                    $id_atual = $arr_valor_atual[0];
                    $valor_atual = $arr_valor_atual[1];
                    for ($j =  0; $j < $limiteMontante; $j++) {
                        $arr_valor_passagem = explode("/", $values["montante"][$j]);
                        $id_passagem = $arr_valor_passagem[0];
                        $valor_passagem = $arr_valor_passagem[1];
                        if (($id_atual == $id_passagem) && ($i != $j) && ($i < $j)) {
                            $valor_atual += $valor_passagem;
                            array_splice($values["montante"], $j);
                        }
                    }
                    $var = $id_atual . "/" . $valor_atual;
                    array_push($arrSemRepeticoes, $var);
                }
            }
            return $arrSemRepeticoes;
        }
    }

    public function comercializadoraApi()
    {
        if ($this->semRepeticoes()) {
            $values = $this->semRepeticoes();
            $totalComercializadora = count($values);
            $somaTotalComercializadora = 0;
            if ($totalComercializadora > 0) {
                $arrNomeComercializadora = [];
                $arrPath = [];
                for ($i = 0; $i < $totalComercializadora; $i++) {
                    $comercializadora_atual = explode("/", $values[$i]);
                    $id_comercializadora_atual = $comercializadora_atual[0];
                    $urlComercializadora = "https://api.develop.clientes.witzler.com.br/api/comercializadora?id_comercializadora=" . $id_comercializadora_atual;
                    @$recolheComercializadora = file_get_contents($urlComercializadora, false, $this->getContextToken());
                    @$resultComercializadora = json_decode($recolheComercializadora);
                    foreach ($resultComercializadora as $comercializadora) {
                        $nome_comercializadora = $comercializadora->nome;
                        $path_comercializadora = $comercializadora->path;

                        array_push($arrNomeComercializadora, $nome_comercializadora);
                        array_push($arrPath, $path_comercializadora);
                    }

                    $somaTotalComercializadora += $comercializadora_atual[1];
                }
                $newArray = array("nomeComercializadora" => $arrNomeComercializadora, "path" => $arrPath, "values" => $values);
                $this->setSomaTotalComercializadora($somaTotalComercializadora);
                $this->setValoresGrafico($newArray);
            }
        }
    }

    public function varChartData($data, $labelsName = array(), $labelsId = array(), $backgroundColor = array(), $borderColor = array())
    {
        $this->comercializadoraApi();
        if ($this->getValoresGrafico()) {
            $arrayValores = $this->getValoresGrafico();
            $cores = ["rgba(29, 107, 170, 0.7)", "rgba(76, 142, 173, 0.7)", "rgba(135, 187, 45, 1)", "#E98E29", "rgba(134, 134, 134, 0.8)", "rgba(17, 55, 89, 0.8)", "#DA70D6", "#DC143C", "#B22222", "#483D8B"];
            echo "
            var $data = {
                labels: [
            ";
            for ($i = 0; $i < count($arrayValores[$labelsId[0]]); $i++) {
                echo "'" . $arrayValores[$labelsId[0]][$i] . "', ";
            }
            echo "
                ], datasets: [
            ";
            for ($i = 0; $i < count($labelsName); $i++) {
                echo "
                    {
                        label: '$labelsName[$i]',
                        backgroundColor: [
                ";
                $cont_cor = 0;
                for ($i = 0; $i < count($arrayValores[$labelsId[0]]); $i++) {
                    echo "'$cores[$cont_cor]', ";
                    $cont_cor++;
                    if ($cont_cor == 10) {
                        $cont_cor = 0;
                    }
                }
                echo "
                        ],
                        data: [
                ";
                if (count($arrayValores[$labelsId[0]])) {
                    for ($j = 0; $j < count($arrayValores[$labelsId[0]]); $j++) {
                        $valores = explode("/", $arrayValores[$labelsId[1]][$j]);
                        $v_per = ($valores[1] / $this->getSomaTotalComercializadora()) * 100;
                        echo round($v_per, 1) . ", ";
                    }
                } else {
                    echo 0;
                }
                echo "
                        ],
                    }, 
                ";
            }
            echo "
                ]
            };
            ";
        }
    }

    public function configChartData($data, $tipo = "", $titleText)
    {
        if ($this->getIdGrafico()) {
            $id = $this->getIdGrafico();
            echo "
                var ctx = document.getElementById('$id').getContext('2d');
                window.comercializadora = new Chart(ctx, {
                    type: '$tipo',
                    data: $data,
                    options: {
                        responsive: false,
                        maintainAspectRatio: true,
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: false,
                            text: '$titleText'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true,
                            duration: 2000,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    var labelText = data.labels[tooltipItem.index];
                                    var valueData = data.datasets[0].data[tooltipItem.index];
                                    return labelText + \": \" + formatadorDeNumeroInglesParaNumeroBrasileiro(valueData);
                                },
                            }
                        },
                    }
                });
            ";
        }
    }
}
