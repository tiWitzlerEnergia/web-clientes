<?php

class ListaUnidades extends Usuario
{
    // Atributos
    public $idUnidades;
    public $nomeUnidades;
    public $idDistribuidoraUnidades;
    public $ucUnidades;
    public $medidorUnidades;
    public $cookieUnidadeAtiva;

    // Métodos especiais:
    public function __construct()
    {
        $this->definirUsuario();
        $this->procurarUnidadesApi();
    }

    /**
     * Get the value of idUnidades
     */
    public function getIdUnidades()
    {
        return $this->idUnidades;
    }

    /**
     * Set the value of idUnidades
     *
     * @return  self
     */
    public function setIdUnidades($idUnidades)
    {
        $this->idUnidades = $idUnidades;

        return $this;
    }

    /**
     * Get the value of nomeUnidades
     */
    public function getNomeUnidades()
    {
        return $this->nomeUnidades;
    }

    /**
     * Set the value of nomeUnidades
     *
     * @return  self
     */
    public function setNomeUnidades($nomeUnidades)
    {
        $this->nomeUnidades = $nomeUnidades;

        return $this;
    }

    /**
     * Get the value of idDistribuidoraUnidades
     */
    public function getIdDistribuidoraUnidades()
    {
        return $this->idDistribuidoraUnidades;
    }

    /**
     * Set the value of idDistribuidoraUnidades
     *
     * @return  self
     */
    public function setIdDistribuidoraUnidades($idDistribuidoraUnidades)
    {
        $this->idDistribuidoraUnidades = $idDistribuidoraUnidades;

        return $this;
    }

    /**
     * Get the value of ucUnidades
     */
    public function getUcUnidades()
    {
        return $this->ucUnidades;
    }

    /**
     * Set the value of ucUnidades
     *
     * @return  self
     */
    public function setUcUnidades($ucUnidades)
    {
        $this->ucUnidades = $ucUnidades;

        return $this;
    }

    /**
     * Get the value of medidorUnidades
     */
    public function getMedidorUnidades()
    {
        return $this->medidorUnidades;
    }

    /**
     * Set the value of medidorUnidades
     *
     * @return  self
     */
    public function setMedidorUnidades($medidorUnidades)
    {
        $this->medidorUnidades = $medidorUnidades;

        return $this;
    }

    /**
     * Get the value of cookieUnidadeAtiva
     */ 
    public function getCookieUnidadeAtiva()
    {
        return $this->cookieUnidadeAtiva;
    }

    /**
     * Set the value of cookieUnidadeAtiva
     *
     * @return  self
     */ 
    public function setCookieUnidadeAtiva($cookieUnidadeAtiva)
    {
        $this->cookieUnidadeAtiva = $cookieUnidadeAtiva;

        return $this;
    }

    // Métodos publicos e protegidos:
    public function procurarUnidadesApi()
    {
        $arrayIdUnidade = [];
        $arrayNomeUnidade = [];
        $arrayDistribuidoraId = [];
        $arrayUcUnidade = [];
        $arrayMedidorUnidade = [];

        $url = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=" . $this->getUsuario();
        $recolhe = file_get_contents($url, false, $this->getContextToken());
        $resultado = json_decode($recolhe);
        if (isset($resultado)) {
            foreach ($resultado as $unidade) {
                $idUnidade = $unidade->id_unidades;
                $nomeUnidade = $unidade->nome;
                $distribuidoraUnidade = $unidade->distribuidora;
                $valor_uc = $unidade->uc_unidade;
                $medidorUnidade = $unidade->medidor;

                array_push($arrayIdUnidade, $idUnidade);
                array_push($arrayNomeUnidade, $nomeUnidade);
                array_push($arrayDistribuidoraId, $distribuidoraUnidade);
                array_push($arrayUcUnidade, $valor_uc);
                array_push($arrayMedidorUnidade, $medidorUnidade);
            }

            if (count($arrayIdUnidade) > 0) {
                $this->setIdUnidades($arrayIdUnidade);
                $this->setNomeUnidades($arrayNomeUnidade);
                $this->setIdDistribuidoraUnidades($arrayDistribuidoraId);
                $this->setUcUnidades($arrayUcUnidade);
                $this->setMedidorUnidades($arrayMedidorUnidade);
            }
        }
    }

    // Métodos publicos
    public function criarVisual($id, $cookie)
    {
        if (count($this->getIdUnidades()) > 0) {
            echo "<select id='$id' class='custom-select dropdown-menu-right dropdown2 dropdown' onChange='mudarUnidade(\"$id\", \"$cookie\")'>";
            echo "<option class='dropdown-item' selected disabled>Selecione uma unidade...</option>";
            for ($i = 0; $i < count($this->getIdUnidades()); $i++) {
                echo "<option id='" . $this->getIdUnidades()[$i] . "' value='" . $this->getIdUnidades()[$i] . "' class='dropdown-item'>" . $this->getNomeUnidades()[$i] . "</option>";
            }
            echo "</select>";
        } else {
            echo "<script>console.log('Sem unidades');</script>";
        }
    }
}
