<?php
class Colaboradores
{
    // Atributos
    private $id_colaborador;
    private $administrador;
    private $email;
    private $nome_colaborador;
    private $password_colaborador;
    private $path_img;
    private $telefone_1;
    private $telefone_2;
    private $username;
    private $admin_colaborador;

    // Metodos especiais
    // Constructor pra procurar listaClientesUsuario
    public function constructListaClientesUsuario($id_colaborador)
    {
        $this->setIdColaborador($id_colaborador);
    }

    // Construct para procurar em info/usuario-colaborador/$username
    public function constructInfoUsuarioColaborador($username)
    {
        $this->setUsername($username);
    }

    /**
     * Get the value of id_colaborador
     */
    public function getIdColaborador()
    {
        return $this->id_colaborador;
    }

    /**
     * Set the value of id_colaborador
     */
    public function setIdColaborador($id_colaborador): self
    {
        $this->id_colaborador = $this->sanitizarVariavel($id_colaborador);

        return $this;
    }

    /**
     * Get the value of administrador
     */
    public function getAdministrador()
    {
        return $this->administrador;
    }

    /**
     * Set the value of administrador
     */
    public function setAdministrador($administrador): self
    {
        $this->administrador = $this->sanitizarVariavel($administrador);

        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     */
    public function setEmail($email): self
    {
        $this->email = $this->sanitizarVariavel($email);

        return $this;
    }

    /**
     * Get the value of nome_colaborador
     */
    public function getNomeColaborador()
    {
        return $this->nome_colaborador;
    }

    /**
     * Set the value of nome_colaborador
     */
    public function setNomeColaborador($nome_colaborador): self
    {
        $this->nome_colaborador = $this->sanitizarVariavel($nome_colaborador);

        return $this;
    }

    /**
     * Get the value of password_colaborador
     */
    public function getPasswordColaborador()
    {
        return $this->password_colaborador;
    }

    /**
     * Set the value of password_colaborador
     */
    public function setPasswordColaborador($password_colaborador): self
    {
        $this->password_colaborador = $this->sanitizarVariavel($password_colaborador);

        return $this;
    }

    /**
     * Get the value of path_img
     */
    public function getPathImg()
    {
        return $this->path_img;
    }

    /**
     * Set the value of path_img
     */
    public function setPathImg($path_img): self
    {
        $this->path_img = $this->sanitizarVariavel($path_img);

        return $this;
    }

    /**
     * Get the value of telefone_1
     */
    public function getTelefone1()
    {
        return $this->telefone_1;
    }

    /**
     * Set the value of telefone_1
     */
    public function setTelefone1($telefone_1): self
    {
        $this->telefone_1 = $this->sanitizarVariavel($telefone_1);

        return $this;
    }

    /**
     * Get the value of telefone_2
     */
    public function getTelefone2()
    {
        return $this->telefone_2;
    }

    /**
     * Set the value of telefone_2
     */
    public function setTelefone2($telefone_2): self
    {
        $this->telefone_2 = $this->sanitizarVariavel($telefone_2);

        return $this;
    }

    /**
     * Get the value of username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of username
     */
    public function setUsername($username): self
    {
        $this->username = $this->sanitizarVariavel($username);

        return $this;
    }

    /**
     * Get the value of admin_colaborador
     */
    public function getAdminColaborador()
    {
        return $this->admin_colaborador;
    }

    /**
     * Set the value of admin_colaborador
     */
    public function setAdminColaborador($admin_colaborador): self
    {
        $this->admin_colaborador = $this->sanitizarVariavel($admin_colaborador);

        return $this;
    }

    // Métodos publicos e protegidos:
    public function sanitizarVariavel($var)
    {
        $sanitizar = $var;
        $sanitizar = str_replace(' ', '', $sanitizar);    //Tira espaços em branco
        $sanitizar = strip_tags($sanitizar);
        $sanitizar = stripcslashes($sanitizar);

        return $sanitizar;
    }

    public function validateListaClientesUsuario()
    {
        $id_colaborador = $this->getIdColaborador();

        if ($id_colaborador && is_null($id_colaborador) || !is_numeric($id_colaborador)) {
            return "Invalid id_colaborador";
        } else {
            return 200;
        }
    }

    public function validateInfoUsuarioColaborador()
    {
        $username = $this->getUsername();

        if ($username && is_null($username)) {
            return "Invalid username";
        } else {
            return 200;
        }
    }

    // Recolhe a lista de clientes de um usuario determinado
    public function listaClientesUsuarioApi()
    {
        $validate = $this->validateListaClientesUsuario();

        if ($validate == 200) {
            $id_colaborador = $this->getIdColaborador();
            $url = "https://api.develop.clientes.witzler.com.br/api/webcliente/info/clientes-colaborador/$id_colaborador";

            $httpOptions = array('http' => array(
                'method'  => 'GET',
            ));
            $context  = stream_context_create($httpOptions);
            $recolhe = file_get_contents($url, false, $context);
            $resultado = json_decode($recolhe);
            return $resultado;
        } else {
            return array();
        }
    }

    // Recolhe info de colaborador
    public function infoUsuarioColaboradorApi()
    {
        $validate = $this->validateInfoUsuarioColaborador();

        if ($validate == 200) {
            $username = $this->getUsername();
            $url = "https://api.develop.clientes.witzler.com.br/api/usuarioscolaboradores/info/usuario-colaborador/$username";

            $httpOptions = array('http' => array(
                'method'  => 'GET',
            ));
            $context  = stream_context_create($httpOptions);
            $recolhe = file_get_contents($url, false, $context);
            $resultado = json_decode($recolhe);
            return $resultado;
        } else {
            return array();
        }
    }

    // Recolhe lista de todos os clientes (somente contas admin de colaboradores)
    public function listaClientesAdminApi()
    {
        $url = "https://api.develop.clientes.witzler.com.br/api/webcliente/info/clientes-colaborador/admin/";
        $httpOptions = array('http' => array(
            'method'  => 'GET',
        ));

        $context  = stream_context_create($httpOptions);
        $recolhe = file_get_contents($url, false, $context);
        $resultado = json_decode($recolhe);
        return $resultado;
    }
}
