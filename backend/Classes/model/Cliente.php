<?php
class Cliente
{
    // Atributos
    private $id_cliente;
    private $nome;
    private $email;
    private $username;
    private $nome_fantasia;
    private $colaborador_tec_id;
    private $colaborador_comercial_id;
    private $cow_client_id;
    private $email_recp;
    private $type_client;

    // Metodos especiais
    public function constructInfoUsuarioClienteById($id_cliente)
    {
        $this->setIdCliente($id_cliente);
    }

    /**
     * Get the value of id_cliente
     */
    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    /**
     * Set the value of id_cliente
     */
    public function setIdCliente($id_cliente): self
    {
        $this->id_cliente = $this->sanitizarVariavel($id_cliente);

        return $this;
    }

    /**
     * Get the value of nome
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set the value of nome
     */
    public function setNome($nome): self
    {
        $this->nome = $this->sanitizarVariavel($nome);

        return $this;
    }

    /**
     * Get the value of type_client
     */
    public function getType_client()
    {
        return $this->type_client;
    }

    /**
     * Set the value of type_client
     */
    public function setType_client($type_client): self
    {
        $this->type_client = $this->sanitizarVariavel($type_client);

        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     */
    public function setEmail($email): self
    {
        $this->email = $this->sanitizarVariavel($email);

        return $this;
    }

    /**
     * Get the value of username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of username
     */
    public function setUsername($username): self
    {
        $this->username = $this->sanitizarVariavel($username);

        return $this;
    }

    /**
     * Get the value of nome_fantasia
     */
    public function getNomeFantasia()
    {
        return $this->nome_fantasia;
    }

    /**
     * Set the value of nome_fantasia
     */
    public function setNomeFantasia($nome_fantasia): self
    {
        $this->nome_fantasia = $this->sanitizarVariavel($nome_fantasia);

        return $this;
    }

    /**
     * Get the value of colaborador_tec_id
     */
    public function getColaboradorTecId()
    {
        return $this->colaborador_tec_id;
    }

    /**
     * Set the value of colaborador_tec_id
     */
    public function setColaboradorTecId($colaborador_tec_id): self
    {
        $this->colaborador_tec_id = $this->sanitizarVariavel($colaborador_tec_id);

        return $this;
    }

    /**
     * Get the value of colaborador_comercial_id
     */
    public function getColaboradorComercialId()
    {
        return $this->colaborador_comercial_id;
    }

    /**
     * Set the value of colaborador_comercial_id
     */
    public function setColaboradorComercialId($colaborador_comercial_id): self
    {
        $this->colaborador_comercial_id = $this->sanitizarVariavel($colaborador_comercial_id);

        return $this;
    }

    /**
     * Get the value of cow_client_id
     */
    public function getCowClientId()
    {
        return $this->cow_client_id;
    }

    /**
     * Set the value of cow_client_id
     */
    public function setCowClientId($cow_client_id): self
    {
        $this->cow_client_id = $this->sanitizarVariavel($cow_client_id);

        return $this;
    }

    /**
     * Get the value of email_recp
     */
    public function getEmailRecp()
    {
        return $this->email_recp;
    }

    /**
     * Set the value of email_recp
     */
    public function setEmailRecp($email_recp): self
    {
        $this->email_recp = $this->sanitizarVariavel($email_recp);

        return $this;
    }

    // Métodos publicos e protegidos:
    public function sanitizarVariavel($var)
    {
        $sanitizar = $var;
        $sanitizar = str_replace(' ', '', $sanitizar);    //Tira espaços em branco
        $sanitizar = strip_tags($sanitizar);
        $sanitizar = stripcslashes($sanitizar);

        return $sanitizar;
    }

    public function validateInfoUsuarioClienteById()
    {
        $id_cliente = $this->getIdCliente();

        if ($id_cliente && is_null($id_cliente) || !is_numeric($id_cliente)) {
            return "Invalid id_cliente";
        } else {
            return 200;
        }
    }

    // Obter dados do usuario pelo id da empresa
    public function infoUsuarioClienteByIdApi()
    {
        $validate = $this->validateInfoUsuarioClienteById();

        if ($validate == 200) {
            $id_cliente = $this->getIdCliente();

            $url = "https://api.develop.clientes.witzler.com.br/api/webcliente/info/usuario-cliente/$id_cliente";

            $httpOptions = array('http' => array(
                'method'  => 'GET',
            ));
            $context  = stream_context_create($httpOptions);
            $recolhe = file_get_contents($url, false, $context);
            $resultado = json_decode($recolhe);
            return $resultado;
        } else {
            return array();
        }
    }
}
