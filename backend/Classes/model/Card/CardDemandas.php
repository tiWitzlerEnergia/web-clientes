<?php
class CardDemandas extends Grafico
{
    // Atributos:
    protected $dataGrafico;
    protected $idUnidadeGrafico;
    private $c_demanda_fponta;
    private $c_demanda_ponta;
    private $demanda_fponta;
    private $demanda_ponta;

    // Metodos especiais:
    public function __construct($idGrafico, $dataGrafico, $idUnidadeGrafico)
    {
        $parametrosApi = array("id" => array(), "c_demanda_fponta" => array(), "c_demanda_ponta" => array(), "demanda_fponta" => array(), "demanda_ponta" => array());

        $this->definirUsuario();
        $this->setIdGrafico($idGrafico);
        $this->setDataGrafico($dataGrafico);
        $this->setIdUnidadeGrafico($idUnidadeGrafico);
        $this->setParametrosApi($parametrosApi);
        $this->setUrlApi("demanda/data_unidade?mes_ref=" . $this->getDataGrafico() . "&unidade_id=" .  $this->getIdUnidadeGrafico());
        $this->recolherDados();                     // Define valoresGrafico
        //$this->updateValues();
    }

    /**
     * Get the value of dataGrafico
     */
    public function getDataGrafico()
    {
        return $this->dataGrafico;
    }

    /**
     * Set the value of dataGrafico
     *
     * @return  self
     */
    public function setDataGrafico($dataGrafico)
    {
        $dataGrafico = date("Y-m-01", strtotime($dataGrafico));
        $this->dataGrafico = $dataGrafico;

        return $this;
    }

    /**
     * Get the value of idUnidadeGrafico
     */
    public function getIdUnidadeGrafico()
    {
        return $this->idUnidadeGrafico;
    }

    /**
     * Set the value of idUnidadeGrafico
     *
     * @return  self
     */
    public function setIdUnidadeGrafico($idUnidadeGrafico)
    {
        $this->idUnidadeGrafico = $idUnidadeGrafico;

        return $this;
    }

    /**
     * Get the value of c_demanda_fponta
     */
    public function getCDemandaFponta()
    {
        return $this->c_demanda_fponta;
    }

    /**
     * Set the value of c_demanda_fponta
     */
    public function setCDemandaFponta($c_demanda_fponta): self
    {
        $this->c_demanda_fponta = $c_demanda_fponta;

        return $this;
    }

    /**
     * Get the value of c_demanda_ponta
     */
    public function getCDemandaPonta()
    {
        return $this->c_demanda_ponta;
    }

    /**
     * Set the value of c_demanda_ponta
     */
    public function setCDemandaPonta($c_demanda_ponta): self
    {
        $this->c_demanda_ponta = $c_demanda_ponta;

        return $this;
    }

    /**
     * Get the value of demanda_fponta
     */
    public function getDemandaFponta()
    {
        return $this->demanda_fponta;
    }

    /**
     * Set the value of demanda_fponta
     */
    public function setDemandaFponta($demanda_fponta): self
    {
        $this->demanda_fponta = $demanda_fponta;

        return $this;
    }

    /**
     * Get the value of demanda_ponta
     */
    public function getDemandaPonta()
    {
        return $this->demanda_ponta;
    }

    /**
     * Set the value of demanda_ponta
     */
    public function setDemandaPonta($demanda_ponta): self
    {
        $this->demanda_ponta = $demanda_ponta;

        return $this;
    }

    // Metodos publicos:
    public function varChartData($data, $labelsName = array(), $labelsId = array(), $backgroundColor = array(), $borderColor = array())
    {
        $values = $this->getValoresGrafico();
        //var_dump($values);
    }

    public function configChartData($data, $tipo = "", $titleText)
    {
        if ($this->getIdGrafico()) {
            $id = $this->getIdGrafico();
        }
    }

    public function updateValues()
    {
        $contador = 0;
        while (empty($this->getValoresGrafico()) || $contador > 12) {
            $data = $this->$this->getDataGrafico();
            $atual = date("Y-m-01", strtotime("-1 month", strtotime($data)));
            $this->setDataGrafico($atual);
            $this->setUrlApi("demanda/data_unidade?mes_ref=" . $this->getDataGrafico() . "&unidade_id=" .  $this->getIdUnidadeGrafico());
            $this->recolherDados();                     // Define valoresGrafico
        }
    }

    public function setValuesCard()
    {
        $values = $this->getValoresGrafico();

        $this->setCDemandaFponta($values["c_demanda_fponta"][0]);
        $this->setCDemandaPonta($values["c_demanda_ponta"][0]);
        $this->setDemandaFponta($values["demanda_fponta"][0]);
        $this->setDemandaPonta($values["demanda_ponta"][0]);
    }
}
