<?php
class Registros
{
    // Atributos
    private $id;
    private $id_usuario;
    private $nome;  // Nome usuario
    private $route;
    private $dispositivo;
    private $system;
    private $system_name;
    private $browser;
    private $data;
    private $token;
    private $contextToken;

    // Metodos especiais
    protected function definirUsuario()
    {
        $this->setNome($_SESSION['usuario']);
        $this->setIdUsuario($_SESSION['cliente_id']);
        $this->setToken($_SESSION['token']);
        $this->setContextToken();
    }

    public function constructSalvarInfoRegistro($route, $dispositivo, $system, $system_name, $browser, $data)
    {
        $this->definirUsuario();
        $this->setRoute($route);
        $this->setDispositivo($dispositivo);
        $this->setSystem($system);
        $this->setSystemName($system_name);
        $this->setBrowser($browser);
        $this->setData($data);
    }

    public function constructSalvarRotas($route, $data)
    {
        $this->definirUsuario();
        $this->setRoute($route);
        $this->setData($data);
    }

    public function constructSalvarInfoLogin($dispositivo, $system, $system_name, $browser, $data)
    {
        $this->definirUsuario();
        $this->setDispositivo($dispositivo);
        $this->setSystem($system);
        $this->setSystemName($system_name);
        $this->setBrowser($browser);
        $this->setData($data);
    }

    public function constructListarRegistros($id_usuario)
    {
        $this->setIdUsuario($id_usuario);
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */
    public function setId($id): self
    {
        $this->id = validateValue($id);

        return $this;
    }

    /**
     * Get the value of id_usuario
     */
    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    /**
     * Set the value of id_usuario
     */
    public function setIdUsuario($id_usuario): self
    {
        $this->id_usuario = validateValue($id_usuario);

        return $this;
    }

    /**
     * Get the value of nome
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set the value of nome
     */
    public function setNome($nome): self
    {
        $this->nome = validateValue($nome);

        return $this;
    }

    /**
     * Get the value of route
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set the value of route
     */
    public function setRoute($route): self
    {
        $this->route = validateValue($route);

        return $this;
    }

    /**
     * Get the value of dispositivo
     */
    public function getDispositivo()
    {
        return $this->dispositivo;
    }

    /**
     * Set the value of dispositivo
     */
    public function setDispositivo($dispositivo): self
    {
        $this->dispositivo = validateValue($dispositivo);

        return $this;
    }

    /**
     * Get the value of system
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * Set the value of system
     */
    public function setSystem($system): self
    {
        $this->system = validateValue($system);

        return $this;
    }

    /**
     * Get the value of system_name
     */
    public function getSystemName()
    {
        return $this->system_name;
    }

    /**
     * Set the value of system_name
     */
    public function setSystemName($system_name): self
    {
        $this->system_name = validateValue($system_name);

        return $this;
    }

    /**
     * Get the value of browser
     */
    public function getBrowser()
    {
        return $this->browser;
    }

    /**
     * Set the value of browser
     */
    public function setBrowser($browser): self
    {
        $this->browser = validateValue($browser);

        return $this;
    }

    /**
     * Get the value of data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     */
    public function setData($data): self
    {
        $this->data = validateValue($data);

        return $this;
    }

    /**
     * Get the value of contextToken
     */
    public function getContextToken()
    {
        return $this->contextToken;
    }

    /**
     * Set the value of contextToken
     */
    public function setContextToken(): self
    {
        if ($this->getToken()) {
            $context = stream_context_create(array(
                'http' => array(
                    'header' => "Authorization: Bearer " . $this->getToken(),
                ),
            ));
            $this->contextToken = $context;
        }

        return $this;
    }

    /**
     * Get the value of token
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set the value of token
     */
    public function setToken($token): self
    {
        $this->token = $token;

        return $this;
    }

    // Metodos publicos
    public function validateSalvarInfoRegistro()
    {
        $id_usuario = $this->getIdUsuario();
        $nome = $this->getNome();
        $route = $this->getRoute();
        $dispositivo = $this->getDispositivo();
        $system = $this->getSystem();
        $system_name = $this->getSystemName();
        $browser = $this->getBrowser();
        $data = $this->getData();

        if ($id_usuario && is_null($id_usuario)) {
            // Validate id_usuario:
            return 'Invalid id_usuario';
        } else if ($nome && is_null($nome)) {
            // Validate nome:
            return 'Invalid nome';
        } else if ($system && is_null($system)) {
            // Validate system:
            return 'Invalid system';
        } else if ($browser && is_null($browser)) {
            // Validate browser
            return 'Invalid browser';
        } else if ($system_name && is_null($system_name)) {
            // Validate system_name
            return 'Invalid system_name';
        } else if ($dispositivo && is_null($dispositivo)) {
            // Validate dispositivo
            return 'Invalid dispositivo';
        } else if ($route && is_null($route)) {
            // Validate route
            return 'Invalid route';
        } else if ($data && is_null($data)) {
            // Validate data
            return 'Invalid data';
        } else {
            return 200;
        }
    }

    public function validateSalvarRotas()
    {
        $id_usuario = $this->getIdUsuario();
        $nome = $this->getNome();
        $route = $this->getRoute();
        $data = $this->getData();

        if ($id_usuario && is_null($id_usuario)) {
            // Validate id_usuario:
            return 'Invalid id_usuario';
        } else if ($nome && is_null($nome)) {
            // Validate nome:
            return 'Invalid nome';
        } else if ($route && is_null($route)) {
            // Validate route
            return 'Invalid route';
        } else if ($data && is_null($data)) {
            // Validate data
            return 'Invalid data';
        } else {
            return 200;
        }
    }

    public function validateSalvarInfoLogin()
    {
        $id_usuario = $this->getIdUsuario();
        $nome = $this->getNome();
        $dispositivo = $this->getDispositivo();
        $system = $this->getSystem();
        $system_name = $this->getSystemName();
        $browser = $this->getBrowser();
        $data = $this->getData();

        if ($id_usuario && is_null($id_usuario)) {
            // Validate id_usuario:
            return 'Invalid id_usuario';
        } else if ($nome && is_null($nome)) {
            // Validate nome:
            return 'Invalid nome';
        } else if ($system && is_null($system)) {
            // Validate system:
            return 'Invalid system';
        } else if ($browser && is_null($browser)) {
            // Validate browser
            return 'Invalid browser';
        } else if ($system_name && is_null($system_name)) {
            // Validate system_name
            return 'Invalid system_name';
        } else if ($dispositivo && is_null($dispositivo)) {
            // Validate dispositivo
            return 'Invalid dispositivo';
        } else if ($data && is_null($data)) {
            // Validate data
            return 'Invalid data';
        } else {
            return 200;
        }
    }

    public function validateListarRegistros()
    {
        $id_usuario = $this->getIdUsuario();

        if ($id_usuario && is_null($id_usuario)) {
            // Validate id_usuario:
            return 'Invalid id_usuario';
        } else {
            return 200;
        }
    }

    // Request para salvar informações de sistema, browser e aba do usuário na db pelo postgred
    public function salvarInfoLoginRequestPg()
    {
        $validate = $this->validateSalvarInfoLogin();

        if ($validate == 200) {
            // Chama classe de pdo e valida os dados que foram inseridos na instancia
            $pdo = new PgConnectController();

            // Recolhe dados da instancia atual
            $id_usuario = $this->getIdUsuario();
            $nome = $this->getNome();
            $dispositivo = $this->getDispositivo();
            $system = $this->getSystem();
            $system_name = $this->getSystemName();
            $browser = $this->getBrowser();
            $data = $this->getData();

            // Monta array para enviar na query:
            $values = array(":nome" => $nome, ":system" => $system, ":browser" => $browser, ":system_name" => $system_name, ":dispositivo" => $dispositivo, ":data" => $data, ":data_login" => $data);

            // Excuta insert de query:
            $result = $pdo->insertQuery("INSERT INTO pc.web_login_acesso (nome, system, browser, system_name, dispositivo, data, data_login) VALUES (:nome, :system, :browser, :system_name, :dispositivo, :data, :data_login)", $values);

            // Retorna resposta montada dentro de insertQuery para ver se o insert deu certo ou não
            return $result;
        } else {
            return array();
        }
    }

    // Request para salvar informações de rota na db pelo postgred
    public function salvarInfoRotasRequestPg()
    {
        $validate = $this->validateSalvarRotas();

        if ($validate == 200) {
            // Chama classe de pdo e valida os dados que foram inseridos na instancia
            $pdo = new PgConnectController();

            // Recolhe dados da instancia atual
            $id_usuario = $this->getIdUsuario();
            $nome = $this->getNome();
            $route = $this->getRoute();
            $data = $this->getData();

            // Monta array para enviar na query:
            $values = array(":nome" => $nome, ":route" => $route, ":data" => $data, ":data_login" => $data);

            // Excuta insert de query:
            $result = $pdo->insertQuery("INSERT INTO pc.web_login_acesso (nome, route, data, data_login) VALUES (:nome, :route, :data, :data_login)", $values);

            // Retorna resposta montada dentro de insertQuery para ver se o insert deu certo ou não
            return $result;
        } else {
            return array();
        }
    }

    // Request para salvar informações de sistema, browser e aba do usuário na db pelo postgred
    public function salvarInfoRegistroRequestPg()
    {
        $validate = $this->validateSalvarInfoRegistro();

        if ($validate == 200) {
            // Chama classe de pdo e valida os dados que foram inseridos na instancia
            $pdo = new PgConnectController();

            // Recolhe dados da instancia atual
            $id_usuario = $this->getIdUsuario();
            $nome = $this->getNome();
            $route = $this->getRoute();
            $dispositivo = $this->getDispositivo();
            $system = $this->getSystem();
            $system_name = $this->getSystemName();
            $browser = $this->getBrowser();
            $data = $this->getData();

            // Monta array para enviar na query:
            $values = array(":nome" => $nome, ":system" => $system, ":browser" => $browser, ":system_name" => $system_name, ":dispositivo" => $dispositivo, ":route" => $route, ":data" => $data, ":data_login" => $data);

            // Excuta insert de query:
            $result = $pdo->insertQuery("INSERT INTO pc.web_login_acesso (nome, system, browser, system_name, dispositivo, route, data, data_login) VALUES (:nome, :system, :browser, :system_name, :dispositivo, :route, :data, :data_login)", $values);

            // Retorna resposta montada dentro de insertQuery para ver se o insert deu certo ou não
            return $result;
        } else {
            return array();
        }
    }

    // Request para listar todas as informacoes salvas de sistema e browser dos visitantes na db do postgres:
    public function listarRegistrosRequestPg()
    {
        $pdo = new PgConnectController();
        $result = $pdo->selectQuery("SELECT * FROM pc.web_login_acesso");

        return $result;
    }
}

