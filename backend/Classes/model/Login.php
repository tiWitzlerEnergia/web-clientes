<?php
class Login extends Usuario
{
    // Atributos:
    protected $usuarioLogin;
    protected $senhaLogin;

    // Métodos especiais:
    public function __construct($usuario, $senha)
    {
        $this->setUsuarioLogin($usuario);
        $this->setSenhaLogin($senha);
    }
    /**
     * Get the value of usuarioLogin
     */
    public function getUsuarioLogin()
    {
        return $this->usuarioLogin;
    }

    /**
     * Set the value of usuarioLogin
     *
     * @return  self
     */
    private function setUsuarioLogin($usuarioLogin)
    {
        $this->usuarioLogin = $this->sanitizarVariavel($usuarioLogin);

        return $this;
    }

    /**
     * Get the value of senhaLogin
     */
    private function getSenhaLogin()
    {
        return $this->senhaLogin;
    }

    /**
     * Set the value of senhaLogin
     *
     * @return  self
     */
    private function setSenhaLogin($senhaLogin)
    {
        $this->senhaLogin = $this->sanitizarVariavel($senhaLogin);

        return $this;
    }

    // Métodos publicos:
    public function fazerLogin()
    {
        $usuario = $this->getUsuarioLogin();
        $senha = $this->getSenhaLogin();
        $url = "https://api.develop.clientes.witzler.com.br/api/usuarios/auth";
        $request = '{
            "u_password": "' . $senha . '",
            "username": "' . $usuario . '"
        }';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json']);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        /**/
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        /**/
        $result = curl_exec($curl);
        $err = curl_error($curl);
        if (!$err) {
            $response = json_decode($result, true);
            if (isset($response['token'])) {
                $token = $response['token'];
                if ($token == $response['token'] && $usuario == $response['username']) {
                    session_start();
                    $_SESSION['usuario'] = $usuario;
                    $_SESSION['senha'] = $senha;
                    $_SESSION['token'] = $response['token'];
                    $_SESSION['CREATED'] = time();
                    $_SESSION['cliente_id'] = 0;
                    $_SESSION['type_client'] = 0;

                    $this->definirUsuario();
                    $registros = new RegistrosController();
                    //$registros->salvarInfoLoginRegistroPg();
                    $this->pegarUnidades();
                    $this->pegarIdCliente();
                } else {
                    //Mensagem de erro de login
                    $this->loginInfoColaborador();
                    //header("Location: index.php?erro=login");
                    //exit();
                }
            } else {
                //Mensagem de erro de login:
                $this->loginInfoColaborador();
                //header("Location: index.php?erro=login");
                //exit();
            }
        } else {
            echo '<br/><div class="alert alert-danger" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Curl Error: ' . $err . '</div>';
        }
    }

    public function loginInfoColaborador()
    {
        $colaborador = new ColaboradoresController();

        $usuario = $this->getUsuarioLogin();
        $senha = $this->getSenhaLogin();

        $infoColaborador = $colaborador->infoUsuarioColaborador($usuario);

        if (isset($infoColaborador) && !empty($infoColaborador) && $usuario == $infoColaborador['username']) {
//	        var_dump($infoColaborador);
//	        var_dump($senha);
	       $hashGuardado = $infoColaborador['password_colaborador'];
	       if(password_verify($senha, $hashGuardado)) {
		       session_start();
		    $_SESSION['usuario'] = $usuario;
		    $_SESSION['senha'] = $senha;
		    $_SESSION['id_colaborador'] = $infoColaborador['id_colaborador'];
		    $_SESSION['admin_colaborador'] = $infoColaborador['administrador'];
		    

		    header("Location: ../dashboard/index-colaborador.php");
	       } else {
		    //Mensagem de erro de login:
		    header("Location: index.php?erro=login");
		    exit();
	       }
	       
	       
	        
            
        } else {
            //Mensagem de erro de login:
            header("Location: index.php?erro=login");
            exit();
        }
    }

    public function pegarUnidades()
    {
        if ($this->getContextToken()) {
            $url = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=" . $this->getUsuario();
            $recolhe = file_get_contents($url, false, $this->getContextToken());
            $result = json_decode($recolhe);
            if (isset($result)) {
                foreach ($result as $unidades) {
                    $cliente_id = $unidades->cliente_id;
                }
                $_SESSION['cliente_id'] = $cliente_id;
                $this->definirUsuario();
            }
        }
    }

    public function pegarUnidadesColaborador()
    {
        if ($this->getContextToken()) {
            $url = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=" . $this->getUsuario();
            $recolhe = file_get_contents($url, false, $this->getContextToken());
            $result = json_decode($recolhe);
            if (isset($result)) {
                foreach ($result as $unidades) {
                    $cliente_id = $unidades->cliente_id;
                }
                $_SESSION['cliente_id'] = $cliente_id;
                $this->definirUsuarioColaborador();
            }
        }
    }

    public function pegarIdCliente()
    {
        if ($this->getId_usuario() && $this->getContextToken()) {
            // CRIA UMA VARIAVEL E ARMAZENA AHORA ATUAL DO FUSO-HORÁRIO DEFINIDO (BRASÍLIA)
            $timezone = new DateTimeZone('America/Sao_Paulo');
            date_default_timezone_set('America/Sao_Paulo');
            /*$dataLogin = date('d/m/Y H:i:s', time());
            $h = "3"; //HORAS DO FUSO ((BRASÍLIA = -3) COLOCA-SE SEM O SINAL -).
            $hm = $h * 60;
            $ms = $hm * 60;
            //COLOCA-SE O SINAL DO FUSO ((BRASÍLIA = -3) SINAL -) ANTES DO ($ms). DATA
            $gmdata = gmdate("d/m/Y", time() - ($ms));
            //COLOCA-SE O SINAL DO FUSO ((BRASÍLIA = -3) SINAL -) ANTES DO ($ms). HORA
            $gmhora = gmdate("g:i", time() - ($ms));
            $dataLogin = $gmdata . " " . $gmhora;*/
            $dataLogin = date('Y-m-d H:i:s');

            $url = "https://api.develop.clientes.witzler.com.br/api/informacaoCliente?id_cliente=" . $this->getId_usuario();
            $recolhe = file_get_contents($url, false, $this->getContextToken());
            $result = json_decode($recolhe);
            if (isset($result)) {
                foreach ($result as $usuarioCliente) {
                    $nome_usuario_fantasia = $usuarioCliente->nome_fantasia;
                    $tipo_cliente = $usuarioCliente->type_client;
                }
                $nome = $nome_usuario_fantasia;
                $_SESSION['type_client'] = $tipo_cliente;
                
                $ipBanidos = array('187.85.6.63', '177.87.155.166', '177.100.222.84');
                $ipAtual = $_POST['ip'];

                if (!in_array($ipAtual, $ipBanidos)) {
                    $urlRegistraLogin = "https://api.develop.clientes.witzler.com.br/api/loginacesso/registraacesso";
                    $request = '{
                        "nome":"' . $nome . '",
                        "data_login":"' . $dataLogin . '"
                    }';
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $urlRegistraLogin);
                    curl_setopt($curl, CURLOPT_POST, true);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json']);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
                    /**/
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    /**/
                    $result = curl_exec($curl);
                    $err = curl_error($curl);
                }
                if (!$err) {
                    //if ($_SESSION['usuario'] == "adminwitzler" && $_POST['senha'] == "9c=Pv#!=sxEtQBeQ") {
                    if ($this->getUsuario() == "adminwitzler" && $_POST['senha'] == "9c=Pv#!=sxEtQBeQ") {
                        header("Location: ../dashboard-admin/index.php");
                        exit();
                    } else {
                        if (!$this->getUsuarioNovo()) {
                            if (!$this->getUsuarioColaborador()) {
                                header("Location: ../dashboard/index.php");
                            } else {
                                header("Location: ../dashboard/index-colaborador.php");
                            }
                        } else {
                            header("Location: ../migracao/index.php");
                        }
                        exit();
                    }
                }
            }
        }
    }
}
