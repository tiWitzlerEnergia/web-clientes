<?php
class TipoBandeira extends Usuario
{
    // Atributos:
    public $idBandeira;
    public $tipoBandeira;
    public $dataInicio;
    public $mes;
    public $ano;

    // Metodos especiais:
    public function __construct($mes, $ano)
    {
        $this->setMes($mes);
        $this->setAno($ano);
        $this->definirUsuario();
        $this->pegarBandeira();
    }

    /**
     * Get the value of idBandeira
     */
    public function getIdBandeira()
    {
        return $this->idBandeira;
    }

    /**
     * Set the value of idBandeira
     *
     * @return  self
     */
    public function setIdBandeira($idBandeira)
    {
        $this->idBandeira = $idBandeira;

        return $this;
    }

    /**
     * Get the value of tipoBandeira
     */
    public function getTipoBandeira()
    {
        return $this->tipoBandeira;
    }

    /**
     * Set the value of tipoBandeira
     *
     * @return  self
     */
    public function setTipoBandeira($tipoBandeira)
    {
        $this->tipoBandeira = $tipoBandeira;

        return $this;
    }

    /**
     * Get the value of dataInicio
     */
    public function getDataInicio()
    {
        return $this->dataInicio;
    }

    /**
     * Set the value of dataInicio
     *
     * @return  self
     */
    public function setDataInicio($dataInicio)
    {
        $this->dataInicio = $dataInicio;

        return $this;
    }

    /**
     * Get the value of mes
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set the value of mes
     *
     * @return  self
     */
    public function setMes($mes)
    {
        $this->mes = $mes;

        return $this;
    }

    /**
     * Get the value of ano
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * Set the value of ano
     *
     * @return  self
     */
    public function setAno($ano)
    {
        $this->ano = $ano;

        return $this;
    }

    // Metodos publicos:
    public function pegarBandeira()
    {
        $mes = $this->getMes();
        $ano = $this->getAno();
        $url = "https://api.develop.clientes.witzler.com.br/api/bandeiras/$mes/$ano";
        $recolhe = file_get_contents($url, false, $this->getContextToken());
        $resultado = json_decode($recolhe);
        //var_dump($resultado);
        if (isset($resultado)) {
            /*foreach ($resultado as $bandeira) {
                $id_bandeira = $bandeira->id_bandeira;
                $tipo_bandeira = $bandeira->tipo_bandeira;
                $data_inicio = $bandeira->data_inicio;
            }
            $this->setIdBandeira($id_bandeira);
            $this->setTipoBandeira($tipo_bandeira);
            $this->setDataInicio($data_inicio);*/

            $id_bandeira = $resultado->id_bandeira;
            $tipo_bandeira = isset($resultado->tipo_bandeira) ? $resultado->tipo_bandeira : 3;
            $data_inicio = $resultado->data_inicio;
            $this->setIdBandeira($id_bandeira);
            $this->setTipoBandeira($tipo_bandeira);
            $this->setDataInicio($data_inicio);
        } 
        if(is_null($resultado)){
        	//$this->setTipoBandeira(3);
        	$mesatual = $this->getMes();
        	$anoatual = $this->getAno();
        	
        	$mesnovo = $mesatual - 1;
        	$anonovo = $anoatual;
        	if($mesnovo == 0){
        		$mesnovo = 12;
        		$anonovo = $anoatual - 1;
        	}
        	$this->setMes($mesnovo);
        	$this->setAno($anonovo);
        	$this->pegarBandeira();
        }
    }
}
