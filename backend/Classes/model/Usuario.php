<?php
class Usuario
{
    // Atributos:
    protected $usuario;
    protected $id_usuario;
    protected $token;
    protected $contextToken;
    protected $totUnidades;
    protected $usuarioNovo; //Boolean
    protected $usuarioColaborador;   // Boolean
    protected $adminColaborador;   // Boolean

    // Métodos especiais:
    /**
     * Get the value of usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set the value of usuario
     *
     * @return  self
     */
    private function setUsuario($usuario)
    {
        $this->usuario = $this->sanitizarVariavel($usuario);

        return $this;
    }

    /**
     * Get the value of id_usuario
     */
    //protected function getId_usuario()
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * Set the value of id_usuario
     *
     * @return  self
     */
    private function setId_usuario($id_usuario)
    {
        $this->id_usuario = $this->sanitizarVariavel($id_usuario);

        return $this;
    }

    /**
     * Get the value of token
     */
    protected function getToken()
    {
        return $this->token;
    }

    /**
     * Set the value of token
     *
     * @return  self
     */
    private function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get the value of contextToken
     */
    public function getContextToken()
    {
        return $this->contextToken;
    }

    /**
     * Set the value of contextToken
     *
     * @return  self
     */
    private function setContextToken()
    {
        if ($this->getToken()) {
            $context = stream_context_create(array(
                'http' => array(
                    'header' => "Authorization: Bearer " . $this->getToken(),
                ),
            ));
            $this->contextToken = $context;
        }
    }

    /**
     * Get the value of totUnidades
     */
    public function getTotUnidades()
    {
        return $this->totUnidades;
    }

    /**
     * Set the value of totUnidades
     *
     * @return  self
     */
    public function setTotUnidades()
    {
        if ($this->getUsuario() && $this->getContextToken()) {
            $arrIdUnidades = [];
            $url = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=" . $this->getUsuario();
            $recolhe = file_get_contents($url, false, $this->getContextToken());
            $resultado = json_decode($recolhe);
            if (isset($resultado)) {
                foreach ($resultado as $unidade) {
                    $id_unidades = $unidade->id_unidades;

                    array_push($arrIdUnidades, $id_unidades);
                }
                
                $totUnidades = count($arrIdUnidades);

                $this->totUnidades = $totUnidades;
                return $this;
            }
        }
    }

    /**
     * Get the value of usuarioNovo
     */ 
    public function getUsuarioNovo()
    {
        return $this->usuarioNovo;
    }

    /**
     * Set the value of usuarioNovo
     *
     * @return  self
     */ 
    public function setUsuarioNovo()
    {
        $usuarioNovo = !($this->getTotUnidades() > 0);
        $this->usuarioNovo = $usuarioNovo;

        return $this;
    }

    /**
     * Get the value of usuarioColaborador
     */
    public function getUsuarioColaborador()
    {
        return $this->usuarioColaborador;
    }

    /**
     * Set the value of usuarioColaborador
     */
    public function setUsuarioColaborador($usuarioColaborador): self
    {
        $this->usuarioColaborador = $usuarioColaborador;

        return $this;
    }

    /**
     * Get the value of adminColaborador
     */
    public function getAdminColaborador()
    {
        return $this->adminColaborador;
    }

    /**
     * Set the value of adminColaborador
     */
    public function setAdminColaborador($adminColaborador): self
    {
        $this->adminColaborador = $adminColaborador;

        return $this;
    }

    // Métodos publicos e protegidos:
    public function sanitizarVariavel($var)
    {
        $sanitizar = $var;
        $sanitizar = str_replace(' ', '', $sanitizar);    //Tira espaços em branco
        $sanitizar = strip_tags($sanitizar);
        $sanitizar = stripcslashes($sanitizar);

        return $sanitizar;
    }

    public function definirUsuario()
    {
        $this->setUsuario($_SESSION['usuario']);
        $this->setId_usuario($_SESSION['cliente_id']);
        $this->setToken($_SESSION['token']);
        $this->setContextToken();
        $this->setTotUnidades();
        $this->setUsuarioNovo();
        $this->setUsuarioColaborador(false);
    }

    public function definirUsuarioColaborador() {
        $this->setUsuario($_SESSION['usuario']);
        $this->setId_usuario($_SESSION['id_colaborador']);
        //$this->setToken($_SESSION['token']);
        $this->setContextToken();
        $this->setTotUnidades();
        $this->setUsuarioNovo();
        $this->setUsuarioColaborador(true);
        $this->setAdminColaborador($_SESSION['admin_colaborador']);
    }
}
