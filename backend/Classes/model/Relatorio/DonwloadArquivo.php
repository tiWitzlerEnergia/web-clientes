<?php
class DonwloadArquivo extends ResultadosRelatorio
{
    // Atributos
    protected $idPdf;
    protected $urlArquivoPdf;

    // Métodos especiais:
    public function __construct($id, $data)
    {
        $this->definirUsuario();
        $this->setIdUnidadeAtiva($id);
        $this->setDataAtiva($data);
        //$this->pegarPdf();
    }

    /**
     * Get the value of idPdf
     */
    public function getIdPdf()
    {
        return $this->idPdf;
    }

    /**
     * Set the value of idPdf
     *
     * @return  self
     */
    public function setIdPdf($idPdf)
    {
        $this->idPdf = $idPdf;

        return $this;
    }

    /**
     * Get the value of urlArquivoPdf
     */
    public function getUrlArquivoPdf()
    {
        return $this->urlArquivoPdf;
    }

    /**
     * Set the value of urlArquivoPdf
     *
     * @return  self
     */
    public function setUrlArquivoPdf($urlArquivoPdf)
    {
        // Ao definir url, primeiro fazer decrypt:
        if (isset($urlArquivoPdf) && $urlArquivoPdf != "") {
            $prefixSrc = "/var/www/webWitzler1.0/backend/assets/files/clientes/";
            $src = $prefixSrc . $urlArquivoPdf;
            $srcPdf = $urlArquivoPdf;

            //URL API:
            $urlDecryptPdf = "https://api.develop.clientes.witzler.com.br/api/decripta-pdf?arquivo=" . $src;
            $context = stream_context_create(array(
                'http' => array(
                    'header' => "Authorization: Bearer " . $_SESSION['token'],
                ),
            ));
            @$recolheDecryptPdf = file_get_contents($urlDecryptPdf, false, $context);
            @$resultDecryptPdf = $recolheDecryptPdf;
            if (isset($resultDecryptPdf) && !empty($resultDecryptPdf)) {
                // Change pdfs:
                $pdfvalue = substr($srcPdf, 0, -4) . ".decrypt.pdf";

                // echo da rota onde está o arquivo
                $this->urlArquivoPdf =  "https://clientes.witzler.com.br/backend/assets/files/clientes/" . $pdfvalue;
            } else {
                $this->urlArquivoPdf = "";
            }
        } else {
            $this->urlArquivoPdf = "";
        }
        //$this->urlArquivoPdf = "https://clientes.witzler.com.br/backend/assets/files/clientes/" . $urlArquivoPdf;

        return $this;
    }

    // Metodos publicos:
    public function pegarPdf()
    {
        $finalArray = array();
        if ($this->getDataAtiva() && $this->getIdUnidadeAtiva()) {
            $url = "https://api.develop.clientes.witzler.com.br/api/relatorios?data_referencia=" . $this->getDataAtiva() . "&id_unidades=" . $this->getIdUnidadeAtiva();
            $recolhe = file_get_contents($url, false, $this->getContextToken());
            $resultado = json_decode($recolhe);
            if ($resultado) {
                foreach ($resultado as $pdf) {
                    $id = $pdf->id;
                    $caminho = $pdf->caminho;

                    //$this->setIdPdf($id);
                    //$this->setUrlArquivoPdf($caminho);
                    $finalArray[] = array("id" => $id, "caminho" => $caminho);
                }
            }
        }
        return $finalArray;
    }

    public function downloadPdf()
    {
        // Configuramos os headers que serão enviados para o browser
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="relatorio' . $this->getDataAtiva() . '"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($this->getUrlArquivoPdf()));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');

        // Envia o arquivo para o cliente
        readfile($this->getUrlArquivoPdf());
    }
}
