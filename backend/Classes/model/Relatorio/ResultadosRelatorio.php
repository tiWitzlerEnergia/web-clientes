<?php
class ResultadosRelatorio extends Usuario
{
    // Atributos:
    protected $idUnidadeAtiva;          // Recolhida ao criar a classe
    protected $dataAtiva;               // Recolhida ao criar a classe.
    protected $idRelatorio;
    protected $idCliente;
    protected $previa;
    protected $custoEfetivo;
    protected $consumo;
    protected $aclEnergiaContrato;
    protected $aclEnergiaCp;
    protected $aclEnergiaSwap;
    protected $aclFatDist;
    protected $aclCceeLiq;
    protected $aclCceeEncargo;
    protected $aclImposto;
    protected $acrTusd;
    protected $acrImposto;
    protected $acrOutros;
    protected $bandeira;
    protected $imagemAtendente;
    protected $nomeAtendente;
    protected $emailAtendente;

    // Métodos especiais:
    public function __construct($idUnidadeAtiva, $dataAtiva)
    {
        $this->definirUsuario();
        $this->setIdUnidadeAtiva($idUnidadeAtiva);
        $this->setDataAtiva($dataAtiva);
        $this->pegarResultadoRelatorio();
        $this->pegarBandeiraDB();
        $this->dadosAtendente();
    }

    /**
     * Get the value of idRelatorio
     */
    public function getIdRelatorio()
    {
        return $this->idRelatorio;
    }

    /**
     * Set the value of idRelatorio
     *
     * @return  self
     */
    public function setIdRelatorio($idRelatorio)
    {
        $this->idRelatorio = $idRelatorio;

        return $this;
    }

    /**
     * Get the value of idUnidadeAtiva
     */
    public function getIdUnidadeAtiva()
    {
        return $this->idUnidadeAtiva;
    }

    /**
     * Set the value of idUnidadeAtiva
     *
     * @return  self
     */
    public function setIdUnidadeAtiva($idUnidadeAtiva)
    {
        $this->idUnidadeAtiva = $idUnidadeAtiva;

        return $this;
    }

    /**
     * Get the value of dataAtiva
     */
    public function getDataAtiva()
    {
        return $this->dataAtiva;
    }

    /**
     * Set the value of dataAtiva
     *
     * @return  self
     */
    public function setDataAtiva($dataAtiva)
    {
        $this->dataAtiva = $dataAtiva;

        return $this;
    }

    /**
     * Get the value of idCliente
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set the value of idCliente
     *
     * @return  self
     */
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get the value of previa
     */
    public function getPrevia()
    {
        return $this->previa;
    }

    /**
     * Set the value of previa
     *
     * @return  self
     */
    public function setPrevia($previa)
    {
        $this->previa = $previa;

        return $this;
    }

    /**
     * Get the value of custoEfetivo
     */
    public function getCustoEfetivo()
    {
        return $this->custoEfetivo;
    }

    /**
     * Set the value of custoEfetivo
     *
     * @return  self
     */
    public function setCustoEfetivo($custoEfetivo)
    {
        $this->custoEfetivo = $custoEfetivo;

        return $this;
    }

    /**
     * Get the value of consumo
     */
    public function getConsumo()
    {
        return $this->consumo;
    }

    /**
     * Set the value of consumo
     *
     * @return  self
     */
    public function setConsumo($consumo)
    {
        $this->consumo = $consumo;

        return $this;
    }

    /**
     * Get the value of aclEnergiaContrato
     */
    public function getAclEnergiaContrato()
    {
        return $this->aclEnergiaContrato;
    }

    /**
     * Set the value of aclEnergiaContrato
     *
     * @return  self
     */
    public function setAclEnergiaContrato($aclEnergiaContrato)
    {
        $this->aclEnergiaContrato = $aclEnergiaContrato;

        return $this;
    }

    /**
     * Get the value of aclEnergiaCp
     */
    public function getAclEnergiaCp()
    {
        return $this->aclEnergiaCp;
    }

    /**
     * Set the value of aclEnergiaCp
     *
     * @return  self
     */
    public function setAclEnergiaCp($aclEnergiaCp)
    {
        $this->aclEnergiaCp = $aclEnergiaCp;

        return $this;
    }

    /**
     * Get the value of aclEnergiaSwap
     */
    public function getAclEnergiaSwap()
    {
        return $this->aclEnergiaSwap;
    }

    /**
     * Set the value of aclEnergiaSwap
     *
     * @return  self
     */
    public function setAclEnergiaSwap($aclEnergiaSwap)
    {
        $this->aclEnergiaSwap = $aclEnergiaSwap;

        return $this;
    }

    /**
     * Get the value of aclFatDist
     */
    public function getAclFatDist()
    {
        return $this->aclFatDist;
    }

    /**
     * Set the value of aclFatDist
     *
     * @return  self
     */
    public function setAclFatDist($aclFatDist)
    {
        $this->aclFatDist = $aclFatDist;

        return $this;
    }

    /**
     * Get the value of aclCceeLiq
     */
    public function getAclCceeLiq()
    {
        return $this->aclCceeLiq;
    }

    /**
     * Set the value of aclCceeLiq
     *
     * @return  self
     */
    public function setAclCceeLiq($aclCceeLiq)
    {
        $this->aclCceeLiq = $aclCceeLiq;

        return $this;
    }

    /**
     * Get the value of aclCceeEncargo
     */
    public function getAclCceeEncargo()
    {
        return $this->aclCceeEncargo;
    }

    /**
     * Set the value of aclCceeEncargo
     *
     * @return  self
     */
    public function setAclCceeEncargo($aclCceeEncargo)
    {
        $this->aclCceeEncargo = $aclCceeEncargo;

        return $this;
    }

    /**
     * Get the value of aclImposto
     */
    public function getAclImposto()
    {
        return $this->aclImposto;
    }

    /**
     * Set the value of aclImposto
     *
     * @return  self
     */
    public function setAclImposto($aclImposto)
    {
        $this->aclImposto = $aclImposto;

        return $this;
    }

    /**
     * Get the value of acrTusd
     */
    public function getAcrTusd()
    {
        return $this->acrTusd;
    }

    /**
     * Set the value of acrTusd
     *
     * @return  self
     */
    public function setAcrTusd($acrTusd)
    {
        $this->acrTusd = $acrTusd;

        return $this;
    }

    /**
     * Get the value of acrImposto
     */
    public function getAcrImposto()
    {
        return $this->acrImposto;
    }

    /**
     * Set the value of acrImposto
     *
     * @return  self
     */
    public function setAcrImposto($acrImposto)
    {
        $this->acrImposto = $acrImposto;

        return $this;
    }

    /**
     * Get the value of acrOutros
     */
    public function getAcrOutros()
    {
        return $this->acrOutros;
    }

    /**
     * Set the value of acrOutros
     *
     * @return  self
     */
    public function setAcrOutros($acrOutros)
    {
        $this->acrOutros = $acrOutros;

        return $this;
    }

    /**
     * Get the value of bandeira
     */
    public function getBandeira()
    {
        return $this->bandeira;
    }

    /**
     * Set the value of bandeira
     *
     * @return  self
     */
    public function setBandeira($bandeira)
    {
        $this->bandeira = $bandeira;

        return $this;
    }

    /**
     * Get the value of imagemAtendente
     */
    public function getImagemAtendente()
    {
        return $this->imagemAtendente;
    }

    /**
     * Set the value of imagemAtendente
     *
     * @return  self
     */
    public function setImagemAtendente($imagemAtendente)
    {
        $this->imagemAtendente = "https://clientes.witzler.com.br/backend/assets/img/collaborator/" . $imagemAtendente . ".jpg";

        return $this;
    }

    /**
     * Get the value of nomeAtendente
     */
    public function getNomeAtendente()
    {
        return $this->nomeAtendente;
    }

    /**
     * Set the value of nomeAtendente
     *
     * @return  self
     */
    public function setNomeAtendente($nomeAtendente)
    {
        $this->nomeAtendente = $nomeAtendente;

        return $this;
    }

    /**
     * Get the value of emailAtendente
     */
    public function getEmailAtendente()
    {
        return $this->emailAtendente;
    }

    /**
     * Set the value of emailAtendente
     *
     * @return  self
     */
    public function setEmailAtendente($emailAtendente)
    {
        $this->emailAtendente = $emailAtendente;

        return $this;
    }

    // Métodos publicos:
    public function pegarResultadoRelatorio()
    {
        $url = "https://api.develop.clientes.witzler.com.br/api/relatorioResumo?data_ref=" . $this->getDataAtiva() . "&id_unid=" . $this->getIdUnidadeAtiva();
        $recolhe = file_get_contents($url, false, $this->getContextToken());
        $resultado = json_decode($recolhe);
        if (isset($resultado)) {
            foreach ($resultado as $relatorio) {
                $idRelatorio = $relatorio->id;
                $id_cliente = $relatorio->id_cliente;
                $previa = $relatorio->previa;
                $custo_efetivo = $relatorio->custo_efetivo;
                $consumo = $relatorio->consumo;
                $acl_energia_contrato = $relatorio->acl_energia_contrato;
                $acl_energia_cp = $relatorio->acl_energia_cp;
                $acl_energia_swap = $relatorio->acl_energia_swap;
                $acl_fat_dist = $relatorio->acl_fat_dist;
                $acl_ccee_liq = $relatorio->acl_ccee_liq;
                $acl_ccee_encargo = $relatorio->acl_ccee_encargo;
                $acl_imposto = $relatorio->acl_imposto;
                $acr_tusd = $relatorio->acr_tusd;
                //$acr_imposto = $relatorio->acr_imposto; (anterior)
                $acr_imposto = $relatorio->acr_energia;
                $acr_outros = $relatorio->acr_outros;

                // Atualiza valores da classe::
                $this->setIdRelatorio($idRelatorio);
                $this->setIdCliente($id_cliente);
                $this->setPrevia($previa);
                $this->setCustoEfetivo($custo_efetivo);
                $this->setConsumo($consumo);
                $this->setAclEnergiaContrato($acl_energia_contrato);
                $this->setAclEnergiaCp($acl_energia_cp);
                $this->setAclEnergiaSwap($acl_energia_swap);
                $this->setAclFatDist($acl_fat_dist);
                $this->setAclCceeLiq($acl_ccee_liq);
                $this->setAclCceeEncargo($acl_ccee_encargo);
                $this->setAclImposto($acl_imposto);
                $this->setAcrTusd($acr_tusd);
                $this->setAcrImposto($acr_imposto);
                $this->setAcrOutros($acr_outros);
            }
        }
    }

    public function pegarBandeiraDB()
    {
        $conn_string = "host=192.168.11.160 port=5432 dbname=postgres user=apirole password=W1tzl3r3n3rg1@!#";
        $conn = pg_connect($conn_string);
        if (!$conn) {
            echo "ERROR";
            exit;
        }
        $result = pg_query($conn, "select * from pc.web_bandeiras order by id_bandeira desc");
        if (!$result) {
            echo "ERROR in result!";
            exit;
        }
        $row = pg_fetch_assoc($result);
        $tipoBandeira = $row['tipo_bandeira'];
        if ($tipoBandeira == 0) {
            $valor = '<div class="col-4" style="align-self: center;"><img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERDE/GREEN/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERDE_24 px BLUE.svg" class="centro"></div><div class="col-8" style="align-self: center;"><p class="atendente mb-0">BANDEIRA TARIFÁRIA</p><H3><b>Verde</b></H3></div>                  <span style="font-size: 19px; color: green; font-weight: 900;">VER. <img src="" height=40></span>';
        } elseif ($tipoBandeira == 1) {
            $valor = '<div class="col-4" style="align-self: center;"><img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_AMARELA/YELLOW/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_AMARELA_24 px BLUE.svg" class="centro"></div><div class="col-8" style="align-self: center;"><p class="atendente mb-0">BANDEIRA TARIFÁRIA</p><H3><b>Amarela</b></H3></div>';
        } elseif ($tipoBandeira == 2) {
            $valor = '<div class="col-4" style="align-self: center;"><img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERMELHA/RED/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS VERMELHA_24 px BLUE.svg" class="centro"></div><div class="col-8" style="align-self: center;"><p class="atendente mb-0">BANDEIRA TARIFÁRIA</p><H3><b>Vermelha</b></H3></div>';
        } else {
            $valor = '<div class="col-4" style="align-self: center;"><img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERMELHA/RED/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS VERMELHA_24 px BLUE.svg" class="centro"></div><div class="col-8" style="align-self: center;"><p class="atendente mb-0">BANDEIRA TARIFÁRIA</p><H3><b>Vermelha</b></H3></div>';
        }

        $this->setBandeira($valor);
    }

    public function dadosAtendente()
    {
        $url = "https://api.develop.clientes.witzler.com.br/api/atendenteTecnico?username=" . $this->getUsuario();
        $recolhe = file_get_contents($url, false, $this->getContextToken());
        $resultado = json_decode($recolhe);
        if (isset($resultado)) {
            foreach ($resultado as $atendente) {
                //Recolhendo todos os dados casso sejam utilizados: (APENAS OS QUE TEM * ESTAO SENDO UTILIZADOS)
                $id_colaborador = $atendente->id_colaborador;
                $nome_colaborador = $atendente->nome_colaborador;
                $email = $atendente->email;
                $telefone_1 = $atendente->telefone_1;
                $telefone_2 = $atendente->telefone_2;
                $path_img = $atendente->path_img;
            }

            $this->setNomeAtendente($nome_colaborador);
            $this->setImagemAtendente($path_img);
            $this->setEmailAtendente($email);
        }
    }
}
