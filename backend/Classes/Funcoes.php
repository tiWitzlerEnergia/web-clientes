<?php
// Define functions gerais:
function get_client_ip()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } else if (isset($_SERVER['REMOTE_ADDR'])) {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    } else {
        $ipaddress = 'UNKNOWN';
    }
    return $ipaddress;
}

function existValue($value, $undefined = "")
{
    $validate = (isset($value) && $value != null) ? $value : $undefined;
    return validateValue($validate);
}

function validateValue($value)
{
    $value = stripcslashes($value);
    $value = strip_tags($value);

    return $value;
}

function verificaEmail($email)
{

    /* Verifica se o email e valido */
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

        /* Obtem o dominio do email */
        list($usuario, $dominio) = explode("@", $email);

        /* Faz um verificacao de DNS no dominio */
        if (checkdnsrr($dominio, "MX") == 1) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function formataData($value, $format = "d/m/Y")
{
    $final = date($format, strtotime($value));
    return $final;
}

function sanitizarVarString($value)
{
    $newvalue = htmlentities($value, ENT_QUOTES, 'UTF-8');
    $newvalue = htmlspecialchars($newvalue);
    $newvalue = stripslashes($newvalue);
    $newvalue = trim($newvalue);
    $newvalue = filter_var($newvalue, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

    return $newvalue;
}

function sanitizarVarInt($value)
{
    if (is_numeric($value)) {
        $newvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
        return $newvalue;
    } else {
        return "";
    }
}

function sanitizarVarDouble($value)
{
    if (is_numeric($value)) {
        $newvalue = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        return $newvalue;
    } else {
        return "";
    }
}

function sendCurl($url, $data, $post = false, $token = "")
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, $post);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    $response = curl_exec($curl);
    curl_close($curl);
    return json_decode($response);
}

function fileGetCont($url, $data = "", $token = "")
{
    $json = file_get_contents($url);
    $values = json_decode($json);
    return $values;
}

function getUserBrowser()
{
    $finalvalues = array("browserDetect" => "", "systemDetect" => "", "systemNameDetected" => "");
    $navegador = $_SERVER['HTTP_USER_AGENT'];

    if (isset($navegador) && $navegador != "") {
        $navegador_search = urlencode($navegador);
        $url = "http://www.useragentstring.com/?uas=$navegador_search&getJSON=all";
        $values = fileGetCont($url);

        //http://www.useragentstring.com/pages/api.php

        $verf = filter_input(INPUT_SERVER, "HTTP_USER_AGENT", FILTER_DEFAULT);
        if (strpos($verf, 'Edge') || strpos($verf, 'Edg')) {
            $browserDetect = 'Edge';
        } elseif (strpos($verf, 'OPR')) {
            $browserDetect = 'Opera';
        } else {
            $browserDetect = isset($values->agent_name) && $values->agent_name != "" ? $values->agent_name : "Desconhecido";
        }

        // Use the function
        if (isMobile()) {
            // Do something for only mobile users
            $dispositivo = isset($values->os_name) && $values->os_name != "" ? $values->os_name : "Mobile desconhecido";
        } else {
            // Do something for only desktop users
            $dispositivo = "Desktop";
        }

        $systemDetect = isset($values->os_type) && $values->os_type != "" ? $values->os_type : "Desconhecido";
        $systemNameDetected = isset($values->os_name) && $values->os_name != "" ? $values->os_name : "Desconhecido";

        //var_dump($values);
        $finalvalues = array("browserDetect" => $browserDetect, "systemDetect" => $systemDetect, "systemNameDetected" => $systemNameDetected, "dispositivo" => $dispositivo);
    }

    return $finalvalues;
}

function isMobile()
{
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

function arrayToXml($array, &$xml){
    foreach ($array as $key => $value) {
        if(is_int($key)){
            $key = "e";
        }
        if(is_array($value)){
            $label = $xml->addChild($key);
            arrayToXml($value, $label);
        }
        else {
            $xml->addChild($key, $value);
        }
    }

    return $xml;
}

