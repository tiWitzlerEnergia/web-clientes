<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//Config para datas
setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

//Verifica se existe usuario para chamar na api (RECOLHENDO NOME E MEDIDOR):
if(isset($_SESSION['usuario']) && isset($_SESSION['token'])){
    $usuario = $_SESSION['usuario'];

    //Cria arrays para armazenar dados:
    $arrayNomeUnidade = [];
    $arrayMedidorUnidade = [];
    $arrayIdUnidade = [];
    $arrayDistribuidoraId = [];
    $arrayUcUnidade = [];
    //Requisiçao pra api:
    $urlUnidades = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=".$usuario;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    $recolheUnidades = file_get_contents($urlUnidades, false, $context);
    $resultUnidades = json_decode($recolheUnidades);
    if(isset($resultUnidades)){
        foreach($resultUnidades as $unidades){
            $nomeUnidade = $unidades->nome;
            $idUnidade = $unidades->id_unidades;
            $medidorUnidade = $unidades->medidor;
            $distribuidoraUnidade = $unidades->distribuidora;
            $valor_uc = $unidades->uc_unidade;

            //Adiciona nos arrays:
            array_push($arrayNomeUnidade, $nomeUnidade);
            array_push($arrayMedidorUnidade, $medidorUnidade);
            array_push($arrayIdUnidade, $idUnidade);
            array_push($arrayDistribuidoraId, $distribuidoraUnidade);
            array_push($arrayUcUnidade, $valor_uc);
        }
    }

    //Pegar nomes de distribuidora:
    //Definindo dados necessarios pra requisiçao:
    $tamanhoDistribuidora = count($arrayDistribuidoraId);
    //Array para armazenar nomes:
    $arrayNomeDistribuidora = [];
    $arrayDescricaoDistribuidora = [];
    
    //Começa ciclo para pegar de todas as id geradas em unidades:
    for($i = 0; $i < $tamanhoDistribuidora; $i++){
        //Requisiçao pra api por id:
        $urlDistribuidora = "https://api.develop.clientes.witzler.com.br/api/distribuidora?id_distribuidora=".$arrayDistribuidoraId[$i];
        $recolheDistribuidora = file_get_contents($urlDistribuidora, false, $context);
        $resultDistribuidora = json_decode($recolheDistribuidora);
        if(isset($resultDistribuidora)){
            foreach($resultDistribuidora as $distribuidora){
                $nomeDistribuidora = $distribuidora->nome_distribuidora;
                $descricaoDistribuidora = $distribuidora->descricao_distribuidora;
                //Adiciona nos array:
                array_push($arrayNomeDistribuidora, $nomeDistribuidora);
                array_push($arrayDescricaoDistribuidora, $descricaoDistribuidora);
            }
        }
    }


    //Pegar consumo (mes anterior e mes atual):
    $tamanhoIdUnidades = count($arrayIdUnidade);
    //var_dump($arrayIdUnidade);
    //Mes atual:
    $arrayTotalAcumuladoMensalAtual = [];
    $comecoMesAtual = date('Y-m-01');
    $fimMesAtual = date('Y-m-t');
    for($i = 0; $i < $tamanhoIdUnidades; $i++){
        $urlAcumuladoMensalAtual = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo/fora/ponta?dataFinal=".$fimMesAtual."&dataInicial=".$comecoMesAtual."&unidade_id=".$arrayIdUnidade[$i];
        $recolheAcumuladoMensalAtual = file_get_contents($urlAcumuladoMensalAtual, false, $context);
        $resultAcumuladoMensalAtual = json_decode($recolheAcumuladoMensalAtual);
        if(isset($resultAcumuladoMensalAtual)){
            $acumuladoForaPontaAtual = $resultAcumuladoMensalAtual->foraPonta;
            $acumuladoPontaAtual = $resultAcumuladoMensalAtual->ponta; 
            $totalAcumuladoMensalAtual = $acumuladoForaPontaAtual+$acumuladoPontaAtual;
            $totalAcumuladoMensalAtual = number_format($totalAcumuladoMensalAtual, 2, ',', '');
            array_push($arrayTotalAcumuladoMensalAtual, $totalAcumuladoMensalAtual);
        }
    }
    //Mes anterior:
    $arrayTotalAcumuladoMensalAnterior = [];
    $comecoMesAnterior = date('Y-m-d', strtotime('-1 months', strtotime(date('Y-m-01'))));
    $fimMesAnterior = date('Y-m-d', strtotime('-1 months', strtotime(date('Y-m-t'))));
    for($i = 0; $i < $tamanhoIdUnidades; $i++){
        $urlAcumuladoMensalAnterior = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo/fora/ponta?dataFinal=".$fimMesAnterior."&dataInicial=".$comecoMesAnterior."&unidade_id=".$arrayIdUnidade[$i];
        $recolheAcumuladoMensalAnterior = file_get_contents($urlAcumuladoMensalAnterior, false, $context);
        $resultAcumuladoMensalAnterior = json_decode($recolheAcumuladoMensalAnterior);
        if(isset($resultAcumuladoMensalAnterior)){
            $acumuladoForaPontaAnterior = $resultAcumuladoMensalAnterior->foraPonta;
            $acumuladoPontaAnterior = $resultAcumuladoMensalAnterior->ponta;
            $totalAcumuladoMensalAnterior = $acumuladoForaPontaAnterior+$acumuladoPontaAnterior;
            $totalAcumuladoMensalAnterior = number_format($totalAcumuladoMensalAnterior, 2, ',', '');
            array_push($arrayTotalAcumuladoMensalAnterior, $totalAcumuladoMensalAnterior);
        }
    }



    //Pegar valores de demanda (mes anterior e mes atual) - Recicla variaveis de consumo:

    
    for($i = 0; $i < $tamanhoIdUnidades; $i++){
        $arrayDemandaForaPonta[$i] = [];
        $arrayDemandaPonta[$i] = [];
        $arrayDemandaDatas[$i] = [];
        $urlDemanda = "https://api.develop.clientes.witzler.com.br/api/unidade/demanda?unidade_id=".$arrayIdUnidade[$i];
        $recolheDemanda = file_get_contents($urlDemanda, false, $context);
        $resultDemanda = json_decode($recolheDemanda);
        if(isset($resultDemanda)){
            foreach($resultDemanda as $demanda){
                $demandaMes = $demanda->mes_ref;
                $demandaPonta = $demanda->demanda_ponta;
                $demandaForaPonta = $demanda->demanda_fora_ponta;
                
                //Formata valores:
                $demandaMes = substr($demandaMes, 0, 10);
                if(isset($demandaPonta)){
                    $demandaPonta = $demandaPonta."(P)";
                }else{
                    $demandaPonta = "0(P)";
                }
                if(isset($demandaForaPonta)){
                    $demandaForaPonta = $demandaForaPonta."(FP)";
                }else{
                    $demandaForaPonta = "0(FP)";
                }

                array_push($arrayDemandaDatas[$i], $demandaMes);
                array_push($arrayDemandaPonta[$i], $demandaPonta);
                array_push($arrayDemandaForaPonta[$i], $demandaForaPonta);
            }
        }
        if($resultDemanda == []){
            $arrayDemandaPonta[$i] = ["0(P)", "0(P)"];
            $arrayDemandaForaPonta[$i] = ["0(FP)", "0(FP)"];
        }
    }
}
?>