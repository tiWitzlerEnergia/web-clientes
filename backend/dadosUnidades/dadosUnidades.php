<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<select id="idUnidades" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="updateUnidades()">
<?php
if(isset($meus_arquivos) && $meus_arquivos == 1){
?>
    <option class="dropdown-item" selected disabled>Selecione uma unidade...</option>
<?php
}
        //Array para armazenar id:
        $arrayIdOption = [];
        //Config para requisitar da api
        $urlUnidades = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=".$_SESSION['usuario'];
        $context = stream_context_create(array(
            'http' => array(
                'header' => "Authorization: Bearer ".$_SESSION['token'],
            ),
        ));
        $recolheUnidades = file_get_contents($urlUnidades, false, $context);
        $resultUnidades = json_decode($recolheUnidades);
            foreach($resultUnidades as $unidades){
                //$cliente_id = $unidades->cliente_id;
                $id_option = $unidades->id_unidades;
?>
                <option id="<?php echo $id_option; ?>" value="<?php echo $unidades->nome; ?>" class="dropdown-item"><?php echo $unidades->nome; ?></option>                
<?php
                //array push:
                array_push($arrayIdOption, $id_option);
            }
        //$_SESSION['cliente_id'] = $cliente_id;
?>
</select>
    <script type="text/javascript">
        //Configurando tempo de vida da cookie ao executar funçao:
        var tempoCookieLoad = new Date();
        tempoCookieLoad.setTime(tempoCookieLoad.getTime() + 1*3600*1000);
        var pegarID = $("#idUnidades option").attr('id');
        document.cookie = "id_unidades = "+pegarID+"; expires="+tempoCookieLoad.toUTCString();


        function updateUnidades() {
            //Configurando tempo de vida da cookie ao executar funçao:
            var tempoCookie = new Date();
            tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

            //Definindo valores da funçao:
            var selectUnidades = document.getElementById('idUnidades');
            var optionUnidades = selectUnidades.options[selectUnidades.selectedIndex];
            var recojeUnidades = selectUnidades.options[selectUnidades.selectedIndex].id;
            console.log(recojeUnidades);
            document.cookie = "id_unidades = "+recojeUnidades+"; expires="+tempoCookie.toUTCString();
        }
        updateUnidades();
    </script>