<?php
if (isset($_SESSION['usuario'])) {
    $usuario = $_SESSION['usuario'];

    //Cria arrays para armazenar dados:
    $arrayNomeUnidade = [];
    $arrayIdUnidade = [];
    $arrayContainer = [];

    //Requisiçao pra api:
    $urlUnidades = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=" . $usuario;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    $recolheUnidades = file_get_contents($urlUnidades, false, $context);
    $resultUnidades = json_decode($recolheUnidades);
    if (isset($resultUnidades)) {
        foreach ($resultUnidades as $unidades) {
            $nomeUnidade = $unidades->nome;
            $idUnidade = $unidades->id_unidades;

            //Adiciona nos arrays:
            array_push($arrayContainer, $nomeUnidade . "/" . $idUnidade);
            //array_push($arrayNomeUnidade, $nomeUnidade);
            //array_push($arrayIdUnidade, $idUnidade);
        }
    } else {
        // caso onde nao consegue pegar id.
    }
    sort($arrayContainer);
    //print_r($arrayContainer);
    $contagem = count($arrayContainer);
    for($i = 0; $i < $contagem; $i++){
        $novoContainer = explode("/", $arrayContainer[$i]);
        $nomeUnidadeOrdenado = $novoContainer[0];
        $idUnidadeOrdenado = $novoContainer[1];

        array_push($arrayNomeUnidade, $nomeUnidadeOrdenado);
        array_push($arrayIdUnidade, $idUnidadeOrdenado);
    }

    $totalUnidades = count($arrayIdUnidade);
    $arrValorEconomizado12Meses = [];
    $arrMedPerEconomizado12Meses = [];
    if ($totalUnidades >= 0) {
        for ($i = 0; $i < $totalUnidades; $i++) {
            $arrValorEconomizado[$i] = [];    // separa os valores economizados por unidade em ordem de chegada.
            $arrDataEconomizado[$i] = [];
            $arrPorcentagemEconomia[$i] = [];

            $urlValorEconomia[$i] = "https://api.develop.clientes.witzler.com.br/api/listando/economias/unidade?id_unidades=" . $arrayIdUnidade[$i];
            @$recolheValorEconomia[$i] = file_get_contents($urlValorEconomia[$i], false, $context);
            @$resultValorEconomia[$i] = json_decode($recolheValorEconomia[$i]);
            if (isset($resultValorEconomia[$i])) {
                foreach ($resultValorEconomia[$i] as $valorEconomiaUnidade) {
                    $valor_economizado = $valorEconomiaUnidade->valor_economizado;
                    $data_referencia = $valorEconomiaUnidade->data_referencia;
                    $porcentagem_economia = $valorEconomiaUnidade->porcentagem_economia;

                    array_push($arrValorEconomizado[$i], $valor_economizado);
                    array_push($arrDataEconomizado[$i], $data_referencia);
                    array_push($arrPorcentagemEconomia[$i], $porcentagem_economia);
                }
            }
        }

        // Pega soma dos 12 meses:
        for ($i = 0; $i < $totalUnidades; $i++) {
            $somaUnidade12Meses = 0;
            $somaPer12Meses = 0;
            $totalValoresPer = count($arrPorcentagemEconomia[$i]);

            if ($totalValoresPer >= 12) {
                for ($j = 1; $j <= 12; $j++) {
                    $index = $totalValoresPer - $j;
                    $somaUnidade12Meses += $arrValorEconomizado[$i][$index];
                    $somaPer12Meses += $arrPorcentagemEconomia[$i][$index];
                    //echo $arrDataEconomizado[$i][$index]."<br/>";
                }
                $medPer12Meses = $somaPer12Meses / 12;
            } else {
                //Caso onde são menores que 12:
                for ($j = 0; $j < $totalValoresPer; $j++) {
                    $somaUnidade12Meses += $arrValorEconomizado[$i][$j];
                    $somaPer12Meses += $arrPorcentagemEconomia[$i][$j];
                }
                $medPer12Meses = $somaPer12Meses / $totalValoresPer;
            }
            array_push($arrValorEconomizado12Meses, $somaUnidade12Meses);
            array_push($arrMedPerEconomizado12Meses, $medPer12Meses);
        }
        $valorTotalEconomizado12Meses = array_sum($arrValorEconomizado12Meses);
        $medTotalPerEconomia12Meses = (array_sum($arrMedPerEconomizado12Meses) / count($arrMedPerEconomizado12Meses));

        // Valores de todos os meses retornados da API:
        $somaValorEconomizadoTodasUnidades = 0;
        $arrMedPerEconomiaUnidade = [];
        for ($i = 0; $i < $totalUnidades; $i++) {
            $somaValorEconomizadoTodasUnidades += array_sum($arrValorEconomizado[$i]);

            // Soma de percentagem:
            $valorMedPercentagem = (array_sum($arrPorcentagemEconomia[$i]) / count($arrPorcentagemEconomia[$i]));
            array_push($arrMedPerEconomiaUnidade, $valorMedPercentagem);
        }
        $medTotalPerEconomia = (array_sum($arrMedPerEconomiaUnidade) / count($arrMedPerEconomiaUnidade));
    } else {
        // Retornar mensagem que nao tem unidades.
    }
}
