<select id="idRegiao" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="updatePld()">
    <option id="1" value="Região Suldeste/Centro-Oeste" class="dropdown-item">Região Suldeste/Centro-Oeste</option>
    <option id="2" value="Região Sul" class="dropdown-item">Região Sul</option>
    <option id="3" value="Região Nordeste" class="dropdown-item">Região Nordeste</option>
    <option id="4" value="Região Norte" class="dropdown-item">Região Norte</option>
</select>
<script type="text/javascript">
    //Configurando tempo de vida da cookie ao executar funçao:
    var tempoCookieLoad = new Date();
    tempoCookieLoad.setTime(tempoCookieLoad.getTime() + 1 * 3600 * 1000);
    var pegarPld = $("#idRegiao option").attr('id');
    document.cookie = "idPld = " + pegarPld + "; expires=" + tempoCookieLoad.toUTCString();


    function updatePld() {
        //Configurando tempo de vida da cookie ao executar funçao:
        var tempoCookie = new Date();
        tempoCookie.setTime(tempoCookie.getTime() + 1 * 3600 * 1000);

        //Definindo valores da funçao:
        var selectPld = document.getElementById('idRegiao');
        var optionPld = selectPld.options[selectPld.selectedIndex];
        var recojePld = selectPld.options[selectPld.selectedIndex].id;
        console.log(recojePld);
        document.cookie = "idPld = " + recojePld + "; expires=" + tempoCookie.toUTCString();
    }
    updatePld();
</script>