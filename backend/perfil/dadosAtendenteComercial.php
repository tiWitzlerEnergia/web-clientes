<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
if(isset($_SESSION['usuario'])){
    $urlDadosAtendenteComercial = "https://api.develop.clientes.witzler.com.br/api/atendenteComercial?username=".$_SESSION['usuario'];
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheDadosAtendenteComercial =file_get_contents($urlDadosAtendenteComercial, false, $context);
    @$resultDadosAtendenteComercial = json_decode($recolheDadosAtendenteComercial);
    if(isset($resultDadosAtendenteComercial)){
        foreach($resultDadosAtendenteComercial as $dadosAtendenteComercial){
            //Recolhendo todos os dados casso sejam utilizados: (APENAS OS QUE TEM * ESTAO SENDO UTILIZADOS)
            $idAtendenteComercial = $dadosAtendenteComercial->id_colaborador;
            $nomeAtendenteComercial = $dadosAtendenteComercial->nome_colaborador;   //*
            $emailAtendenteComercial = $dadosAtendenteComercial->email;
            $telefoneAtendenteComercial = $dadosAtendenteComercial->telefone_1;     //*
            $telefoneAtendenteComercial2 = $dadosAtendenteComercial->telefone_2;    //*
            $imagemAtendenteComercial = $dadosAtendenteComercial->path_img;      //*
        }
        $urlImagemAtendenteComercial = "https://clientes.witzler.com.br/backend/assets/img/collaborator/".$imagemAtendenteComercial.".jpg";
    }else{
        echo "Erro";
    }
}
?>