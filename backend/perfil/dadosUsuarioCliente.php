<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
if(isset($_SESSION['cliente_id'])){
    $urlUsuarioCliente = "https://api.develop.clientes.witzler.com.br/api/informacaoCliente?id_cliente=".$_SESSION['cliente_id'];
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheUsuarioCliente = file_get_contents($urlUsuarioCliente, false, $context);
    @$resultUsuarioCliente = json_decode($recolheUsuarioCliente);
    if(isset($resultUsuarioCliente)){
        foreach($resultUsuarioCliente as $usuarioCliente){
            $nome_usuario_fantasia = $usuarioCliente->nome_fantasia;
        }
        $urlLogoUsuario = "https://clientes.witzler.com.br/backend/assets/img/client/".$_SESSION['cliente_id'].".jpg";
    }else{
        echo "Erro";
    }
}
?>