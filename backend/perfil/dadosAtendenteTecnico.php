<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
if(isset($_SESSION['usuario'])){
    $urlAtendenteTecnico = "https://api.develop.clientes.witzler.com.br/api/atendenteTecnico?username=".$_SESSION['usuario'];
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheAtendenteTecnico = file_get_contents($urlAtendenteTecnico, false, $context);
    @$resultAtendenteTecnico = json_decode($recolheAtendenteTecnico);
    if(isset($resultAtendenteTecnico)){
        foreach($resultAtendenteTecnico as $atendenteTecnico){
            //Recolhendo todos os dados casso sejam utilizados: (APENAS OS QUE TEM * ESTAO SENDO UTILIZADOS)
            $idAtendenteTecnico = $atendenteTecnico->id_colaborador;
            $nomeAtendenteTecnico = $atendenteTecnico->nome_colaborador;    //*
            $emailAtendenteTecnico = $atendenteTecnico->email;
            $telefoneAtendenteTecnico = $atendenteTecnico->telefone_1;      //*
            $telefoneAtendenteTecnico2 = $atendenteTecnico->telefone_2;     //*
            $imagemAtendenteTecnico = $atendenteTecnico->path_img;          //*
        }
        $urlImagemAtendenteTecnico = "https://clientes.witzler.com.br/backend/assets/img/collaborator/".$imagemAtendenteTecnico.".jpg";
    }else{
        echo "Erro";
    }
}
?>