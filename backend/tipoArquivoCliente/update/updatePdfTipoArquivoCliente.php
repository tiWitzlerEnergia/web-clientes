<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php session_start(); ?>

<option class="dropdown-item" selected disabled>Selecione um arquivo...</option>
<?php

//Definindo variaveis pra URL
if (isset($_GET['data_tipo_arquivo_cliente']) && isset($_GET['tipo_arquivo_arquivo_cliente']) && isset($_GET['id_unidades'])) {
    $data_tipo_arquivo_cliente = $_GET['data_tipo_arquivo_cliente'];
    $tipo_arquivo_arquivo_cliente = $_GET['tipo_arquivo_arquivo_cliente'];
    $id_unidades_arquivo_cliente = $_GET['id_unidades'];
    $src = $_GET['src'];
    //URL:
    $urlPdfTipoArquivoCliente = "https://api.develop.clientes.witzler.com.br/api/arquivos/cliente?data_referencia=" . $data_tipo_arquivo_cliente . "&tipo_arquivo=" . $tipo_arquivo_arquivo_cliente . "&unidade_id=" . $id_unidades_arquivo_cliente;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    @$recolhePdfTipoArquivoCliente = file_get_contents($urlPdfTipoArquivoCliente, false, $context);
    @$resultPdfTipoArquivoCliente = json_decode($recolhePdfTipoArquivoCliente);
    if (isset($resultPdfTipoArquivoCliente)) {
        foreach ($resultPdfTipoArquivoCliente as $pdf_tipo_arquivo_cliente) {
            $nome = $pdf_tipo_arquivo_cliente->caminho;
            $nome = explode("/", $nome);
            $ultimo = end($nome);
?>
            <option id="<?php echo $pdf_tipo_arquivo_cliente->id; ?>" value="https://clientes.witzler.com.br/backend/assets/files/clientes/<?php echo $pdf_tipo_arquivo_cliente->caminho; ?>" class="dropdown-item"><?php echo $ultimo; ?></option>
<?php
        }
    }

    // Uma vez terminou de carregar todos os option com seus selects, recolhe a rota que foi escolhida para procurar na api e descriptografar:
    $urlDescriptografarArquivoCliente = "";
}


?>