<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php session_start(); ?>
<option class="dropdown-item" selected disabled>Selecione um tipo...</option>
<?php
if(isset($_GET['id_unidades'])){
    //ARRAY:
    $listaTipoArquivo = [];
    $listaNomes = [];
    //CONFIG PRA PEGAR DADOS DA API:
    $id_unidades = $_GET['id_unidades'];
    $urlArquivoCliente = "https://api.develop.clientes.witzler.com.br/api/tipos/arquivos/cliente?unidade_id=".$id_unidades;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recojeArquivoCliente = file_get_contents($urlArquivoCliente, false, $context);
    @$resultArquivoCliente = json_decode($recojeArquivoCliente);
    if(isset($resultArquivoCliente)){
        foreach($resultArquivoCliente as $arquivoCliente){
            $unidade_id_tipo_arquivo = $arquivoCliente->tipo_arquivo;
            $nome_tipo_arquivo = $arquivoCliente->tipo_arquivo_descricao;
            array_push($listaTipoArquivo, $unidade_id_tipo_arquivo);
            array_push($listaNomes, $nome_tipo_arquivo);
            $listaTipoArquivoFinal = array_unique($listaTipoArquivo);
            $listaNomesFinal =  array_unique($listaNomes);
        }
        $tamanhoLista = count($listaTipoArquivo);
        for($i = 0; $i < $tamanhoLista; $i++){
            if(isset($listaTipoArquivoFinal[$i])){
?>
                <option class="dropdown-item" id="<?php echo $listaTipoArquivoFinal[$i]; ?>" value="<?php echo $listaNomesFinal[$i]; ?>"><?php echo $listaNomesFinal[$i]; ?></option>
<?php
            }
        }
    }
}
?>