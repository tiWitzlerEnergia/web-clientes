<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<select id="idUnidadesArquivoClientes" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="updateUnidadesArquivoCliente()">
<!--<option class="dropdown-item" selected disabled>Selecione uma unidade...</option>-->
    <?php
    if(isset($_SESSION['usuario'])){
        $urlUnidadesArquivoCliente = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=".$_SESSION['usuario'];
        $context = stream_context_create(array(
            'http' => array(
                'header' => "Authorization: Bearer ".$_SESSION['token'],
            ),
        ));
        @$recolheUnidadesArquivoCliente = file_get_contents($urlUnidadesArquivoCliente, false, $context);
        @$resultUnidadesArquivoCliente = json_decode($recolheUnidadesArquivoCliente);
        if(isset($resultUnidadesArquivoCliente)){
            foreach($resultUnidadesArquivoCliente as $unidades_arquivo_cliente){
    ?>
                <option id="<?php echo $unidades_arquivo_cliente->id_unidades; ?>" value="<?php echo $unidades_arquivo_cliente->nome; ?>" class="dropdown-item"><?php echo $unidades_arquivo_cliente->nome; ?></option>                
    <?php
            }
        }
    }
    ?>
</select>
    <script type="text/javascript">
        function updateUnidadesArquivoCliente() {
            //Configurando tempo de vida da cookie ao executar funçao:
            var tempoCookie = new Date();
            tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

            //Definindo valores da funçao:
            var selectUnidadesArquivoClientes = document.getElementById('idUnidadesArquivoClientes');
            var optionUnidades = selectUnidadesArquivoClientes.options[selectUnidadesArquivoClientes.selectedIndex];
            var recojeUnidadesArquivoClientes = selectUnidadesArquivoClientes.options[selectUnidadesArquivoClientes.selectedIndex].id;
            console.log('id arquivo clientes:'+recojeUnidadesArquivoClientes);
            document.cookie = "id_unidades_arquivo_cliente = "+recojeUnidadesArquivoClientes+"; expires="+tempoCookie.toUTCString();
        }

        //updateUnidadesArquivoCliente();
    </script>