<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<select id="diretorioTipoArquivoCliente" name="diretorioTipoArquivoCliente" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="abrir.location = options[selectedIndex].value">
    <option class="dropdown-item" selected disabled>Selecione um arquivo...</option>
<?php
//Definindo variaveis pra URL
if(isset($_COOKIE['data_tipo_arquivo_cliente']) && isset($_COOKIE['tipo_arquivo_arquivo_cliente']) && isset($_COOKIE['id_unidades'])){
    $data_tipo_arquivo_cliente_old = $_COOKIE['data_tipo_arquivo_cliente'];
    $data_tipo_arquivo_cliente = date('Y-m-d', strtotime($data_tipo_arquivo_cliente_old));
    $tipo_arquivo_arquivo_cliente = $_COOKIE['tipo_arquivo_arquivo_cliente'];
    $id_unidades_arquivo_cliente = $_COOKIE['id_unidades'];
    //URL:
    $urlPdfTipoArquivoCliente = "https://api.develop.clientes.witzler.com.br/api/arquivos/?data_referencia=".$data_tipo_arquivo_cliente."&tipo_arquivo=".$tipo_arquivo_arquivo_cliente."&unidade_id=".$id_unidades_arquivo_cliente;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolhePdfTipoArquivoCliente = file_get_contents($urlPdfTipoArquivoCliente, false, $context);
    @$resultPdfTipoArquivoCliente = json_decode($recolhePdfTipoArquivoCliente);
    if(isset($resultPdfTipoArquivoCliente)){
        foreach($resultPdfTipoArquivoCliente as $pdf_tipo_arquivo_cliente){
            $nome = $pdf_tipo_arquivo_cliente->caminho;
            $nome = explode("/", $nome);
            $ultimo = end($nome);
?>
            <option id="<?php echo $pdf_tipo_arquivo_cliente->id; ?>" value="<?php echo $pdf_tipo_arquivo_cliente->caminho; ?>" class="dropdown-item"><?php echo $ultimo; ?></option>
<?php
        }
    }
}
?>
</select>
<script>
    $('#diretorioTipoArquivoCliente').on('change',function(){
        var src = $(this).find('option:selected').val();
        var last_id_unidades = $("#idUnidades").find(':selected').attr('id');
        var last_calendario_meus_arquivos = "01-"+$("#datasUnidade").val();
        var last_tipo_arquivo = $("#tipoArquivoPdf").find(':selected').attr('id');

        //$('#abrir').attr('src', src);
        // Envia o src (gerado pela api em $pdf_tipo_arquivo_cliente->caminho) para descriptografar esse arquivo:
        $.ajax({
            type: 'GET',
            url: '../../../backend/arquivosPdf/descriptografarPdf.php',
            async: true,
            data: {
                'data_unidade':  last_calendario_meus_arquivos,
                'id_unidades': last_id_unidades,
                'tipo_arquivo': last_tipo_arquivo,
                'src': src
            },
            success: function(pdf){
                console.log(pdf);
                $('#abrir').attr('src', pdf);
            }
        });

        $.ajax({
            type: 'GET',
            url: '../../../backend/arquivosPdf/deletePdf.php',
            async: true,
            data: {
                'data_unidade':  last_calendario_meus_arquivos,
                'id_unidades': last_id_unidades,
                'tipo_arquivo': last_tipo_arquivo,
                'src': src
            },
            success: function(resultpdf){
                console.log(resultpdf);
            }
        });

        //Refresh do outro input que gera pdf:
        $("#diretorioArquivo").html("");
        $.ajax({
            type: 'GET',
            url: '../../../backend/arquivosPdf/updateArquivosPdf.php',
            data: {
                'data_unidade':  last_calendario_meus_arquivos,
                'id_unidades': last_id_unidades,
                'tipo_arquivo': last_tipo_arquivo,
            },
            success: function(s){
                $("#diretorioArquivo").html(s);
            }
        });
    });
</script>