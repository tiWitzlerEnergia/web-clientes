<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<select id="tipoArquivoCliente" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="updateTipoArquivoCliente()">
    <option class="dropdown-item" selected disabled>Selecione um tipo...</option>

<?php
if(isset($_COOKIE['id_unidades']) || isset($arrayIdOption[0])){
    //ARRAY:
    $listaTipoArquivo = [];
    $listaNomes = [];
    //CONFIG PRA PEGAR DADOS DA API:
    $id_unidades = $_COOKIE['id_unidades'];
    //recolhendo variaveis uteis:
    if(isset($_COOKIE['id_unidades'])){
        $id_unidades = $_COOKIE['id_unidades'];
    }else{
        $id_unidades = $arrayIdOption[0];
    }
    $urlArquivoCliente = "https://api.develop.clientes.witzler.com.br/api/tipos/arquivos/?unidade_id=".$id_unidades;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recojeArquivoCliente = file_get_contents($urlArquivoCliente, false, $context);
    @$resultArquivoCliente = json_decode($recojeArquivoCliente);
    if(isset($resultArquivoCliente)){
        foreach($resultArquivoCliente as $arquivoCliente){
            $unidade_id_tipo_arquivo = $arquivoCliente->tipo_arquivo;
            $nome_tipo_arquivo = $arquivoCliente->tipo_arquivo_descricao;
            array_push($listaTipoArquivo, $unidade_id_tipo_arquivo);
            array_push($listaNomes, $nome_tipo_arquivo);
            $listaTipoArquivoFinal = array_unique($listaTipoArquivo);
            $listaNomesFinal =  array_unique($listaNomes);

        }
        $tamanhoLista = count($listaTipoArquivo);
        for($i = 0; $i < $tamanhoLista; $i++){
            if(isset($listaTipoArquivoFinal[$i])){
?>
                <option class="dropdown-item" id="<?php echo $listaTipoArquivoFinal[$i]; ?>" value="<?php echo $listaNomesFinal[$i]; ?>"><?php echo $listaNomesFinal[$i]; ?></option>
<?php
            }
        }
    }
}
?>
</select>
<script>
    function updateTipoArquivoCliente() {
            //Configurando tempo de vida da cookie ao executar funçao:
            var tempoCookie = new Date();
            tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

            //Definindo valores da funçao:
            var selectTipoArquivoCliente = document.getElementById('tipoArquivoCliente');
            var optionTipoArquivoCliente = selectTipoArquivoCliente.options[selectTipoArquivoCliente.selectedIndex];
            var recojeTipoArquivoCliente = selectTipoArquivoCliente.options[selectTipoArquivoCliente.selectedIndex].id;
            console.log('id de tipo arquivo cliente:'+recojeTipoArquivoCliente);
            document.cookie = "tipo_arquivo_arquivo_cliente = "+recojeTipoArquivoCliente+"; expires="+tempoCookie.toUTCString();
    }

    alert("<?php var_dump($listaTipoArquivoFinal); ?>");
    //updateTipoArquivoCliente();
</script>