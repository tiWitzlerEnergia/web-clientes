<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<?php
/*if (isset($_COOKIE['id_unidades']) || isset($arrayIdOption[0])) {
    //Lista dados necessarios para pegar dados:
    if (isset($_COOKIE['id_unidades'])) {
        $id_unidade = $_COOKIE['id_unidades'];
    } else {
        $id_unidade = $arrayIdOption[0];
    }

    //Cria var para armazenar dados:
    $totalEconomiaUnidade = 0;

    //Requisiçao pra api:
    $urlListandoEconomiaUnidade = "https://api.develop.clientes.witzler.com.br/api/listando/economias/unidade?id_unidades=" . $id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    @$recolheListandoEconomiaUnidade = file_get_contents($urlListandoEconomiaUnidade, false, $context);
    @$resultListandoEconomiaUnidade = json_decode($recolheListandoEconomiaUnidade);
    if (isset($resultListandoEconomiaUnidade)) {
        foreach ($resultListandoEconomiaUnidade as $listandoEconomia) {
            //Declara as variaveis e depois soma no totalEconomia:
            $valor_economizado = $listandoEconomia->valor_economizado;

            $totalEconomiaUnidade += $valor_economizado;
        }
    }
}else{
    echo "<script>console.log('Erro ao recolher unidade em Listando Economia Unidade.');</script>";
    $totalEconomiaUnidade = 0;
}
$totalEconomiaUnidade = round($totalEconomiaUnidade, 2);
echo "<script>console.log('Economia unidade total: $totalEconomiaUnidade');</script>";*/

if (isset($_SESSION['usuario'])) {
    $usuario = $_SESSION['usuario'];

    //Cria arrays para armazenar dados:
    $arrayNomeUnidade = [];
    $arrayIdUnidade = [];

    //Requisiçao pra api:
    $urlUnidades = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=" . $usuario;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    $recolheUnidades = file_get_contents($urlUnidades, false, $context);
    $resultUnidades = json_decode($recolheUnidades);
    if (isset($resultUnidades)) {
        foreach ($resultUnidades as $unidades) {
            $nomeUnidade = $unidades->nome;
            $idUnidade = $unidades->id_unidades;

            //Adiciona nos arrays:
            array_push($arrayNomeUnidade, $nomeUnidade);
            array_push($arrayIdUnidade, $idUnidade);
        }
    } else {
        // caso onde nao consegue pegar id.
    }

    $totalUnidades = count($arrayIdUnidade);
    $arrSomaValorEconomiaUnidade = [];
    if ($totalUnidades >= 0) {
        for ($i = 0; $i < $totalUnidades; $i++) {
            $totalEconomiaUnidade = 0;

            //Requisiçao pra api para pegar valores de economia:
            $urlListandoEconomiaUnidade[$i] = "https://api.develop.clientes.witzler.com.br/api/listando/economias/unidade?id_unidades=" . $arrayIdUnidade[$i];
            @$recolheListandoEconomiaUnidade[$i] = file_get_contents($urlListandoEconomiaUnidade[$i], false, $context);
            @$resultListandoEconomiaUnidade[$i] = json_decode($recolheListandoEconomiaUnidade[$i]);
            if (isset($resultListandoEconomiaUnidade[$i])) {
                foreach ($resultListandoEconomiaUnidade[$i] as $listandoEconomia) {
                    //Declara as variaveis e depois soma no totalEconomia:
                    $valor_economizado = $listandoEconomia->valor_economizado;

                    $totalEconomiaUnidade += $valor_economizado;
                }
            }
            array_push($arrSomaValorEconomiaUnidade, $totalEconomiaUnidade);
        }
    }
}
