<?php
session_start();

if (isset($_SESSION['usuario'])) {
    $usuario = $_SESSION['usuario'];
    $id_usuario = $_SESSION['cliente_id'];
    $arrayIdUnidade = [];
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    //Requisiçao pra api:
    $urlCo2 = "https://api.develop.clientes.witzler.com.br/api/sustentabilidade?cliente_id=" . $id_usuario;
    @$recolheCo2 = file_get_contents($urlCo2, false, $context);
    @$resultCo2 = json_decode($recolheCo2);
    if (isset($resultCo2)) {
        foreach ($resultCo2 as $co2) {
            $co2_evitado_em_ton = $co2->co2_evitado_em_ton;
            $campos_de_futebol = $co2->campos_de_futebol;
        }


        //echo "<h5 style='color: #0c2338; font-weight: bold; margin-bottom: 0;'>". "Com o mercado livre de energia você deixou de emitir  </h5>";
        //echo "<h5 style='color: #0c2338; font-weight: bold; margin-bottom: 0;'>". number_format($co2_evitado_em_ton, 2, ",", ".") . "Ton de CO2, equivalente a " . number_format($campos_de_futebol, 2, ",", ".") . " campos de futebol </h5>";
        //echo "<h5 style='color: #0c2338; font-weight: bold; margin-bottom: 0;'>"."de árvores plantadas!</h5>";

        /*echo "<h5 style='color: #0c2338; font-weight: bold; margin-bottom: 0;'>" . "Com o mercado livre de energia você deixou de emitir  </h5>";
        echo "<div style='color: #0c2338; font-weight: bold; margin-bottom: 0; font-size: 17.5px; display: flex'><div style='font-weight: bolder;'>" . number_format($co2_evitado_em_ton, 2, ",", ".") . "</div><div>Ton de CO2, equivalente a " . number_format($campos_de_futebol, 2, ",", ".") . " campos de futebol </div></div>";
        echo "<h5 style='color: #0c2338; font-weight: bold; margin-bottom: 0;'>" . "de árvores plantadas!</h5>";*/
        echo "<h5 class='h5Co2'>Com o mercado livre de energia você deixou de emitir ". number_format($co2_evitado_em_ton, 2, ",", ".") ."Ton de CO2, equivalente a ". number_format($campos_de_futebol, 2, ",", ".") ." campos de futebol de árvores plantadas!</h5>";
    }
}
