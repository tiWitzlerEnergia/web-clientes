<?php
session_start();
if (isset($_GET['id_unidades'])) {
    // Pega variaveis:
    $id_unidades = $_GET['id_unidades'];

    // Var que armazena valor e depois vai fazer echo:
    $totalEconomiaUnidade = 0;

    // Requisicao para a API:
    $urlListandoEconomiaUnidade = "https://api.develop.clientes.witzler.com.br/api/listando/economias/unidade?id_unidades=" . $id_unidades;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    @$recolheListandoEconomiaUnidade = file_get_contents($urlListandoEconomiaUnidade, false, $context);
    @$resultListandoEconomiaUnidade = json_decode($recolheListandoEconomiaUnidade);
    if (isset($resultListandoEconomiaUnidade)) {
        foreach ($resultListandoEconomiaUnidade as $economia_unidade) {
            //Declara variaveis e soma no final:
            $valor_economizado = $economia_unidade->valor_economizado;

            $totalEconomiaUnidade += $valor_economizado;
        }
    }
} else {
    $totalEconomiaUnidade = 0;
}
$totalEconomiaUnidade = round($totalEconomiaUnidade, 2);
echo "R$ ".$totalEconomiaUnidade;
