<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php session_start(); ?>
<option class="dropdown-item" selected disabled>Selecione um tipo...</option>
<?php
if(isset($_GET['id_unidades'])){
    $id_unidades = $_GET['id_unidades'];
    $urlTipos = "https://api.develop.clientes.witzler.com.br/api/tipos/arquivos/?unidade_id=".$id_unidades;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheTipos = file_get_contents($urlTipos, false, $context);
    @$resultTipos = json_decode($recolheTipos);
    if(isset($resultTipos)){
        foreach($resultTipos as $Tipos){
?>
            <option id="<?php echo $Tipos->tipo_arquivo; ?>" value="<?php echo $Tipos->tipo_arquivo_descricao; ?>" class="dropdown-item"><?php echo $Tipos->tipo_arquivo_descricao; ?></option>
<?php
        }
    }
}
?>