<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<select id="tipoArquivoPdf" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="updateTipos()">
    <option class="dropdown-item" selected disabled>Selecione um tipo...</option>
<?php


if(isset($_COOKIE['id_unidades']) || isset($arrayIdOption[0])){

    // Array Vazio
    $listaTipoArquivo = [];
    $listaNomes = [];

    if(isset($_COOKIE['id_unidades'])){
        $id_unidades = $_COOKIE['id_unidades'];
    }else{
        $id_unidades = $arrayIdOption[0];
    }
    $urlTipos = "https://api.develop.clientes.witzler.com.br/api/tipos/arquivos/?unidade_id=".$id_unidades;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheTipos = file_get_contents($urlTipos, false, $context);
    @$resultTipos = json_decode($recolheTipos);
    
    if(isset($resultTipos)){
        foreach($resultTipos as $Tipos){
            $tipo_arquivo = $Tipos->tipo_arquivo;
            $tipo_arquivo_descricao = $Tipos->tipo_arquivo_descricao;
            array_push($listaTipoArquivo, $tipo_arquivo);
            array_push($listaNomes, $tipo_arquivo_descricao);
            $listaTipoArquivoFinal = array_unique($listaTipoArquivo);
            $listaNomesFinal = array_unique($listaNomes);
        }
        $tamanhoLista = count($listaTipoArquivo);
        for($i = 0; $i < $tamanhoLista; $i++){
            if(isset($listaTipoArquivoFinal[$i])){
            
?>
            <option id="<?php echo $listaTipoArquivoFinal[$i]; ?>" value="<?php echo $listaNomesFinal[$i]; ?>" class="dropdown-item"><?php echo $listaNomesFinal[$i]; ?></option>
<?php
            }
        }
    }
}
?>
</select>
<script>
    function updateTipos() {
            //Configurando tempo de vida da cookie ao executar funçao:
            var tempoCookie = new Date();
            tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

            //Definindo valores da funçao:
            var selectTipos = document.getElementById('tipoArquivoPdf');
            var optionTipos = selectTipos.options[selectTipos.selectedIndex];
            var recojeTipos = selectTipos.options[selectTipos.selectedIndex].id;
            console.log(recojeTipos);
            document.cookie = "tipo_arquivo = "+recojeTipos+"; expires="+tempoCookie.toUTCString();
    }

    updateTipos();
</script>