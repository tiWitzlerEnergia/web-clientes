<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<?php
session_start();
require_once "../../Classes/autoload.php";
if (isset($_GET['calendario_unidade']) && isset($_GET['final_mes']) && isset($_GET['id_unidades'])) {
    $dataInicial_old = $_GET['calendario_unidade'];
    $dataInicial = date('Y-m-d', strtotime($dataInicial_old));
    $dataFinal = $_GET['final_mes'];
    $id_unidade = $_GET['id_unidades'];
    $bar = new MedicaoMensalConsumo("medicao_mensal_de_consumo", "medidas/periodo/fora/ponta/diario?dataFinal=$dataFinal&dataInicial=$dataInicial&unidade_id=$id_unidade", array("data_medicao" => array(), "foraPonta" => array(), "ponta" => array()), $dataInicial)
?>
    <canvas id="medicao_mensal_de_consumo" class="mx-auto fade-in" width="1540" height="380" style="display: block;"></canvas>
    <script>
        <?php
            // $bar->varChartData("medicaoMensalConsumo", array("Fora Ponta", "Ponta"), array("data_medicao", "foraPonta", "ponta"), array("rgba(29, 107, 170, 0.7)", "rgba(76, 142, 173, 0.7)"), array("rgba(29, 107, 170, 0.7)", "rgba(76, 142, 173, 0.7)"), 1);
            $bar->varChartData("medicaoMensalConsumo", array("Fora Ponta", "Ponta"), array("data_medicao", "foraPonta", "ponta"), array($_SESSION['cor-custom-1'], $_SESSION['cor-custom-2']), array($_SESSION['cor-custom-1'], $_SESSION['cor-custom-2']), 1);
            $bar->configChartData("medicaoMensalConsumo", "bar", "teste bar");
        ?>
    </script>
<?php
}
?>