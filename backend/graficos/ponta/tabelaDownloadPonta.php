<?php
session_start();
require_once "../../Classes/autoload.php";
if (isset($_GET['calendario_unidade']) && isset($_GET['final_mes']) && isset($_GET['id_unidades'])) {
    $dataInicial_old = $_GET['calendario_unidade'];
    $dataInicial = date('Y-m-d', strtotime($dataInicial_old));
    $dataFinal = $_GET['final_mes'];
    $id_unidade = $_GET['id_unidades'];
    $mesDataInicial = date("m", strtotime($dataFinal));
    $anoDataInicial = date("Y", strtotime($dataFinal));

    $bar = new MedicaoMensalConsumo("medicao_mensal_de_consumo", "medidas/periodo/fora/ponta/diario?dataFinal=$dataFinal&dataInicial=$dataInicial&unidade_id=$id_unidade", array("data_medicao" => array(), "foraPonta" => array(), "ponta" => array()), $dataInicial);

    $bar->gerarValoresDownload($mesDataInicial, $anoDataInicial);
}
