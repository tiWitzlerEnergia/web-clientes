<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//if((isset($_COOKIE['calendario_unidade']) && isset($_COOKIE['final_mes']) && isset($_COOKIE['id_unidades']))){    //IF ANTIGO
if(isset($ultimaData) && isset($arrayIdOption[0])){     //If que pega o ultimo mes
    //GRAFICO ACUMULADO MENSAL
    $dataInicial = $ultimaData;
    $dataFinal = date("Y-m-t", strtotime($ultimaData));
    if(isset($_COOKIE['id_unidades'])){
        $id_unidade = $_COOKIE['id_unidades'];
    }else{
        $id_unidade = $arrayIdOption[0];
    }
    //Criando arrays para armazenar os dados com array_push dentro do foreach:
    $foraPonta = [];
    $ponta = [];
    $data_medicao = [];
    $dataLimite = date("t");
    //Passa url e recolhe infos:
    $urlGraficoFPontas = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo/fora/ponta/diario?dataFinal=".$dataFinal."&dataInicial=".$dataInicial."&unidade_id=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheGraficoFPontas = file_get_contents($urlGraficoFPontas, false, $context);
    @$resultGraficoFPontas = json_decode($recolheGraficoFPontas);
    if(isset($resultGraficoFPontas)){
        foreach($resultGraficoFPontas as $fpontas){
            //Declarando os dados da requisicao:
            $unidadeForaPonta = $fpontas->foraPonta;
            $unidadePonta = $fpontas->ponta;
            $unidadeDataMedicao = $fpontas->data_medicao;
            $unidadeDataMedicao = substr($unidadeDataMedicao, 8, -18);
            //Armazenando dados dentro do array:
            array_push($foraPonta, $unidadeForaPonta);
            array_push($ponta, $unidadePonta);
            array_push($data_medicao, $unidadeDataMedicao);
        
        }
        if(empty($data_medicao)){
            $dataLimite = date("t");
            for($i = 1; $i <= $dataLimite; $i++){
                array_push($data_medicao, $i);
                array_push($foraPonta, 0);
                array_push($ponta, 0);
            }
        }
    }else{
        if(empty($data_medicao)){
            $dataLimite = date("t");
            for($i = 1; $i <= $dataLimite; $i++){
                array_push($data_medicao, $i);
                array_push($foraPonta, 0);
                array_push($ponta, 0);
            }
        }
    }

}else{
    if(empty($data_medicao)){
        $dataLimite = date("t");
        $foraPonta = [];
        $ponta = [];
        $data_medicao = [];
        for($i = 1; $i <= $dataLimite; $i++){
            $diaPonta = 1;
            array_push($data_medicao, $i);
            array_push($foraPonta, 0);
            array_push($ponta, 0);
            $diaPonta++;
        }
    }
}
?>
