<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//Pegar id:
if(isset($_SESSION['usuario'])){
    $arrayIdOption = [];
    $urlUnidades = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=".$_SESSION['usuario'];
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    $recolheUnidades = file_get_contents($urlUnidades, false, $context);
    $resultUnidades = json_decode($recolheUnidades);
    if(isset($resultUnidades)){
        foreach($resultUnidades as $unidades){
            $id_option = $unidades->id_unidades;
            //array push:
            array_push($arrayIdOption, $id_option);
        }
    }
}


/*
$arrayIdOption2 = [];
$urlUnidades2 = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=".$_SESSION['usuario'];
$context = stream_context_create(array(
    'http' => array(
        'header' => "Authorization: Bearer ".$_SESSION['token'],
    ),
));
$recolheUnidades2 = file_get_contents($urlUnidades2, false, $context);
$resultUnidades2 = json_decode($recolheUnidades2);
foreach($resultUnidades2 as $unidade){
    $id_option2 = $unidade->id_unidades;
    //array push:
    array_push($arrayIdOption2, $id_option2);
}*/


//PROCURA DATAS PARA GRAFICO DE DASHBOARD
$data_medicao2 = [];
$dataFinal2 = date('Y-m-t');
$urlGraficoFPontas2 = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo/fora/ponta/diario?dataFinal=".$dataFinal2."&dataInicial=2019-01-01&unidade_id=".$arrayIdOption[0];
@$recolheGraficoFPontas2 = file_get_contents($urlGraficoFPontas2, false, $context);
@$resultGraficoFPontas2 = json_decode($recolheGraficoFPontas2);
if(isset($resultGraficoFPontas2)){
    foreach($resultGraficoFPontas2 as $fpontas2){
        //Declarando os dados da requisicao:
        $unidadeDataMedicao2 = $fpontas2->data_medicao;
        $unidadeDataMedicao2 = substr($unidadeDataMedicao2, 0, 7);
        //Armazenando dados dentro do array:
        array_push($data_medicao2, $unidadeDataMedicao2);
    }
    $ultimaData = end($data_medicao2)."-01";
}


?>