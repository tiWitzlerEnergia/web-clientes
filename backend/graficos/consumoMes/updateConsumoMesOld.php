<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
session_start();
?>
<canvas id="medicao_mensal_de_consumo" class="fade-in" width="750" height="350" style="display: block; position: relative;"></canvas>
<?php
if ((isset($_COOKIE['calendario_unidade']) && isset($_COOKIE['final_mes']) && isset($_COOKIE['id_unidades'])) || (isset($_GET['calendario_unidade']) && isset($_GET['final_mes']) && isset($_GET['id_unidades']))) {
    //GRAFICO ACUMULADO MENSAL
    //pega data e formata
    $dataInicial_old = $_GET['calendario_unidade'];
    $dataInicial = date('Y-m-d', strtotime($dataInicial_old));

    $dataFinal = $_GET['final_mes'];
    $id_unidade = $_GET['id_unidades'];
    //Criando arrays para armazenar os dados com array_push dentro do foreach:
    $foraPonta = [];
    $ponta = [];
    $data_medicao = [];
    //Passa url e recolhe infos:
    $urlGraficoFPontas = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo/fora/ponta/diario?dataFinal=" . $dataFinal . "&dataInicial=" . $dataInicial . "&unidade_id=" . $id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    @$recolheGraficoFPontas = file_get_contents($urlGraficoFPontas, false, $context);
    @$resultGraficoFPontas = json_decode($recolheGraficoFPontas);
    if (isset($resultGraficoFPontas)) {
        foreach ($resultGraficoFPontas as $fpontas) {
            //Declarando os dados da requisicao:
            $unidadeForaPonta = $fpontas->foraPonta;
            $unidadePonta = $fpontas->ponta;
            $unidadeDataMedicao = $fpontas->data_medicao;
            $unidadeDataMedicao = substr($unidadeDataMedicao, 8, -18);
            //Armazenando dados dentro do array:
            array_push($foraPonta, $unidadeForaPonta);
            array_push($ponta, $unidadePonta);
            array_push($data_medicao, $unidadeDataMedicao);
        }
        if (empty($data_medicao)) {
            $dataLimite = date("t");
            for ($i = 1; $i <= $dataLimite; $i++) {
                array_push($data_medicao, $i);
                array_push($foraPonta, 0);
                array_push($ponta, 0);
            }
        }
    } else {
        if (empty($data_medicao)) {
            $dataLimite = date("t");
            for ($i = 1; $i <= $dataLimite; $i++) {
                array_push($data_medicao, $i);
                array_push($foraPonta, 0);
                array_push($ponta, 0);
            }
        }
    }
} else {
    if (empty($data_medicao)) {
        $dataLimite = date("t");
        $foraPonta = [];
        $ponta = [];
        $data_medicao = [];
        for ($i = 1; $i <= $dataLimite; $i++) {
            $diaPonta = 1;
            array_push($data_medicao, $diaPonta);
            array_push($foraPonta, 0);
            array_push($ponta, 0);
            $diaPonta++;
        }
    }
}

$finalMesAtual = date("t", strtotime($dataInicial));
?>
<script>
    //GRAFICO Medição Mensal de Consumo
    var barChartData = {
        labels: [
            <?php
            $limiteDataMedicao = count($data_medicao);
            /*for($o = 0; $o<$limiteDataMedicao; $o++){
            $dia = substr($data_medicao[$o], 8, -18);
            echo "'Dia ".$data_medicao[$o]."', ";
          }*/
            $diaDataMedicao = 1;
            for ($o = 0; $o < $finalMesAtual; $o++) {
                $labelDataMedicao = isset($data_medicao[$o]) ? $data_medicao[$o] : $diaDataMedicao;
                echo "'$labelDataMedicao', ";

                $diaDataMedicao++;
            }
            ?>
        ],
        datasets: [{
            label: 'Fora Ponta',
            backgroundColor: "rgba(29, 107, 170, 0.7)",
            data: [
                <?php
                $limiteForaPonta = count($foraPonta);
                /*for($i = 0; $i<$limiteForaPonta; $i++){
						echo $foraPonta[$i].', ';
                    }*/
                for ($i = 0; $i < $finalMesAtual; $i++) {
                    $valorForaPonta = isset($foraPonta[$i]) ? $foraPonta[$i] : 0;
                    echo $valorForaPonta . ', ';
                }
                ?>
            ]
        }, {
            label: 'Ponta',
            backgroundColor: "rgba(76, 142, 173, 0.7)",
            data: [
                <?php
                $limitePonta = count($ponta);
                /*for($j = 0; $j<$limitePonta; $j++){
						echo $ponta[$j].', ';
                        }*/
                for ($j = 0; $j < $finalMesAtual; $j++) {
                    $valorPonta = isset($ponta[$j]) ? $ponta[$j] : 0;
                    echo $valorPonta . ', ';
                }
                ?>
            ]
        }]

    };
    var ctx = document.getElementById('medicao_mensal_de_consumo').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
                    responsive: false,
                    maintainAspectRatio: true,
					title: {
						display: false,
						text: 'Gráfico de Medição'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
                    },
                    animation: {
						duration: 4000,
                    }
				}
			});
</script>