<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//if((isset($_COOKIE['data_consumo_posto_tarifario']) && isset($_COOKIE['final_mes_consumo_posto_tarifario']) && isset($_COOKIE['id_unidades'])) || (isset($_COOKIE['data_consumo_posto_tarifario']) && isset($_COOKIE['final_mes_consumo_posto_tarifario']) && isset($arrayIdOption[0]))){
if(isset($ultimaData) && isset($arrayIdOption[0])){ 
    //Declarnado variaveis
    //$dataInicialPostoTarifario = $_COOKIE['data_consumo_posto_tarifario'];
    //$dataFinalPostoTarifario = $_COOKIE['final_mes_consumo_posto_tarifario'];

    $dataInicialPostoTarifario = $ultimaData;
    $dataFinalPostoTarifario = date("Y-m-t", strtotime($ultimaData));
    if(isset($_COOKIE['id_unidades'])){
        $id_unidade = $_COOKIE['id_unidades'];
    }else{
        $id_unidade = $arrayIdOption[0];
    }
    //config pra pegar dados da api
    $totalForaPonta = 0;
    $totalPonta = 0;
    $urlTotalFPontas = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo/fora/ponta/diario?dataFinal=".$dataFinalPostoTarifario."&dataInicial=".$dataInicialPostoTarifario."&unidade_id=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheTotalFPontas = file_get_contents($urlTotalFPontas, false, $context);
    @$resultTotalFPontas = json_decode($recolheTotalFPontas);
    if(isset($resultTotalFPontas)){
        foreach($resultTotalFPontas as $totalfpontas){
            $unidade_ponta = $totalfpontas->ponta;
            $unidade_fora_ponta = $totalfpontas->foraPonta;
            $totalPonta += $unidade_ponta;
            $totalForaPonta += $unidade_fora_ponta;
        }
    }else{
        $totalPonta = 1;
        $totalForaPonta = 1;
    }
}else{
    $totalPonta = 1;
    $totalForaPonta = 1;
}
?>