<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php session_start(); ?>
<canvas class="mx-auto chartjs-render-monitor fade-in" id="chartDoughnutPostoTarifario" width="1107" height="269" style="display: block; height: 300px; width: 1231px;"></canvas>
<?php
if(isset($_GET['data_consumo_posto_tarifario']) && isset($_GET['final_mes_consumo_posto_tarifario']) && isset($_GET['id_unidades'])){
    //Formatar datas
    $dataInicialPostoTarifario_old = $_GET['data_consumo_posto_tarifario'];
    $dataInicialPostoTarifario = date('Y-m-d', strtotime($dataInicialPostoTarifario_old));
    $dataFinalPostoTarifario_old = $_GET['final_mes_consumo_posto_tarifario'];
    $dataFinalPostoTarifario = date('Y-m-d', strtotime($dataFinalPostoTarifario_old));

    $id_unidade = $_GET['id_unidades'];
    $totalForaPonta = 0;
    $totalPonta = 0;
    $urlTotalFPontas = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo/fora/ponta/diario?dataFinal=".$dataFinalPostoTarifario."&dataInicial=".$dataInicialPostoTarifario."&unidade_id=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheTotalFPontas = file_get_contents($urlTotalFPontas, false, $context);
    @$resultTotalFPontas = json_decode($recolheTotalFPontas);
    if(isset($resultTotalFPontas)){
        foreach($resultTotalFPontas as $totalfpontas){
            $unidade_ponta = $totalfpontas->ponta;
            $unidade_fora_ponta = $totalfpontas->foraPonta;
            $totalPonta += $unidade_ponta;
            $totalForaPonta += $unidade_fora_ponta;
        }
    }else{
        $totalPonta = 1;
        $totalForaPonta = 1;
    }
}else{
    $totalPonta = 1;
    $totalForaPonta = 1;
}
?>
<script>
    var barChartData4 = {
        labels: ['Ponta (KWh)', 'Fora Ponta (KWh)'],
        datasets: [{
            label: 'Dataset 1',
            backgroundColor: [
                "rgba(76, 142, 173, 0.7)",
                "rgba(29, 107, 170, 0.7)",
            ],
            data: [
                <?php echo $totalPonta; ?>,
                <?php echo $totalForaPonta; ?>
            ]
        }]
    };
		
    var ctx4 = document.getElementById('chartDoughnutPostoTarifario').getContext('2d');
        window.myBar = new Chart(ctx4, {
            type: 'doughnut',
            data: barChartData4,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    position: 'top',
                },
                title: {
                    display: false,
                    text: 'Consumo por Posto Tarifário'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true,
                    duration: 2000,
                }
            }
        });
</script>