<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    var barChartData4 = {
        labels: ['Ponta (KWh)', 'Fora Ponta (KWh)'],
        datasets: [{
            label: 'Dataset 1',
            backgroundColor: [
                "rgba(76, 142, 173, 0.7)",
                "rgba(29, 107, 170, 0.7)",
            ],
            data: [
                <?php echo $totalPonta; ?>,
                <?php echo $totalForaPonta; ?>
            ]
        }]
    };
		
    var ctx4 = document.getElementById('chartDoughnutPostoTarifario').getContext('2d');
        window.myBar = new Chart(ctx4, {
            type: 'doughnut',
            data: barChartData4,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    position: 'top',
                },
                title: {
                    display: false,
                    text: 'Consumo por Posto Tarifário'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true,
                    duration: 2000,
                }
            }
        });
</script>