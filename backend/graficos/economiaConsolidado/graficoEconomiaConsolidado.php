<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
if(isset($_SESSION['usuario'])){
    //GRAFICO ECONOMIA CONSOLIDADO
    $usuario = $_SESSION['usuario'];
    //Criando arrays para armazenar os dados com array_push dentro do foreach:
    $valor_economizado = [];
    $data_referencia_economia_consolidado = [];
    $valorEconomizadoTotal = 0;
    //Passa url e recolhe infos:
    $urlGraficoEconomiaConsolidado = "https://api.develop.clientes.witzler.com.br/api/listando/economias/cliente?username=".$usuario;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheGraficoEconomiaConsolidado = file_get_contents($urlGraficoEconomiaConsolidado, false, $context);
    @$resultGraficoEconomiaConsolidado = json_decode($recolheGraficoEconomiaConsolidado);
    if(isset($resultGraficoEconomiaConsolidado)){
        /*foreach($resultGraficoEconomiaConsolidado as $economiaConsolidado){
            //Declarando os dados da requisicao:
            $unidadeValorEconomizado = $economiaConsolidado->valor_economizado;
            $unidadeValorEconomizado = number_format($unidadeValorEconomizado, 2, '.', '');
            $unidadeDataReferenciaEconomiaConsolidado = $economiaConsolidado->data_referencia;
            $anoDataReferencia = substr($unidadeDataReferenciaEconomiaConsolidado, 0, 4);
            $mesDataReferencia = substr($unidadeDataReferenciaEconomiaConsolidado, 5, -3);
            $dataFinalReferencia = $mesDataReferencia.'/'.$anoDataReferencia;
            //Armazenando dados dentro do array:
            array_push($valor_economizado, $unidadeValorEconomizado);
            array_push($data_referencia_economia_consolidado, $dataFinalReferencia);
            $valorEconomizadoTotal += $unidadeValorEconomizado;
            $valorEconomizadoTotalFormatado = number_format($valorEconomizadoTotal, 2);
        }*/

        //=========================================================================================
        $listaValoresGerais = [];
        foreach($resultGraficoEconomiaConsolidado as $economiaConsolidado){
            //Declarando os dados da requisicao:
            $unidadeValorEconomizado = $economiaConsolidado->valor_economizado;
            $unidadeValorEconomizado = number_format($unidadeValorEconomizado, 2, '.', '');
            $unidadeDataReferenciaEconomiaConsolidado = $economiaConsolidado->data_referencia;
            $anoDataReferencia = substr($unidadeDataReferenciaEconomiaConsolidado, 0, 4);
            $mesDataReferencia = substr($unidadeDataReferenciaEconomiaConsolidado, 5, -3);
            $dataFinalReferencia = $mesDataReferencia.'/'.$anoDataReferencia;

            $unidadeGeral = $unidadeValorEconomizado.";".$dataFinalReferencia;
            array_push($listaValoresGerais, $unidadeGeral);
            //Armazenando dados dentro do array:
            //array_push($valor_economizado, $unidadeValorEconomizado);
            array_push($data_referencia_economia_consolidado, $dataFinalReferencia);
            $valorEconomizadoTotal += $unidadeValorEconomizado;
            $valorEconomizadoTotalFormatado = number_format($valorEconomizadoTotal, 2, ',', '.');

            $data_referencia_economia_consolidado = array_unique($data_referencia_economia_consolidado);
        }

        $tamanhoListaGeral = count($listaValoresGerais);
        $tamanhoDatas = count($data_referencia_economia_consolidado);
        $valorFinal = [];
        for($j = 0; $j < $tamanhoDatas; $j++){
            $valorPorData = 0;
            if($data_referencia_economia_consolidado != ""){
                for($i = 0; $i < $tamanhoListaGeral; $i++){
                    list($unidade, $data) = explode(";", $listaValoresGerais[$i]);
                    if($data == $data_referencia_economia_consolidado[$j]){
                        $valorPorData += $unidade;
                    }
                }
            }
            array_push($valor_economizado, $valorPorData);
        }
        //=========================================================================================

        
    }else{
        
    }

}else{
    echo "teste";
}
?>