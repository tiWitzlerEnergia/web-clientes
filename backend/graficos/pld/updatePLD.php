<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<?php
session_start();
require_once "../../Classes/autoload.php";
if (isset($_GET['id_regiao']) && isset($_GET['data_pld'])) {
    $id_region = $_GET['id_regiao'];
    $dataDia_old = $_GET['data_pld'];
    $dataDia = date('Y-m-d', strtotime($dataDia_old));
    $pld = new Pld("valores_pld", "pldhorario?date_ref=$dataDia&region=$id_region", array("id" => array(), "value" => array()));
?>
    <canvas id="valores_pld" class="mx-auto fade-in col-12" height="296" style="display: block; position: relative;"></canvas>
    <script>
        <?php
        $pld->varChartData("graficoPld", array("Consumo PLD"), array("value"), array("#34aee4"), array("#34aee9"));
        $pld->configChartData("graficoPld", "line", "Grafico de PDL");
        ?>
    </script>
<?php
}
?>
