<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//Declarando listas:
$arrIdPld = [];
$arrValuePld = [];
$arrHourPld = [];
//Config para extrair dados da API e deixar pronto pra listar no chartjs:
$dataDia = date('Y-m-d');
$urlPld = "https://api.develop.clientes.witzler.com.br/api/pldhorario?date_ref=" . $dataDia . "&region=" . 1;
$context = stream_context_create(array(
    'http' => array(
        'header' => "Authorization: Bearer " . $_SESSION['token'],
    ),
));
@$recolhePld = file_get_contents($urlPld, false, $context);
@$resultPld = json_decode($recolhePld);
if (isset($resultPld)) {
    foreach ($resultPld as $pld) {
        $id = $pld->id;
        $value = $pld->value;
        $hour = $pld->value;

        array_push($arrIdPld, $id);
        array_push($arrValuePld, $value);
        array_push($arrHourPld, $hour);
    }
    $region = 1;
    if (empty($arrValuePld)) {
        $arrValuePld = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
}
?>