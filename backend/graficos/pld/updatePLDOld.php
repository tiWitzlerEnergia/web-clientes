<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<?php session_start(); ?>
<canvas id="valores_pld" width="750" class="mx-auto fade-in" height="350" style="display: block; position: relative;"></canvas>
<?php
//Declarando listas:
if (isset($_GET['id_regiao']) && isset($_GET['data_pld'])) {
    $id_region = $_GET['id_regiao'];
    $dataDia_old = $_GET['data_pld'];
    $dataDia = date('Y-m-d', strtotime($dataDia_old));

    $arrIdPld = [];
    $arrValuePld = [];
    $arrHourPld = [];
    //Config para extrair dados da API e deixar pronto pra listar no chartjs:
    $urlPld = "https://api.develop.clientes.witzler.com.br/api/pldhorario?date_ref=" . $dataDia . "&region=" . $id_region;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    @$recolhePld = file_get_contents($urlPld, false, $context);
    @$resultPld = json_decode($recolhePld);
    if (isset($resultPld)) {
        foreach ($resultPld as $pld) {
            $id = $pld->id;
            $value = $pld->value;
            $hour = $pld->value;

            array_push($arrIdPld, $id);
            array_push($arrValuePld, $value);
            array_push($arrHourPld, $hour);
        }
        $region = 1;
        if (empty($arrValuePld)) {
            $arrValuePld = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        }
    }
}
?>
<script>
    //GRAFICO chartLinhaDia:
    var barChartData3 = {
        labels: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
        datasets: [{
            label: 'Consumo PLD',
            backgroundColor: "#34aee4",
            borderColor: "#34aee9",
            pointRadius: 0,
            fill: false,
            data: [
                <?php
                /*for ($i = 0; $i < 24; $i++) {
                    echo "0, ";
                }*/
                $totalPld = count($arrValuePld);
                for ($i = 0; $i < $totalPld; $i++) {
                    echo $arrValuePld[$i] . ", ";
                }
                ?>
            ]
        }]
    };

    var ctx3 = document.getElementById('valores_pld').getContext('2d');
    window.myLine = new Chart(ctx3, {
        type: 'line',
        data: barChartData3,
        options: {
            title: {
                display: false,
                text: 'Consumo por dia de PLD'
            },
            tooltips: {
                mode: 'index',
                intersect: false
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Hora'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                    }
                }]
            },
            animation: {
                animateScale: true,
                animateRotate: true,
                duration: 4000,
            }
        }
    });
</script>