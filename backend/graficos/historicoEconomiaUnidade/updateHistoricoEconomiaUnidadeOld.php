<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php session_start(); ?>
<canvas id="historicoEconomia" class="mx-auto fade-in" width="1349" height="320" style="display: block; min-height: 320px; width: 1500px;"></canvas>

<script>
    var altura = window.innerHeight;
    var width = window.innerWidth;

    if(width < 700){
        var tamanho = 5;
    }else if(width > 1200){
        var tamanho = 12;
    }else if(width > 700 && width < 820){
        var tamanho = 7;
    }else if(width > 820 && width < 1024){
        var tamanho = 8.5;
    }else if(width > 1024 && width < 1200){
        var tamanho = 10;
    }
</script>
<?php
if(isset($_GET['id_unidades'])){
    $id_unidade = $_GET['id_unidades'];

    $listaPorcentagemEconomia = [];
    $listaValorEconomizado = [];
    $listaDataReferencia = [];
    $listaACR = [];
    $listaACL = [];
    //Passa url e recolhe infos:
    $urlHistoricoEconomia = "https://api.develop.clientes.witzler.com.br/api/economia/ultimos13Meses?id_unidade=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheHistoricoEconomia = file_get_contents($urlHistoricoEconomia, false, $context);
    @$resultHistoricoEconomia = json_decode($recolheHistoricoEconomia);
    if(isset($resultHistoricoEconomia)){
        foreach($resultHistoricoEconomia as $historicoEconomia13){
            //====== Declarando os dados da requisicao: ======
            $porcentagemEconomia = $historicoEconomia13->porcentagem_economia;
            $valorEconomizado = $historicoEconomia13->valor_economizado;
            $dataReferencia = $historicoEconomia13->data_referencia;
            //====== CALCULANDO ACR, ACL E ECONOMIA ======
            $unidadeACR = ($valorEconomizado / $porcentagemEconomia);
            $unidadeACL = $valorEconomizado / $porcentagemEconomia - $valorEconomizado;
            //====== Ajustando valores: ======
            //De ACR:
            $unidadeFormatadaACR = number_format($unidadeACR, 2);
            $unidadeFormatadaACR = str_replace(',', '', $unidadeFormatadaACR);
            //De ACL:
            $unidadeFormatadaACL = number_format($unidadeACL, 2);
            $unidadeFormatadaACL = str_replace(',', '', $unidadeFormatadaACL);
            // De porcentagem:
            $conversaoPorcentagem = ($porcentagemEconomia * 100);
            $unidadeListaPorcentagemEconomia = number_format($conversaoPorcentagem, 2);
            //De valor economizado:
            $unidadeValorEconomizado = $valorEconomizado;
            $unidadeValorEconomizado = number_format($unidadeValorEconomizado, 2);
            $unidadeValorEconomizado = str_replace(',', '', $unidadeValorEconomizado);
            //De data:
            $anoDataReferencia = substr($dataReferencia, 0, 4);
            $mesDataReferencia = substr($dataReferencia, 5, -3);
            $dataFinalReferencia = $mesDataReferencia."/".$anoDataReferencia;
            //====== Armazenando dados dentro do array: =======
            array_push($listaACR, $unidadeFormatadaACR);
            array_push($listaACL, $unidadeFormatadaACL);
            array_push($listaPorcentagemEconomia, $unidadeListaPorcentagemEconomia);
            array_push($listaValorEconomizado, $unidadeValorEconomizado);
            array_push($listaDataReferencia, $dataFinalReferencia);
        }

    }else{
        
    }
}else{
}
?>
<script>

var dataHistoricoEconomia = {
    labels: [
        <?php
            $tamanhoDataReferencia = count($listaDataReferencia);
            /*for($i = 0; $i < $tamanhoDataReferencia; $i++){
                echo '"'.$listaDataReferencia[$i].'", ';
            }*/
            for($i = 0; $i < 12; $i++){
                $valorLabel = isset($listaDataReferencia[$i]) ? $listaDataReferencia[$i] : "---";
                echo "'$valorLabel', ";
            }
        ?>
    ],
    datasets:[
        {
            label: "Valor ACR",
            data: [
                <?php
                    $tamanhoListaACR = count($listaACR);
                    /*for($j = 0; $j < $tamanhoListaACR; $j++){
                        echo $listaACR[$j].", ";
                    }*/
                    for($j = 0; $j < 12; $j++){
                        $valorACR = isset($listaACR[$j]) ? $listaACR[$j] : 0;
                        echo $valorACR.", ";
                    }
                ?>
            ],
            backgroundColor: "rgba(134, 134, 134, 0.8)",
            yAxisID: 'A',
        },
        {
            label: "Valor ACL",
            data: [
                <?php
                    $tamanhoListaACL = count($listaACL);
                    /*for($n = 0; $n < $tamanhoListaACL; $n++){
                        echo $listaACL[$n].", ";
                    }*/
                    for($n = 0; $n < 12; $n++){
                        $valorACL = isset($listaACL[$n]) ? $listaACL[$n] : 0;
                        echo $valorACL.", ";
                    }
                ?>
            ],
            backgroundColor: "rgba(17, 55, 89, 0.8)",
            yAxisID: 'A'
        },
        {
            label: "Economia [R$]",
            data: [
                <?php
                    $tamanhoListaValorEconomizado = count($listaValorEconomizado);
                    /*for($m = 0; $m < $tamanhoListaValorEconomizado; $m++){
                        echo $listaValorEconomizado[$m].", ";
                    }*/
                    for($m = 0; $m < 12; $m++){
                        $valorReais = isset($listaValorEconomizado[$m]) ? $listaValorEconomizado[$m] : 0;
                        echo $valorReais.", ";
                    }
                ?>
            ],
            backgroundColor: "rgba(135, 187, 45, 0.8)",
            yAxisID: 'A' 
        },
        {
            label: "Economia [%]",
            data: [
                <?php
                    $tamanhoListaPorcentagemEconomia = count($listaPorcentagemEconomia);
                    /*for($x = 0; $x < $tamanhoListaPorcentagemEconomia; $x++){
                        echo $listaPorcentagemEconomia[$x].", ";
                    }*/
                    for($x = 0; $x < 12; $x++){
                        $valorPer = isset($listaPorcentagemEconomia[$x]) ? $listaPorcentagemEconomia[$x] : 0;
                        echo $valorPer.", ";
                    }
                ?>
            ],
            backgroundColor: "#34aee4",
            borderColor: "#34aee4",
            borderWidth: 2,
            type: "line",
            fill: false,
            yAxisID: 'B'
        }
    ]
};

var ctx = document.getElementById("historicoEconomia").getContext('2d');
window.historicoEconomia = new Chart(ctx, {
    type: 'bar',
    data: dataHistoricoEconomia,
    options: {
        responsive: false,
        maintainAspectRatio: true,
        legend:{
            position: 'bottom',
            labels: {
                /*fontSize: tamanho,*/
                //boxWidth: 8,
            },
        },
        scales: {
            yAxes: [{
                id: 'A',
                position: 'left',
                stacked: false,
                ticks: {
                /*fontSize: tamanho,*/
                }
            },
            {
                id: 'B',
                position: 'left',
                type: 'linear',
                ticks: {
                    min: 0,
                    /*fontSize: tamanho,*/
                },
                stacked: false,
                //display: false
            },
            ],
            xAxes: [{
                stacked: false,
                ticks: {
                    /*fontSize: tamanho,*/
                },
            }]
        },
        animation: {
			duration: 4000,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
    },
});
</script>