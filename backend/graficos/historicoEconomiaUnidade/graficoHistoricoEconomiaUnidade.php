<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
if(isset($_COOKIE['id_unidades']) || isset($arrayIdOption[0])){
    //recolhendo variaveis uteis:
    if(isset($_COOKIE['id_unidades'])){
        $id_unidade = $_COOKIE['id_unidades'];
    }else{
        $id_unidade = $arrayIdOption[0];
    }
    
    //Criando arrays pra armazenar dados
    $listaPorcentagemEconomia = [];
    $listaValorEconomizado = [];
    $listaDataReferencia = [];
    $listaACR = [];
    $listaACL = [];
    //Passa url e recolhe infos:
    $urlHistoricoEconomia = "https://api.develop.clientes.witzler.com.br/api/economia/ultimos13Meses?id_unidade=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheHistoricoEconomia = file_get_contents($urlHistoricoEconomia, false, $context);
    @$resultHistoricoEconomia = json_decode($recolheHistoricoEconomia);
    if(isset($resultHistoricoEconomia)){
        foreach($resultHistoricoEconomia as $historicoEconomia13){
            //====== Declarando os dados da requisicao: ======
            $porcentagemEconomia = $historicoEconomia13->porcentagem_economia;
            $valorEconomizado = $historicoEconomia13->valor_economizado;
            $dataReferencia = $historicoEconomia13->data_referencia;
            //====== CALCULANDO ACR, ACL E ECONOMIA ======
            $unidadeACR = ($valorEconomizado / $porcentagemEconomia);
            $unidadeACL = $valorEconomizado / $porcentagemEconomia - $valorEconomizado;
            //====== Ajustando valores: ======
            //De ACR:
            $unidadeFormatadaACR = number_format($unidadeACR, 2);
            $unidadeFormatadaACR = str_replace(',', '', $unidadeFormatadaACR);
            //De ACL:
            $unidadeFormatadaACL = number_format($unidadeACL, 2);
            $unidadeFormatadaACL = str_replace(',', '', $unidadeFormatadaACL);
            // De porcentagem:
            $conversaoPorcentagem = ($porcentagemEconomia * 100);
            $unidadeListaPorcentagemEconomia = number_format($conversaoPorcentagem, 2);
            //De valor economizado:
            $unidadeValorEconomizado = $valorEconomizado;
            $unidadeValorEconomizado = number_format($unidadeValorEconomizado, 2);
            $unidadeValorEconomizado = str_replace(',', '', $unidadeValorEconomizado);
            //De data:
            $anoDataReferencia = substr($dataReferencia, 0, 4);
            $mesDataReferencia = substr($dataReferencia, 5, -3);
            $dataFinalReferencia = $mesDataReferencia."/".$anoDataReferencia;
            //====== Armazenando dados dentro do array: =======
            array_push($listaACR, $unidadeFormatadaACR);
            array_push($listaACL, $unidadeFormatadaACL);
            array_push($listaPorcentagemEconomia, $unidadeListaPorcentagemEconomia);
            array_push($listaValorEconomizado, $unidadeValorEconomizado);
            array_push($listaDataReferencia, $dataFinalReferencia);
        }

    }else{
     echo "<script>alert('erro');</script>";
    }
}else{
    echo "<script>alert('erro');</script>";
}
?>