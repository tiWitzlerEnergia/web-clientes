<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<?php
session_start();

require_once "../../Classes/autoload.php";
$id_unidade = $_GET['id_unidades'];
$historicoEconomia = new HistoricoEconomia("historicoEconomia", "economia/ultimos13Meses?id_unidade=$id_unidade");
$historicoEconomia->montarValores();
?>
<canvas id="historicoEconomia" class="mx-auto fade-in" width="1349" height="320" style="display: block; min-height: 320px; width: 1500px;"></canvas>
<script>
    <?php
    $historicoEconomia->varChartData("dataHistorico", array("Valor ACR", "Valor ACL", "Economia [R$]", "Economia [%]"), array("dataReferencia", "valorACR", "valorACL", "valorEconomizado", "porcentagemEconomia"), array("rgba(134, 134, 134, 0.8)", "rgba(17, 55, 89, 0.8)", "rgba(135, 187, 45, 0.8)", "#34aee4"), array("rgba(134, 134, 134, 0.8)", "rgba(17, 55, 89, 0.8)", "rgba(135, 187, 45, 0.8)", "#34aee4"));
    $historicoEconomia->configChartData("dataHistorico", "bar", "Historico Economia Unidade");
    ?>
</script>