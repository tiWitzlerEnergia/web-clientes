<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php include "graficoHistoricoEconomiaUnidade.php"; ?>

<script>
    var altura = window.innerHeight;
    var width = window.innerWidth;

    if(width < 700){
        var tamanho = 10;
    }else if(width > 1200){
        var tamanho = 12;
    }else if(width > 700 && width < 820){
        var tamanho = 7;
    }else if(width > 820 && width < 1024){
        var tamanho = 8.5;
    }else if(width > 1024 && width < 1200){
        var tamanho = 10;
    }
</script>


<script>

var dataHistoricoEconomia = {
    labels: [
        <?php
            $tamanhoDataReferencia = count($listaDataReferencia);
            /*for($i = 0; $i < $tamanhoDataReferencia; $i++){
                echo '"'.$listaDataReferencia[$i].'", ';
            }*/
            for($i = 0; $i < 12; $i++){
                $valorLabel = isset($listaDataReferencia[$i]) ? $listaDataReferencia[$i] : "---";
                echo "'$valorLabel', ";
            }
        ?>
    ],
    datasets:[
        {
            label: "Valor ACR",
            data: [
                <?php
                    $tamanhoListaACR = count($listaACR);
                    /*for($j = 0; $j < $tamanhoListaACR; $j++){
                        echo $listaACR[$j].", ";
                    }*/
                    for($j = 0; $j < 12; $j++){
                        $valorACR = isset($listaACR[$j]) ? $listaACR[$j] : 0;
                        echo $valorACR.", ";
                    }
                ?>
            ],
            backgroundColor: "rgba(134, 134, 134, 0.8)",
            yAxisID: 'A',
        },
        {
            label: "Valor ACL",
            data: [
                <?php
                    $tamanhoListaACL = count($listaACL);
                    /*for($n = 0; $n < $tamanhoListaACL; $n++){
                        echo $listaACL[$n].", ";
                    }*/
                    for($n = 0; $n < 12; $n++){
                        $valorACL = isset($listaACL[$n]) ? $listaACL[$n] : 0;
                        echo $valorACL.", ";
                    }
                ?>
            ],
            backgroundColor: "rgba(17, 55, 89, 0.8)",
            yAxisID: 'A'
        },
        {
            label: "Economia [R$]",
            data: [
                <?php
                    $tamanhoListaValorEconomizado = count($listaValorEconomizado);
                    /*for($m = 0; $m < $tamanhoListaValorEconomizado; $m++){
                        echo $listaValorEconomizado[$m].", ";
                    }*/
                    for($m = 0; $m < 12; $m++){
                        $valorReais = isset($listaValorEconomizado[$m]) ? $listaValorEconomizado[$m] : 0;
                        echo $valorReais.", ";
                    }
                ?>
            ],
            backgroundColor: "rgba(135, 187, 45, 0.8)",
            yAxisID: 'A' 
        },
        {
            label: "Economia [%]",
            data: [
                <?php
                    $tamanhoListaPorcentagemEconomia = count($listaPorcentagemEconomia);
                    /*for($x = 0; $x < $tamanhoListaPorcentagemEconomia; $x++){
                        echo $listaPorcentagemEconomia[$x].", ";
                    }*/
                    for($x = 0; $x < 12; $x++){
                        $valorPer = isset($listaPorcentagemEconomia[$x]) ? $listaPorcentagemEconomia[$x] : 0;
                        echo $valorPer.", ";
                    }
                ?>
            ],
            backgroundColor: "#34aee4",
            borderColor: "#34aee4",
            borderWidth: 2,
            type: "line",
            fill: false,
            yAxisID: 'B'
        }
    ]
};

var ctx = document.getElementById("historicoEconomia").getContext('2d');
window.historicoEconomia = new Chart(ctx, {
    type: 'bar',
    data: dataHistoricoEconomia,
    options: {
        responsive: false,
        maintainAspectRatio: true,
        legend:{
            position: 'bottom',
            labels: {
                /*fontSize: tamanho,*/
                //boxWidth: 20,
            },
        },
        scales: {
            yAxes: [{
                id: 'A',
                position: 'left',
                stacked: false,
                ticks: {
                /*fontSize: tamanho,*/
                }
            },
            {
                id: 'B',
                position: 'left',
                type: 'linear',
                ticks: {
                    min: 0,
                    /*fontSize: tamanho,*/
                },
                stacked: false,
                //display: false
            },
            ],
            xAxes: [{
                stacked: false,
                ticks: {
                    /*fontSize: tamanho,*/
                },
            }]
        },
        animation: {
			duration: 4000,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
    },
});
</script>