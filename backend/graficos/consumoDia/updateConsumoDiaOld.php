<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php session_start(); ?>
<canvas class="mx-auto fade-in" id="chartConsumoDia" width="750" height="350" style="display: block; height: 350px; width: 690px;"></canvas>
<?php
if(isset($_GET['data_consumo_dia']) && isset($_GET['id_unidades'])){
    //Declara URL:
    //Formateia data
    $dataDia_old = $_GET['data_consumo_dia'];
    $dataDia = date('Y-m-d', strtotime($dataDia_old));

    $id_unidade_dia = $_GET['id_unidades'];
    if(isset($_GET['id_unidades'])){
        $id_unidade_dia = $_GET['id_unidades'];
    }
    //Declarando listas:
    $listaAtivoCDia = [];
    //Config para extrair dados da API e deixar pronto pra listar no chartjs:
    $urlDia = "https://api.develop.clientes.witzler.com.br/api/periodo/ativoc?data_busca=".$dataDia."&unidade_id=".$id_unidade_dia;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheDia = file_get_contents($urlDia, false, $context);
    @$resultDia = json_decode($recolheDia);
    if(isset($resultDia)){
        foreach($resultDia as $dia){
            $unidadeAtivoCDia = $dia->ativo_c;
            array_push($listaAtivoCDia, $unidadeAtivoCDia);
        }
        if(empty($listaAtivoCDia)){
            $listaAtivoCDia = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        }
    }
}else{
    if(empty($listaAtivoCDia)){
        $listaAtivoCDia = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
}
?>
<script>
        //GRAFICO chartLinhaDia:
        var barChartData3 = {
            labels: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
                datasets: [{
                    label: 'Consumo geral (KWh)',
                    /*backgroundColor: window.chartColors.yellow,
                    borderColor: window.chartColors.yellow,*/
                    backgroundColor: "#34aee4",
                    borderColor: "#34aee9",
                    pointRadius: 0,
                    fill: true,
                    data: [
                        <?php
                            $tamanhoListaDia = count($listaAtivoCDia);
                            for($i = 0; $i < 24; $i++){
                                if(isset($listaAtivoCDia[$i])){
                                    echo $listaAtivoCDia[$i].", ";
                                }else{
                                    echo "0, ";
                                }
                            }
                        ?>
                    ]
                }]
            };
            
        var ctx3 = document.getElementById('chartConsumoDia').getContext('2d');
            window.myLine = new Chart(ctx3, {
                type: 'line',
                data: barChartData3,
                options: {
                    title: {
                        display: false,
                        text: 'Consumo por dia'
                        },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: false,
                    maintainAspectRatio: true,
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Hora'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                            }
                        }]
                    },
                    animation: {
						duration: 4000,
                    }
                }
            });
</script>