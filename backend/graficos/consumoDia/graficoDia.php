<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//if((isset($_COOKIE['data_consumo_dia']) && isset($_COOKIE['id_unidades'])) || isset($_COOKIE['data_consumo_dia']) && isset($arrayIdOption[0])){
if(isset($ultimaData) && isset($arrayIdOption[0])){
    //Declara URL:
    if(isset($_COOKIE['data_consumo_dia'])){
        $dataDia = $_COOKIE['data_consumo_dia'];
    }else{
        $dataDia = $ultimaData;
    }
    if(isset($_COOKIE['id_unidades'])){
        $id_unidade_dia = $_COOKIE['id_unidades'];
    }else{
        $id_unidade_dia = $arrayIdOption[0];
    }
    //Declarando listas:
    $listaAtivoCDia = [];
    //Config para extrair dados da API e deixar pronto pra listar no chartjs:
    $urlDia = "https://api.develop.clientes.witzler.com.br/api/periodo/ativoc?data_busca=".$dataDia."&unidade_id=".$id_unidade_dia;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheDia = file_get_contents($urlDia, false, $context);
    @$resultDia = json_decode($recolheDia);
    if(isset($resultDia)){
        foreach($resultDia as $dia){
            $unidadeAtivoCDia = $dia->ativo_c;
            array_push($listaAtivoCDia, $unidadeAtivoCDia);
        }
        if(empty($listaAtivoCDia)){
            $listaAtivoCDia = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        }
    }
}else{
    if(empty($listaAtivoCDia)){
        $listaAtivoCDia = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
}
?>