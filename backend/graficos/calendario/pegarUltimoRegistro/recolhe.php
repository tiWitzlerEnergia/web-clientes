<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//Pegar id:
if(isset($_SESSION['usuario'])){
    $arrayIdOption = [];
    $urlUnidades = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=".$_SESSION['usuario'];
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    $recolheUnidades = file_get_contents($urlUnidades, false, $context);
    $resultUnidades = json_decode($recolheUnidades);
    if(isset($resultUnidades)){
        foreach($resultUnidades as $unidades){
            $id_option = $unidades->id_unidades;
            //array push:
            array_push($arrayIdOption, $id_option);
        }
    }
}


//PROCURA DATAS PARA GRAFICO DE CALENDARIO
$data_medicao2 = [];
$dataFinal2 = date('Y-m-t');
//Declarando url para consumir api
$urlCalendario = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo?data_final=".$dataFinal2."&data_inicial=2020-01-01&unidade_id=".$arrayIdOption[0];
$context = stream_context_create(array(
    'http' => array(
        'header' => "Authorization: Bearer ".$_SESSION['token']
    )
));
@$recolheCalendario = file_get_contents($urlCalendario, false, $context);
@$resultCalendario = json_decode($recolheCalendario);
if(isset($resultCalendario)){
    foreach($resultCalendario as $calendario){
        $data_medicao = $calendario->data_medicao;      //*
        $data_medicao = substr($data_medicao, 0, 7);
        array_push($data_medicao2, $data_medicao);
    }
    $ultimaData = end($data_medicao2)."-01";
}


?>