<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
session_start();
if (isset($_GET['id_unidades'])) {
    //Declarando variaveis que podem ser úteis
    $id_unidade = $_GET['id_unidades'];
    //Formatar datas
    $dataAtual_old = $_GET['data_calendario'];
    $dataAtual = date('Y-m-d', strtotime($dataAtual_old));
    $fimDataAtual_old = $_GET['final_mes_calendario'];
    $fimDataAtual = date('Y-m-d', strtotime($fimDataAtual_old));
    //==============
    $ano = $_GET['ano_novo_calendario'];
    $mes = $_GET['mes_novo_calendario'];
    $diaFinal = $_GET['ultimo_dia_calendario'];
    //Declarando listass:
    $listaId_medidas = [];
    $listaAtivo_c = [];
    $listaReativo_c = [];
    $listaAtivo_g = [];
    $listaReativo_g = [];
    $listaDia_da_semana = [];
    $listaData_medicao = [];
    $listaHora_do_dia = [];
    $listaUnidade_id = [];
    $listaTipo_medida = [];
    $listaStatus = [];
    $listaMsg = [];
    //Declarando url para consumir api
    $token = $_SESSION['token'];
    $urlCalendario = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo?data_final=" . $fimDataAtual . "&data_inicial=" . $dataAtual . "&unidade_id=" . $id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'method' => "GET",
            'header' => "Authorization: Bearer $token",
        ),
    ));
    @$recolheCalendario = file_get_contents($urlCalendario, false, $context);
    @$resultCalendario = json_decode($recolheCalendario);
    if (isset($resultCalendario)) {
        foreach ($resultCalendario as $calendario) {
            //recolhendo dados: (apenas os que tem * se utilizam)
            $id_medidas = $calendario->id_medidas;
            $ativo_c = $calendario->ativo_c;                //*
            $reativo_c = $calendario->reativo_c;
            $ativo_g = $calendario->ativo_g;
            $reativo_g = $calendario->reativo_g;
            $dia_da_semana = $calendario->dia_da_semana;    //*
            $data_medicao = $calendario->data_medicao;      //*
            $hora_do_dia = $calendario->hora_do_dia;        //*
            $unidade_id = $calendario->unidade_id;
            $tipo_medida = $calendario->tipo_medida;
            $status = $calendario->status;
            //===================================================
            //Pegar hora e converter em formato aceitavel para conversao:
            $hora_do_dia = $hora_do_dia - 01;
            $hora_do_dia = $hora_do_dia . ":00:00";
            //==================================================
            $data_medicao = $data_medicao . $hora_do_dia;
            //Tratando data_medicao:
            list($ano, $mes, $dia) = explode("-", $data_medicao);
            //Tratando o dia:
            list($dia, $resto) = explode("T", $dia);
            $a = "$dia-$mes-$ano $hora_do_dia";
            $time = strtotime($a);
            $newformat = date('Y-m-d H:i:s', $time);
            //==================================================
            $msg = "$newformat;$dia_da_semana;$ativo_c";
            //==================================================
            array_push($listaMsg, $msg);
            array_push($listaAtivo_c, $ativo_c);
        }
        if (empty($listaMsg)) {
            for ($i = 0; $i < $diaFinal; $i++) {
                for ($j = 0; $j <= $diaFinal; $j++) {
                    if ($j < 10) {
                        $hora = "0$j";
                    } else {
                        $hora = $j;
                    }
                    $msg = $ano . "-" . $mes . "-" . $hora . ";0;0";
                    array_push($listaMsg, $msg);
                    array_push($listaAtivo_c, 0);
                }
            }
        }
    } else {
        for ($i = 0; $i < $diaFinal; $i++) {
            for ($j = 0; $j < 24; $j++) {
                if ($j < 10) {
                    $hora = "0$j";
                } else {
                    $hora = $j;
                }
                $msg = $ano . "-" . $mes . "-" . $hora . ";0;0";
                array_push($listaMsg, $msg);
                array_push($listaAtivo_c, 0);
            }
        }
    }



    function date_sort($a, $b)
    {
        return strcmp($a, $b); //matriz b mapping
    }
    usort($listaMsg, "date_sort");
} else {
    for ($i = 0; $i < $diaFinal; $i++) {
        for ($j = 0; $j < 24; $j++) {
            if ($j < 10) {
                $hora = "0$j";
            } else {
                $hora = $j;
            }
            $msg = $ano . "-" . $mes . "-" . $hora . ";0;0";
            array_push($listaMsg, $msg);
            array_push($listaAtivo_c, 0);
        }
    }
}
?>

<?php
if (isset($_GET['ultimo_dia_calendario']) && isset($_GET['mes_novo_calendario']) && isset($_GET['ano_novo_calendario'])) {
    $limiteData = $_GET['ultimo_dia_calendario'];
    $mes = $_GET['mes_novo_calendario'];
    $ano = $_GET['ano_novo_calendario'];
    $finalDoMes = $_GET['ultimo_dia_calendario'];
    $tamanhoArray = count($listaMsg);
} else {
    //Pegar mes atual com php para evitar erros de variaveis
    $atual = date("Y-m-d");
    //Ultimo dia do mes atual:
    $limiteData = date("t");
    $finalDoMes = date("t");
    //Mes e ano:
    $mes = date("m");
    $ano = date("Y");
}
for ($i = 0; $i < $finalDoMes; $i++) {
    $number = $i;
    $diaTd = $i + 01;
    if ($diaTd < 10) {
        $diaTd = "0" . $diaTd;
    }
    if ($number == 0 || $number % 7 == 0) {
        echo "<tr>";
    }
?>
    <td id="<?php echo $diaTd . "-" . $mes . "-" . $ano; ?>" class="fade-in" style="cursor: pointer; height: 160px; min-width: 223px !important;">
        <div class="chartjs-size-monitor">
            <div class="chartjs-size-monitor-expand">
                <div class=""></div>
            </div>
            <div class="chartjs-size-monitor-shrink">
                <div class=""></div>
            </div>
        </div>
        <canvas id="chart<?php echo $diaTd . "-" . $mes . "-" . $ano; ?>" resize="true" class="chartjs-render-monitor fade-in" width="223" height="160" style="display: block;"></canvas>
    </td>
<?php
    if ($number == 6) {
        echo "</tr>";
    }
}
?>
<script>
    var listaData = [];
    <?php
    $tamanhoLista = count($listaMsg);
    for ($n = 0; $n < $finalDoMes + 1; $n++) {
        $listaValor[$n] = [];
        $listaData[$n] = [];
        $listaSemana[$n] = [];
    }
    for ($i = 0; $i < $tamanhoLista; $i++) {
        list($data, $semana, $valor) = explode(";", $listaMsg[$i]);
        //PEGANDO APENAS O DIA:
        $diaParaData = substr($data, 8, 2);
        //CONVERTE O DIA PARA EVITAR PROBLEMAS COM ARRAY_PUSH
        if ($diaParaData < 10) {
            $indice = substr($diaParaData, 1);
        } else {
            $indice = $diaParaData;
        }
        //Recolher valores por dia:
        array_push($listaValor[$indice], $valor);
        //Recolher datas por dia:
        array_push($listaData[$indice], $data);
    }
    //Gerar todos os chartjs para o codigo
    for ($j = 0; $j < $finalDoMes; $j++) {
        //Config para pegar os dias e agrupar eles por id nas <td>
        $diaChartjs = $j + 01;
        if ($diaChartjs < 10) {
            $diaChartjs = "0" . $diaChartjs;
        }
    ?>

        var data<?php echo $diaChartjs; ?> = {
            // caso estiver errado o labels, copiar e copar aquele comentado em chartCalendario.php
            labels: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
            datasets: [{
                label: 'Consumo (KWh): ',
                backgroundColor: "#34aee4",
                borderColor: "#34aee4",
                lineTension: 0.1,
                borderWidth: 2,
                pointBorderWidth: 1,
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: [
                    <?php
                    if (!empty($listaValor[$j + 1])) {
                        for ($i = 0; $i < 24; $i++) {
                            $valor_atual = isset($listaValor[$j + 1][$i]) ? $listaValor[$j + 1][$i] : 0;
                            echo "$valor_atual, ";
                            $maiorValor = max($listaAtivo_c);
                        }
                    } else {
                        for ($i = 0; $i < 24; $i++) {
                            echo "0, ";
                        }
                    }
                    ?>
                ],
                fill: false,
            }]
        }
        listaData.push(data<?php echo $diaChartjs; ?>);
        var config<?php echo $diaChartjs; ?> = {
            type: 'line',
            data: data<?php echo $diaChartjs; ?>,
            responsive: true,
            maintainAspectRatio: false,
            options: {
                responsive: true,
                title: {
                    display: true,
                    fontSize: 15,
                    text: '<?php echo $diaChartjs; ?> - <?php $day = $ano . "-" . $mes . "-" . $diaChartjs; setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese'); date_default_timezone_get('America/Sao_Paulo'); echo strftime('%A', strtotime($day)); ?>'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                legend: {
                    display: false,
                    padding: 0,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        visible: false,
                        display: false,
                        gridLines: {
                            drawBorder: false,
                            display: false,
                        },
                    }],
                    yAxes: [{
                        visible: false,
                        display: false,
                        ticks: {
                            max: <?php echo $maiorValor; ?>,
                            min: 0
                        },
                        gridLines: {
                            drawBorder: false,
                            display: false,
                        },
                    }]
                },
                animation: {
                    duration: 4000,
                }
            }
        };

        var ctx<?php echo $diaChartjs; ?> = document.getElementById('chart<?php echo $diaChartjs . "-" . $mes . "-" . $ano; ?>').getContext('2d');
        window.myLine<?php echo $diaChartjs; ?> = new Chart(ctx<?php echo $diaChartjs; ?>, config<?php echo $diaChartjs; ?>);

    <?php
    }
    ?>

    //console.log('imprimindo config:'+config01.value());
</script>
<?php include "comparacao/funcaoMuda.php"; ?>