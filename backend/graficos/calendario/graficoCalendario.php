<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
if (isset($_COOKIE['id_unidades']) || isset($arrayIdOption[0])) {
    //Declarando variaveis que podem ser úteis
    if (isset($_COOKIE['id_unidades'])) {
        $id_unidade = $_COOKIE['id_unidades'];
    } else {
        $id_unidade = $arrayIdOption[0];
    }

    //if(isset($_COOKIE['data_calendario']) && isset($_COOKIE['final_mes_calendario']) && isset($_COOKIE['ano_novo_calendario']) && isset($_COOKIE['mes_novo_calendario']) && isset($_COOKIE['ultimo_dia_calendario'])){
    if (isset($ultimaData)) {
        //$dataAtual = $_COOKIE['data_calendario'];
        //$fimDataAtual = $_COOKIE['final_mes_calendario'];
        //$ano = $_COOKIE['ano_novo_calendario'];
        //$mes = $_COOKIE['mes_novo_calendario'];
        //$diaFinal = $_COOKIE['ultimo_dia_calendario'];
        $dataAtual = $ultimaData;
        $fimDataAtual = date("Y-m-t", strtotime($ultimaData));
        $ano = date("Y", strtotime($dataAtual));
        $mes = date("m", strtotime($dataAtual));
        $diaFinal = date("t", strtotime($dataAtual));
    } else {
        $dataAtual = date("Y-m-01");
        $fimDataAtual = date("Y-m-t");
        $ano = date("Y");
        $mes = date("m");
        $diaFinal = date("t");
    }
    //Declarando listas:
    $listaId_medidas = [];
    $listaAtivo_c = [];
    $listaReativo_c = [];
    $listaAtivo_g = [];
    $listaReativo_g = [];
    $listaDia_da_semana = [];
    $listaData_medicao = [];
    $listaHora_do_dia = [];
    $listaUnidade_id = [];
    $listaTipo_medida = [];
    $listaStatus = [];
    $listaMsg = [];
    //Declarando url para consumir api
    $urlCalendario = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo?data_final=" . $fimDataAtual . "&data_inicial=" . $dataAtual . "&unidade_id=" . $id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token']
        )
    ));
    @$recolheCalendario = file_get_contents($urlCalendario, false, $context);
    @$resultCalendario = json_decode($recolheCalendario);
    if (isset($resultCalendario)) {
        foreach ($resultCalendario as $calendario) {
            //recolhendo dados: (apenas os que tem * se utilizam)
            $id_medidas = $calendario->id_medidas;
            $ativo_c = $calendario->ativo_c;                //*
            $reativo_c = $calendario->reativo_c;
            $ativo_g = $calendario->ativo_g;
            $reativo_g = $calendario->reativo_g;
            $dia_da_semana = $calendario->dia_da_semana;    //*
            $data_medicao = $calendario->data_medicao;      //*
            $hora_do_dia = $calendario->hora_do_dia;        //*
            $unidade_id = $calendario->unidade_id;
            $tipo_medida = $calendario->tipo_medida;
            $status = $calendario->status;
            //===================================================
            //Pegar hora e converter em formato aceitavel para conversao:
            $hora_do_dia = $hora_do_dia - 01;
            $hora_do_dia = $hora_do_dia . ":00:00";
            //==================================================
            $data_medicao = $data_medicao . $hora_do_dia;
            //Tratando data_medicao:
            list($ano, $mes, $dia) = explode("-", $data_medicao);
            //Tratando o dia:
            list($dia, $resto) = explode("T", $dia);
            $a = "$dia-$mes-$ano $hora_do_dia";
            $time = strtotime($a);
            $newformat = date('Y-m-d H:i:s', $time);
            //==================================================
            $msg = "$newformat;$dia_da_semana;$ativo_c";
            //==================================================
            array_push($listaMsg, $msg);
            array_push($listaAtivo_c, $ativo_c);
        }
        if (empty($listaMsg)) {
            for ($i = 0; $i < $diaFinal; $i++) {
                for ($j = 0; $j <= $diaFinal; $j++) {
                    if ($j < 10) {
                        $hora = "0$j";
                    } else {
                        $hora = $j;
                    }
                    $msg = $ano . "-" . $mes . "-" . $hora . ";0;0";
                    array_push($listaMsg, $msg);
                    array_push($listaAtivo_c, 0);
                }
            }
        }
    } else {
        for ($i = 0; $i < $diaFinal; $i++) {
            for ($j = 0; $j < 24; $j++) {
                if ($j < 10) {
                    $hora = "0$j";
                } else {
                    $hora = $j;
                }
                $msg = $ano . "-" . $mes . "-" . $hora . ";0;0";
                array_push($listaMsg, $msg);
                array_push($listaAtivo_c, 0);
            }
        }
    }



    function date_sort($a, $b)
    {
        return strcmp($a, $b); //matriz b mapping
    }
    usort($listaMsg, "date_sort");
} else {
    for ($i = 0; $i < $diaFinal; $i++) {
        for ($j = 0; $j < 24; $j++) {
            if ($j < 10) {
                $hora = "0$j";
            } else {
                $hora = $j;
            }
            $msg = $ano . "-" . $mes . "-" . $hora . ";0;0";
            array_push($listaMsg, $msg);
            array_push($listaAtivo_c, 0);
        }
    }
}
?>