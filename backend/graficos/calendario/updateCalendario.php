<?php
session_start();
require_once "../../Classes/autoload.php";
if (isset($_GET['id_unidades'])) {
    $id_unidade = $_GET['id_unidades'];
    $dataAtual_old = isset($_GET['data_calendario']) ? $_GET['data_calendario'] : date("Y-m-d");
    $dataAtual = date('Y-m-d', strtotime($dataAtual_old));
    $fimDataAtual_old = isset($_GET['final_mes_calendario']) ? $_GET['final_mes_calendario'] : date("Y-m-t");
    $fimDataAtual = date('Y-m-d', strtotime($fimDataAtual_old));
    $ano = isset($_GET['ano_novo_calendario']) ? $_GET['ano_novo_calendario'] : date("Y");
    $mes = isset($_GET['mes_novo_calendario']) ? $_GET['mes_novo_calendario'] : date("m");
    $diaFinal = isset($_GET['ultimo_dia_calendario']) ? $_GET['ultimo_dia_calendario'] : date("t");
    $limiteData = $diaFinal;
    for ($i = 0; $i < $diaFinal; $i++) {
        $number = $i;
        $diaTd = $i + 01;
        if ($diaTd < 10) {
            $diaTd = "0" . $diaTd;
        }
        if ($number == 0 || $number % 7 == 0) {
            echo "<tr>";
        }
        echo "
            <td id=\"$diaTd-$mes-$ano\" class=\"fade-in\" style=\"cursor: pointer; height: 160px; min-width: 223px !important;\">
                <div class=\"chartjs-size-monitor\">
                    <div class=\"chartjs-size-monitor-expand\">
                        <div class=\"\"></div>
                    </div>
                    <div class=\"chartjs-size-monitor-shrink\">
                        <div class=\"\"></div>
                    </div>
                </div>
                <canvas id=\"chart$diaTd-$mes-$ano\" resize=\"true\" class=\"chartjs-render-monitor fade-in\" width=\"223\" height=\"160\" style=\"display: block;\"></canvas>
            </td>
        ";
        if ($number == 6) {
            echo "</tr>";
        }
    }
    
    $calendarioUpdate = new Calendario("calendariochart", $id_unidade, $dataAtual);
    $calendarioUpdate->gerarGrafico();
    echo"<pre>";var_dump($calendarioUpdate->updateDados());echo"</pre>";
    include "comparacao/funcaoMuda.php";
}
