<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<script>
    var arrHoras = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
    <?php
    $tamanhoLista = count($listaMsg);
    for ($n = 0; $n < $finalDoMes + 1; $n++) {
        $listaValor[$n] = [];
        $listaData[$n] = [];
        $listaSemana[$n] = [];
    }
    for ($i = 0; $i < $tamanhoLista; $i++) {
        list($data, $semana, $valor) = explode(";", $listaMsg[$i]);
        //PEGANDO APENAS O DIA:
        $diaParaData = substr($data, 8, 2);
        //CONVERTE O DIA PARA EVITAR PROBLEMAS COM ARRAY_PUSH
        if ($diaParaData < 10) {
            $indice = substr($diaParaData, 1);
        } else {
            $indice = $diaParaData;
        }
        //Recolher valores por dia:
        array_push($listaValor[$indice], $valor);
        //Recolher datas por dia:
        array_push($listaData[$indice], $data);
    }
    //Gerar todos os chartjs para o codigo
    for ($j = 0; $j < $finalDoMes; $j++) {
        //Config para pegar os dias e agrupar eles por id nas <td>
        $diaChartjs = $j + 01;
        if ($diaChartjs < 10) {
            $diaChartjs = "0" . $diaChartjs;
        }
    ?>
        var data<?php echo $diaChartjs; ?> = {
            /*labels: [
                <?php
                /*if (!empty($listaData)) {
                    //Pegando horarios dinamicamente (formato de 24h)
                    for ($i = 0; $i < 24; $i++) {
                        $hora = isset($listaData[$j + 1][$i]) ? $listaData[$j + 1][$i] : 0;
                        $hora = substr($hora, 10, 6);
                        echo "'" . $hora . "', ";
                    }
                } else {
                    for ($i = 0; $i < 24; $i++) {
                        echo "'0$i', ";
                    }
                }*/
                ?>
            ],*/
            labels: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
            datasets: [{
                label: 'Consumo (KWh): ',
                backgroundColor: "#34aee4",
                borderColor: "#34aee4",
                lineTension: 0.1,
                borderWidth: 2,
                pointBorderWidth: 1,
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: [
                    <?php
                    if (!empty($listaValor[$j + 1])) {
                        for ($i = 0; $i < 24; $i++) {
                            $valor_atual = isset($listaValor[$j + 1][$i]) ? $listaValor[$j + 1][$i] : 0;
                            echo "$valor_atual, ";
                            $maiorValor = max($listaAtivo_c);
                        }
                    } else {
                        for ($i = 0; $i < 24; $i++) {
                            echo "0, ";
                        }
                    }
                    ?>
                ],
                fill: false,
            }]
        }
        var config<?php echo $diaChartjs; ?> = {
            type: 'line',
            data: data<?php echo $diaChartjs; ?>,
            responsive: true,
            maintainAspectRatio: false,
            options: {
                responsive: true,
                title: {
                    display: true,
                    fontSize: 15,
                    text: '<?php echo $diaChartjs; ?> - <?php $day = $ano . "-" . $mes . "-" . $diaChartjs; setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese'); date_default_timezone_get('America/Sao_Paulo'); echo strftime('%A', strtotime($day)); ?>'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                legend: {
                    display: false,
                    padding: 0,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        visible: false,
                        display: false,
                        gridLines: {
                            drawBorder: false,
                            display: false,
                        },
                    }],
                    yAxes: [{
                        visible: false,
                        display: false,
                        ticks: {
                            max: <?php echo $maiorValor; ?>,
                            min: 0
                        },
                        gridLines: {
                            drawBorder: false,
                            display: false,
                        },
                    }]
                },
                animation: {
                    duration: 4000,
                }
            }
        };

        var ctx<?php echo $diaChartjs; ?> = document.getElementById('chart<?php echo $diaChartjs . "-" . $mes . "-" . $ano; ?>').getContext('2d');
        var myLine<?php echo $diaChartjs; ?> = new Chart(ctx<?php echo $diaChartjs; ?>, config<?php echo $diaChartjs; ?>);

    <?php
    }
    ?>

    //console.log('imprimindo config:'+config01.value());
</script>