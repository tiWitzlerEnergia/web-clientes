<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<?php 
if(isset($comeca)){ 
?>
<canvas id="comparacao" width="1540" height="380" style="display: none; position: relative;"></canvas>
<?php
}else{
?>
<canvas id="comparacao" width="1540" height="380" style="display: block; position: relative;"></canvas>
<?php
}
?>
<script>
var dynamicColors = function(){
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
};
<?php
if(isset($_GET['listaItensEscolhidos'])){
    $ano = $_GET['ano'];
    $mes = $_GET['mes'];
    $listaItens = $_GET['listaItensEscolhidos'];
    $tamanhoItens = count($listaItens);
?>
    //Teste
    <?php
    //Cria array para media a media:
        $listaMedia = [];
    //Começa fazer os loop. O primeiro para ir pelos dias escolhidos e o segundo para ir as 24h do dia.
        for($n = 0; $n < $tamanhoItens; $n++){
            for($m = 0; $m < 24; $m++){

                
            }
        }
    ?>
    //===============================


    //Criar array que vai conter os dados para media no grafico
    var listaMedia = [];
    var teste = 0;
    var mediaHora = 0;
    //For para fazer a media de cada unidade 
    for(var n = 0; n < 24; n++){
        var unidade = data01["datasets"][0].data[n];
        
        <?php
            for($m = 0; $m < $tamanhoItens; $m++){
        ?>
                teste += data<?php echo $listaItens[$m]; ?>["datasets"][0].data[n];
                mediaHora = (teste/<?php echo $tamanhoItens; ?>);
        <?php
            }
        ?>
        
        listaMedia.push(mediaHora);
        teste = 0;
    }

    var config = {
			type: 'line',
			data: {
				labels: [
                    "00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"
                ],
				datasets: [
                    //MEDIA:
                    //Precisamos de ($listaItens[$i]["datasets"][0].data[$i] + $listaItens[$i]["datasets"][0].data[$i])/$tamanhoItens (ou algo do tipo)
                    {
                        label: 'Media',
                        backgroundColor: window.chartColors.red,
                        borderColor: window.chartColors.red,
                        lineTension: 0.1,
                        borderWidth: 2,
                        pointBorderWidth: 1,
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: listaMedia,
                        fill: false,
                    },
                    <?php  
                        for($i = 0; $i < $tamanhoItens; $i++){
                    ?>
                            {
                                label: '<?php echo $listaItens[$i]."-".$mes."-".$ano; ?>',
                                backgroundColor: dynamicColors(),
                                borderColor: dynamicColors(),
                                lineTension: 0.1,
                                borderWidth: 2,
                                pointBorderWidth: 1,
                                pointHoverBorderWidth: 2,
                                pointRadius: 1,
                                pointHitRadius: 10,
                                data: data<?php echo $listaItens[$i]; ?>["datasets"][0].data,
                                fill: false,
                            },
                    <?php
                        }
                    ?>
                ]
            },
			options: {
				tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: false,
                maintainAspectRatio: true,
                legend: {
                    position: 'bottom',
                },
                animation: {
					duration: 4000,
                }
			}
    };
<?php
}else{
?>
    var config = {
			type: 'line',
			data: {
				labels: [
                    "00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"
                ],
				datasets: [
                            {
                                label: 'Media',
                                backgroundColor: window.chartColors.red,
                                borderColor: window.chartColors.red,
                                lineTension: 0.1,
                                borderWidth: 2,
                                pointBorderWidth: 1,
                                pointHoverBorderWidth: 2,
                                pointRadius: 1,
                                pointHitRadius: 10,
                                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                fill: false,
                            },
                ]
            },
			options: {
				tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: false,
                maintainAspectRatio: true,
                legend: {
                    position: 'bottom',
                },
                animation: {
					duration: 4000,
                }
			}
    };
<?php
}
?>

	var ctxComparacao = document.getElementById('comparacao').getContext('2d');
	window.myLine = new Chart(ctxComparacao, config);
</script>

