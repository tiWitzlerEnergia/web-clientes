<!--
* Clientes
* @version v1.1
* 16/10/2020
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<?php
$maximoC = isset($maximoC) ? $maximoC : 0;
$minimoC = isset($minimoC) ? $minimoC : 0;
$onde_estou = isset($onde_estou) ? $onde_estou : 0;
$consumido = isset($consumido) ? $consumido : 0;
?>
<script>
	var barChartPrevia = {
		labels: [
			'Máximo', 'Mínimo', 'Consumo Medido Atual', 'Consumo Previsto'
		],
		datasets: [{
				type: 'line',
				lineThickness: 4,
				label: 'Máximo',
				steppedLine: true,
				fill: false,
				borderWidth: 3,
				lineTension: 0,
				backgroundColor: "rgba(29, 107, 170, 0.7)",
				borderColor: "rgba(29, 107, 170, 0.7)",
				borderDash: [5, 4],
				pointRadius: 0,
				data: [
					<?php
					echo $maximoC . ", " . $maximoC . ", " . $maximoC . ", " . $maximoC;
					?>
				]
			}, {
				type: 'line',
				lineThickness: 4,
				label: 'Mínimo',
				steppedLine: true,
				fill: false,
				borderWidth: 3,
				lineTension: 0,
				backgroundColor: "rgba(76, 142, 173, 0.7)",
				borderColor: "rgba(76, 142, 173, 0.7)",
				borderDash: [5, 4],
				fill: false,
				pointRadius: 0,
				data: [
					<?php
					echo $minimoC . ", " . $minimoC . ", " . $minimoC . ", " . $minimoC;
					?>
				]
			},
			{
				label: 'Consumo Medido Atual',
				borderColor: "#e98e29",
				backgroundColor: "#e98e29",
				data: [
					0,
					<?php echo $onde_estou; ?>,
					0,
					0
				]
			},
			{
				label: 'Consumo Previsto',
				borderColor: "#34aee4",
				backgroundColor: "#34aee4",
				data: [
					0,
					0,
					<?php echo $consumido; ?>,
					0
				]
			}
		]

	};
	var ctx = document.getElementById('valores_previa').getContext('2d');
	window.myBar = new Chart(ctx, {
		type: 'bar',
		data: barChartPrevia,
		options: {
			responsive: true,
			maintainAspectRatio: false,
			title: {
				display: false,
				text: 'Gráfico Valores Prévia'
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			scales: {
				xAxes: [{

				}],
				yAxes: [{
					//stacked: true
				}]
			},
			animation: {
				animateScale: true,
				animateRotate: true,
				duration: 4000,
			}
		}
	});
</script>
