<?php

//$dataprevia = '2020-10-01';
//$unidadeId = 1206;
if (isset($arrayIdOption[0])) {
    //Declaracao das variaveis dinâmicas
    $dataPrevia =  date("Y-m-01");
    if (isset($_COOKIE['id_unidades'])) {
        $id_unidade = $_COOKIE['id_unidades'];
    } else {
        $id_unidade = $arrayIdOption[0];
    }
    //PEga os dados da API
    $urlPrevia = "https://api.develop.clientes.witzler.com.br/api/webprevia?data_previa=" . $dataPrevia . "&unidade_id=" . $arrayIdOption[0];
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    //Carregar valores de maximo e minimo para o grafico (na mesma barra).
    $recolheValoresPrevia = file_get_contents($urlPrevia, false, $context);
    $resultValoresPrevia = json_decode($recolheValoresPrevia);
    if (isset($resultValoresPrevia)) {
        foreach ($resultValoresPrevia as $previa) {
            $data_previa = $previa->data_previa;
            $data_previa = substr($data_previa, 0, 10);
            $unidade_id = $previa->unidade_id;
            $maximoC = $previa->max_c;
            $minimoC = $previa->min_c;
            $contratado = $previa->contratado;
            $consumido = $previa->consumido;
            $consumo_medido = $previa->consumo_medido;

            $onde_estou = $consumo_medido;
        }
    }else{
        $maximoC = 0;
        $minimoC = 0;
        $onde_estou = 0;
    }
}else{
    $maximoC = 0;
    $minimoC = 0;
    $onde_estou = 0;
}
