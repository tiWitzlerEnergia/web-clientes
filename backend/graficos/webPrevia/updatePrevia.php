<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php session_start(); ?>
<canvas id="valores_previa" width="750" height="350" style="display: block; position: relative;"></canvas>
<?php
if (isset($_GET['id_unidades'])) {
    $id_unidade = $_GET['id_unidades'];
    $dataPrevia = date("Y-m-01");
    $urlPrevia = "https://api.develop.clientes.witzler.com.br/api/webprevia?data_previa=" . $dataPrevia . "&unidade_id=" . $id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    //Carregar valores de maximo e minimo para o grafico (na mesma barra).
    $recolheValoresPrevia = file_get_contents($urlPrevia, false, $context);
    $resultValoresPrevia = json_decode($recolheValoresPrevia);
    if (isset($resultValoresPrevia)) {
        foreach ($resultValoresPrevia as $previa) {
            $data_previa = $previa->data_previa;
            $data_previa = substr($data_previa, 0, 10);
            $unidade_id = $previa->unidade_id;
            $maximoC = $previa->max_c;
            $minimoC = $previa->min_c;
            $contratado = $previa->contratado;
            $consumido = $previa->consumido;
            $consumo_medido = $previa->consumo_medido;

            $onde_estou = $consumo_medido;
        }
    } else {
        $maximoC = 0;
        $minimoC = 0;
        $onde_estou = 0;
    }
} else {
    $maximoC = 0;
    $minimoC = 0;
    $onde_estou = 0;
}
?>
<script>
    var barChartPrevia = {
		labels: [
			'', '', '', ''
		],
		datasets: [{
				type: 'line',
				label: 'Máximo',
				backgroundColor: "rgba(29, 107, 170, 0.7)",
				borderColor: "rgba(29, 107, 170, 0.7)",
                pointRadius: 0,
				fill: false,
				data: [
					<?php
					echo $maximoC . ", " . $maximoC . ", " . $maximoC . ", " . $maximoC;
					?>
				]
			}, {
				type: 'line',
				label: 'Mínimo',
				backgroundColor: "rgba(76, 142, 173, 0.7)",
				borderColor: "rgba(76, 142, 173, 0.7)",
				fill: false,
				pointRadius: 0,
				data: [
					<?php
					echo $minimoC . ", " . $minimoC . ", " . $minimoC . ", " . $minimoC;
					?>
				]
			},
			{
				label: 'Onde estou?',
				borderColor: "#FF0000",
				backgroundColor: "#FF0000",
				data: [
					0,
					<?php echo $onde_estou; ?>,
					0,
					0
				]
			},
			{
				label: 'Consumo medido',
				borderColor: "#00ff00",
				backgroundColor: "#00ff00",
				data: [
					0,
					0,
					<?php echo $consumo_medido; ?>,
					0
				]
			}
		]

	};
	var ctx = document.getElementById('valores_previa').getContext('2d');
	window.myBar = new Chart(ctx, {
		type: 'bar',
		data: barChartPrevia,
		options: {
			responsive: false,
			maintainAspectRatio: true,
			title: {
				display: false,
				text: 'Gráfico Valores Prévia'
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			scales: {
				xAxes: [{
					
				}],
				yAxes: [{
					//stacked: true
				}]
			}
		}
	});
</script>
</script>