<?php
session_start();
require_once "../../Classes/autoload.php";

if (isset($_GET['id_unidades']) && isset($_GET['ano']) && isset($_GET['tipo'])) {
    $id_unidades_contrato =  $_GET['id_unidades'];
    $ano_request = $_GET['ano'];
    $tipo = $_GET['tipo'];
    $comercializadora = new Comercializadoras($ano_request, "chartDoughnutComercializadora", $id_unidades_contrato);
?>
    <canvas id="chartDoughnutComercializadora" class="mx-auto fade-in" width="288" height="230" style="display: block; width: 288px !important; height: 230px !important"></canvas>
    <script>
        <?php
        $comercializadora->varChartData("chartComercializadoras", array("Distribuidoras"), array("nomeComercializadora", "values"));
        $comercializadora->configChartData("chartComercializadoras", "doughnut", "Comercializadoras");
        ?>
    </script>
<?php
//var_dump($comercializadora);
}
?>