<?php
function random_color_part()
{
    return rand(0, 255);
}

function random_color()
{
    return "'rgb(" . random_color_part() . ", " . random_color_part() . ", " . random_color_part() . ")', ";
}

$cores = ["rgba(29, 107, 170, 0.7)", "rgba(76, 142, 173, 0.7)", "rgba(135, 187, 45, 1)", "#E98E29", "rgba(134, 134, 134, 0.8)", "rgba(17, 55, 89, 0.8)", "#DA70D6", "#DC143C", "#B22222", "#483D8B"];
?>
<script>
    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };
    var barChartComercializadoras = {
        labels: [
            <?php
            if ($totalAnos) {
                for ($i = 0; $i < $totalComercializadora; $i++) {
                    echo "'" . $arrNomeComercializadora[$i] . " [%]', ";
                }
            } else {
                echo "Sem distribuidora";
            }
            ?>
        ],
        datasets: [{
            label: 'Distribuidoras',
            //backgroundColor: [ "rgba(76, 142, 173, 0.7)",],
            backgroundColor: [
                <?php
                $cont_cor = 0;
                for ($i = 0; $i < $totalComercializadora; $i++) {
                    echo "'$cores[$cont_cor]', ";
                    $cont_cor++;
                    if($cont_cor == 10){
                        $cont_cor = 0;
                    }
                }
                ?>
            ],
            data: [
                <?php
                /*if ($totalAnos) {
                    for ($i = 0; $i < $totalAnos; $i++) {
                        $v_unidade[$i] = 0;
                        for ($j = 0; $j < 12; $j++) {
                            $v_unidade[$i] += $arrV[$arrVAno[$i]][$j];
                        }
                        echo $v_unidade[$i].", ";
                    }
                }
                $totalMontantes = count($arrMontante);
                if ($totalMontantes) {
                    for($i = 0; $i < $totalMontantes; $i++){
                        $valores = explode("/", $arrMontante[$i]);
                        echo $valores[1].", ";
                    }
                }*/
                if ($totalComercializadora) {
                    for ($i = 0; $i < $totalComercializadora; $i++) {
                        $valores = explode("/", $arrSemRepeticoes[$i]);
                        $v_per = ($valores[1] / $somaTotalComercializadora) * 100;
                        echo round($v_per, 1) . ", ";
                    }
                } else {
                    echo 0;
                }
                ?>
            ]
        }]
    };

    var ctxComercializadora = document.getElementById('chartDoughnutComercializadora').getContext('2d');
    window.myBar = new Chart(ctxComercializadora, {
        type: 'doughnut',
        data: barChartComercializadoras,
        options: {
            responsive: false,
            maintainAspectRatio: true,
            legend: {
                position: 'top',
            },
            title: {
                display: false,
                text: 'Comercializadoras'
            },
            animation: {
                animateScale: true,
                animateRotate: true,
                duration: 2000,
            }
        }
    });
</script>