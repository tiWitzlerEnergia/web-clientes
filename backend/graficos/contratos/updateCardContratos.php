<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<?php
session_start();
require_once "../../Classes/autoload.php";

$id_unidades_contrato =  $_GET['id_unidades'];
$data = $_GET['ano'];
$cards = new CardContratos($data, $id_unidades_contrato);
if (empty($cards->getIndicesAnoAtual())) {
?>
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 fade-in" id="contrato">
        <div class="card-accent-secondary card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td style="border-top: 0;">
                                    <div class="col-12 container row noPadding noMargin">
                                        <div class="col-12 noPadding text-center">
                                            <h1>Sem contratos</h1>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php
} else {
    $tamanhoContratos = count($cards->getIndicesAnoAtual());
    for ($i = 0; $i < $tamanhoContratos; $i++) {
        $cards->gerarCard($i, "block");
    }
}
?>