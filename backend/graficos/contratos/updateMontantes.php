<?php
session_start();
require_once "../../Classes/autoload.php";

if (isset($_GET['id_unidades']) && isset($_GET['ano']) && isset($_GET['tipo'])) {
    $id_unidades_contrato =  $_GET['id_unidades'];
    $ano_request = $_GET['ano'];
    $tipo = $_GET['tipo'];

    $montantes = new Montantes($ano_request, "chartLineMontantes", $id_unidades_contrato);
?>
    <canvas id="chartLineMontantes" class="mx-auto fade-in" width="510" height="280" style="display: block;"></canvas>
    <script>
        <?php
        $montantes->varChartData("charMontantes", array("Flex máx", "Valor médio", "Flex min"), array(), array("#c8c8c8", "#E98E29", "#c8c8c8"), array("#c8c8c8", "#E98E29", "#c8c8c8"));
        $montantes->configChartData("charMontantes", "", "Montante");
        ?>
    </script>
<?php
}
?>