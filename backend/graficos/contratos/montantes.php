<script>
    var labelsChart = [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro',
    ];
    var lineChartComercializadoras = {
        labels: labelsChart,
        datasets: [{
                label: 'Flex máx',
                data: [
                    <?php
                    if ($totalAnos) {
                        if ($totalAnos == 1) {
                            for ($i = 0; $i < $totalAnos; $i++) {
                                for ($j = 0; $j < 12; $j++) {
                                    echo round(($arrV[$arrVAno[$i]][$j]) * (1 + ($arrFlex_max[$arrVAno[$i]] / 100)), 2) . ", ";
                                }
                            }
                        } else {
                            //caso onde tem mais de 1 unidade por ano:
                            for ($i = 0; $i < 12; $i++) {
                                $v_mes_atual = 0;
                                for ($j = 0; $j < $totalAnos; $j++) {
                                    $v_mes_atual += ($arrV[$arrVAno[$j]][$i]) * (1 + ($arrFlex_max[$arrVAno[$j]] / 100));
                                }
                                echo round($v_mes_atual, 2) . ", ";
                            }
                        }
                    } else {
                        for ($i = 0; $i < 12; $i++) {
                            echo "0, ";
                        }
                    }
                    ?>
                ],
                type: "line",
                fill: false,
                backgroundColor: "#c8c8c8",
                borderColor: "#c8c8c8",
                borderWidth: 2,
                lineTension: 0,
                borderDash: [5, 5],
            },
            {
                label: 'Valor médio',
                data: [
                    <?php
                    if ($totalAnos) {
                        if ($totalAnos == 1) {
                            for ($i = 0; $i < $totalAnos; $i++) {
                                for ($j = 0; $j < 12; $j++) {
                                    echo round(($arrV[$arrVAno[$i]][$j]), 2) . ", ";
                                }
                            }
                        } else {
                            //caso onde tem mais de 1 unidade por ano:
                            for ($i = 0; $i < 12; $i++) {
                                $v_mes_atual = 0;
                                for ($j = 0; $j < $totalAnos; $j++) {
                                    $v_mes_atual += ($arrV[$arrVAno[$j]][$i]);
                                }
                                echo round($v_mes_atual, 2) . ", ";
                            }
                        }
                    } else {
                        for ($i = 0; $i < 12; $i++) {
                            echo "0, ";
                        }
                    }
                    ?>
                ],
                type: "line",
                fill: false,
                backgroundColor: "#E98E29",
                borderColor: "#E98E29",
                borderWidth: 2,
                lineTension: 0
            },
            {
                label: 'Flex min',
                data: [
                    <?php
                    if ($totalAnos) {
                        if ($totalAnos == 1) {
                            for ($i = 0; $i < $totalAnos; $i++) {
                                for ($j = 0; $j < 12; $j++) {
                                    echo round(($arrV[$arrVAno[$i]][$j]) * (1 - ($arrFlex_min[$arrVAno[$i]] / 100)), 2) . ", ";
                                }
                            }
                        } else {
                            //caso onde tem mais de 1 unidade por ano:
                            for ($i = 0; $i < 12; $i++) {
                                $v_mes_atual = 0;
                                for ($j = 0; $j < $totalAnos; $j++) {
                                    $v_mes_atual += ($arrV[$arrVAno[$j]][$i]) * (1 - ($arrFlex_min[$arrVAno[$j]] / 100));
                                }
                                echo round($v_mes_atual, 2) . ", ";
                            }
                        }
                    } else {
                        for ($i = 0; $i < 12; $i++) {
                            echo "0, ";
                        }
                    }
                    ?>
                ],
                type: "line",
                fill: false,
                backgroundColor: "#c8c8c8",
                borderColor: "#c8c8c8",
                borderWidth: 2,
                lineTension: 0,
                borderDash: [5, 5],
            }
        ]
    };

    var ctxComercializadora = document.getElementById('chartLineMontantes').getContext('2d');
    window.myBar = new Chart(ctxComercializadora, {
        type: 'line',
        data: lineChartComercializadoras,
        options: {
            responsive: false,
            maintainAspectRatio: true,
            legend: {
                position: 'top',
            },
            title: {
                display: false,
                text: 'Comercializadoras'
            },
            animation: {
                animateScale: true,
                animateRotate: true,
                duration: 2000,
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                    },
                }, ]
            }
        }
    });
</script>