<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//Pegar id:
if(isset($_SESSION['usuario'])){
    @$arrayIdOption = [];
    @$urlUnidades = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=".$_SESSION['usuario'];
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheUnidades = file_get_contents($urlUnidades, false, $context);
    @$resultUnidades = json_decode($recolheUnidades);
    if(isset($resultUnidades)){
        foreach($resultUnidades as $unidades){
            $id_option = $unidades->id_unidades;
            //array push:
            array_push($arrayIdOption, $id_option);
        }
    }
}


//Pegar ultimo registro de data para consumo dia:
$armazenaItem = [];
$dataFinal2 = date('Y-m-d');
while(empty($armazenaItem)){
    @$urlFatorPotencia = "https://api.develop.clientes.witzler.com.br/api/periodo/fator/potencia/hora?data_busca=".$dataFinal2."&unidade_id=".$arrayIdOption[0];
    @$recolheFatorPotencia = file_get_contents($urlFatorPotencia, false, $context);
    @$resultFatorPotencia = json_decode($recolheFatorPotencia);
    if(isset($resultFatorPotencia)){
        foreach($resultFatorPotencia as $fatorPotencia){
            $item = $fatorPotencia->hora_do_dia;
            array_push($armazenaItem, $item);
        }
        $ultimaData = $dataFinal2;
    }
    
    $dataFinal2 = date("Y-m-d", strtotime($dataFinal2."- 1 days"));
}
?>