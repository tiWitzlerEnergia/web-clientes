<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//if((isset($_COOKIE['id_unidades']) && isset($_COOKIE['data_fator_potencia'])) || (isset($arrayIdOption[0]) && isset($_COOKIE['data_fator_potencia']))){
if((isset($arrayIdOption[0]) && isset($ultimaData)) || (isset($_COOKIE['id_unidades']) && isset($_COOKIE['data_fator_potencia']))){
    //Declara variaveis para as requisicoes na api:
    if(isset($_COOKIE['data_fator_potencia'])){
        $dataFatorPotencia = $_COOKIE['data_fator_potencia'];
    }else{
        $dataFatorPotencia = $ultimaData;
    }
    if(isset($_COOKIE['id_unidades'])){
        $id_unidades_fator_potencia = $_COOKIE['id_unidades'];
    }else{
        $id_unidades_fator_potencia = $arrayIdOption[0];
    }
    //Declara arrays para armazenar os dados:
    $listaCapacitivo = [];
    $listaHora = [];
    //Config php para chamar os valores da api com as variaveis:
    $urlFatorPotencia = "https://api.develop.clientes.witzler.com.br/api/periodo/fator/potencia/hora?data_busca=".$dataFatorPotencia."&unidade_id=".$id_unidades_fator_potencia;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheFatorPotencia = file_get_contents($urlFatorPotencia, false, $context);
    @$resultFatorPotencia = json_decode($recolheFatorPotencia);
    if(isset($resultFatorPotencia)){
        foreach($resultFatorPotencia as $fatorPotencia){
            $unidadeCapacitivo = $fatorPotencia->capacitivo;
            $unidadeHora = $fatorPotencia->hora_do_dia;
            //Formatando a hora:
            $unidadeHora = $unidadeHora-1;
            if($unidadeHora < 10){
                $unidadeHora = "0$unidadeHora";
            }
            //Formatando Unidade:
            $unidadeCapacitivo = number_format($unidadeCapacitivo, 2);
            //Adiciona nos arrays:
            array_push($listaCapacitivo, $unidadeCapacitivo);
            array_push($listaHora, $unidadeHora);
        }
        //Zera grafico em caso de nao ter dados da API
        if(empty($listaCapacitivo)){
            for($i = 0; $i < 24; $i++){
                array_push($listaCapacitivo, 0.00);
                if($i < 10){
                    $i = "0$i";
                }
                array_push($listaHora, $i);
            }
        }
    }else{
        if(empty($listaCapacitivo)){
        
            $listaCapacitivo = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            $listaHora = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"];
        }
    }
}else{
    if(empty($listaCapacitivo)){
        
        $listaCapacitivo = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $listaHora = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"];
    }
}
?>