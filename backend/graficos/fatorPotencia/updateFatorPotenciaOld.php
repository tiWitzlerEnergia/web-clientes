<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php session_start(); ?>
<canvas id="chartBarFatorPotencia" class="fade-in" width="1540" height="380" style="display: block; position: relative;"></canvas>

<?php
if(isset($_GET['id_unidades']) && isset($_GET['data_fator_potencia'])){
    //Declara variaveis para as requisicoes na api:
    //Formatar data
    $dataFatorPotencia_old = $_GET['data_fator_potencia'];
    $dataFatorPotencia = date('Y-m-d', strtotime($dataFatorPotencia_old));

    $id_unidades_fator_potencia = $_GET['id_unidades'];
    //Declara arrays para armazenar os dados:
    $listaCapacitivo = [];
    $listaHora = [];
    //Config php para chamar os valores da api com as variaveis:
    $urlFatorPotencia = "https://api.develop.clientes.witzler.com.br/api/periodo/fator/potencia/hora?data_busca=".$dataFatorPotencia."&unidade_id=".$id_unidades_fator_potencia;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheFatorPotencia = file_get_contents($urlFatorPotencia, false, $context);
    @$resultFatorPotencia = json_decode($recolheFatorPotencia);
    if(isset($resultFatorPotencia)){
        foreach($resultFatorPotencia as $fatorPotencia){
            $unidadeCapacitivo = $fatorPotencia->capacitivo;
            $unidadeHora = $fatorPotencia->hora_do_dia;
            //Formatando a hora:
            $unidadeHora = $unidadeHora-1;
            if($unidadeHora < 10){
                $unidadeHora = "0$unidadeHora";
            }
            //Formatando Unidade:
            $unidadeCapacitivo = number_format($unidadeCapacitivo, 2);
            //Adiciona nos arrays:
            array_push($listaCapacitivo, $unidadeCapacitivo);
            array_push($listaHora, $unidadeHora);
        }
        //Zera grafico em caso de nao ter dados da API
        if(empty($listaCapacitivo)){
            for($i = 0; $i < 24; $i++){
                array_push($listaCapacitivo, 0.00);
                if($i < 10){
                    $i = "0$i";
                }
                array_push($listaHora, $i);
            }
        }
    }else{
        if(empty($listaCapacitivo)){
        
            $listaCapacitivo = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            $listaHora = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"];
        }
    }
}else{
    if(empty($listaCapacitivo)){
        
        $listaCapacitivo = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $listaHora = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"];
    }
}
?>
<script>
    var barChartData5 = {
                labels: [
                    <?php
                        $tamanhoListaHora = count($listaHora);
                        for($i = 0; $i < $tamanhoListaHora; $i++){
                            echo "'".$listaHora[$i].":00', ";
                        }
                    ?>
                ],
                datasets: [{
                    type: 'bar',
                    label: 'Fator de potencia',
                    backgroundColor: "#ace2e2ab",
                    data: [
                        <?php
                            $tamanhoListaCapacitivo = count($listaCapacitivo);
                            for($i = 0; $i < $tamanhoListaCapacitivo; $i++){
                                echo $listaCapacitivo[$i].", ";
                            }    
                        ?>
                    ],
                },
                {
                    type: 'line', 
                    label: 'Limite indutivo',
                    data: [0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92, 0.92],
                    fill: false,
                    borderWidth: 1,
                    borderColor: 'rgba(255, 159, 64, 0.8)',
                    borderDash: [5,4],
                    lineTension: 0,
                    steppedLine: true
                },
                {
                    type: 'line', 
                    label: 'Limite capacitivo',
                    data: [-0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92, -0.92],
                    fill: false,
                    borderWidth: 1,
                    borderColor: 'rgba(255, 159, 64, 0.8)',
                    borderDash: [5,4],
                    lineTension: 0,
                    steppedLine: true
                }
                ]
    };
		
    var ctx5 = document.getElementById('chartBarFatorPotencia').getContext('2d');
        window.myBar = new Chart(ctx5, {
            type: 'bar',
            data: barChartData5,
            options: {
                responsive: false,
                maintainAspectRatio: true,
                legend:{
                    fullWidth : false,
                    position: "bottom",
                    
                    labels: {
                        /*fontSize: tamanho,*/
                        boxWidth: borda,
                        padding: 2
                    },
                },
                title: {
                    display: false,
                    text: 'Consumo por Posto Tarifário'
                },
                scales: {
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true
                        },
                        stacked: false,
                        ticks: {
                            /*fontSize: tamanho,*/
                            min: 0,
                            stepSize: 0.5
                        }
                    }],  
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true
                        },
                        stacked: false,
                        ticks: {
                            /*fontSize: tamanho,*/
                        }
                    }],
                },
                animation: {
					duration: 4000,
                },
                chartArea: {
                    width: '100%',
                    height: '100%',
                    padding: {
                        top: 0,
                        bottom: 0,
                    },
                },
            }
        });
</script>