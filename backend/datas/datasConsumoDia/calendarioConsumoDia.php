<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<input type="text" id="datasCalendarioConsumoDia" class="custom-select dropdown-menu-right dropdown2 dropdown" style="text-align: center;" onChange="updateCalendarioConsumoDia()"/>

<?php
if(isset($_COOKIE['id_unidades'])){
$id_unidades = $_COOKIE['id_unidades'];
$urlDatas = "https://api.develop.clientes.witzler.com.br/api/datas/arquivos/unidade?unidade_id=".$id_unidades;
$context = stream_context_create(array(
    'http' => array(
        'header' => "Authorization: Bearer ".$_SESSION['token'],
    ),
));
$recolheDatas = file_get_contents($urlDatas, false, $context);
$resultDatas = json_decode($recolheDatas);
foreach($resultDatas as $datas){
?>

<?php
}}
?>


<script type="text/javascript">
        var d = new Date();
        var dia = d.getDate();
        var mes = d.getMonth() + 1;
        var ano = d.getFullYear();
        //var dataCompleta = ano + "-" + mes + "-" + 01;
        //var dataCompleta = "<?php //echo $ultimaData; ?>";
        var dataCompleta = "<?php echo date('d-m-Y', strtotime($ultimaData)); ?>";

        function updateCalendarioConsumoDia() {
            //Configurando tempo de vida da cookie ao executar funçao:
            var tempoCookie = new Date();
            tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

            //Definindo valores da funçao:
            var selectCalendario = document.getElementById('datasCalendarioConsumoDia');
            var recojeCalendario = selectCalendario.value;
            console.log('Data nova de consumo dia:'+recojeCalendario);
            document.cookie = "data_consumo_dia = "+recojeCalendario+"; expires="+tempoCookie.toUTCString();
            
            /*var anoNovo = recojeCalendario.substring(4, 0);
            var mesNovo = recojeCalendario.substring(5, 7);*/
            var anoNovo = recojeCalendario.substring(6, 10);
            var mesNovo = recojeCalendario.substring(5, 3);
            var totalDias = new Date();
            /*var ultimo = new Date(anoNovo, mesNovo, 0);
            var ultimoDia = ultimo.toString("yyyy/mm/dd");
            var ultimoFormateado = ultimoDia.substring(0, 2);*/
            var ultimoFormateado = recojeCalendario.substring(0, 2);
            var finalNovaData = anoNovo+'-'+mesNovo+'-'+ultimoFormateado;
            //document.cookie = "final_mes_consumo_dia = "+finalNovaData+"; expires="+tempoCookie.toUTCString();
            //document.cookie = "ultimo_dia_consumo_dia = "+ultimoFormateado+"; expires="+tempoCookie.toUTCString();
            console.log('Final da data de consumo dia:'+finalNovaData);
            //console.log('Ultimo dia de consumo dia:'+ultimoFormateado);
        }
        
        //updateCalendarioConsumoDia();
        
        

        $('#datasCalendarioConsumoDia').datepicker({
            //format: "yyyy-mm-dd",
            format: "dd-mm-yyyy",
            //viewMod: "months",
            //minViewMode: "months",
            language: "pt-BR",
            zIndexOffset: 10000,
            //defaultDate: new Date()
        }).datepicker('update', dataCompleta);
</script>