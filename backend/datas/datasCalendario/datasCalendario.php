<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<input type="text" id="datasCalendario" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="updateDatasCalendario()"/>

<?php
if(isset($_COOKIE['id_unidades'])){
    $id_unidades = $_COOKIE['id_unidades'];
    $urlDatas = "https://api.develop.clientes.witzler.com.br/api/datas/arquivos/unidade?unidade_id=".$id_unidades;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheDatas = file_get_contents($urlDatas, false, $context);
    @$resultDatas = json_decode($recolheDatas);
    if(isset($resultDatas)){
        foreach($resultDatas as $datas){
?>

<?php
        }
    }
}
?>


<script type="text/javascript">

        var d = new Date();
        var dia = d.getDate();
        var mes = d.getMonth() + 1;
        var ano = d.getFullYear();
        //var dataCompleta = "<?php echo $ultimaData; ?>";
        var dataCompleta = "<?php echo date('m-Y', strtotime($ultimaData)); ?>";
        
        function updateDatasCalendario() {
            //Configurando tempo de vida da cookie ao executar funçao:
            var tempoCookie = new Date();
            tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

            //Definindo valores da funçao:
            var selectData = document.getElementById('datasCalendario');
            var recojeData = '01-'+selectData.value;
            console.log(recojeData);
            document.cookie = "data_calendario = "+recojeData+"; expires="+tempoCookie.toUTCString();

            /*var anoNovo = recojeData.substring(4, 0);
            var mesNovo = recojeData.substring(5, 7);*/
            var anoNovo = recojeData.substring(6, 10);
            var mesNovo = recojeData.substring(5, 3);
            var totalDias = new Date();
            var ultimo = new Date(anoNovo, mesNovo, 0);
            var ultimoDia = ultimo.toString("yyyy/mm/dd");
            var ultimoFormateado = ultimoDia.substring(8, 10);
            var finalNovaData = anoNovo+'-'+mesNovo+'-'+ultimoFormateado;
            document.cookie = "final_mes_calendario = "+finalNovaData+"; expires="+tempoCookie.toUTCString();
            document.cookie = "ultimo_dia_calendario = "+ultimoFormateado+"; expires="+tempoCookie.toUTCString();
            document.cookie = "mes_novo_calendario = "+mesNovo+"; expires="+tempoCookie.toUTCString();
            document.cookie = "ano_novo_calendario = "+anoNovo+"; expires="+tempoCookie.toUTCString();
            console.log('Final da data de calendario:'+finalNovaData);
            console.log('Ultimo dia de calendario:'+ultimoFormateado);
            console.log('Mes:'+mesNovo);
            console.log('Ano:'+anoNovo);
        }

        //updateDatasCalendario();


        $('#datasCalendario').datepicker({
            format: "mm-yyyy",
            viewMod: "months",
            minViewMode: "months",
            language: "pt-BR",
            zIndexOffset: 10000,
            //defaultDate: new Date()
        }).datepicker('update', dataCompleta);
</script>