<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<input type="text" id="datasCalendario" class="custom-select dropdown-menu-right dropdown2 dropdown" style="text-align: center;" onChange="updateCalendario()"/>

<?php
if(isset($_COOKIE['id_unidades'])){
    $id_unidades = $_COOKIE['id_unidades'];
    $urlDatas = "https://api.develop.clientes.witzler.com.br/api/datas/arquivos/unidade?unidade_id=".$id_unidades;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    $recolheDatas = file_get_contents($urlDatas, false, $context);
    $resultDatas = json_decode($recolheDatas);
    foreach($resultDatas as $datas){
?>

<?php
    }
}
?>


<script type="text/javascript">
        var d = new Date();
        var dia = d.getDate();
        var mes = d.getMonth() + 1;
        var ano = d.getFullYear();
        //var dataCompleta = "<?php echo $ultimaData; ?>";
        var dataCompleta = "<?php echo date('m-Y', strtotime($ultimaData)); ?>";
        //var teste = dia+"-"+mes+"-"+ano;

        $('#datasCalendario').datepicker({
            /*format: "yyyy-mm-dd",*/
            format: "mm-yyyy",
            viewMod: "months",
            minViewMode: "months",
            language: "pt-BR",
            zIndexOffset: 10000,
            //defaultDate: new Date()
        }).datepicker('update', dataCompleta);







        function updateCalendario() {
            //Configurando tempo de vida da cookie ao executar funçao:
            var tempoCookie = new Date();
            tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

            //Definindo valores da funçao:
            var selectCalendario = document.getElementById('datasCalendario');
            var recojeCalendario = "01-"+selectCalendario.value;
            console.log('Data nova:'+recojeCalendario);
            document.cookie = "calendario_unidade = "+recojeCalendario+"; expires="+tempoCookie.toUTCString();
            
            /*var anoNovo = recojeCalendario.substring(4, 0);
            var mesNovo = recojeCalendario.substring(5, 7);*/
            var anoNovo = recojeCalendario.substring(6, 10);
            var mesNovo = recojeCalendario.substring(5, 3);
            var totalDias = new Date();
            var ultimo = new Date(anoNovo, mesNovo, 0);
            var ultimoDia = ultimo.toString("yyyy/mm/dd");
            var ultimoFormateado = ultimoDia.substring(8, 10);
            var finalNovaData = anoNovo+'-'+mesNovo+'-'+ultimoFormateado;
            document.cookie = "final_mes = "+finalNovaData+"; expires="+tempoCookie.toUTCString();
            //document.cookie = "ultimo_dia ="+ultimoFormateado+"; expires="+tempoCookie.toUTCString();
            console.log('Final da data nova:'+finalNovaData);
            console.log('Ultimo dia:'+ultimoFormateado);
           
        }

        //updateCalendario();

</script>