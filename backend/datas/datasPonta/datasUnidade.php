<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<input type="text" id="datasUnidade" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="updateDatas();"/>

<?php
if(isset($_COOKIE['id_unidades'])){
    $id_unidades = $_COOKIE['id_unidades'];
    $urlDatas = "https://api.develop.clientes.witzler.com.br/api/datas/arquivos/unidade?unidade_id=".$id_unidades;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheDatas = file_get_contents($urlDatas, false, $context);
    @$resultDatas = json_decode($recolheDatas);
    if(isset($resultDatas)){
        foreach($resultDatas as $datas){
?>

<?php
        }
    }
}
?>


<script type="text/javascript">

        var d = new Date();
        var dia = d.getDate();
        var mes = d.getMonth() + 1;
        var ano = d.getFullYear();
        var dataCompleta = ano + "-" + mes + "-" + 01;
        
        function updateDatas() {
            //Configurando tempo de vida da cookie ao executar funçao:
            var tempoCookie = new Date();
            tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

            //Definindo valores da funçao:
            var selectData = document.getElementById('datasUnidade');
            var recojeData = selectData.value;
            console.log(recojeData);
            //console.log('TESTANDO')
            document.cookie = "data_unidade = "+recojeData+"; expires="+tempoCookie.toUTCString();
            
        }

        updateDatas();


        $('#datasUnidade').datepicker({
            format: "yyyy-mm-dd",
            viewMod: "months",
            minViewMode: "months",
            language: "pt-BR",
            zIndexOffset: 10000,
            //defaultDate: new Date()
        }).datepicker('update', dataCompleta);
        
</script>