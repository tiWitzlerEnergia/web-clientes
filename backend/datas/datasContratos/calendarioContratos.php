<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<input type="text" id="datasContratos" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="updateDatascontratos()"/>


<script type="text/javascript">
        var d = new Date();
        var dia = d.getDate();
        var mes = d.getMonth() + 1;
        var ano = d.getFullYear();
        //var dataCompleta = ano;
        var dataCompleta = "<?php echo date('Y'); ?>";

        function updateDatascontratos() {
            //Configurando tempo de vida da cookie ao executar funçao:
            var tempoCookie = new Date();
            tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

            //Definindo valores da funçao:
            var selectCalendario = document.getElementById('datasContratos');
            var recojeCalendario = selectCalendario.value;
            console.log('Data nova de datasContratos:'+recojeCalendario);
            document.cookie = "data_contratos = "+recojeCalendario+"; expires="+tempoCookie.toUTCString();
        }
    

        $('#datasContratos').datepicker({
            format: "yyyy",
            viewMod: "years",
            minViewMode: "years",
            language: "pt-BR",
            zIndexOffset: 10000,
            //defaultDate: new Date()
        }).datepicker('update', dataCompleta);
</script>