<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<input type="text" id="datasCalendarioFatorPotencia" class="custom-select dropdown-menu-right dropdown2 dropdown" style="text-align: center;" onChange="updateCalendarioFatorPotencia()"/>

<?php
if(isset($_COOKIE['id_unidades'])){
$id_unidades = $_COOKIE['id_unidades'];
$urlDatas = "https://api.develop.clientes.witzler.com.br/api/datas/arquivos/unidade?unidade_id=".$id_unidades;
$context = stream_context_create(array(
    'http' => array(
        'header' => "Authorization: Bearer ".$_SESSION['token'],
    ),
));
$recolheDatas = file_get_contents($urlDatas, false, $context);
$resultDatas = json_decode($recolheDatas);
foreach($resultDatas as $datas){
?>

<?php
}}
?>


<script type="text/javascript">
        var d = new Date();
        var dia = d.getDate();
        var mes = d.getMonth() + 1;
        var ano = d.getFullYear();
        //var dataCompleta = ano + "-" + mes + "-" + 01;
        var dataCompleta = "<?php echo $ultimaData; ?>";

        function updateCalendarioFatorPotencia() {
            //Configurando tempo de vida da cookie ao executar funçao:
            var tempoCookie = new Date();
            tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

            //Definindo valores da funçao:
            var selectCalendario = document.getElementById('datasCalendarioFatorPotencia');
            var recojeCalendario = selectCalendario.value;
            console.log('Data nova de fator potencia:'+recojeCalendario);
            document.cookie = "data_fator_potencia = "+recojeCalendario+"; expires="+tempoCookie.toUTCString();
            
            var anoNovo = recojeCalendario.substring(4, 0);
            var mesNovo = recojeCalendario.substring(5, 7);
            var totalDias = new Date();
            var ultimo = new Date(anoNovo, mesNovo, 0);
            var ultimoDia = ultimo.toString("yyyy/mm/dd");
            var ultimoFormateado = ultimoDia.substring(8, 10);
            var finalNovaData = anoNovo+'-'+mesNovo+'-'+ultimoFormateado;
            //document.cookie = "final_mes_fator_potencia = "+finalNovaData+"; expires="+tempoCookie.toUTCString();
            //document.cookie = "ultimo_dia_fator_potencia = "+ultimoFormateado+"; expires="+tempoCookie.toUTCString();
            console.log('Final da data de fator potencia:'+finalNovaData);
            //console.log('Ultimo dia de fator potencia:'+ultimoFormateado);
        }

        updateCalendarioFatorPotencia();

        // Não está funcionando

        $('#datasCalendarioFatorPotencia').datepicker({
            format: "yyyy-mm-dd",
            //viewMod: "months",
            //minViewMode: "months",
            language: "pt-BR",
            zIndexOffset: 10000,
            //defaultDate: new Date()
        }).datepicker('update', dataCompleta);
</script>