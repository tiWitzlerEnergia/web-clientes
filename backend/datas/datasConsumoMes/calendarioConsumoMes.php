<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

    
<input type="text" id="datasCalendarioConsumoMes" class="custom-select dropdown-menu-right dropdown2 dropdown" style="max-width: 50%; text-align: center;" onChange="updateCalendarioConsumoMes()"/>

<?php
if(isset($_COOKIE['id_unidades'])){
$id_unidades = $_COOKIE['id_unidades'];
$urlDatas = "https://api.develop.clientes.witzler.com.br/api/datas/arquivos/unidade?unidade_id=".$id_unidades;
$context = stream_context_create(array(
    'http' => array(
        'header' => "Authorization: Bearer ".$_SESSION['token'],
    ),
));
$recolheDatas = file_get_contents($urlDatas, false, $context);
$resultDatas = json_decode($recolheDatas);
foreach($resultDatas as $datas){
?>

<?php
}}
?>


<script type="text/javascript">
        var d = new Date();
        var dia = d.getDate();
        var mes = d.getMonth() + 1;
        var ano = d.getFullYear();
        var dataCompleta = ano + "-" + mes + "-" + 01;

        function updateCalendarioConsumoMes() {
            //Configurando tempo de vida da cookie ao executar funçao:
            var tempoCookie = new Date();
            tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

            //Definindo valores da funçao:
            var selectCalendario = document.getElementById('datasCalendarioConsumoMes');
            var recojeCalendario = selectCalendario.value;
            console.log('Data nova de consumo mes:'+recojeCalendario);
            document.cookie = "data_consumo_mes = "+recojeCalendario+"; expires="+tempoCookie.toUTCString();
            
            var anoNovo = recojeCalendario.substring(4, 0);
            var mesNovo = recojeCalendario.substring(5, 7);
            var totalMess = new Date();
            var ultimo = new Date(anoNovo, mesNovo, 0);
            var ultimoMes = ultimo.toString("yyyy/mm/dd");
            var ultimoFormateado = ultimoMes.substring(8, 10);
            var finalNovaData = anoNovo+'-'+mesNovo+'-'+ultimoFormateado;
            document.cookie = "final_dia_consumo_mes = "+finalNovaData+"; expires="+tempoCookie.toUTCString();
            //document.cookie = "ultimo_dia_consumo_mes ="+ultimoFormateado+"; expires="+tempoCookie.toUTCString();
            console.log('Final da data de consumo mes:'+finalNovaData);
            console.log('Ultimo mes de consumo mes:'+ultimoFormateado);
        }

        //updateCalendarioConsumoMes();

        

        $('#datasCalendarioConsumoMes').datepicker({
            format: "yyyy-mm-dd",
            viewMod: "months",
            minViewMode: "months",
            language: "pt-BR",
            zIndexOffset: 10000,
            //defaultDate: new Date()
        }).datepicker('update', dataCompleta);
</script>