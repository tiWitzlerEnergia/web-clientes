<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<input type="text" id="dataTipoArquivoCliente" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="updateDataTipoArquivoCliente()"/>
<?php
if(isset($_COOKIE['id_unidades'])){
    $id_unidades = $_COOKIE['id_unidades'];
    $urlDataTipoArquivoCliente = "https://api.develop.clientes.witzler.com.br/api/datas/arquivos/unidade?unidade_id=".$id_unidades;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheDataTipoArquivoCliente = file_get_contents($urlDataTipoArquivoCliente, false, $context);
    @$resultDataTipoArquivoCliente = json_decode($recolheDataTipoArquivoCliente);
    if(isset($resultDataTipoArquivoCliente)){
        foreach($resultDataTipoArquivoCliente as $dataTipoArquivoCliente){
            //Config da escolha das datas
        }
    }
}
?>
<script type="text/javascript">

    var d = new Date();
    var dia = d.getDate();
    var mes = d.getMonth() + 1;
    var ano = d.getFullYear();
    var dataCompleta = ano + "-" + mes + "-" + 01;

    function updateDataTipoArquivoCliente() {
        //Configurando tempo de vida da cookie ao executar funçao:
        var tempoCookie = new Date();
        tempoCookie.setTime(tempoCookie.getTime() + 1*3600*1000);

        //Definindo valores da funçao:
        var selectDataTipoArquivoCliente = document.getElementById('dataTipoArquivoCliente');
        var recojeDataTipoArquivoCliente = selectDataTipoArquivoCliente.value;
        console.log(recojeDataTipoArquivoCliente);
        document.cookie = "data_tipo_arquivo_cliente = "+recojeDataTipoArquivoCliente+"; expires="+tempoCookie.toUTCString();
    }

    //updateDataTipoArquivoCliente();


    $('#dataTipoArquivoCliente').datepicker({
        format: "yyyy-mm-dd",
        viewMod: "months",
        minViewMode: "months",
        language: "pt-BR",
        zIndexOffset: 10000,
        //defaultDate: new Date()
    }).datepicker('update', dataCompleta);
</script>