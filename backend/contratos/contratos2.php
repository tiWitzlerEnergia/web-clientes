<?php
$context = stream_context_create(array(
    'http' => array(
        'header' => "Authorization: Bearer " . $_SESSION['token'],
    ),
));

if (isset($_COOKIE['id_unidades']) || isset($arrayIdOption[0])) {
    $id_unidades_contrato =  isset($arrayIdOption[0]) ? $arrayIdOption[0] : $_COOKIE['id_unidades'];
    $u = 0;
    //Config para datas
    setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');

    $arrAno = [];
    $arrCow_id_contrato = [];
    $arrE_flex = [];
    $arrE_sazo = [];
    $arrFlex_max = [];
    $arrFlex_min = [];
    $arrId = [];
    $arrId_cliente = [];
    $arrId_distribuidora = [];
    $arrId_montantes = [];
    $arrN_contract = [];
    $arrP_carga = [];
    $arrPreco = [];
    $arrSazo_max = [];
    $arrSazo_min = [];
    $arrVEscolhido = [];
    $arrNVEscolhido = [];

    $urlContratos = "https://api.develop.clientes.witzler.com.br/api/contrato2?id_unidades=" . $id_unidades_contrato;
    @$recolheContratos = file_get_contents($urlContratos, false, $context);
    @$resultContratos = json_decode($recolheContratos);
    foreach ($resultContratos as $contratos) {
        $arrV[$u] = [];

        $ano = $contratos->ano;
        $cow_id_contrato = $contratos->cow_id_contrato;
        $e_flex = $contratos->e_flex;
        $e_sazo = $contratos->e_sazo;
        $flex_max = $contratos->flex_max;
        $flex_min = $contratos->flex_min;
        $id = $contratos->id;
        $id_cliente = $contratos->id_cliente;
        $id_distribuidora = $contratos->id_distribuidora;
        $id_montantes = $contratos->id_montantes;
        $n_contract = $contratos->n_contract;
        $p_carga = $contratos->p_carga;
        $preco = $contratos->preco;
        $sazo_max = $contratos->sazo_max;
        $sazo_min = $contratos->sazo_min;
        $v1 = $contratos->v1;
        $v2 = $contratos->v2;
        $v3 = $contratos->v3;
        $v4 = $contratos->v4;
        $v5 = $contratos->v5;
        $v6 = $contratos->v6;
        $v7 = $contratos->v7;
        $v8 = $contratos->v8;
        $v9 = $contratos->v9;
        $v10 = $contratos->v10;
        $v11 = $contratos->v11;
        $v12 = $contratos->v12;

        //array_push:
        array_push($arrAno, $ano);
        array_push($arrCow_id_contrato, $cow_id_contrato);
        array_push($arrE_flex, $e_flex);
        array_push($arrE_sazo, $e_sazo);
        array_push($arrFlex_max, $flex_max);
        array_push($arrFlex_min, $flex_min);
        array_push($arrId, $id);
        array_push($arrId_cliente, $id_cliente);
        array_push($arrId_distribuidora, $id_distribuidora);
        array_push($arrId_montantes, $id_montantes);
        array_push($arrN_contract, $n_contract);
        array_push($arrP_carga, $p_carga);
        array_push($arrPreco, $preco);
        array_push($arrSazo_max, $sazo_max);
        array_push($arrSazo_min, $sazo_min);
        array_push($arrV[$u], $v1);
        array_push($arrV[$u], $v2);
        array_push($arrV[$u], $v3);
        array_push($arrV[$u], $v4);
        array_push($arrV[$u], $v5);
        array_push($arrV[$u], $v6);
        array_push($arrV[$u], $v7);
        array_push($arrV[$u], $v8);
        array_push($arrV[$u], $v9);
        array_push($arrV[$u], $v10);
        array_push($arrV[$u], $v11);
        array_push($arrV[$u], $v12);

        $u++;
    }
}

$totalMontante = count($arrId);
$arrVAno = [];
$arrMontante = [];
$arrMontanteId = [];
for ($i = 0; $i < $totalMontante; $i++) {
    if ($arrAno[$i] == $ano_atual_contrato) {
        $ano_montante_id = $i;
        array_push($arrVAno, $ano_montante_id);
    }
}
$totalAnos = count($arrVAno);

//Fazendo soma de v1 a v12 separado por ano:
for ($i = 0; $i < $totalAnos; $i++) {
    //v_unidade é a soma total de todos os v1 dada uma comercializadora
    $v_unidade[$i] = 0;
    for ($j = 0; $j < 12; $j++) {
        $v_unidade[$i] += $arrV[$arrVAno[$i]][$j];
    }
    //Agora criamos uma variável que vai ter $id_comercializadora + soma total dos v
    $valor_montante = $arrId_distribuidora[$arrVAno[$i]] . "/" . $v_unidade[$i];

    array_push($arrMontanteId, $arrId_distribuidora[$arrVAno[$i]]);
    array_push($arrMontante, $valor_montante);
}
$limiteMontante = count($arrMontante);

function get_duplicates($array)
{
    return array_unique(array_diff_assoc($array, array_unique($array)));
}

$list = get_duplicates($arrMontanteId);

$arrSemRepeticoes = [];
for ($i = 0; $i < $limiteMontante; $i++) {
    if (isset($arrMontante[$i])) {
        $arr_valor_atual = explode("/", $arrMontante[$i]);
        $id_atual = $arr_valor_atual[0];
        $valor_atual = $arr_valor_atual[1];
        for ($j =  0; $j < $limiteMontante; $j++) {
            $arr_valor_passagem = explode("/", $arrMontante[$j]);
            $id_passagem = $arr_valor_passagem[0];
            $valor_passagem = $arr_valor_passagem[1];
            if (($id_atual == $id_passagem) && ($i != $j) && ($i < $j)) {
                $valor_atual += $valor_passagem;
                array_splice($arrMontante, $j);
            }
        }
        $var = $id_atual."/".$valor_atual;
        array_push($arrSemRepeticoes, $var);
        echo "<script>console.log('Posicao $id_atual: $valor_atual')</script>";
    }
}

//recolhe comercializadora
$totalComercializadora = count($arrSemRepeticoes);
$somaTotalComercializadora = 0;
if ($totalComercializadora > 0) {
    $arrNomeComercializadora = [];
    $arrPath = [];
    for ($i = 0; $i < $totalComercializadora; $i++) {
        $comercializadora_atual = explode("/", $arrSemRepeticoes[$i]);
        $id_comercializadora_atual = $comercializadora_atual[0];
        $urlComercializadora = "https://api.develop.clientes.witzler.com.br/api/comercializadora?id_comercializadora=" . $id_comercializadora_atual;
        @$recolheComercializadora = file_get_contents($urlComercializadora, false, $context);
        @$resultComercializadora = json_decode($recolheComercializadora);
        foreach ($resultComercializadora as $comercializadora) {
            $nome_comercializadora = $comercializadora->nome;
            $path_comercializadora = $comercializadora->path;

            array_push($arrNomeComercializadora, $nome_comercializadora);
            array_push($arrPath, $path_comercializadora);
        }

        $somaTotalComercializadora += $comercializadora_atual[1];
    }
}

echo "<script>console.log($somaTotalComercializadora);</script>";