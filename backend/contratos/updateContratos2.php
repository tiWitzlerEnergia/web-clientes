<?php
session_start();
?>
<canvas id="chartLineMontantes" class="mx-auto fade-in" width="510" height="280" style="display: block;"></canvas>
<script>
    var labelsChart = [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro',
    ];
</script>
<?php
if (isset($_GET['id_unidades']) && isset($_GET['ano']) && isset($_GET['tipo'])) {
    $id_unidades_contrato =  $_GET['id_unidades'];
    $ano_request = $_GET['ano'];
    $tipo = $_GET['tipo'];
    $u = 0;
    //Config para datas
    setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');

    $arrAno = [];
    $arrCow_id_contrato = [];
    $arrE_flex = [];
    $arrE_sazo = [];
    $arrFlex_max = [];
    $arrFlex_min = [];
    $arrId = [];
    $arrId_cliente = [];
    $arrId_distribuidora = [];
    $arrId_montantes = [];
    $arrN_contract = [];
    $arrP_carga = [];
    $arrPreco = [];
    $arrSazo_max = [];
    $arrSazo_min = [];
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    $urlContratos = "https://api.develop.clientes.witzler.com.br/api/contrato2?id_unidades=" . $id_unidades_contrato;
    @$recolheContratos = file_get_contents($urlContratos, false, $context);
    @$resultContratos = json_decode($recolheContratos);
    foreach ($resultContratos as $contratos) {

        $arrV[$u] = [];
        $ano = $contratos->ano;
        $cow_id_contrato = $contratos->cow_id_contrato;
        $e_flex = $contratos->e_flex;
        $e_sazo = $contratos->e_sazo;
        $flex_max = $contratos->flex_max;
        $flex_min = $contratos->flex_min;
        $id = $contratos->id;
        $id_cliente = $contratos->id_cliente;
        $id_distribuidora = $contratos->id_distribuidora;
        $id_montantes = $contratos->id_montantes;
        $n_contract = $contratos->n_contract;
        $p_carga = $contratos->p_carga;
        $preco = $contratos->preco;
        $sazo_max = $contratos->sazo_max;
        $sazo_min = $contratos->sazo_min;
        $v1 = $contratos->v1;
        $v2 = $contratos->v2;
        $v3 = $contratos->v3;
        $v4 = $contratos->v4;
        $v5 = $contratos->v5;
        $v6 = $contratos->v6;
        $v7 = $contratos->v7;
        $v8 = $contratos->v8;
        $v9 = $contratos->v9;
        $v10 = $contratos->v10;
        $v11 = $contratos->v11;
        $v12 = $contratos->v12;

        //array_push:
        array_push($arrAno, $ano);
        array_push($arrCow_id_contrato, $cow_id_contrato);
        array_push($arrE_flex, $e_flex);
        array_push($arrE_sazo, $e_sazo);
        array_push($arrFlex_max, $flex_max);
        array_push($arrFlex_min, $flex_min);
        array_push($arrId, $id);
        array_push($arrId_cliente, $id_cliente);
        array_push($arrId_distribuidora, $id_distribuidora);
        array_push($arrId_montantes, $id_montantes);
        array_push($arrN_contract, $n_contract);
        array_push($arrP_carga, $p_carga);
        array_push($arrPreco, $preco);
        array_push($arrSazo_max, $sazo_max);
        array_push($arrSazo_min, $sazo_min);
        array_push($arrV[$u], $v1);
        array_push($arrV[$u], $v2);
        array_push($arrV[$u], $v3);
        array_push($arrV[$u], $v4);
        array_push($arrV[$u], $v5);
        array_push($arrV[$u], $v6);
        array_push($arrV[$u], $v7);
        array_push($arrV[$u], $v8);
        array_push($arrV[$u], $v9);
        array_push($arrV[$u], $v10);
        array_push($arrV[$u], $v11);
        array_push($arrV[$u], $v12);

        $u++;
    }
}
?>

<?php
$totalMontante = count($arrId);
$arrVAno = [];
for ($i = 0; $i < $totalMontante; $i++) {
    if ($arrAno[$i] == $ano_request) {
        $ano_montante_id = $i;
        array_push($arrVAno, $ano_montante_id);
    }
}
$totalAnos = count($arrVAno);
?>

<script>
    console.log(<?php echo $ano_request; ?>);
    var lineChartComercializadoras = {
        labels: labelsChart,
        datasets: [{
                label: 'Flex máx',
                data: [
                    <?php
                    if ($totalAnos) {
                        if ($totalAnos == 1) {
                            for ($i = 0; $i < $totalAnos; $i++) {
                                for ($j = 0; $j < 12; $j++) {
                                    echo round(($arrV[$arrVAno[$i]][$j]) * (1 + ($arrFlex_max[$arrVAno[$i]] / 100)), 2) . ", ";
                                }
                            }
                        } else {
                            //caso onde tem mais de 1 unidade por ano:
                            for ($i = 0; $i < 12; $i++) {
                                $v_mes_atual = 0;
                                for ($j = 0; $j < $totalAnos; $j++) {
                                    $v_mes_atual += ($arrV[$arrVAno[$j]][$i]) * (1 + ($arrFlex_max[$arrVAno[$j]] / 100));
                                }
                                echo round($v_mes_atual, 2) . ", ";
                            }
                        }
                    } else {
                        for ($i = 0; $i < 12; $i++) {
                            echo "0, ";
                        }
                    }
                    ?>
                ],
                type: "line",
                fill: false,
                backgroundColor: "#c8c8c8",
                borderColor: "#c8c8c8",
                borderWidth: 2,
                lineTension: 0,
                borderDash: [5, 5],
            },
            {
                label: 'Valor médio',
                data: [
                    <?php
                    if ($totalAnos) {
                        if ($totalAnos == 1) {
                            for ($i = 0; $i < $totalAnos; $i++) {
                                for ($j = 0; $j < 12; $j++) {
                                    echo round(($arrV[$arrVAno[$i]][$j]), 2) . ", ";
                                }
                            }
                        } else {
                            //caso onde tem mais de 1 unidade por ano:
                            for ($i = 0; $i < 12; $i++) {
                                $v_mes_atual = 0;
                                for ($j = 0; $j < $totalAnos; $j++) {
                                    $v_mes_atual += ($arrV[$arrVAno[$j]][$i]);
                                }
                                echo round($v_mes_atual, 2) . ", ";
                            }
                        }
                    } else {
                        for ($i = 0; $i < 12; $i++) {
                            echo "0, ";
                        }
                    }
                    ?>
                ],
                type: "line",
                fill: false,
                backgroundColor: "#E98E29",
                borderColor: "#E98E29",
                borderWidth: 2,
                lineTension: 0
            },
            {
                label: 'Flex min',
                data: [
                    <?php
                    if ($totalAnos) {
                        if ($totalAnos == 1) {
                            for ($i = 0; $i < $totalAnos; $i++) {
                                for ($j = 0; $j < 12; $j++) {
                                    echo round(($arrV[$arrVAno[$i]][$j]) * (1 - ($arrFlex_min[$arrVAno[$i]] / 100)), 2) . ", ";
                                }
                            }
                        } else {
                            //caso onde tem mais de 1 unidade por ano:
                            for ($i = 0; $i < 12; $i++) {
                                $v_mes_atual = 0;
                                for ($j = 0; $j < $totalAnos; $j++) {
                                    $v_mes_atual += ($arrV[$arrVAno[$j]][$i]) * (1 - ($arrFlex_min[$arrVAno[$j]] / 100));
                                }
                                echo round($v_mes_atual, 2) . ", ";
                            }
                        }
                    } else {
                        for ($i = 0; $i < 12; $i++) {
                            echo "0, ";
                        }
                    }
                    ?>
                ],
                type: "line",
                fill: false,
                backgroundColor: "#c8c8c8",
                borderColor: "#c8c8c8",
                borderWidth: 2,
                lineTension: 0,
                borderDash: [5, 5],
            }
        ]
    };

    var ctxComercializadora = document.getElementById('chartLineMontantes').getContext('2d');
    window.myBar = new Chart(ctxComercializadora, {
        type: 'line',
        data: lineChartComercializadoras,
        options: {
            responsive: false,
            maintainAspectRatio: true,
            legend: {
                position: 'top',
            },
            title: {
                display: false,
                text: 'Comercializadoras'
            },
            animation: {
                animateScale: true,
                animateRotate: true,
                duration: 2000,
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                    },
                }, ]
            }
        }
    });
</script>