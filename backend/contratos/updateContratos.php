<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php session_start(); ?>
<?php
if (isset($_GET['id_unidades']) && isset($_GET['ano'])) {
    $id_unidades_contrato =  $_GET['id_unidades'];
    $ano_request = $_GET['ano'];
    $u = 0;
    //Config para datas
    setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');

    $arrAno = [];
    $arrCow_id_contrato = [];
    $arrE_flex = [];
    $arrE_sazo = [];
    $arrFlex_max = [];
    $arrFlex_min = [];
    $arrId = [];
    $arrId_cliente = [];
    $arrId_distribuidora = [];
    $arrId_montantes = [];
    $arrN_contract = [];
    $arrP_carga = [];
    $arrPreco = [];
    $arrSazo_max = [];
    $arrSazo_min = [];
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    $urlContratos = "https://api.develop.clientes.witzler.com.br/api/contrato2?id_unidades=" . $id_unidades_contrato;
    @$recolheContratos = file_get_contents($urlContratos, false, $context);
    @$resultContratos = json_decode($recolheContratos);
    foreach ($resultContratos as $contratos) {

        $arrV[$u] = [];
        $ano = $contratos->ano;
        $cow_id_contrato = $contratos->cow_id_contrato;
        $e_flex = $contratos->e_flex;
        $e_sazo = $contratos->e_sazo;
        $flex_max = $contratos->flex_max;
        $flex_min = $contratos->flex_min;
        $id = $contratos->id;
        $id_cliente = $contratos->id_cliente;
        $id_distribuidora = $contratos->id_distribuidora;
        $id_montantes = $contratos->id_montantes;
        $n_contract = $contratos->n_contract;
        $p_carga = $contratos->p_carga;
        $preco = $contratos->preco;
        $sazo_max = $contratos->sazo_max;
        $sazo_min = $contratos->sazo_min;
        $v1 = $contratos->v1;
        $v2 = $contratos->v2;
        $v3 = $contratos->v3;
        $v4 = $contratos->v4;
        $v5 = $contratos->v5;
        $v6 = $contratos->v6;
        $v7 = $contratos->v7;
        $v8 = $contratos->v8;
        $v9 = $contratos->v9;
        $v10 = $contratos->v10;
        $v11 = $contratos->v11;
        $v12 = $contratos->v12;

        //array_push:
        array_push($arrAno, $ano);
        array_push($arrCow_id_contrato, $cow_id_contrato);
        array_push($arrE_flex, $e_flex);
        array_push($arrE_sazo, $e_sazo);
        array_push($arrFlex_max, $flex_max);
        array_push($arrFlex_min, $flex_min);
        array_push($arrId, $id);
        array_push($arrId_cliente, $id_cliente);
        array_push($arrId_distribuidora, $id_distribuidora);
        array_push($arrId_montantes, $id_montantes);
        array_push($arrN_contract, $n_contract);
        array_push($arrP_carga, $p_carga);
        array_push($arrPreco, $preco);
        array_push($arrSazo_max, $sazo_max);
        array_push($arrSazo_min, $sazo_min);
        array_push($arrV[$u], $v1);
        array_push($arrV[$u], $v2);
        array_push($arrV[$u], $v3);
        array_push($arrV[$u], $v4);
        array_push($arrV[$u], $v5);
        array_push($arrV[$u], $v6);
        array_push($arrV[$u], $v7);
        array_push($arrV[$u], $v8);
        array_push($arrV[$u], $v9);
        array_push($arrV[$u], $v10);
        array_push($arrV[$u], $v11);
        array_push($arrV[$u], $v12);

        $u++;
    }
}
if (isset($arrId)) {
    $totalMontante = count($arrId);
    $arrVAno = [];
    $arrMontante = [];
    $arrMontanteId = [];
    for ($i = 0; $i < $totalMontante; $i++) {
        if ($arrAno[$i] == $ano_request) {
            $ano_montante_id = $i;
            array_push($arrVAno, $ano_montante_id);
        }
    }
    $totalAnos = count($arrVAno);
}

if (isset($totalAnos)) {
    //Fazendo soma de v1 a v12 separado por ano:
    for ($i = 0; $i < $totalAnos; $i++) {
        //v_unidade é a soma total de todos os v1 dada uma comercializadora
        $v_unidade[$i] = 0;
        for ($j = 0; $j < 12; $j++) {
            $v_unidade[$i] += $arrV[$arrVAno[$i]][$j];
        }
        //Agora criamos uma variável que vai ter $id_comercializadora + soma total dos v
        $valor_montante = $arrId_distribuidora[$arrVAno[$i]] . "/" . $v_unidade[$i];

        array_push($arrMontanteId, $arrId_distribuidora[$arrVAno[$i]]);
        array_push($arrMontante, $valor_montante);
    }
    $limiteMontante = count($arrMontante);
}

if (isset($arrMontante)) {
    //recolhe comercializadora
    $totalComercializadora = count($arrMontante);
    if ($totalComercializadora > 0) {
        $arrNomeComercializadora = [];
        $arrPath = [];
        for ($i = 0; $i < $totalComercializadora; $i++) {
            $comercializadora_atual = explode("/", $arrMontante[$i]);
            $id_comercializadora_atual = $comercializadora_atual[0];
            $urlComercializadora = "https://api.develop.clientes.witzler.com.br/api/comercializadora?id_comercializadora=" . $id_comercializadora_atual;
            @$recolheComercializadora = file_get_contents($urlComercializadora, false, $context);
            @$resultComercializadora = json_decode($recolheComercializadora);
            foreach ($resultComercializadora as $comercializadora) {
                $nome_comercializadora = $comercializadora->nome;
                $path_comercializadora = $comercializadora->path;

                array_push($arrNomeComercializadora, $nome_comercializadora);
                array_push($arrPath, $path_comercializadora);
            }
        }
    }
}

$contarChart = 1;

if (empty($arrMontante)) {
?>
    <script>
        $("#updateContratos").html("<div id='loadContratos' class='col-12 d-flex align-items-center text-center justify-content-center' style='height: 550px; background: white;'><span class='loader'></span></div>");
        setTimeout(function() {
            document.getElementById('loadContratos').remove();
            for (var i = 0; i < <?php echo $tamanhoContratos; ?>; i++) {
                document.getElementById('contrato').style.display = 'block';
            }
        }, 2500);
    </script>
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 fade-in" id="contrato">
        <div class="card-accent-secondary card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td style="border-top: 0;">
                                    <div class="col-12 container row noPadding noMargin">
                                        <div class="col-12 noPadding text-center">
                                            <h1>Sem contratos</h1>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php
} else {
    for ($i = 0; $i < $totalAnos; $i++) {
    ?>
        <div class="col-12 col-sm-6 col-md-6 col-lg-4 fade-in">
            <div class="card-accent-secondary card">
                <div class="text-center h-25 card-header">
                    <div class="row">
                        <div class="col">
                            <div class="mb-0 pt-3 card-title">
                                <img height="50" src="https://clientes.witzler.com.br/backend/assets/img/distributor/<?php echo $arrPath[$i]; ?>">
                                <hr>
                                <h5 class="mt-3"><?php echo "Janeiro/" . $arrAno[$arrVAno[$i]] . " A " . "Dezembro/" . $arrAno[$arrVAno[$i]]; ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="col-12 container row noPadding noMargin">
                                            <div class="col-6 noPadding text-left">
                                                Comercializadora:
                                            </div>
                                            <div class="col-6 noPadding text-right">
                                                <?php echo $arrNomeComercializadora[$i]; ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12 container row noPadding noMargin">
                                            <div class="col-6 noPadding text-left">
                                                Número do contrato:
                                            </div>
                                            <div class="col-6 noPadding text-right">
                                                <?php echo $arrN_contract[$arrVAno[$i]]; ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12 container row noPadding noMargin">
                                            <div class="col-6 noPadding text-left">
                                                Preço Contratado [R$/MWh]:
                                            </div>
                                            <div class="col-6 noPadding text-right">
                                                <?php echo round($arrPreco[$arrVAno[$i]], 2); ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12 container row noPadding noMargin">
                                            <div class="col-6 noPadding text-left">
                                                Flexibilidade:
                                            </div>
                                            <div class="col-6 noPadding text-right">
                                                <?php echo $arrFlex_min[$arrVAno[$i]] . "/" . $arrFlex_max[$arrVAno[$i]]; ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12 container row noPadding noMargin">
                                            <div class="col-6 noPadding text-left">
                                                Sazonalidade:
                                            </div>
                                            <div class="col-6 noPadding text-right">
                                                <?php echo $arrSazo_min[$arrVAno[$i]] . "/" . $arrSazo_max[$arrVAno[$i]]; ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="height: 250px; ">

                                            <div class="chartWrapper" style="position: relative;">
                                                <div class="chartAreaWrapper" id="graficoContrato<?php echo $contarChart; ?>" style="overflow-x: auto;">
                                                    <canvas id="chartDoughnutContrato<?php echo $contarChart; ?>" height="250" width="400" class="mx-auto" style="display: block;"></canvas>
                                                </div>
                                            </div>
                                            <script>
                                                var barChartContrato<?php echo $contarChart; ?> = {
                                                    labels: labelsChart,
                                                    datasets: [{
                                                            label: 'Flex máx',
                                                            data: [
                                                                <?php
                                                                for ($j = 0; $j < 12; $j++) {
                                                                    echo round(($arrV[$arrVAno[$i]][$j]) * (1 + ($arrFlex_max[$arrVAno[$i]] / 100)), 2) .
                                                                        ", ";
                                                                }
                                                                ?>
                                                            ],
                                                            type: "line",
                                                            fill: false,
                                                            backgroundColor: "#c8c8c8",
                                                            borderColor: "#c8c8c8",
                                                            borderWidth: 2,
                                                            lineTension: 0,
                                                            borderDash: [5, 5],
                                                        },
                                                        {
                                                            label: 'Valor médio',
                                                            data: [
                                                                <?php
                                                                for ($j = 0; $j < 12; $j++) {
                                                                    echo round(($arrV[$arrVAno[$i]][$j]), 2) .
                                                                        ", ";
                                                                }
                                                                ?>
                                                            ],
                                                            type: "line",
                                                            fill: false,
                                                            backgroundColor: "#E98E29",
                                                            borderColor: "#E98E29",
                                                            borderWidth: 2,
                                                            lineTension: 0
                                                        },
                                                        {
                                                            label: 'Flex min',
                                                            data: [
                                                                <?php
                                                                for ($j = 0; $j < 12; $j++) {
                                                                    echo round(($arrV[$arrVAno[$i]][$j]) * (1 - ($arrFlex_min[$arrVAno[$i]] / 100)), 2) . ", ";
                                                                }
                                                                ?>
                                                            ],
                                                            type: "line",
                                                            fill: false,
                                                            backgroundColor: "#c8c8c8",
                                                            borderColor: "#c8c8c8",
                                                            borderWidth: 2,
                                                            lineTension: 0,
                                                            borderDash: [5, 5],
                                                        }
                                                    ]
                                                };

                                                var ctxContrato<?php echo $contarChart; ?> = document.getElementById('chartDoughnutContrato<?php echo $contarChart; ?>').getContext('2d');
                                                window.myBar = new Chart(ctxContrato<?php echo $contarChart; ?>, {
                                                    type: 'line',
                                                    data: barChartContrato<?php echo $contarChart; ?>,
                                                    options: {
                                                        responsive: false,
                                                        maintainAspectRatio: true,
                                                        legend: {
                                                            position: 'top',
                                                        },
                                                        title: {
                                                            display: false,
                                                            text: 'Comercializadoras'
                                                        },
                                                        animation: {
                                                            animateScale: true,
                                                            animateRotate: true,
                                                            duration: 2000,
                                                        },
                                                        scales: {
                                                            yAxes: [{
                                                                ticks: {
                                                                    min: 0,
                                                                },
                                                            }, ]
                                                        }
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


<?php

        $contarChart++;
    }
}
?>