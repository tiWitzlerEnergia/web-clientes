<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php session_start(); ?>
<div class="row" id="updateContratos">
<?php 
if(isset($_GET['id_unidades'])){
    //recolhendo variaveis uteis:
    $id_unidades_contrato = $_GET['id_unidades'];
    //Config para datas
    setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');
    //Arrays:
    $armazenaInicio_suprimento = [];
    $armazenaFim_suprimento = [];
    $armazenaMontante_percentual = [];
    $armazenaSazo_inf = [];
    $armazenaSazo_sup = [];
    $armazenaFlex_inf = [];
    $armazenaFlex_sup = [];
    $armazenaMontante = [];
    $armazenaNome = [];
    $armazenaPath = [];
    $armazenaNumero_contrato = [];
    $armazenaPreco_base = [];
    //passando dados para consumir api
    $urlContratos = "https://api.develop.clientes.witzler.com.br/api/unidades/contratos?id_unidades=".$id_unidades_contrato;
    $context = stream_context_create(array(
      'http' => array(
          'header' => "Authorization: Bearer ".$_SESSION['token'],
      ),
    ));
    @$recolheContratos = file_get_contents($urlContratos, false, $context);
    @$resultContratos = json_decode($recolheContratos);
    if(isset($resultContratos)){
        foreach($resultContratos as $contratos){
            $unidadeInicio_suprimento = $contratos->inicio_suprimento;
            $unidadeFim_suprimento = $contratos->fim_suprimento;
            $unidadeMontante_percentual = $contratos->montante_percentual;
            $unidadeSazo_inf = $contratos->sazo_inf;
            $unidadeSazo_sup = $contratos->sazo_sup;
            $unidadeFlex_inf = $contratos->flex_inf;
            $unidadeFlex_sup = $contratos->flex_sup;
            $unidadeMontante = $contratos->montante;
            $unidadeNome = $contratos->nome;
            $unidadePath = $contratos->path;
            $unidadeNumero_contrato = $contratos->numero_contrato;
            $unidadePreco_base = $contratos->preco_base;
            //Converter datas em portugues:
            $unidadeInicio_suprimento = strftime('%B/%Y', strtotime($unidadeInicio_suprimento));
            $unidadeFim_suprimento = strftime('%B/%Y', strtotime($unidadeFim_suprimento));
            $separaInicio_suprimento =  explode("/", $unidadeInicio_suprimento);
            $anoInicio = $separaInicio_suprimento[1];
            $separaFim_suprimento = explode("/", $unidadeFim_suprimento);
            $anoFim = $separaFim_suprimento[1];
            //Formatar percentuais:
            $unidadeMontante = number_format($unidadeMontante, 2);
            //Array push:
            array_push($armazenaInicio_suprimento, $unidadeInicio_suprimento);
            array_push($armazenaFim_suprimento, $unidadeFim_suprimento);
            array_push($armazenaMontante_percentual, $unidadeMontante_percentual);
            array_push($armazenaSazo_inf, $unidadeSazo_inf);
            array_push($armazenaSazo_sup, $unidadeSazo_sup);
            array_push($armazenaFlex_inf, $unidadeFlex_inf);
            array_push($armazenaFlex_sup, $unidadeFlex_sup);
            array_push($armazenaMontante, $unidadeMontante);
            array_push($armazenaNome, $unidadeNome);
            array_push($armazenaPath, "https://clientes.witzler.com.br/backend/assets/img/distributor/".$unidadePath);
            array_push($armazenaNumero_contrato, $unidadeNumero_contrato);
            array_push($armazenaPreco_base, $unidadePreco_base);
        }
    }
}
if(empty($armazenaNome)){

}else{
   $tamanhoContratos = count($armazenaNome);
   for($i = 0; $i < $tamanhoContratos; $i++){
?>
         <div class="col-12 col-sm-6 col-md-6 col-lg-4 fade-in">
            <div class="card-accent-secondary card">
               <div class="text-center h-25 card-header">
                  <div class="row">
                     <div class="col">
                        <div class="mb-0 pt-3 card-title">
                           <img height="50" src="<?php echo $armazenaPath[$i]; ?>">
                           <hr>
                           <h5 class="mt-3"><?php echo strtoupper($armazenaInicio_suprimento[$i])." - ".strtoupper($armazenaFim_suprimento[$i]); ?></h5>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-body">
                  <div class="table-responsive">
                     <table class="table">
                        <tbody>
                           <tr>
                              <td>Comercializadora:</td>
                              <td class="text-right"><?php echo $armazenaNome[$i]; ?></td>
                           </tr>
                           <tr>
                              <td>Número do contrato:</td>
                              <td class="text-right"><?php echo $armazenaNumero_contrato[$i]; ?></td>
                           </tr>
                           <tr>
                              <td>Preço Contratado [R$/MWh]</td>
                              <td class="text-right"><?php echo $armazenaPreco_base[$i]; ?></td>
                           </tr>
                           <tr>
                              <td>Volume Médio [MWMéd]</td>
                              <td class="text-right"><?php echo $armazenaMontante[$i]; ?></td>
                           </tr>
                           <tr>
                              <td>Flexibilidade [%]</td>
                              <td class="text-right"><?php echo $armazenaFlex_sup[$i]; ?></td>
                           </tr>
                           <tr>
                              <td>Sazonalidade [%]</td>
                              <td class="text-right"><?php echo $armazenaSazo_sup[$i]; ?></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      

<?php
   }
}
?>
</div>