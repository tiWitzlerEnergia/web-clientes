<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
if(isset($_COOKIE['id_unidades']) || isset($arrayIdOption[0])){
    //recolhendo variaveis uteis:
    if(isset($_COOKIE['id_unidades'])){
        $id_unidades_contrato = $_COOKIE['id_unidades'];
    }else{
        $id_unidades_contrato = $arrayIdOption[0];
    }
    //Config para datas
    setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');
    //Arrays:
    $armazenaAnos = [];
    $armazenaInicio_suprimento = [];
    $armazenaFim_suprimento = [];
    $armazenaMontante_percentual = [];
    $armazenaSazo_inf = [];
    $armazenaSazo_sup = [];
    $armazenaFlex_inf = [];
    $armazenaFlex_sup = [];
    $armazenaMontante = [];
    $armazenaNome = [];
    $armazenaPath = [];
    $armazenaNumero_contrato = [];
    $armazenaPreco_base = [];
    //passando dados para consumir api
    $urlContratos = "https://api.develop.clientes.witzler.com.br/api/unidades/contratos?id_unidades=".$id_unidades_contrato;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheContratos = file_get_contents($urlContratos, false, $context);
    @$resultContratos = json_decode($recolheContratos);
    if(isset($resultContratos)){
        foreach($resultContratos as $contratos){
            $unidadeInicio_suprimento = $contratos->inicio_suprimento;
            $unidadeFim_suprimento = $contratos->fim_suprimento;
            $unidadeMontante_percentual = $contratos->montante_percentual;
            $unidadeSazo_inf = $contratos->sazo_inf;
            $unidadeSazo_sup = $contratos->sazo_sup;
            $unidadeFlex_inf = $contratos->flex_inf;
            $unidadeFlex_sup = $contratos->flex_sup;
            $unidadeMontante = $contratos->montante;
            $unidadeNome = $contratos->nome;
            $unidadePath = $contratos->path;
            $unidadeNumero_contrato = $contratos->numero_contrato;
            $unidadePreco_base = $contratos->preco_base;
            //Converter datas em portugues:
            $unidadeInicio_suprimento = strftime('%B/%Y', strtotime($unidadeInicio_suprimento));
            $unidadeFim_suprimento = strftime('%B/%Y', strtotime($unidadeFim_suprimento));
            $separaInicio_suprimento =  explode("/", $unidadeInicio_suprimento);
            $anoInicio = $separaInicio_suprimento[1];
            $separaFim_suprimento = explode("/", $unidadeFim_suprimento);
            $anoFim = $separaFim_suprimento[1];
            //Formatar percentuais:
            $unidadeMontante = number_format($unidadeMontante, 2);
            //Array push:
            array_push($armazenaInicio_suprimento, $unidadeInicio_suprimento);
            array_push($armazenaFim_suprimento, $unidadeFim_suprimento);
            array_push($armazenaMontante_percentual, $unidadeMontante_percentual);
            array_push($armazenaSazo_inf, $unidadeSazo_inf);
            array_push($armazenaSazo_sup, $unidadeSazo_sup);
            array_push($armazenaFlex_inf, $unidadeFlex_inf);
            array_push($armazenaFlex_sup, $unidadeFlex_sup);
            array_push($armazenaMontante, $unidadeMontante);
            array_push($armazenaNome, $unidadeNome);
            array_push($armazenaPath, "https://clientes.witzler.com.br/backend/assets/img/distributor/".$unidadePath);
            array_push($armazenaNumero_contrato, $unidadeNumero_contrato);
            array_push($armazenaPreco_base, $unidadePreco_base);
        }
    }
}
?>