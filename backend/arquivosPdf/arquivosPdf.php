<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<!--<select id="diretorioArquivo" name="diretorioArquivo" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="abrir.location = options[selectedIndex].value">-->
<select id="diretorioArquivo" name="diretorioArquivo" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="">
    <option class="dropdown-item" selected disabled>Selecione um arquivo...</option>
    <?php
    //Definindo variaveis pra URL
    if ((isset($_COOKIE['data_unidade']) && isset($_COOKIE['tipo_arquivo']) && isset($_COOKIE['id_unidades'])) || (isset($_COOKIE['data_unidade']) && isset($_COOKIE['tipo_arquivo']) && isset($arrayIdOption[0]))) {
        $data_unidade_old = $_COOKIE['data_unidade'];
        $data_unidade = date('Y-m-d', strtotime($data_unidade_old));
        $tipo_arquivo = $_COOKIE['tipo_arquivo'];
        if (isset($_COOKIE['id_unidades'])) {
            $id_unidades_contrato = $_COOKIE['id_unidades'];
        } else {
            $id_unidades_contrato = $arrayIdOption[0];
        }
        //URL:
        $urlPdf = 'https://api.develop.clientes.witzler.com.br/api/arquivos/?data_referencia=' . $data_unidade . '&tipo_arquivo=' . $tipo_arquivo . '&unidade_id=' . $id_unidade;
        $context = stream_context_create(array(
            'http' => array(
                'header' => "Authorization: Bearer " . $_SESSION['token'],
            ),
        ));
        @$recolhePdf = file_get_contents($urlPdf, false, $context);
        @$resultPdf = json_decode($recolhePdf);
        if (isset($resultPdf)) {
            foreach ($resultPdf as $pdf) {
                $nome = $pdf->caminho;
                $nome = explode("/", $nome);
                $ultimo = end($nome);
    ?>
                <option id="<?php echo $pdf->id; ?>" value="https://clientes.witzler.com.br/backend/assets/files/clientes/<?php echo $pdf->caminho; ?>" class="dropdown-item"><?php echo $ultimo; ?></option>
    <?php
            }
        }
    }
    ?>
</select>

<script>
    $('#diretorioArquivo').on('change', function() {
        var src = $(this).find('option:selected').val(); // verify
        console.log(src);
        //Refresh do outro input que gera pdf:
        //Declarando variaveis de FILTROS CLIENTE:
        var last_id_tipo_unidades_cliente = $("#tipoArquivoCliente").find(':selected').attr('id');
        var last_calendario_cliente = $("#dataTipoArquivoCliente").val();
        var last_id_unidades = $("#idUnidades").find(':selected').attr('id');
        $("#diretorioTipoArquivoCliente").html("");
        $.ajax({
            type: 'GET',
            url: '../../backend/tipoArquivoCliente/update/updatePdfTipoArquivoCliente.php',
            //url: '../../backend/arquivosPdf/copyarquivopdf.php',
            //url: 'https://clientes.witzler.com.br/backend/arquivosPdf/copyarquivopdf.php',
            timeout : 30000,
            data: {
                'data_tipo_arquivo_cliente': last_calendario_cliente,
                'id_unidades': last_id_unidades,
                'tipo_arquivo_arquivo_cliente': last_id_tipo_unidades_cliente,
                'src': src
            },
            /*
            beforeSend: function(carrega) {
                $("#atualizapdf").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");

            },
            */
            success: function(s) {
                $("#diretorioTipoArquivoCliente").html(s);
                //$('#abrir').attr('src', src); 
                /*
                setTimeout(function() {
                    console.log(s);
                    $("#atualizapdf").html('<iframe id="abrir" class="fade-in" name="abrir" src="' + src + '" width=100% height=800></iframe>');
                    //$("#atualizapdf").html('<iframe id="abrir" class="fade-in" name="abrir" src="pdfcopias/meupdf.pdf" width=100% height=800></iframe>');
                }, 2500);
                */
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log("Error ao executar copy");
                console.log(xhr.status);
                console.log(thrownError);
            }
        });

        $.ajax({
            type: 'GET',
            url: '../../backend/arquivosPdf/descriptografarPdf.php',
            timeout : 30000,
            data: {
                'data_unidade': last_calendario_cliente,
                'id_unidades': last_id_unidades,
                'tipo_arquivo': last_id_tipo_unidades_cliente,
                'src': src
            },
            beforeSend: function(carrega) {
                $("#atualizapdf").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");

            },
            success: function(pdf) {
                setTimeout(function() {
                    console.log(pdf);
                    $("#atualizapdf").html('<iframe id="abrir" class="fade-in" name="abrir" src="' + pdf + '" width=100% height=800></iframe>');

                    $.ajax({
                        type: 'GET',
                        url: '../../backend/arquivosPdf/deletePdf.php',
                        async: true,
                        data: {
                            'src': src
                        },
                        success: function(resultpdf){
                            console.log(resultpdf);
                        }
                    });
                }, 2500);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log("Error ao executar copy");
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    });
</script>