<?php
// Antes de fazer o show do pdf, copiamos ele em um diretorio:
//$arquivo_origem = $_GET['src'];
$arquivo_origem = $_GET['src'];
//$arquivo_destino = "../../frontend/meus-arquivos/pdfcopias/meupdf.pdf";
$arquivo_destino = "https://clientes.witzler.com.br/frontend/meus-arquivos/pdfcopias/meupdf.pdf";
//copy($arquivo_origem, $arquivo_destino);

if (!copy($arquivo_origem, $arquivo_destino)) {
    $errors = error_get_last();
    $errortype = $errors['type'];
    $errormsg = $errors["message"];
    echo "<script>console.log('error do tipo: $errortype');</script>";
    echo "<script>console.log('error msg: $errormsg');</script>";
} else {
    echo "<script>console.log('Copiado com sucesso');</script>";
}

//$teste1 = shell_exec("cp -p $arquivo_origem $arquivo_destino 2>&1 ");