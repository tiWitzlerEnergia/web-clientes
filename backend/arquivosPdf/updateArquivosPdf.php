<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php session_start(); ?>
<option class="dropdown-item" selected disabled>Selecione um arquivo...</option>
<?php
//Definindo variaveis pra URL
if(isset($_GET['data_unidade']) && isset($_GET['tipo_arquivo']) && isset($_GET['id_unidades'])){
    //Formatando datas:
    $data_unidade_old = $_GET['data_unidade'];
    $data_unidade = date('Y-m-d', strtotime($data_unidade_old));
    $tipo_arquivo = $_GET['tipo_arquivo'];
    $id_unidade = $_GET['id_unidades'];
    //URL:
    $urlPdf = 'https://api.develop.clientes.witzler.com.br/api/arquivos/?data_referencia='.$data_unidade.'&tipo_arquivo='.$tipo_arquivo.'&unidade_id='.$id_unidade;
    
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolhePdf = file_get_contents($urlPdf, false, $context);
    @$resultPdf = json_decode($recolhePdf);
    if(isset($resultPdf)){
        //var_dump($resultPdf);
        foreach($resultPdf as $pdf){
            $nome = $pdf->caminho;
            $nome = explode("/", $nome);
            $ultimo = end($nome);
?>
            <option id="<?php echo $pdf->id; ?>" value="<?php echo $pdf->caminho; ?>" class="dropdown-item"><?php echo $ultimo; ?></option>
<?php
        }
    }
}
?>