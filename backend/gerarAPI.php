<?php
//Config para datas
setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

$context = stream_context_create(array(
    'http' => array(
        'header' => "Authorization: Bearer " . $_SESSION['token'],
    ),
));

//Criar funcao que vai fazer requisicao com a API e vai retornar os array com os valores.
function gerarConteudo($nome, $id_unidade)
{
    switch ($nome) {
            //contrato2
        case "contrato2":
            //Recolhe id unidade para procurar no contrato:
            //$id_unidades_contrato =  isset($arrayIdOption[0]) ? $arrayIdOption[0] : $_COOKIE['id_unidades'];
            $id_unidades_contrato = $id_unidade;
            //Gerar arrays (ver como melhorar isto depois):
            $arrAno = [];
            $arrCow_id_contrato = [];
            $arrE_flex = [];
            $arrE_sazo = [];
            $arrFlex_max = [];
            $arrFlex_min = [];
            $arrId = [];
            $arrId_cliente = [];
            $arrId_distribuidora = [];
            $arrId_montantes = [];
            $arrN_contract = [];
            $arrP_carga = [];
            $arrPreco = [];
            $arrSazo_max = [];
            $arrSazo_min = [];
            //passando dados para consumir api
            $context = stream_context_create(array(
                'http' => array(
                    'header' => "Authorization: Bearer " . $_SESSION['token'],
                ),
            ));
            $urlContratos = "https://api.develop.clientes.witzler.com.br/api/contrato2?id_unidades=" . $id_unidades_contrato;
            @$recolheContratos = file_get_contents($urlContratos, false, $context);
            @$resultContratos = json_decode($recolheContratos);
            foreach ($resultContratos as $contratos) {
                $ano = $contratos->ano;
                $cow_id_contrato = $contratos->cow_id_contrato;
                $e_flex = $contratos->e_flex;
                $e_sazo = $contratos->e_sazo;
                $flex_max = $contratos->flex_max;
                $flex_min = $contratos->flex_min;
                $id = $contratos->id;
                $id_cliente = $contratos->id_cliente;
                $id_distribuidora = $contratos->id_distribuidora;
                $id_montantes = $contratos->id_montantes;
                $n_contract = $contratos->n_contract;
                $p_carga = $contratos->p_carga;
                $preco = $contratos->preco;
                $sazo_max = $contratos->sazo_max;
                $sazo_min = $contratos->sazo_min;

                //array_push:
                array_push($arrAno, $ano);
                array_push($arrCow_id_contrato, $cow_id_contrato);
                array_push($arrE_flex, $e_flex);
                array_push($arrE_sazo, $e_sazo);
                array_push($arrFlex_max, $flex_max);
                array_push($arrFlex_min, $flex_min);
                array_push($arrId, $id);
                array_push($arrId_cliente, $id_cliente);
                array_push($arrId_distribuidora, $id_distribuidora);
                array_push($arrId_montantes, $id_montantes);
                array_push($arrN_contract, $n_contract);
                array_push($arrP_carga, $p_carga);
                array_push($arrPreco, $preco);
                array_push($arrSazo_max, $sazo_max);
                array_push($arrSazo_min, $sazo_min);
            }
            break;

            //caso padrão:
        default:
            echo "<script>console.log('Sem resultados...');</script>";
            break;
    }
}
