<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->



<?php
session_start();
if (isset($_GET['calendario_unidade']) && isset($_GET['final_mes']) && isset($_GET['id_unidades'])) {
    $dataInicial = $_GET['calendario_unidade'];
    $dataFinal = $_GET['final_mes'];
    $id_unidade = $_GET['id_unidades'];
    //CARD FATOR POTENCIA CAPACITATIVO:
    $urlFatorPotencia = "https://api.develop.clientes.witzler.com.br/api/media/fator/potencia/capacitivo?data_final=" . $dataFinal . "&data_inicial=" . $dataInicial . "&unidade_id=" . $id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer " . $_SESSION['token'],
        ),
    ));
    @$recolheFatorPotencia = file_get_contents($urlFatorPotencia, false, $context);
    @$resultFatorPotencia = json_decode($recolheFatorPotencia);
    $somaTotal = 0; //VARIAVEL QUE ARMAZENARA OS VALORES
    $total = 0;
    if (isset($resultFatorPotencia)) {
        foreach ($resultFatorPotencia as $fatorPotencia) {
            $ativo_c = $fatorPotencia->ativo_c;
            $reativo_c = $fatorPotencia->reativo_c;
            $valorFinal = cos(atan($reativo_c / $ativo_c));
            $somaTotal += $valorFinal;
            $total++;
        }
        @$media = $somaTotal / $total;
        @$mediaFormatada = number_format($media, 2, ',', '.');
    }
}

//Se existir, mostra o valor dado ou um por defeito em caso que nao exista
if (isset($mediaFormatada)) {
    if (is_nan($media)) {
        echo "--";
    } else {
        echo $mediaFormatada;
    }
} else {
    echo "--";
}
?>