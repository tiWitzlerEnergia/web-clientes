<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
session_start();
//Configuracoes pra cosumir api
if(isset($_GET['calendario_unidade']) && isset($_GET['final_mes']) && isset($_GET['id_unidades'])){
    $dataInicial = $_GET['calendario_unidade'];
    $dataFinal = $_GET['final_mes'];
    $id_unidade = $_GET['id_unidades'];
    //CARD ACUMULO MENSAL
    $urlAcumuladoMensal = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo/fora/ponta?dataFinal=".$dataFinal."&dataInicial=".$dataInicial."&unidade_id=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheAcumuladoMensal = file_get_contents($urlAcumuladoMensal, false, $context);
    @$resultAcumuladoMensal = json_decode($recolheAcumuladoMensal);
    if(isset($resultAcumuladoMensal)){
        $acumuladoForaPonta = $resultAcumuladoMensal->foraPonta;
        $acumuladoPonta = $resultAcumuladoMensal->ponta;
        $totalAcumuladoMensal = $acumuladoForaPonta + $acumuladoPonta;
        $totalAcumuladoMensal = number_format($totalAcumuladoMensal, 2, ',', '.');
    }else{
        echo "--";
    }
}

//Se existir, mostra o valor dado ou um por defeito em caso que nao exista
if(isset($totalAcumuladoMensal)){ 
    echo $totalAcumuladoMensal; 
 } else{
    echo "--";
 }
?>
