<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
session_start();
if(isset($_GET['calendario_unidade']) && isset($_GET['final_mes']) && isset($_GET['id_unidades'])){
    $dataInicial = $_GET['calendario_unidade'];
    $dataFinal = $_GET['final_mes'];
    $id_unidade = $_GET['id_unidades'];
    //CARD FATOR POTENCIA INDUTIVO:
    $urlFatorPotenciaIndutivo = "https://api.develop.clientes.witzler.com.br/api/media/fator/potencia/indutivo?data_final=".$dataFinal."&data_inicial=".$dataInicial."&unidade_id=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheFatorPotenciaIndutivo = file_get_contents($urlFatorPotenciaIndutivo, false, $context);
    @$resultFatorPotenciaIndutivo = json_decode($recolheFatorPotenciaIndutivo);
    $somaTotalIndutivo = 0; //VARIAVEL QUE ARMAZENARA OS VALORES
    $totalIndutivo = 0;
    if(isset($resultFatorPotenciaIndutivo)){
        foreach($resultFatorPotenciaIndutivo as $indutivo){
            $ativo_c_indutivo = $indutivo->ativo_c;
            $reativo_c_indutivo = $indutivo->reativo_c;
            $valorFinalIndutivo = cos(atan($reativo_c_indutivo/$ativo_c_indutivo));
            $somaTotalIndutivo += $valorFinalIndutivo;
            $totalIndutivo++;
        }
        @$mediaIndutiva = $somaTotalIndutivo/$totalIndutivo;
        @$mediaFormatadaIndutiva = number_format($mediaIndutiva, 2, ',', '.');
    }
}

//Se existir, mostra o valor dado ou um por defeito em caso que nao exista
if(isset($mediaFormatadaIndutiva)){
    if(is_nan($mediaIndutiva)){
       echo "--"; 
    }else{ 
       echo $mediaFormatadaIndutiva; 
    } 
}else{
    echo "--";
}
?>
