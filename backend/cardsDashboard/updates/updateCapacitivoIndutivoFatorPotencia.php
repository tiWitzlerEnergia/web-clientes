<?php
session_start();
//var_dump($_COOKIE);
//var_dump($_SESSION['id_unidade']);

//if(!isset($_COOKIE['id_unidades'])) {
if(!isset($_GET['id_unidades']) || !isset($_GET['data_fator_potencia'])){
    echo 'ERROR ID_UNIDADE OU DATA';   
} else {
    //var_dump($_GET['data_fator_potencia']);

    $arrRetornar = [];
    $id_unidade = $_GET['id_unidades'];
    $dataInicial = $_GET['data_fator_potencia'];
    $dataFinal = date("Y-m-d", strtotime('+1 days', strtotime($dataInicial)));
    

    //echo $dataFinal;

    

    
    //CARD FATOR POTENCIA 1:
    $urlFatorPotencia = "https://api.develop.clientes.witzler.com.br/api/media/fator/potencia/capacitivo?data_final=".$dataFinal."&data_inicial=".$dataInicial."&unidade_id=".$id_unidade;
    
    //var_dump($urlFatorPotencia);
    
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));

    @$recolheFatorPotencia = file_get_contents($urlFatorPotencia, false, $context);
    @$resultFatorPotencia = json_decode($recolheFatorPotencia);
    $somaTotal = 0; //VARIAVEL QUE ARMAZENARA OS VALORES
    $total = 0;
    if(isset($resultFatorPotencia)){
        foreach($resultFatorPotencia as $fatorPotencia){
            $ativo_c = $fatorPotencia->ativo_c;
            $reativo_c = $fatorPotencia->reativo_c;
            $valorFinal = cos(atan($reativo_c/$ativo_c));
            $somaTotal += $valorFinal;
            $total++;
        }
        @$media = $somaTotal/$total;
        @$mediaFormatada = number_format($media, 2, ',', '.'); // <- resultado final
        if(is_nan($media)){
            $mediaFormatada = 0;
        }
        //echo $mediaFormatada;

        array_push($arrRetornar, $mediaFormatada);
    }
    //CARD FATOR POTENCIA INDUTIVO:
    $urlFatorPotenciaIndutivo = "https://api.develop.clientes.witzler.com.br/api/media/fator/potencia/indutivo?data_final=".$dataFinal."&data_inicial=".$dataInicial."&unidade_id=".$id_unidade;
    
    //var_dump($urlFatorPotenciaIndutivo);
    
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));

    @$recolheFatorPotenciaIndutivo = file_get_contents($urlFatorPotenciaIndutivo, false, $context);
    @$resultFatorPotenciaIndutivo = json_decode($recolheFatorPotenciaIndutivo);
    $somaTotalIndutivo = 0; //VARIAVEL QUE ARMAZENARA OS VALORES
    $totalIndutivo = 0;
    if(isset($resultFatorPotenciaIndutivo)){
        foreach($resultFatorPotenciaIndutivo as $indutivo){
            $ativo_c_indutivo = $indutivo->ativo_c;
            $reativo_c_indutivo = $indutivo->reativo_c;
            $valorFinalIndutivo = cos(atan($reativo_c_indutivo/$ativo_c_indutivo));
            $somaTotalIndutivo += $valorFinalIndutivo;
            $totalIndutivo++;
        }
        @$mediaIndutiva = $somaTotalIndutivo/$totalIndutivo;
        @$mediaFormatadaIndutiva = number_format($mediaIndutiva, 2, ',', '.'); // <- resultado final
        //echo $mediaFormatadaIndutiva;
        if(is_nan($mediaIndutiva)){
            $mediaFormatadaIndutiva = 0;
        }

        array_push($arrRetornar, $mediaFormatadaIndutiva);
    }

    //var_dump($arrRetornar);
    echo json_encode($arrRetornar);
    //echo $_SESSION['token'];
}