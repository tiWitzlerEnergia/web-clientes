<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
if(isset($_GET['calendario_unidade'])){
    $dataInicial = $_GET['calendario_unidade'];
}
if(isset($dataInicial)){
    setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_get('America/Sao_Paulo');
    $dataCard = strftime('%B de %Y', strtotime($dataInicial));
    echo " ".strtoupper($dataCard);
 }else{
    setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_get('America/Sao_Paulo');
    $dataCard = strftime('%B de %Y', strtotime('today'));
    echo " ".strtoupper($dataCard);
 }
?>