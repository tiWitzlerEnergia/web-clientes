<?php
session_start();
require_once "../../Classes/autoload.php";
$id_unidade = $_GET['id_unidades'];
//$mes_ref = date("Y-m-d");
//$mes_demanda = date("Y-m-01", strtotime("-1 months"));
$mes_ref = $_GET['mes_ref'];
$demanda = new CardDemandas("cardDemanda", $mes_ref, $id_unidade);
$demanda->setValuesCard();

$demandafponta = is_numeric($demanda->getDemandaFponta()) ? $demanda->getDemandaFponta() : 0.00;
$demandaponta = is_numeric($demanda->getDemandaPonta()) ? $demanda->getDemandaPonta() : 0.00;
$cdemandafponta = is_numeric($demanda->getCDemandaFponta()) ? $demanda->getCDemandaFponta() : 0.00;
$cdemandaponta = is_numeric($demanda->getCDemandaPonta()) ? $demanda->getCDemandaPonta() : 0.00;

?>
<div class="col-12 d-flex" id="updateDemandasCard">
    <div class="col-6 noPadding">
        <div class="col-12 noPadding mb-2" id="valorDemandaFPonta">
            <h5 style="margin: 0;">
                <?php echo number_format($demandafponta,2,",","."); ?>
                <span class="text-muted small" style="font-size: 10px;">kW</span>
            </h5>
            <span class="text-muted small" style="font-size: 10px;" id="alteraPonta1">
                Demanda Registrada Ponta
            </span>
        </div>
        <div class="col-12 noPadding" id="valorDemandaPonta">
            <h5 style="margin: 0;">
                <?php echo number_format($demandaponta,2,",","."); ?>
                <span class="text-muted small" style="font-size: 10px;">kW</span>
            </h5>
            <span class="text-muted small" style="font-size: 10px;">
                Demanda Registrada Fora Ponta
            </span>
        </div>
    </div>
    <?php
    /*if ($demandafponta == 0) {
        echo "
            <script>
            document.getElementById('valorDemandaFPonta').style.display = 'none';
            </script>
        ";
    }

    if ($demandaponta == 0) {
        
    }*/
    ?>

    <div class="col-6 noPadding">
        <div class="col-12 noPadding mb-2" id="valorCDemandaFPonta">
            <h5 style="margin: 0;">
                <?php echo number_format($cdemandafponta,2,",","."); ?>
                <span class="text-muted small" style="font-size: 10px;">kW</span>
            </h5>
            <span class="text-muted small" style="font-size: 10px;" id="alteraPonta2">
                Demanda Contratada Ponta
            </span>
        </div>
        <div class="col-12 noPadding" id="valorCDemandaPonta">
            <h5 style="margin: 0;">
                <?php echo number_format($cdemandaponta,2,",","."); ?>
                <span class="text-muted small" style="font-size: 10px;">kW</span>
            </h5>
            <span class="text-muted small" style="font-size: 10px;">
                Demanda Contratada Fora Ponta
            </span>
        </div>
    </div>

    <?php
    /*if ($cdemandafponta == 0) {
        echo "
            <script>
            document.getElementById('valorCDemandaFPonta').style.display = 'none';
            </script>
        ";
    }

    if ($cdemandaponta == 0) {
        
    }*/

    // atualiza textos:
    if ($cdemandaponta == 0 && $demandaponta == 0) {
        echo "
            <script>
                $('#alteraPonta1').html('Demanda Registrada');
                $('#alteraPonta2').html('Demanda Contratada');
            </script>
        ";

        echo "
            <script>
            document.getElementById('valorDemandaPonta').style.display = 'none';
            </script>
        ";

        echo "
            <script>
            document.getElementById('valorCDemandaPonta').style.display = 'none';
            </script>
        ";
    }
    ?>
</div>