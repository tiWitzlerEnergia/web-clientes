<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->



<?php
//if(isset($_COOKIE['calendario_unidade']) && isset($_COOKIE['final_mes']) && isset($_COOKIE['id_unidades'])){  //If antigo
if(isset($ultimaData) && isset($arrayIdOption[0])){     //If que pega o ultimo mes
    /*$dataInicial = $_COOKIE['calendario_unidade'];
    $dataFinal = $_COOKIE['final_mes'];
    $id_unidade = $_COOKIE['id_unidades'];*/
    //GRAFICO ACUMULADO MENSAL
    $dataInicial = $ultimaData;
    $dataFinal = date("Y-m-t", strtotime($ultimaData));
    if(isset($_COOKIE['id_unidades'])){
        $id_unidade = $_COOKIE['id_unidades'];
    }else{
        $id_unidade = $arrayIdOption[0];
    }

    //CARD ACUMULO MENSAL
    $urlAcumuladoMensal = "https://api.develop.clientes.witzler.com.br/api/medidas/periodo/fora/ponta?dataFinal=".$dataFinal."&dataInicial=".$dataInicial."&unidade_id=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheAcumuladoMensal = file_get_contents($urlAcumuladoMensal, false, $context);
    @$resultAcumuladoMensal = json_decode($recolheAcumuladoMensal);
    if(isset($resultAcumuladoMensal)){
        $acumuladoForaPonta = $resultAcumuladoMensal->foraPonta;
        $acumuladoPonta = $resultAcumuladoMensal->ponta;
        $totalAcumuladoMensal = $acumuladoForaPonta + $acumuladoPonta;
        $totalAcumuladoMensal = number_format($totalAcumuladoMensal, 2, ',', '.');
    }else{
    }
    //CARD FATOR POTENCIA 1:
    $urlFatorPotencia = "https://api.develop.clientes.witzler.com.br/api/media/fator/potencia/capacitivo?data_final=".$dataFinal."&data_inicial=".$dataInicial."&unidade_id=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheFatorPotencia = file_get_contents($urlFatorPotencia, false, $context);
    @$resultFatorPotencia = json_decode($recolheFatorPotencia);
    $somaTotal = 0; //VARIAVEL QUE ARMAZENARA OS VALORES
    $total = 0;
    if(isset($resultFatorPotencia)){
        foreach($resultFatorPotencia as $fatorPotencia){
            $ativo_c = $fatorPotencia->ativo_c;
            $reativo_c = $fatorPotencia->reativo_c;
            $valorFinal = cos(atan($reativo_c/$ativo_c));
            $somaTotal += $valorFinal;
            $total++;
        }
        @$media = $somaTotal/$total;
        @$mediaFormatada = number_format($media, 2, ',', '.');
    }
    //CARD FATOR POTENCIA INDUTIVO:
    $urlFatorPotenciaIndutivo = "https://api.develop.clientes.witzler.com.br/api/media/fator/potencia/indutivo?data_final=".$dataFinal."&data_inicial=".$dataInicial."&unidade_id=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    @$recolheFatorPotenciaIndutivo = file_get_contents($urlFatorPotenciaIndutivo, false, $context);
    @$resultFatorPotenciaIndutivo = json_decode($recolheFatorPotenciaIndutivo);
    $somaTotalIndutivo = 0; //VARIAVEL QUE ARMAZENARA OS VALORES
    $totalIndutivo = 0;
    if(isset($resultFatorPotenciaIndutivo)){
        foreach($resultFatorPotenciaIndutivo as $indutivo){
            $ativo_c_indutivo = $indutivo->ativo_c;
            $reativo_c_indutivo = $indutivo->reativo_c;
            $valorFinalIndutivo = cos(atan($reativo_c_indutivo/$ativo_c_indutivo));
            $somaTotalIndutivo += $valorFinalIndutivo;
            $totalIndutivo++;
        }
        @$mediaIndutiva = $somaTotalIndutivo/$totalIndutivo;
        @$mediaFormatadaIndutiva = number_format($mediaIndutiva, 2, ',', '.');
    }

    //CARD PLD
    /*$arrayDataInicialPld = [];
    $arrayDataFinalPld = [];
    $arrayPesadaSeCoPld = [];
    $arrayPesadaS = [];
    $arrayPesadaNe - [];
    $arrayPesadaN = [];
    $arrayMediaSeCo = [];
    $arrayMediaS = [];
    $arrayMediaNe = [];
    $arrayMediaN = [];
    $arrayLeveSeCo = [];
    $arrayLeveS = [];
    $arrayLeveNe = [];
    $arrayLeveN = [];*/

    $urlPld = "https://api.develop.clientes.witzler.com.br/api/pld/semanal";
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    $recolhePld = file_get_contents($urlPld, false, $context);
    $resultPld = json_decode($recolhePld);
    if(isset($resultPld)){
        foreach($resultPld as $pld){
            $dataInicialPld = $pld->data_inicial;
            $dataInicialPld = substr($dataInicialPld, 0, 10);
            $dataInicialPld = date('d/m/Y', strtotime($dataInicialPld));
            $dataFinalPld = $pld->data_final;
            $dataFinalPld = substr($dataFinalPld, 0, 10);
            $dataFinalPld = date('d/m/Y', strtotime($dataFinalPld));
            //Pesada
            $pesadaSeCo = $pld->pesada_se_co;
            $pesadaSeCo = number_format($pesadaSeCo, 2, ',', '.');
            $pesadaS = $pld->pesada_s;
            $pesadaS = number_format($pesadaS, 2, ',', '.');
            $pesadaNe = $pld->pesada_ne;
            $pesadaNe = number_format($pesadaNe, 2, ',', '.');
            $pesadaN = $pld->pesada_n;
            $pesadaN = number_format($pesadaN, 2, ',', '.');
            //Media
            $mediaSeCo = $pld->media_se_co;
            $mediaSeCo = number_format($mediaSeCo, 2, ',', '.');
            $mediaS = $pld->media_s;
            $mediaS = number_format($mediaS, 2, ',', '.');
            $mediaNe = $pld->media_ne;
            $mediaNe = number_format($mediaNe, 2, ',', '.');
            $mediaN = $pld->media_n;
            $mediaN = number_format($mediaN, 2, ',', '.');
            //Leve
            $leveSeCo = $pld->leve_se_co;
            $leveSeCo = number_format($leveSeCo, 2, ',', '.');
            $leveS = $pld->leve_s;
            $leveS = number_format($leveS, 2, ',', '.');
            $leveNe = $pld->leve_ne;
            $leveNe = number_format($leveNe, 2, ',', '.');
            $leveN = $pld->leve_n;
            $leveN = number_format($leveN, 2, ',', '.');

            /*array_push($arrayDataInicialPld, $dataInicial);
            array_push($arrayDataFinalPld, $dataFinal);
            array_push($arrayPesadaSeCoPld, $pesadaSeCo);
            array_push($arrayPesadaS, $pesadaS);
            array_push($arrayPesadaNe, $pesadaNe);
            array_push($arrayPesadaN, $pesadaN);
            array_push($arrayMediaSeCo, $mediaSeCo);
            array_push($arrayMediaS, $mediaS);
            array_push($arrayMediaNe, $mediaNe);
            array_push($arrayMediaN, $mediaN);
            array_push($arrayLeveSeCo, $leveSeCo);
            array_push($arrayLeveS, $leveS);
            array_push($arrayLeveNe, $leveNe);
            array_push($arrayLeveN, $leveN);*/
        }
    } 

    /*$dataprevia = '2020-10-01';
    $unidadeId = 1206;

    $urlPrevia = "https://api.develop.clientes.witzler.com.br/api/webprevia?data_previa=".$dataprevia."&unidade_id=".$unidadeId;
        $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    $recolheValoresPrevia = file_get_contents($urlPrevia, false, $context);
    $resultValoresPrevia = json_decode($recolheValoresPrevia);
    if(isset($resultValoresPrevia)){
        foreach($resultValoresPrevia as $previa){
            $data_previa = $previa->data_previa;
            $data_previa = substr($data_previa, 0, 10);
            $unidade_id = $previa->unidade_id;
            $maximoC = $previa->max_c;
            $minimoC = $previa->min_c;
            $contratado = $previa->contratado;
            $consumido = $previa->consumido;

            /*
            var_dump($data_previa);
            var_dump($unidade_id);
            var_dump($maximoC);
            var_dump($minimoC);
            var_dump($contratado);
            var_dump($consumido);//
       }
    }*/


}
else{
    
}
?>