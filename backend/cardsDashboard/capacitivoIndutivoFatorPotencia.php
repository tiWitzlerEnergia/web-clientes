<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
* Muda o valor de capacitivo e indutivo diário.


<script>
    var select = document.getElementById('idUnidades');
    var value = select.options[select.selectedIndex].id;
    var id_unidade = value;
    console.log("Id unidade MIRA: "+id_unidade);
</script>-->

<?php

//var_dump($_COOKIE);
//var_dump($_SESSION['id_unidade']);

if(!isset($ultimaData) && !isset($arrayIdOption[0])){ 
    //echo 'ERROR ID_UNIDADE';   
} else {

    $dataInicial = $ultimaData;
    $dataFinal = date("Y-m-d", strtotime('+1 days', strtotime($dataInicial)));
    if(isset($_COOKIE['id_unidades'])){
        $id_unidade = $_COOKIE['id_unidades'];
    }else{
        $id_unidade = $arrayIdOption[0];
    }

    //CARD FATOR POTENCIA 1:
    $urlFatorPotencia = "https://api.develop.clientes.witzler.com.br/api/media/fator/potencia/capacitivo?data_final=".$dataFinal."&data_inicial=".$dataInicial."&unidade_id=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));

    @$recolheFatorPotencia = file_get_contents($urlFatorPotencia, false, $context);
    @$resultFatorPotencia = json_decode($recolheFatorPotencia);
    $somaTotal = 0; //VARIAVEL QUE ARMAZENARA OS VALORES
    $total = 0;
    if(isset($resultFatorPotencia)){
        foreach($resultFatorPotencia as $fatorPotencia){
            $ativo_c = $fatorPotencia->ativo_c;
            $reativo_c = $fatorPotencia->reativo_c;
            $valorFinal = cos(atan($reativo_c/$ativo_c));
            $somaTotal += $valorFinal;
            $total++;
        }
        @$mediaAtual = $somaTotal/$total;
        @$mediaFormatadaAtual = number_format($media, 2, ',', '.'); // <- resultado final
    }
    //CARD FATOR POTENCIA INDUTIVO:
    $urlFatorPotenciaIndutivo = "https://api.develop.clientes.witzler.com.br/api/media/fator/potencia/indutivo?data_final=".$dataFinal."&data_inicial=".$dataInicial."&unidade_id=".$id_unidade;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));

    @$recolheFatorPotenciaIndutivo = file_get_contents($urlFatorPotenciaIndutivo, false, $context);
    @$resultFatorPotenciaIndutivo = json_decode($recolheFatorPotenciaIndutivo);
    $somaTotalIndutivo = 0; //VARIAVEL QUE ARMAZENARA OS VALORES
    $totalIndutivo = 0;
    if(isset($resultFatorPotenciaIndutivo)){
        foreach($resultFatorPotenciaIndutivo as $indutivo){
            $ativo_c_indutivo = $indutivo->ativo_c;
            $reativo_c_indutivo = $indutivo->reativo_c;
            $valorFinalIndutivo = cos(atan($reativo_c_indutivo/$ativo_c_indutivo));
            $somaTotalIndutivo += $valorFinalIndutivo;
            $totalIndutivo++;
        }
        @$mediaIndutivaAtual = $somaTotalIndutivo/$totalIndutivo;
        @$mediaFormatadaIndutivaAtual = number_format($mediaIndutiva, 2, ',', '.'); // <- resultado final
    }
}