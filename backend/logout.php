<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//Apagando cookies
unset($_COOKIE['id_unidades']);
unset($_COOKIE['calendario_unidade']);
unset($_COOKIE['final_mes']);
unset($_COOKIE['data_calendario']);
unset($_COOKIE['ano_novo_calendario']);
unset($_COOKIE['final_mes_calendario']);
unset($_COOKIE['ultimo_dia_calendario']);
unset($_COOKIE['mes_novo_calendario']);
unset($_COOKIE['data_consumo_dia']);
unset($_COOKIE['data_consumo_posto_tarifario']);
unset($_COOKIE['final_mes_consumo_posto_tarifario']);
unset($_COOKIE['data_fator_potencia']);
unset($_COOKIE['tipo_arquivo']);
unset($_COOKIE['data_unidade']);
unset($_COOKIE['data_tipo_arquivo_cliente']);
unset($_COOKIE['id_colaborador']);
unset($_COOKIE['admin_colaborador']);
setcookie('id_unidades', null, -1, '/');
setcookie('calendario_unidade', null, -1, '/');
setcookie('final_mes', null, -1, '/');
setcookie('data_caledario', null, -1, '/');
setcookie('ano_novo_calendario', null, -1, '/');
setcookie('final_mes_calendario', null, -1, '/');
setcookie('ultimo_dia_calendario', null, -1, '/');
setcookie('mes_novo_calendario', null, -1, '/');
setcookie('data_consumo_dia', null, -1, '/');
setcookie('data_consumo_posto_tarifario', null, -1, '/');
setcookie('final_mes_consumo_posto_tarifario', null, -1, '/');
setcookie('data_fator_potencia', null, -1, '/');
setcookie('tipo_arquivo', null, -1, '/');
setcookie('data_unidade', null, -1, '/');
setcookie('data_tipo_arquivo_cliente', null, -1, '/');
setcookie('id_colaborador', null, -1, '/');
setcookie('admin_colaborador', null, -1, '/');

//Apagando sessao php
session_start();
session_destroy();
header("Location: ../frontend/login/index.php?q=logout");
exit();
?>