<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
session_start();
$opcao = isset($_GET['opcao']) ? $_GET['opcao'] : '';
$valor = isset($_GET['valor']) ? $_GET['valor'] : '';
/*$id_unidades = isset($_GET['id_unidades']) ? $_GET['id_unidades'] : '';
$tipo_arquivo = isset($_GET['tipo_arquivo']) ? $_GET['tipo_arquivo'] : '';
$data_unidade = isset($_GET['data_unidade']) ? $_GET['data_unidade'] : '';*/
if(!empty($opcao)){
    switch($opcao){
        case 'id_unidades':{
            echo getAllIdUnidades();
            break;
        }
        case 'tipo_arquivo':{
            echo getFilterTipoArquivo($valor);
            break;
        }
    }
}

function getAllIdUnidades(){
    $urlUnidades = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=".$_SESSION['usuario'];
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    $recolheUnidades = file_get_contents($urlUnidades, false, $context);
    $resultUnidades = json_decode($recolheUnidades);
}

function getFilterTipoArquivo($id_unidades){
    $id_unidades = $_COOKIE['id_unidades'];
    $urlTipos = "https://api.develop.clientes.witzler.com.br/api/tipos/arquivos/unidade?unidade_id=".$id_unidades;
    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Bearer ".$_SESSION['token'],
        ),
    ));
    $recolheTipos = file_get_contents($urlTipos, false, $context);
    $resultTipos = json_decode($recolheTipos);
}

/*function getFilterDataUnidade($id_unidades){
    $id_unidades = $_COOKIE['id_unidades'];
    $urlDatas = "https://api.develop.clientes.witzler.com.br/api/datas/arquivos/unidade?unidade_id=".$id_unidades;
    $recolheDatas = file_get_contents($urlDatas);
    $resultDatas = json_decode($recolheDatas);
}

function getFilterDiretorioArquivo($data_unidade, $tipo_arquivo, $id_unidades){
    $data_unidade = $_COOKIE['data_unidade'];
    $tipo_arquivo = $_COOKIE['tipo_arquivo'];
    $id_unidade = $_COOKIE['id_unidades'];
    //URL:
    $urlPdf = 'https://api.develop.clientes.witzler.com.br/api/arquivos/unidade?data_referencia='.$data_unidade.'&tipo_arquivo='.$tipo_arquivo.'&unidade_id='.$id_unidade;
    $recolhePdf = file_get_contents($urlPdf);
    $resultPdf = json_decode($recolhePdf);
}*/
?>