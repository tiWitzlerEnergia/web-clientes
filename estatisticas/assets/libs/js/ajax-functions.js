function updateGraficoDispostivosData(ruta = '../') {
    var select = document.getElementById("selectDataDispositivo");
    var data_escolhida = select.options[select.selectedIndex].value;

    $.ajax({
        type: "POST",
        data: {
            "data_escolhida": data_escolhida
        },
        url: `${ruta}backend/config/ajax/updateDispositivosData.php`,
        beforeSend: function() {
            $("#cardAcessosDispositivo").html('<div class="cardLoading"><span class="dashboard-spinner spinner-sm"></span></div>');
        },
        success: function(responses) {
            // Caso onde a requisição deu certo
            console.log(responses);
            $("#cardAcessosDispositivo").html(responses);
        },
        error: function(request, status, error) {
            // Caso onde deu erro na requisição:
            console.log(`${status}: ${error} in ${request}`);
        }
    });
}

function updateGraficoSistemasOpData(ruta = '../') {
    var select = document.getElementById("selectDataSistemaOp");
    var data_escolhida = select.options[select.selectedIndex].value;

    $.ajax({
        type: "POST",
        data: {
            "data_escolhida": data_escolhida
        },
        url: `${ruta}backend/config/ajax/updateSistemasOpData.php`,
        beforeSend: function() {
            $("#cardAcessosSistemaOp").html('<div class="cardLoading"><span class="dashboard-spinner spinner-sm"></span></div>');
        },
        success: function(responses) {
            // Caso onde a requisição deu certo
            console.log(responses);
            $("#cardAcessosSistemaOp").html(responses);
        },
        error: function(request, status, error) {
            // Caso onde deu erro na requisição:
            console.log(`${status}: ${error} in ${request}`);
        }
    });
}

function updateGraficoRotasData(ruta = '../') {
    var select = document.getElementById("selectDataRotas");
    var data_escolhida = select.options[select.selectedIndex].value;

    $.ajax({
        type: "POST",
        data: {
            "data_escolhida": data_escolhida
        },
        url: `${ruta}backend/config/ajax/updateRotasData.php`,
        beforeSend: function() {
            $("#cardAcessosRotas").html('<div class="cardLoading"><span class="dashboard-spinner spinner-sm"></span></div>');
        },
        success: function(responses) {
            // Caso onde a requisição deu certo
            console.log(responses);
            $("#cardAcessosRotas").html(responses);
        },
        error: function(request, status, error) {
            // Caso onde deu erro na requisição:
            console.log(`${status}: ${error} in ${request}`);
        }
    });
}
