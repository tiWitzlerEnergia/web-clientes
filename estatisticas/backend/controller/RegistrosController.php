<?php
class RegistrosController
{
    // Function para listar os dados salvos de registro das informacoes do visitantes no Postgres:
    public function selectInfoRegistroPg()
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();

        $values = $model->listarRegistrosRequestPg();

        return $view->validateSelect($values);
    }

    // Function para listar os registros de dispositivos feitos dos visitantes na DB do postgres:
    public function listarRegistrosDispositivosPg()
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();

        $values = $model->listarRegistrosDispositivosRequestPg();

        return $view->validateSelect($values);
    }

    // Function para listar os dados por dispositivo salvo nas informacoes de visitantes Postgres:
    public function infoRegistroDispositivoPg($dispositivo)
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();

        $model->constructInfoRegistroDispositivo($dispositivo);
        $values = $model->infoRegistroDispositivoRequestPg();

        return $view->validateSelect($values);
    }

    // Function que vai listar os registros de system_name feitos dos visitantes na DB do Postgres:
    public function listarRegistrosSystemNamePg()
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();

        $values = $model->listarRegistrosSystemNameRequestPg();

        return $view->validateSelect($values);
    }

    // Function que vai listar os dados por route salvo nas informacoes de visitantes do Postgres:
    public function infoRegistroRotaPg($route)
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();

        $model->constructInfoRegistroRota($route);
        $values = $model->infoRegistroRotaRequestPg();

        return $view->validateSelect($values);
    }

    // Function que vai listar os registros de route feitos dos visitantes na DB do Postgres:
    public function listarRegistrosRotaPg()
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();

        $values = $model->listarRegistrosRotaRequestPg();

        return $view->validateSelect($values);
    }

    // Function que vai listar os dados por browser salvo nas informacoes de visitantes do Postgres:
    public function infoRegistroBrowserPg($browser)
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();

        $model->constructInfoRegistroBrowser($browser);
        $values = $model->infoRegistrosBrowserRequestPg();

        return $view->validateSelect($values);
    }

    // Function que vai listar os registros de route feitos dos visitantes na DB do Postgres:
    public function listarRegistrosBrowserPg()
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();

        $values = $model->listarRegistrosBrowserRequestPg();

        return $view->validateSelect($values);
    }

    // Function que vai listar os dados por dispositivo salvo nas informacoes de uma certa data na DB do Postgres:
    public function infoRegistroDispositivoPorDataPg($dispositivo, $data)
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();

        $model->constructRegistroDispositivoPorData($dispositivo, $data);
        $values = $model->infoRegistroDispositivoPorDataRequestPg();

        return $view->validateSelect($values);
    }

    // Function que vai listar os dados de todos os dispositivos salvos nas infromacoes de uma certa data na DB do Postgres:
    public function listaRegistroDispositivoPorDataPg($data)
    {
        // Call de classes necessarias:
        $view = new RegistrosView();
        $model = new Registros();

        $model->constructInfoRegistroData($data);
        $values = $model->listaRegistroDispositivoPorDataRequestPg();

        return $view->validateSelect($values);
    }

    // Function que vai listar os dados de todos os sistema operacionais salvos nas informacoes de uma certa data na DB do Postgres:
    public function listarRegistrosSystemNamePorDataPg($data)
    {
        // Call de classes necessarias
        $view = new RegistrosView();
        $model = new Registros();

        $model->constructInfoRegistroData($data);
        $values = $model->listarRegistrosSystemNamePorDataRequestPg();

        return $view->validateSelect($values);
    }

    // Function que vai listar os dados de todas as rotas salvas nas informacoes de uma certa data na DB do Postgres:
    public function listarRegistrosRotaPorDataPg($data)
    {
        // Call de classes necessarias
        $view = new RegistrosView();
        $model = new Registros();

        $model->constructInfoRegistroData($data);
        $values = $model->listarRegistrosRotaPorDataRequestPg();

        return $view->validateSelect($values);
    }

    // Function que cria o grafico horizontal de acessis por dispositvos:
    public function gerarGraficoHorizontalDispositivos($id, $id_element, $dispositivos = array("Desktop", "Android", "iPhone OS"))
    {
        // Call de classes e arrays necessarios:
        $view = new RegistrosView();
        $finalvalues = array();

        $totValues = count($dispositivos) ? count($dispositivos) : 0;

        $id = validateValue($id);
        $id_element = validateValue($id_element);

        for ($i = 0; $i < $totValues; $i++) {
            $dispositivo = validateValue($dispositivos[$i]);

            // Busca de valores dos dispositivos solicitados, verifica e armazena em novo array para mandar a montar o grafico:
            $values = $this->infoRegistroDispositivoPg($dispositivo);
            //$values = $this->infoRegistroDispositivoPg($dispositivo);
            if (!isset($values["status"])) {
                $tot = count($values) ? count($values) : 0;
                $finalvalues[] = array("labels" => $dispositivo, "data" => $tot);
            } else {
                $finalvalues[] = array("labels" => $dispositivo, "data" => 0);
            }
        }

        // Ja recolheu, contou e separou os valores, agora manda para o view criar o grafico
        $grafico = $view->criarGraficoHorizontalDispositivos($id, $id_element, $finalvalues);

        return $grafico;
    }

    // Function que cria o grafico horizontal de acessos por dispositvos em datas:
    public function gerarGraficoHorizontalDispositivosPorData($id, $id_element, $data)
    {
        // Call de classes e arrays necessarios:
        $view = new RegistrosView();
        $finalvalues = array();

        // DispositivosValues armazena os valores recolhidos do select
        $dispositivosValues = $this->listaRegistroDispositivoPorDataPg($data);

        $finalvalues = array();
        $indices = array();

        $totValues = count($dispositivosValues) ? count($dispositivosValues) : 0;

        $id = validateValue($id);
        $id_element = validateValue($id_element);

        if ($totValues > 0 && !isset($dispositivosValues["status"])) {
            for ($i = 0; $i < $totValues; $i++) {
                if (!is_null($dispositivosValues[$i]["dispositivo"])) {
                    $dispositivo = isset($dispositivosValues[$i]["dispositivo"]) && $dispositivosValues[$i]["dispositivo"] != "" ? trim(validateValue($dispositivosValues[$i]["dispositivo"])) : "N/D";
                    $tot = isset($dispositivosValues[$i]["tot"]) && is_numeric($dispositivosValues[$i]["tot"]) ? $dispositivosValues[$i]["tot"] : 0;

                    $finalvalues[] = array("labels" => $dispositivo, "data" => $tot);
                    $indices[] = $dispositivo;
                }
            }

            if (!in_array("Desktop", $indices)) {
                $finalvalues[] = array("labels" => "Desktop", "data" => 0);
            }

            if (!in_array("Android", $indices)) {
                $finalvalues[] = array("labels" => "Android", "data" => 0);
            }

            if (!in_array("iPhone OS", $indices)) {
                $finalvalues[] = array("labels" => "iPhone OS", "data" => 0);
            }

            // Ja recolheu, contou e separou os valores, agora manda para o view criar o grafico
            $grafico = $view->criarGraficoHorizontalDispositivos($id, $id_element, $finalvalues);
        } else {
            $grafico = "";
        }

        return $grafico;
    }

    // Function que crio o grafico horizontal de acessos por system_name:
    public function gerarGraficoHorizontalSystemName($id, $id_element)
    {
        // Call de classes e arrays necessarios:
        $view = new RegistrosView();
        // systemValues é a varaiavel que armazena os valores recolhidos do select
        $systemValues = $this->listarRegistrosSystemNamePg();
        //$systemValues = $this->listarRegistrosSystemNamePg();
        $finalvalues = array();
        $indices = array();

        $totValues = count($systemValues) ? count($systemValues) : 0;

        $id = validateValue($id);
        $id_element = validateValue($id_element);

        if ($totValues > 0 && !isset($systemValues["status"])) {
            for ($i = 0; $i < $totValues; $i++) {
                if (!is_null($systemValues[$i]["system_name"])) {
                    $system_name = isset($systemValues[$i]["system_name"]) && $systemValues[$i]["system_name"] != "" ? trim(validateValue($systemValues[$i]["system_name"])) : "N/D";
                    $tot = isset($systemValues[$i]["tot"]) && is_numeric($systemValues[$i]["tot"]) ? $systemValues[$i]["tot"] : 0;

                    $finalvalues[] = array("labels" => $system_name, "data" => $tot);
                    $indices[] = $system_name;
                }
            }

            if (!in_array("Windows", $indices)) {
                $finalvalues[] = array("labels" => "Windows", "data" => 0);
            }

            if (!in_array("Linux", $indices)) {
                $finalvalues[] = array("labels" => "Linux", "data" => 0);
            }

            if (!in_array("Macintosh", $indices)) {
                $finalvalues[] = array("labels" => "Macintosh", "data" => 0);
            }

            // Ja recolheu, contou e separou os valores, agora manda para o view criar o grafico
            $grafico = $view->criarGraficoHorizontalSystemName($id, $id_element, $finalvalues);
        } else {
            $grafico = "";
        }


        return $grafico;
    }

    // Function que crio o grafico horizontal de acessos por system_name e data:
    public function gerarGraficoHorizontalSystemNamePorData($id, $id_element, $data)
    {
        // Call de classes e arrays necessarios:
        $view = new RegistrosView();
        // systemValues é a varaiavel que armazena os valores recolhidos do select
        $systemValues = $this->listarRegistrosSystemNamePorDataPg($data);
        //$systemValues = $this->listarRegistrosSystemNamePg();
        $finalvalues = array();
        $indices = array();

        $totValues = count($systemValues) ? count($systemValues) : 0;

        $id = validateValue($id);
        $id_element = validateValue($id_element);

        if ($totValues > 0 && !isset($systemValues["status"])) {
            for ($i = 0; $i < $totValues; $i++) {
                if (!is_null($systemValues[$i]["system_name"])) {
                    $system_name = isset($systemValues[$i]["system_name"]) && $systemValues[$i]["system_name"] != "" ? trim(validateValue($systemValues[$i]["system_name"])) : "N/D";
                    $tot = isset($systemValues[$i]["tot"]) && is_numeric($systemValues[$i]["tot"]) ? $systemValues[$i]["tot"] : 0;

                    $finalvalues[] = array("labels" => $system_name, "data" => $tot);
                    $indices[] = $system_name;
                }
            }

            if (!in_array("Windows", $indices)) {
                $finalvalues[] = array("labels" => "Windows", "data" => 0);
            }

            if (!in_array("Linux", $indices)) {
                $finalvalues[] = array("labels" => "Linux", "data" => 0);
            }

            if (!in_array("Macintosh", $indices)) {
                $finalvalues[] = array("labels" => "Macintosh", "data" => 0);
            }

            // Ja recolheu, contou e separou os valores, agora manda para o view criar o grafico
            $grafico = $view->criarGraficoHorizontalSystemName($id, $id_element, $finalvalues);
        } else {
            $grafico = "";
        }


        return $grafico;
    }

    // Function que crio o grafico horizontal de acessos por route:
    public function gerarGraficoHorizontalRoute($id, $id_element)
    {
        // Call de classes e arrays necessarios:
        $view = new RegistrosView();
        // routesValues: Variavel que armazena o array dos valores recolhidos do select
        $routesValues = $this->listarRegistrosRotaPg();
        //$routesValues = $this->listarRegistrosRotaPg();
        $finalvalues = array();
        $indices = array();

        $totValues = count($routesValues) ? count($routesValues) : 0;

        $id = validateValue($id);
        $id_element = validateValue($id_element);

        if ($totValues > 0 && !isset($routesValues["status"])) {
            for ($i = 0; $i < $totValues; $i++) {
                if (!is_null($routesValues[$i]["route"])) {
                    $route = isset($routesValues[$i]["route"]) && $routesValues[$i]["route"] != "" ? explode("/", $routesValues[$i]["route"]) : explode(" ", "N/D N/D");
                    $ultimo = validateValue(end($route));
                    $name = ucfirst(validateValue(prev($route)));
                    $tot = isset($routesValues[$i]["tot"]) && is_numeric($routesValues[$i]["tot"]) ? $routesValues[$i]["tot"] : 0;

                    $finalvalues[] = array("labels" => $name, "data" => $tot);
                    $indices[] = $name;
                }
            }

            if (!in_array("Alerta-reativos", $indices)) {
                $finalvalues[] = array("labels" => "Alerta-reativos", "data" => 0);
            }

            if (!in_array("Calendario", $indices)) {
                $finalvalues[] = array("labels" => "Calendario", "data" => 0);
            }

            if (!in_array("Configuracoes", $indices)) {
                $finalvalues[] = array("labels" => "Configuracoes", "data" => 0);
            }

            if (!in_array("Contratos", $indices)) {
                $finalvalues[] = array("labels" => "Contratos", "data" => 0);
            }

            if (!in_array("Dashboard", $indices)) {
                $finalvalues[] = array("labels" => "Dashboard", "data" => 0);
            }

            if (!in_array("Historico-economia", $indices)) {
                $finalvalues[] = array("labels" => "Historico-economia", "data" => 0);
            }

            if (!in_array("Historico-medicoes", $indices)) {
                $finalvalues[] = array("labels" => "Historico-medicoes", "data" => 0);
            }

            if (!in_array("Map", $indices)) {
                $finalvalues[] = array("labels" => "Map", "data" => 0);
            }

            if (!in_array("Meus-arquivos", $indices)) {
                $finalvalues[] = array("labels" => "Meus-arquivos", "data" => 0);
            }

            if (!in_array("Migracao", $indices)) {
                $finalvalues[] = array("labels" => "Migracao", "data" => 0);
            }

            if (!in_array("NovaPagina", $indices)) {
                $finalvalues[] = array("labels" => "NovaPagina", "data" => 0);
            }

            if (!in_array("Perfil", $indices)) {
                $finalvalues[] = array("labels" => "Perfil", "data" => 0);
            }

            if (!in_array("Relatorio", $indices)) {
                $finalvalues[] = array("labels" => "Relatorio", "data" => 0);
            }

            if (!in_array("Unidades", $indices)) {
                $finalvalues[] = array("labels" => "Unidades", "data" => 0);
            }

            // Ja recolheu, contou e separou os valores, agora manda para o view criar o grafico
            $grafico = $view->criarGraficoHorizontalRoute($id, $id_element, $finalvalues);
        } else {
            $grafico = "";
        }

        return $grafico;
    }

    // Function que crio o grafico horizontal de acessos por route e data:
    public function gerarGraficoHorizontalRoutePorData($id, $id_element, $data)
    {
        // Call de classes e arrays necessarios:
        $view = new RegistrosView();
        // routesValues: Variavel que armazena o array dos valores recolhidos do select
        $routesValues = $this->listarRegistrosRotaPorDataPg($data);
        //$routesValues = $this->listarRegistrosRotaPg();
        $finalvalues = array();
        $indices = array();

        $totValues = count($routesValues) ? count($routesValues) : 0;

        $id = validateValue($id);
        $id_element = validateValue($id_element);

        if ($totValues > 0 && !isset($routesValues["status"])) {
            for ($i = 0; $i < $totValues; $i++) {
                if (!is_null($routesValues[$i]["route"])) {
                    $route = isset($routesValues[$i]["route"]) && $routesValues[$i]["route"] != "" ? explode("/", $routesValues[$i]["route"]) : explode(" ", "N/D N/D");
                    $ultimo = validateValue(end($route));
                    $name = ucfirst(validateValue(prev($route)));
                    $tot = isset($routesValues[$i]["tot"]) && is_numeric($routesValues[$i]["tot"]) ? $routesValues[$i]["tot"] : 0;

                    $finalvalues[] = array("labels" => $name, "data" => $tot);
                    $indices[] = $name;
                }
            }

            if (!in_array("Alerta-reativos", $indices)) {
                $finalvalues[] = array("labels" => "Alerta-reativos", "data" => 0);
            }

            if (!in_array("Calendario", $indices)) {
                $finalvalues[] = array("labels" => "Calendario", "data" => 0);
            }

            if (!in_array("Configuracoes", $indices)) {
                $finalvalues[] = array("labels" => "Configuracoes", "data" => 0);
            }

            if (!in_array("Contratos", $indices)) {
                $finalvalues[] = array("labels" => "Contratos", "data" => 0);
            }

            if (!in_array("Dashboard", $indices)) {
                $finalvalues[] = array("labels" => "Dashboard", "data" => 0);
            }

            if (!in_array("Historico-economia", $indices)) {
                $finalvalues[] = array("labels" => "Historico-economia", "data" => 0);
            }

            if (!in_array("Historico-medicoes", $indices)) {
                $finalvalues[] = array("labels" => "Historico-medicoes", "data" => 0);
            }

            if (!in_array("Map", $indices)) {
                $finalvalues[] = array("labels" => "Map", "data" => 0);
            }

            if (!in_array("Meus-arquivos", $indices)) {
                $finalvalues[] = array("labels" => "Meus-arquivos", "data" => 0);
            }

            if (!in_array("Migracao", $indices)) {
                $finalvalues[] = array("labels" => "Migracao", "data" => 0);
            }

            if (!in_array("NovaPagina", $indices)) {
                $finalvalues[] = array("labels" => "NovaPagina", "data" => 0);
            }

            if (!in_array("Perfil", $indices)) {
                $finalvalues[] = array("labels" => "Perfil", "data" => 0);
            }

            if (!in_array("Relatorio", $indices)) {
                $finalvalues[] = array("labels" => "Relatorio", "data" => 0);
            }

            if (!in_array("Unidades", $indices)) {
                $finalvalues[] = array("labels" => "Unidades", "data" => 0);
            }

            // Ja recolheu, contou e separou os valores, agora manda para o view criar o grafico
            $grafico = $view->criarGraficoHorizontalRoute($id, $id_element, $finalvalues);
        } else {
            $grafico = "";
        }

        return $grafico;
    }
}

