<?php
class GraficosController
{
    // Gerar grafico no geral:
    public function criarGraficoDinamico($id, $id_element, $labels, $data, $config)
    {
        // Inicia classes necessarias:
        $view = new GraficosView();
        $model = new Graficos($id, $id_element, $labels, $data, $config);

        $validate = $model->validateCriarGrafico();
        if ($validate == 200) {
            // Recolhe dados validados:
            $id = $model->getId();
            $id_element = $model->getIdElement();
            $labels = $model->getLabels();
            $data = $model->getData();
            $config = $model->getConfig();

            $dataLabels = $view->mostrarLabels($id, $labels);
            $dataData = $view->mostrarData($id, $data);
            $dataConfig = $view->mostrarConfig($id, $config);
            $dataChart = $view->mostrarChart($id, $id_element);

            return "
                $dataLabels
                $dataData
                $dataConfig
                $dataChart
            ";
        } else {
            return "";
        }
    }
}
