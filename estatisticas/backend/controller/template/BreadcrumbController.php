<?php
class BreadcrumbController
{
    // Classe que vai chamar o tipo da view:
    public function gerarBreadcrumb($tipoconta, $title = "Index", $text = "Descrição da página", $listItem = array(), $addConfig = "", $ruta = '..')
    {
        $view = new BreadcrumbView();
        $model = new Breadcrumb($title, $text, $listItem, $addConfig);
        $title = $model->getTitle();
        $text = $model->getText();
        $listItem = $model->getListItem();
        $addNew = $model->getAddNew();

        switch ($tipoconta) {
            case "admin":
                $view->generateAdmin($title, $text, $listItem, $ruta, $addNew);
                break;

            case "user":
                $view->generateUser($title, $text, $listItem, $ruta, $addNew);
                break;

            default:
                // Tipo de conta não valida, não retorna nada
                break;
        }
    }
}
