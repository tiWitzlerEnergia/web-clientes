<?php
class HeaderController
{
    // Function que vai chamar o tipo da view:
    public function gerarHeader($tipoconta, $usuario = 'Sem nome', $foto = "../assets/images/avatar-1.jpg", $company = "Sem nome", $addConfig = '', $ruta = '..', $rutaAssets = '..') {
        $view = new HeaderView();
        $model = new Header($usuario, $foto, $company, $addConfig);
        $addNew = $model->getAddNew();

        switch ($tipoconta) {
            case "admin":
                $view->generateAdmin($usuario, $foto, $company, $addNew, $ruta, $rutaAssets);
                break;

            case "user":
                $view->generateUser($usuario, $foto, $company, $addNew, $ruta, $rutaAssets);
                break;

            default:
                // Tipo de conta não valida, não retorna nada
                break;
        }
    }
}
