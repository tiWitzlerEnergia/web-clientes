<?php
session_start();
if (empty($_SESSION['usuario']) && empty($_SESSION['senha']) && empty($_SESSION['token'])) {
    //Verifica diretamente da API para saber se os dados nao foram manipulados:
    $url = "https://api.develop.clientes.witzler.com.br/api/usuarios/auth";
    $curl = curl_init();
    //Cria requisição dos dados:
    $request = '{
        "u_password": "' . $_SESSION['senha'] . '",
        "username": "' . $_SESSION['usuario'] . '"
    }';
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json']);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    $response = json_decode($result, true);
    if (empty($response['username']) && empty($response['token'])) {
        header("Location: ../../frontend/login/index.php?erro=nologin");
        exit();
    }

    if ($response['username'] != 'garen' || $_SESSION['usuario'] != 'garen') {
        header("Location: ../../frontend/login/index.php?erro=containvalida");
        exit();
    }
} else {
    //Verifica diretamente da API para saber se os dados nao foram manipulados:
    $url = "https://api.develop.clientes.witzler.com.br/api/usuarios/auth";
    $curl = curl_init();
    //Cria requisição dos dados:
    $request = '{
            "u_password": "' . $_SESSION['senha'] . '",
            "username": "' . $_SESSION['usuario'] . '"
        }';
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json']);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    $response = json_decode($result, true);
    if (empty($response['username']) && empty($response['token'])) {
        header("Location: ../../frontend/login/index.php?erro=nologin");
        exit();
    }

    if ($response['username'] != 'garen' || $_SESSION['usuario'] != 'garen') {
        header("Location: ../../frontend/login/index.php?erro=containvalida");
        exit();
    }
}

//Apagando cookies
unset($_COOKIE['id_unidades']);
unset($_COOKIE['calendario_unidade']);
unset($_COOKIE['final_mes']);
unset($_COOKIE['data_calendario']);
unset($_COOKIE['ano_novo_calendario']);
unset($_COOKIE['final_mes_calendario']);
unset($_COOKIE['ultimo_dia_calendario']);
unset($_COOKIE['mes_novo_calendario']);
unset($_COOKIE['data_consumo_dia']);
unset($_COOKIE['data_consumo_posto_tarifario']);
unset($_COOKIE['final_mes_consumo_posto_tarifario']);
unset($_COOKIE['data_fator_potencia']);
unset($_COOKIE['tipo_arquivo']);
unset($_COOKIE['data_unidade']);
unset($_COOKIE['data_tipo_arquivo_cliente']);
setcookie('id_unidades', null, -1, '/');
setcookie('calendario_unidade', null, -1, '/');
setcookie('final_mes', null, -1, '/');
setcookie('data_caledario', null, -1, '/');
setcookie('ano_novo_calendario', null, -1, '/');
setcookie('final_mes_calendario', null, -1, '/');
setcookie('ultimo_dia_calendario', null, -1, '/');
setcookie('mes_novo_calendario', null, -1, '/');
setcookie('data_consumo_dia', null, -1, '/');
setcookie('data_consumo_posto_tarifario', null, -1, '/');
setcookie('final_mes_consumo_posto_tarifario', null, -1, '/');
setcookie('data_fator_potencia', null, -1, '/');
setcookie('tipo_arquivo', null, -1, '/');
setcookie('data_unidade', null, -1, '/');
setcookie('data_tipo_arquivo_cliente', null, -1, '/');

