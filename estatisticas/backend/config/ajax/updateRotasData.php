<?php
require_once("../../autoloadEstatisticas.php");
$registros = new RegistrosController();

$data_escolhida = isset($_POST["data_escolhida"]) && $_POST["data_escolhida"] != '' ? validateValue($_POST["data_escolhida"]) : "hoje";

$grafRoutes = $registros->gerarGraficoHorizontalRoutePorData("routes", "chartRoutes", $data_escolhida);

if ($grafRoutes != "") {
    echo "
        <canvas id=\"chartRoutes\" style=\"height: 350px; width: 100%;\"></canvas>
        <script>
            $grafRoutes
        </script>
    ";
} else {
    echo "
        <div class=\"cardLoading\">
            <h1>Sem registros para a opção selecionada</h1>
        </div>
    ";
}
