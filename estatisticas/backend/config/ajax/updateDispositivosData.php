<?php
require_once("../../autoloadEstatisticas.php");
$registros = new RegistrosController();

$data_escolhida = isset($_POST["data_escolhida"]) && $_POST["data_escolhida"] != '' ? validateValue($_POST["data_escolhida"]) : "hoje";

$grafDispositivos = $registros->gerarGraficoHorizontalDispositivosPorData("dispositivos", "chartDispostivos", $data_escolhida);

if ($grafDispositivos != "") {
    echo "
        <canvas id=\"chartDispostivos\" style=\"height: 300px; width: 100%;\"></canvas>
        <script>
            $grafDispositivos
        </script>
    ";
} else {
    echo "
        <div class=\"cardLoading\">
            <h1>Sem registros para a opção selecionada</h1>
        </div>
    ";
}
