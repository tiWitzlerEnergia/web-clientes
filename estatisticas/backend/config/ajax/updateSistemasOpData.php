<?php
require_once("../../autoloadEstatisticas.php");
$registros = new RegistrosController();

$data_escolhida = isset($_POST["data_escolhida"]) && $_POST["data_escolhida"] != '' ? validateValue($_POST["data_escolhida"]) : "hoje";

$grafSystemName = $registros->gerarGraficoHorizontalSystemNamePorData("systemName", "chartSistemas", $data_escolhida);

if ($grafSystemName != "") {
    echo "
        <canvas id=\"chartSistemas\" style=\"height: 300px; width: 100%;\"></canvas>
        <script>
            $grafSystemName
        </script>
    ";
} else {
    echo "
        <div class=\"cardLoading\">
            <h1>Sem registros para a opção selecionada</h1>
        </div>
    ";
}
