<?php
class PgConnectView
{
    public function validateQuery($values)
    {
        if (is_array($values)) {
            if (isset($values["iderror"]) && isset($values["msg"])) {
                return $values;
            }
        } else {
            //return 200;
            return array("idstatus" => 200, "msg" => "Query executada");
        }
    }

    public function validateFetchAll($values)
    {
        if (is_array($values)) {
            // Deu tudo certo, retorna os valores:
            return $values;
        } else {
            // Aconteceu algo errado, retorna error:
            return array();
        }
    }

    public function validateRowsAffected($values)
    {
        if ($values->rowCount() > 0) {
            return array("idstatus" => 200, "msg" => "Foi atualizado");
        } else {
            // Nenhuma alteracao foi feita:
            return array("iderror" => 406, "msg" => "Não aconteceu nenhuma alteração. Tente novamente");
        }
    }
}
