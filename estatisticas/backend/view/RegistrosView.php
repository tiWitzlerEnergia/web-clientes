<?php
class RegistrosView
{
    // Function que verifica que deu certo insert:
    public function responseRequest($values)
    {
        $msg = new MessagesController();
        if (isset($values["idstatus"]) && $values["idstatus"] = 200) {
            return $msg->message($values["idstatus"], $values["msg"]);
        } else {
            return $msg->message($values["iderror"], $values["msg"]);
        }
    }

    // Function que verifica se select retornou dados ou se eles estão vazios:
    public function validateSelect($values)
    {
        if (isset($values) && !empty($values)) {
            return $values;
        } else {
            $msg = new MessagesController();

            $error = array("iderror" => 404, "msg" => "Não tem nenhum registro feito.");
            return $msg->message($error["iderror"], $error["msg"]);
        }
    }

    // Function que vai montar o grafico horizontal de dispositivos:
    public function criarGraficoHorizontalDispositivos($id, $id_element, $values)
    {
        // Instancia de classes necessarias
        $grafico = new GraficosController();

        if (isset($values) && !empty($values)) {
            // Cria arrays para separar valores de $values  strings:
            $arrBackgroundColor = array('rgba(255, 99, 132, 0.2)', 'rgba(255, 159, 64, 0.2)', 'rgba(255, 205, 86, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(153, 102, 255, 0.2)', 'rgba(201, 203, 207, 0.2)');
            $arrBorderColor = array('rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)');

            $labels_string = "";
            $data_string = "";
            $backgroundColor_string = "";
            $borderColor_string = "";

            // Separa values:
            $totValues = count($values) ? count($values) : 0;
            for ($i = 0; $i < $totValues; $i++) {
                $label = $values[$i]["labels"] && $values[$i]["labels"] != "" ? $values[$i]["labels"] : "N/D";
                $data_value = $values[$i]["data"] && is_numeric($values[$i]["data"]) ? $values[$i]["data"] : 0;
                $backgroundColor_value = $arrBackgroundColor[$i];
                $borderColor_value = $arrBorderColor[$i];

                $labels_string .= "'$label', ";
                $data_string .= "$data_value, ";
                $backgroundColor_string .= "'$backgroundColor_value', ";
                $borderColor_string .= "'$borderColor_value', ";
            }

            $labels = $labels_string;
            $data = "
                datasets: [{
                    axis: 'y',
                    label: 'Total acessos no dispositivo',
                    data: [$data_string],
                    fill: false,
                    backgroundColor: [$backgroundColor_string],
                    borderColor: [$borderColor_string],
                    borderWidth: 1
                }]
            ";
            $config = "
                type: 'bar',
                options: {
                    indexAxis: 'y',
                    //responsive: false,
                    maintainAspectRatio: false,
                    aspectRatio: 1
                }
            ";

            $final = $grafico->criarGraficoDinamico($id, $id_element, $labels, $data, $config);
        } else {
            $final = "";
        }

        return $final;
    }

    // Function que vai montar o grafico horizontal de system_name:
    public function criarGraficoHorizontalSystemName($id, $id_element, $values)
    {
        // Instancia de classes necessarias
        $grafico = new GraficosController();

        if (isset($values) && !empty($values)) {
            // Cria arrays para separar valores de $values  strings:
            $arrBackgroundColor = array('rgba(255, 99, 132, 0.2)', 'rgba(255, 159, 64, 0.2)', 'rgba(255, 205, 86, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(153, 102, 255, 0.2)', 'rgba(201, 203, 207, 0.2)');
            $arrBorderColor = array('rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)');

            $labels_string = "";
            $data_string = "";
            $backgroundColor_string = "";
            $borderColor_string = "";

            // Separa values:
            $totValues = count($values) ? count($values) : 0;
            for ($i = 0; $i < $totValues; $i++) {
                $label = $values[$i]["labels"] && $values[$i]["labels"] != "" ? $values[$i]["labels"] : "N/D";
                $data_value = $values[$i]["data"] && is_numeric($values[$i]["data"]) ? $values[$i]["data"] : 0;
                $backgroundColor_value = $arrBackgroundColor[$i];
                $borderColor_value = $arrBorderColor[$i];

                $labels_string .= "'$label', ";
                $data_string .= "$data_value, ";
                $backgroundColor_string .= "'$backgroundColor_value', ";
                $borderColor_string .= "'$borderColor_value', ";
            }

            $labels = $labels_string;
            $data = "
                datasets: [{
                    axis: 'y',
                    label: 'Total acessos no sistema operacional',
                    data: [$data_string],
                    fill: false,
                    backgroundColor: [$backgroundColor_string],
                    borderColor: [$borderColor_string],
                    borderWidth: 1
                }]
            ";

            $config = "
                type: 'bar',
                options: {
                    indexAxis: 'y',
                    //responsive: false,
                    maintainAspectRatio: false,
                    aspectRatio: 1
                }
            ";

            $final = $grafico->criarGraficoDinamico($id, $id_element, $labels, $data, $config);
        } else {
            $final = "";
        }

        return $final;
    }

    // Function que vai montar o grafico horizontal de system_name:
    public function criarGraficoHorizontalRoute($id, $id_element, $values)
    {
        // Instancia de classes necessarias
        $grafico = new GraficosController();

        if (isset($values) && !empty($values)) {
            // Cria arrays para separar valores de $values  strings:
            $arrBackgroundColor = array('rgba(255, 99, 132, 0.2)', 'rgba(255, 159, 64, 0.2)', 'rgba(255, 205, 86, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(153, 102, 255, 0.2)', 'rgba(201, 203, 207, 0.2)');
            $arrBorderColor = array('rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)');

            $labels_string = "";
            $data_string = "";
            $backgroundColor_string = "";
            $borderColor_string = "";

            // Separa values:
            $totValues = count($values) ? count($values) : 0;
            for ($i = 0; $i < $totValues; $i++) {
                $label = $values[$i]["labels"] && $values[$i]["labels"] != "" ? $values[$i]["labels"] : "N/D";
                $data_value = $values[$i]["data"] && is_numeric($values[$i]["data"]) ? $values[$i]["data"] : 0;
                $backgroundColor_value = $arrBackgroundColor[$i];
                $borderColor_value = $arrBorderColor[$i];

                $labels_string .= "'$label', ";
                $data_string .= "$data_value, ";
                $backgroundColor_string .= "'$backgroundColor_value', ";
                $borderColor_string .= "'$borderColor_value', ";
            }

            $labels = $labels_string;
            $data = "
                datasets: [{
                    axis: 'y',
                    label: 'Total acessos no sistema operacional',
                    data: [$data_string],
                    fill: false,
                    backgroundColor: [$backgroundColor_string],
                    borderColor: [$borderColor_string],
                    borderWidth: 1
                }]
            ";

            $config = "
                type: 'bar',
                options: {
                    indexAxis: 'y',
                    //responsive: false,
                    maintainAspectRatio: false,
                    aspectRatio: 1
                }
            ";

            $final = $grafico->criarGraficoDinamico($id, $id_element, $labels, $data, $config);
        } else {
            $final = "";
        }

        return $final;
    }
}
