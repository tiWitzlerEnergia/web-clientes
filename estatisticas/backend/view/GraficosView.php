<?php
class GraficosView
{
    // Return do labels:
    public function mostrarLabels($id, $values)
    {
        if (isset($id) && $id != "" && isset($values) && $values != "") {
            $id = ucfirst(strtolower($id));

            return "
                var labels$id = [
                    $values
                ];
            ";
        }
    }

    // Return do data:
    public function mostrarData($id, $values)
    {
        if (isset($id) && $id != "" && isset($values) && $values != "") {
            $id = ucfirst(strtolower($id));

            return "
                var data$id = {
                    labels: labels$id,
                    $values
                };
            ";
        }
    }

    // Return do config:
    public function mostrarConfig($id, $values)
    {
        if (isset($id) && $id != "" && isset($values) && $values != "") {
            $id = ucfirst(strtolower($id));

            return "
                var config$id = {
                    data: data$id,
                    $values
                };
            ";
        }
    }

    // Return do chart:
    public function mostrarChart($id, $id_element)
    {
        if (isset($id) && $id != "" && isset($id_element) && $id_element != "") {
            $idChart = ucfirst(strtolower($id));

            return "
                var $id = new Chart(
                    document.getElementById('$id_element'),
                    config$idChart
                );
            ";
        }
    }
}

