<?php
class MenuView
{
    // SCRIPT E CONFIGS PARA ADM
    public function generateAdmin($ativo, $idusuario, $usuario, $addConfig, $ruta, $rutaAssets)
    {
        echo "
            <div class=\"nav-left-sidebar sidebar-dark\">
                <div class=\"menu-list\">
                    <nav class=\"navbar navbar-expand-lg navbar-light\">
                        <a class=\"d-xl-none d-lg-none\" href=\"#\">Dashboard</a>
                        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                            <span class=\"navbar-toggler-icon\"></span>
                        </button>
                        <div class=\"collapse navbar-collapse\" id=\"navbarNav\">
                            <ul class=\"navbar-nav flex-column\">
                                <li class=\"nav-divider\">
                                    Menu
                                </li>
                                <li class=\"nav-item \">
                                    <a class=\"nav-link\" href=\"#\" data-toggle=\"collapse\" aria-expanded=\"false\" data-target=\"#submenu-1\" aria-controls=\"submenu-1\"><i class=\"fa fa-fw fa-user-circle\"></i>Dashboard <span class=\"badge badge-success\">6</span></a>
                                    <div id=\"submenu-1\" class=\"collapse submenu\" style=\"\">
                                        <ul class=\"nav flex-column\">
                                            <li class=\"nav-item\">
                                                <a class=\"nav-link\" id=\"index-dashboard\" href=\"index.php\">Index</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        ";
        

        if ($ativo) {
            echo "
                <script>
                    const elemento = document.getElementById(\"$ativo\");
                    if (elemento.classList) elemento.classList.add(\"active\");
                    else elemento.className += \" active\";
                </script>
            ";
        }
    }

    // SCRIPT E CONFIGS PARA USER
    public function generateUser($ativo, $idusuario, $usuario, $addConfig, $ruta, $rutaAssets)
    {
        echo "
            <div class=\"nav-left-sidebar sidebar-dark\">
                <div class=\"menu-list\">
                    <nav class=\"navbar navbar-expand-lg navbar-light\">
                        <a class=\"d-xl-none d-lg-none\" href=\"#\">Dashboard</a>
                        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                            <span class=\"navbar-toggler-icon\"></span>
                        </button>
                        <div class=\"collapse navbar-collapse\" id=\"navbarNav\">
                            <ul class=\"navbar-nav flex-column\">
                                <li class=\"nav-divider\">
                                    Menu
                                </li>
                                <li class=\"nav-item \">
                                    <a class=\"nav-link\" href=\"#\" data-toggle=\"collapse\" aria-expanded=\"false\" data-target=\"#submenu-1\" aria-controls=\"submenu-1\"><i class=\"fa fa-fw fa-user-circle\"></i>Dashboard <span class=\"badge badge-success\">6</span></a>
                                    <div id=\"submenu-1\" class=\"collapse submenu\" style=\"\">
                                        <ul class=\"nav flex-column\">
                                            <li class=\"nav-item\">
                                                <a class=\"nav-link\" id=\"index-dashboard\" href=\"index.php\">Index</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        ";

        if ($ativo) {
            echo "
                <script>
                    const elemento = document.getElementById(\"$ativo\");
                    if (elemento.classList) elemento.classList.add(\"active\");
                    else elemento.className += \" active\";
                </script>
            ";
        }
    }
}
