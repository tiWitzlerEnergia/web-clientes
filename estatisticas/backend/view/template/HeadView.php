<?php
class HeadView
{
    // SCRIPT E CONFIGS PARA ADM
    public function generateAdmin($title, $addConfig, $ruta, $checkLogin)
    {
        if ($checkLogin) {
            //Implementa a verificação do login do dashboard dentro do sistema
            //$session = new CheckLoginController();
            //$session->check();
            //require_once("../../functions/verificador-estatistica.php");
            if (empty($_SESSION['usuario']) && empty($_SESSION['senha']) && empty($_SESSION['token'])) {
                //Verifica diretamente da API para saber se os dados nao foram manipulados:
                $url = "https://api.develop.clientes.witzler.com.br/api/usuarios/auth";
                $curl = curl_init();
                //Cria requisição dos dados:
                $request = '{
                    "u_password": "' . $_SESSION['senha'] . '",
                    "username": "' . $_SESSION['usuario'] . '"
                }';
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json']);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($curl);
                $response = json_decode($result, true);
                if (empty($response['username']) && empty($response['token'])) {
                    header("Location: ../../frontend/login/index.php?erro=nologin");
                    exit();
                }

                if ($response['username'] != 'garen' || $_SESSION['usuario'] != 'garen') {
                    header("Location: ../../frontend/login/index.php?erro=containvalida");
                    exit();
                }
            } else {
                //Verifica diretamente da API para saber se os dados nao foram manipulados:
                $url = "https://api.develop.clientes.witzler.com.br/api/usuarios/auth";
                $curl = curl_init();
                //Cria requisição dos dados:
                $request = '{
                        "u_password": "' . $_SESSION['senha'] . '",
                        "username": "' . $_SESSION['usuario'] . '"
                    }';
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json']);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($curl);
                $response = json_decode($result, true);
                if (empty($response['username']) && empty($response['token'])) {
                    header("Location: ../../frontend/login/index.php?erro=nologin");
                    exit();
                }

                if ($response['username'] != 'garen' || $_SESSION['usuario'] != 'garen') {
                    header("Location: ../../frontend/login/index.php?erro=containvalida");
                    exit();
                }
            }

            //Apagando cookies
            unset($_COOKIE['id_unidades']);
            unset($_COOKIE['calendario_unidade']);
            unset($_COOKIE['final_mes']);
            unset($_COOKIE['data_calendario']);
            unset($_COOKIE['ano_novo_calendario']);
            unset($_COOKIE['final_mes_calendario']);
            unset($_COOKIE['ultimo_dia_calendario']);
            unset($_COOKIE['mes_novo_calendario']);
            unset($_COOKIE['data_consumo_dia']);
            unset($_COOKIE['data_consumo_posto_tarifario']);
            unset($_COOKIE['final_mes_consumo_posto_tarifario']);
            unset($_COOKIE['data_fator_potencia']);
            unset($_COOKIE['tipo_arquivo']);
            unset($_COOKIE['data_unidade']);
            unset($_COOKIE['data_tipo_arquivo_cliente']);
            setcookie('id_unidades', null, -1, '/');
            setcookie('calendario_unidade', null, -1, '/');
            setcookie('final_mes', null, -1, '/');
            setcookie('data_caledario', null, -1, '/');
            setcookie('ano_novo_calendario', null, -1, '/');
            setcookie('final_mes_calendario', null, -1, '/');
            setcookie('ultimo_dia_calendario', null, -1, '/');
            setcookie('mes_novo_calendario', null, -1, '/');
            setcookie('data_consumo_dia', null, -1, '/');
            setcookie('data_consumo_posto_tarifario', null, -1, '/');
            setcookie('final_mes_consumo_posto_tarifario', null, -1, '/');
            setcookie('data_fator_potencia', null, -1, '/');
            setcookie('tipo_arquivo', null, -1, '/');
            setcookie('data_unidade', null, -1, '/');
            setcookie('data_tipo_arquivo_cliente', null, -1, '/');
        }

        echo "
            <!doctype html>
            <html lang=\"en\">
            
            <head>
                <!-- Required meta tags -->
                <meta charset=\"utf-8\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
                <!-- Bootstrap CSS -->
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/bootstrap/css/bootstrap.min.css\">
                <link href=\"$ruta/assets/vendor/fonts/circular-std/style.css\" rel=\"stylesheet\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/libs/css/style.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/fonts/fontawesome/css/fontawesome-all.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/charts/chartist-bundle/chartist.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/charts/morris-bundle/morris.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/charts/c3charts/c3.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/fonts/flag-icon-css/flag-icon.min.css\">
                <title>$title</title>
                <!-- jquery 3.3.1 -->
                <script src=\"$ruta/assets/vendor/jquery/jquery-3.3.1.min.js\"></script>
                $addConfig
            </head>
        ";
    }

    // SCRIPT E CONFIGS PARA USER
    public function generateUser($title, $addConfig, $ruta, $checkLogin)
    {
        if ($checkLogin) {
            //Implementa a verificação do login do dashboard dentro do sistema
            //$session = new CheckLoginController();
            //$session->check();
            if (empty($_SESSION['usuario']) && empty($_SESSION['senha']) && empty($_SESSION['token'])) {
                //Verifica diretamente da API para saber se os dados nao foram manipulados:
                $url = "https://api.develop.clientes.witzler.com.br/api/usuarios/auth";
                $curl = curl_init();
                //Cria requisição dos dados:
                $request = '{
                    "u_password": "' . $_SESSION['senha'] . '",
                    "username": "' . $_SESSION['usuario'] . '"
                }';
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json']);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($curl);
                $response = json_decode($result, true);
                if (empty($response['username']) && empty($response['token'])) {
                    header("Location: ../../frontend/login/index.php?erro=nologin");
                    exit();
                }

                if ($response['username'] != 'garen' || $_SESSION['usuario'] != 'garen') {
                    header("Location: ../../frontend/login/index.php?erro=containvalida");
                    exit();
                }
            } else {
                //Verifica diretamente da API para saber se os dados nao foram manipulados:
                $url = "https://api.develop.clientes.witzler.com.br/api/usuarios/auth";
                $curl = curl_init();
                //Cria requisição dos dados:
                $request = '{
                        "u_password": "' . $_SESSION['senha'] . '",
                        "username": "' . $_SESSION['usuario'] . '"
                    }';
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json']);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($curl);
                $response = json_decode($result, true);
                if (empty($response['username']) && empty($response['token'])) {
                    header("Location: ../../frontend/login/index.php?erro=nologin");
                    exit();
                }

                if ($response['username'] != 'garen' || $_SESSION['usuario'] != 'garen') {
                    header("Location: ../../frontend/login/index.php?erro=containvalida");
                    exit();
                }
            }

            //Apagando cookies
            unset($_COOKIE['id_unidades']);
            unset($_COOKIE['calendario_unidade']);
            unset($_COOKIE['final_mes']);
            unset($_COOKIE['data_calendario']);
            unset($_COOKIE['ano_novo_calendario']);
            unset($_COOKIE['final_mes_calendario']);
            unset($_COOKIE['ultimo_dia_calendario']);
            unset($_COOKIE['mes_novo_calendario']);
            unset($_COOKIE['data_consumo_dia']);
            unset($_COOKIE['data_consumo_posto_tarifario']);
            unset($_COOKIE['final_mes_consumo_posto_tarifario']);
            unset($_COOKIE['data_fator_potencia']);
            unset($_COOKIE['tipo_arquivo']);
            unset($_COOKIE['data_unidade']);
            unset($_COOKIE['data_tipo_arquivo_cliente']);
            setcookie('id_unidades', null, -1, '/');
            setcookie('calendario_unidade', null, -1, '/');
            setcookie('final_mes', null, -1, '/');
            setcookie('data_caledario', null, -1, '/');
            setcookie('ano_novo_calendario', null, -1, '/');
            setcookie('final_mes_calendario', null, -1, '/');
            setcookie('ultimo_dia_calendario', null, -1, '/');
            setcookie('mes_novo_calendario', null, -1, '/');
            setcookie('data_consumo_dia', null, -1, '/');
            setcookie('data_consumo_posto_tarifario', null, -1, '/');
            setcookie('final_mes_consumo_posto_tarifario', null, -1, '/');
            setcookie('data_fator_potencia', null, -1, '/');
            setcookie('tipo_arquivo', null, -1, '/');
            setcookie('data_unidade', null, -1, '/');
            setcookie('data_tipo_arquivo_cliente', null, -1, '/');
        }

        echo "
            <!doctype html>
            <html lang=\"en\">
            
            <head>
                <!-- Required meta tags -->
                <meta charset=\"utf-8\">
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
                <!-- Bootstrap CSS -->
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/bootstrap/css/bootstrap.min.css\">
                <link href=\"$ruta/assets/vendor/fonts/circular-std/style.css\" rel=\"stylesheet\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/libs/css/style.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/fonts/fontawesome/css/fontawesome-all.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/charts/chartist-bundle/chartist.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/charts/morris-bundle/morris.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/charts/c3charts/c3.css\">
                <link rel=\"stylesheet\" href=\"$ruta/assets/vendor/fonts/flag-icon-css/flag-icon.min.css\">
                <title>$title</title>
                <!-- jquery 3.3.1 -->
                <script src=\"$ruta/assets/vendor/jquery/jquery-3.3.1.min.js\"></script>
                $addConfig
            </head>
        ";
    }
}

