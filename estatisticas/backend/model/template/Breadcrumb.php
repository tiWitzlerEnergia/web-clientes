<?php
class Breadcrumb
{
    // Atributos:
    private $title;
    private $text;
    private $listItem;
    private $addNew;

    // Metodos especiais:
    public function __construct($title, $text, $listItem, $addNew = "")
    {
        $this->setTitle($title);
        $this->setText($text);
        $this->setListItem($listItem);
        $this->setAddNew($addNew);
    }

    /**
     * Get the value of title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     */
    public function setTitle($title): self
    {
        $this->title = validateValue($title);

        return $this;
    }

    /**
     * Get the value of text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set the value of text
     */
    public function setText($text): self
    {
        $this->text = validateValue($text);

        return $this;
    }

    /**
     * Get the value of listItem
     */
    public function getListItem()
    {
        return $this->listItem;
    }

    /**
     * Set the value of listItem
     */
    public function setListItem($listItem): self
    {
        $totItem = count($listItem) ? count($listItem) : 0;
        $final = array();

        if ($totItem > 0) {
            // Sanitiza cada item passado
            for ($i = 0; $i < $totItem; $i++) {
                $value = isset($listItem[$i]) && $listItem[$i] != "" ? validateValue($listItem[$i]) : "";
                array_push($final, $value);
            }
        }

        $this->listItem = $final;

        return $this;
    }

    /**
     * Get the value of addNew
     */
    public function getAddNew()
    {
        return $this->addNew;
    }

    /**
     * Set the value of addNew
     */
    public function setAddNew($addNew): self
    {
        $this->addNew = $addNew;

        return $this;
    }
}
