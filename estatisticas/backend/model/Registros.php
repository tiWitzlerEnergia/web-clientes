<?php
// Classe model da tabela "pc.web_login_acesso"
class Registros
{
    // Atributos:
    private $id;
    private $system;
    private $browser;
    private $system_name;
    private $dispositivo;
    private $route;
    private $data;

    // Metodos especiais
    public function constructInfoRegistroDispositivo($dispositivo)
    {
        $this->setDispositivo($dispositivo);
    }

    public function constructInfoRegistroRota($route)
    {
        $this->setRoute($route);
    }

    public function constructInfoRegistroBrowser($browser)
    {
        $this->setBrowser($browser);
    }

    public function constructInfoRegistroData($data)
    {
        $this->setData($data);
    }

    public function constructRegistroDispositivoPorData($dispositivo, $data)
    {
        $this->setDispositivo($dispositivo);
        $this->setData($data);
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */
    public function setId($id): self
    {
        $this->id = validateValue($id);

        return $this;
    }

    /**
     * Get the value of system
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * Set the value of system
     */
    public function setSystem($system): self
    {
        $this->system = validateValue($system);

        return $this;
    }

    /**
     * Get the value of browser
     */
    public function getBrowser()
    {
        return $this->browser;
    }

    /**
     * Set the value of browser
     */
    public function setBrowser($browser): self
    {
        $this->browser = validateValue($browser);

        return $this;
    }

    /**
     * Get the value of system_name
     */
    public function getSystemName()
    {
        return $this->system_name;
    }

    /**
     * Set the value of system_name
     */
    public function setSystemName($system_name): self
    {
        $this->system_name = validateValue($system_name);

        return $this;
    }

    /**
     * Get the value of dispositivo
     */
    public function getDispositivo()
    {
        return $this->dispositivo;
    }

    /**
     * Set the value of dispositivo
     */
    public function setDispositivo($dispositivo): self
    {
        $this->dispositivo = validateValue($dispositivo);

        return $this;
    }

    /**
     * Get the value of route
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set the value of route
     */
    public function setRoute($route): self
    {
        $this->route = validateValue($route);

        return $this;
    }

    /**
     * Get the value of data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     */
    public function setData($data): self
    {
        $this->data = validateValue($data);

        return $this;
    }

    // Metodos publicos:
    public function validateInfoRegistroDispositivo()
    {
        $dispositivo = $this->getDispositivo();

        if ($dispositivo && is_null($dispositivo)) {
            // Validate dispositivo
            return 'Invalid dispositivo';
        } else {
            return 200;
        }
    }

    public function validateInfoRegistroRota()
    {
        $route = $this->getRoute();

        if ($route && is_null($route)) {
            // Validate route
            return 'Invalid route';
        } else {
            return 200;
        }
    }

    public function validateInfoRegistroBrowser()
    {
        $browser = $this->getBrowser();

        if ($browser && is_null($browser)) {
            // Validate browser
            return 'Invalid browser';
        } else {
            return 200;
        }
    }

    public function validateInfoRegistroData()
    {
        $data = $this->getData();

        if ($data && is_null($data)) {
            // Validate data
            return 'Invalid data';
        } else {
            return 200;
        }
    }

    public function validateRegistroDispositivoPorData()
    {
        $dispositivo = $this->getDispositivo();
        $data = $this->getData();

        if ($dispositivo && is_null($dispositivo)) {
            // Validate dispositivo
            return 'Invalid dispositivo';
        } else if ($data && is_null($data)) {
            // Validate data
            return 'Invalid data';
        } else {
            return 200;
        }
    }

    // Request para listar todas as informacoes salvas de sistema e browser dos visitantes na db do postgres:
    public function listarRegistrosRequestPg()
    {
        $pdo = new PgConnectController();
        $result = $pdo->selectQuery("SELECT * FROM pc.web_login_acesso");

        return $result;
    }

    // Request para listar os registros de dispositivos feitos dos visitantes na DB do postgres:
    public function listarRegistrosDispositivosRequestPg()
    {
        $pdo = new PgConnectController();
        $result = $pdo->selectQuery("SELECT DISTINCT dispositivo, count(id) AS tot FROM pc.web_login_acesso GROUP BY dispositivo;");

        return $result;
    }

    // Request para listar informacoes salvas de um determinado dispositivo pelo postgres:
    public function infoRegistroDispositivoRequestPg()
    {
        $validate = $this->validateInfoRegistroDispositivo();

        if ($validate == 200) {
            // Chama classe de pdo e valida os dados que foram inseridos na instancia
            $pdo = new PgConnectController();

            // Recolhe dados da instancia atual
            $dispositivo = $this->getDispositivo();

            // Monta array para enviar na query:
            $values = array(":dispositivo" => $dispositivo);

            // Excuta insert de query:
            $result = $pdo->selectQuery("SELECT * FROM pc.web_login_acesso WHERE dispositivo = :dispositivo", $values);

            return $result;
        } else {
            return array();
        }
    }

    // Request para listar os registros de sistema operacional feitos dos visitante na db do postgres:
    public function listarRegistrosSystemNameRequestPg()
    {
        $pdo = new PgConnectController();
        $result = $pdo->selectQuery("SELECT DISTINCT system_name, count(id) AS tot FROM pc.web_login_acesso GROUP BY system_name;");

        return $result;
    }

    // Request para listar informacoes registradas de uma rota na db no Postgres:
    public function infoRegistroRotaRequestPg()
    {
        $validate = $this->validateInfoRegistroRota();

        if ($validate == 200) {
            // Chama classe de pdo e valida os dados que foram inseridos na instancia
            $pdo = new PgConnectController();

            // Recolhe dados da instancia atual
            $route = $this->getRoute();

            // Monta array para enviar na query:
            $values = array(":route" => $route);

            // Excuta insert de query:
            $result = $pdo->selectQuery("SELECT * FROM pc.web_login_acesso WHERE route = :route", $values);

            return $result;
        } else {
            return array();
        }
    }

    // Request para listar os registros de route feitos dos visitante na db do Postgres:
    public function listarRegistrosRotaRequestPg()
    {
        $pdo = new PgConnectController();
        $result = $pdo->selectQuery("SELECT DISTINCT route, count(id) AS tot FROM pc.web_login_acesso GROUP BY route;");

        return $result;
    }

    // Request para listar informacoes registradas de um determinado browser na db no Postgres
    public function infoRegistrosBrowserRequestPg()
    {
        $validate = $this->validateInfoRegistroBrowser();

        if ($validate == 200) {
            // Chama classe de pdo e valida os dados que foram inseridos na instancia
            $pdo = new PgConnectController();

            // Recolhe dados da instancia atual
            $browser = $this->getBrowser();

            // Monta array para enviar na query:
            $values = array(":browser" => $browser);

            // Excuta insert de query:
            $result = $pdo->selectQuery("SELECT * FROM pc.web_login_acesso WHERE browser = :browser", $values);

            return $result;
        } else {
            return array();
        }
    }

    // Request para listar os registros de browser feitos dos visitante na db do Postgres
    public function listarRegistrosBrowserRequestPg()
    {
        $pdo = new PgConnectController();
        $result = $pdo->selectQuery("SELECT DISTINCT browser, count(id) AS tot FROM pc.web_login_acesso GROUP BY browser;");

        return $result;
    }

    // Request para obter info dos registros de dispositivo em uma data escolhida:
    public function infoRegistroDispositivoPorDataRequestPg()
    {
        $validate = $this->validateRegistroDispositivoPorData();

        if ($validate == 200) {
            // Chama classe de pdo e valida os dados que foram inseridos na instancia
            $pdo = new PgConnectController();
            $timezone = new DateTimeZone("America/Sao_Paulo");
            $dateTime = new DateTime('now', $timezone);

            // Recolhe dados da instancia atual
            $dispositivo = $this->getDispositivo();
            $data = $this->getData();

            switch ($data) {
                case "hoje":
                    $values = array(":dispositivo" => $dispositivo);
                    $result = $pdo->selectQuery("SELECT DISTINCT dispositivo, count(id) AS tot FROM pc.web_login_acesso WHERE data::date = CURRENT_DATE AND dispositivo = :dispositivo GROUP BY dispositivo;");
                    break;

                case "mes":
                    $data_busca = strval($dateTime->format('Y-m'));
                    $values = array(":dispositivo" => $dispositivo, ":data_busca" => $data_busca);

                    $result = $pdo->selectQuery("SELECT DISTINCT dispositivo, count(id) AS tot FROM pc.web_login_acesso WHERE dispositivo = :dispositivo AND TO_CHAR(data, 'YYYY-MM') = :data_busca GROUP BY dispositivo;", $values);
                    break;

                case "ano":
                    $data_busca = strval($dateTime->format('Y'));
                    $values = array(":dispositivo" => $dispositivo, ":data_busca" => $data_busca);

                    $result = $pdo->selectQuery("SELECT DISTINCT dispositivo, count(id) AS tot FROM pc.web_login_acesso WHERE dispositivo = :dispositivo AND TO_CHAR(data, 'YYYY') = :data_busca GROUP BY dispositivo;", $values);
                    break;

                case "todos":
                    $result = $pdo->selectQuery("SELECT DISTINCT dispositivo, count(id) AS tot FROM pc.web_login_acesso GROUP BY dispositivo;");
                    break;

                default:
                    $values = array(":dispositivo" => $dispositivo);
                    $result = $pdo->selectQuery("SELECT DISTINCT dispositivo, count(id) AS tot FROM pc.web_login_acesso WHERE data::date = CURRENT_DATE AND dispositivo = :dispositivo GROUP BY dispositivo;");
                    break;
            }

            return $result;
        } else {
            return array();
        }
    }

    // Request para obter lista de registros de dispositivos em uma data escolhida:
    public function listaRegistroDispositivoPorDataRequestPg()
    {
        $validate = $this->validateInfoRegistroData();

        if ($validate == 200) {
            // Chama classe de pdo e valida os dados que foram inseridos na instancia
            $pdo = new PgConnectController();
            $timezone = new DateTimeZone("America/Sao_Paulo");
            $dateTime = new DateTime('now', $timezone);

            // Recolhe dados da instancia atual
            $data = $this->getData();

            switch ($data) {
                case "hoje":
                    $result = $pdo->selectQuery("SELECT DISTINCT dispositivo, count(id) AS tot FROM pc.web_login_acesso WHERE data::date = CURRENT_DATE GROUP BY dispositivo;");
                    break;

                case "mes":
                    $data_busca = strval($dateTime->format('Y-m'));
                    $values = array(":data_busca" => $data_busca);

                    $result = $pdo->selectQuery("SELECT DISTINCT dispositivo, count(id) AS tot FROM pc.web_login_acesso WHERE TO_CHAR(data, 'YYYY-MM') = :data_busca GROUP BY dispositivo;", $values);
                    break;

                case "ano":
                    $data_busca = strval($dateTime->format('Y'));
                    $values = array(":data_busca" => $data_busca);

                    $result = $pdo->selectQuery("SELECT DISTINCT dispositivo, count(id) AS tot FROM pc.web_login_acesso WHERE TO_CHAR(data, 'YYYY') = :data_busca GROUP BY dispositivo;", $values);
                    break;

                case "todos":
                    $result = $pdo->selectQuery("SELECT DISTINCT dispositivo, count(id) AS tot FROM pc.web_login_acesso GROUP BY dispositivo;");
                    break;

                default:
                    $result = $pdo->selectQuery("SELECT DISTINCT dispositivo, count(id) AS tot FROM pc.web_login_acesso WHERE data::date = CURRENT_DATE GROUP BY dispositivo;");
                    break;
            }

            return $result;
        } else {
            return array();
        }
    }

    // Request para listar os registros de sistema operacional por data feitos dos visitante na db do postgres:
    public function listarRegistrosSystemNamePorDataRequestPg()
    {
        $validate = $this->validateInfoRegistroData();

        if ($validate == 200) {
            // Chama classe de pdo e valida os dados que foram inseridos na instancia
            $pdo = new PgConnectController();
            $timezone = new DateTimeZone("America/Sao_Paulo");
            $dateTime = new DateTime('now', $timezone);

            // Recolhe dados da instancia atual
            $data = $this->getData();

            switch ($data) {
                case "hoje":
                    $result = $pdo->selectQuery("SELECT DISTINCT system_name, count(id) AS tot FROM pc.web_login_acesso WHERE data::date = CURRENT_DATE GROUP BY system_name;");
                    break;

                case "mes":
                    $data_busca = strval($dateTime->format('Y-m'));
                    $values = array(":data_busca" => $data_busca);

                    $result = $pdo->selectQuery("SELECT DISTINCT system_name, count(id) AS tot FROM pc.web_login_acesso WHERE TO_CHAR(data, 'YYYY-MM') = :data_busca GROUP BY system_name;", $values);
                    break;

                case "ano":
                    $data_busca = strval($dateTime->format('Y'));
                    $values = array(":data_busca" => $data_busca);

                    $result = $pdo->selectQuery("SELECT DISTINCT system_name, count(id) AS tot FROM pc.web_login_acesso WHERE TO_CHAR(data, 'YYYY') = :data_busca GROUP BY system_name;", $values);
                    break;

                case "todos":
                    $result = $pdo->selectQuery("SELECT DISTINCT system_name, count(id) AS tot FROM pc.web_login_acesso GROUP BY system_name;");
                    break;

                default:
                    $result = $pdo->selectQuery("SELECT DISTINCT system_name, count(id) AS tot FROM pc.web_login_acesso WHERE data::date = CURRENT_DATE GROUP BY system_name;");
                    break;
            }

            return $result;
        } else {
            return array();
        }
    }

    // Request para listar os registros de routes por data feitos dos visitantes na db do postgres:
    public function listarRegistrosRotaPorDataRequestPg()
    {
        $validate = $this->validateInfoRegistroData();

        if ($validate == 200) {
            // Chama classe de pdo e valida os dados que foram inseridos na instancia
            $pdo = new PgConnectController();
            $timezone = new DateTimeZone("America/Sao_Paulo");
            $dateTime = new DateTime('now', $timezone);

            // Recolhe dados da instancia atual
            $data = $this->getData();

            switch ($data) {
                case "hoje":
                    $result = $pdo->selectQuery("SELECT DISTINCT route, count(id) AS tot FROM pc.web_login_acesso WHERE data::date = CURRENT_DATE GROUP BY route;");
                    break;

                case "mes":
                    $data_busca = strval($dateTime->format('Y-m'));
                    $values = array(":data_busca" => $data_busca);

                    $result = $pdo->selectQuery("SELECT DISTINCT route, count(id) AS tot FROM pc.web_login_acesso WHERE TO_CHAR(data, 'YYYY-MM') = :data_busca GROUP BY route;", $values);
                    break;

                case "ano":
                    $data_busca = strval($dateTime->format('Y'));
                    $values = array(":data_busca" => $data_busca);

                    $result = $pdo->selectQuery("SELECT DISTINCT route, count(id) AS tot FROM pc.web_login_acesso WHERE TO_CHAR(data, 'YYYY') = :data_busca GROUP BY route;", $values);
                    break;

                case "todos":
                    $result = $pdo->selectQuery("SELECT DISTINCT route, count(id) AS tot FROM pc.web_login_acesso GROUP BY route;");
                    break;

                default:
                    $result = $pdo->selectQuery("SELECT DISTINCT route, count(id) AS tot FROM pc.web_login_acesso WHERE data::date = CURRENT_DATE GROUP BY route;");
                    break;
            }

            return $result;
        } else {
            return array();
        }
    }
}

