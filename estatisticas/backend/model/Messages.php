<?php
class Messages
{
    // Atributos
    private $status;
    private $msg;

    // Metodos especiais:
    public function __construct($status, $msg = "")
    {
        $this->setStatus($status);
        $this->setMsg($msg);
    }

    /**
     * Get the value of status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     */
    public function setStatus($status): self
    {
        $this->status = validateValue($status);

        return $this;
    }

    /**
     * Get the value of msg
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * Set the value of msg
     */
    public function setMsg($msg): self
    {
        $this->msg = validateValue($msg);

        return $this;
    }

    // Metodos publicos:
    // Validate msg and return value
    public function validateValues()
    {
        $status = $this->getStatus();
        $msg = $this->getMsg();

        if ($status && is_null($status)) {
            // Validate status
            return 'Invalid status';
        } else if ($msg && is_null($msg)) {
            //Validar username:
            return 'Invalid msg';
        } else {
            return 200;
        }
    }
}
