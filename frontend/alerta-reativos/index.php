<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<!-- HEAD DA PAGINA -->
<?php include "../recursos/head.php"; ?>
<!-- BODY DA PAGINA -->

<body class="c-app">
    <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
    <?php include "../recursos/menu.php"; ?>

    <div class="c-subheader px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Alerta de Reativos</li>
        </ol>
    </div>
    <!-- Fim do cabeçalho -->
    <!--Començo do conteudo do site-->
    <br />

    <div class="container-fluid">
        <div id="mudarSenha" class="animated fadeIn">
            <div class="justify-content-center row">
                <div class="col"></div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col">
                                    <div sm="8" class="mb-0 card-title"><label class="text-value">Alerta de Reativos</label></div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="mx-auto col-12 col-sm-12 col-md-6 col-lg-4" id="containereAlertaReativos">
                                    <script>
                                        $("#containereAlertaReativos").html("<div id='giftAlertaReativos' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 341px;'><span class='loader'></span></div>");
                                        setTimeout(function() {
                                            document.getElementById('giftAlertaReativos').remove();
                                            document.getElementById('cardAlertaReativos').style.display = 'block';
                                        }, 2500);
                                    </script>

                                    <div class="card-accent-secondary card fade-in" id="cardAlertaReativos" style="display: none;">
                                        <div class="text-center h-25 card-header">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="mb-0 pt-1 card-title justify-content-between row" style="padding: 0 10px;">
                                                        <label class="mb-0 pt-2 text-value">Valores</label>
                                                        <button type="button" class="btn btn-pill btn-light" data-toggle="modal" data-target="#exampleModal">
                                                            <svg class="c-icon">
                                                                <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-lightbulb"></use>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <form method="post" action="../../backend/alertaReativos/updateAlertaReativos.php" class="">
                                                <label>Alertar com:</label>
                                                <div class="mb-3 input-group">
                                                    <div class="input-group-prepend no-marginright">
                                                        <span class="input-group-text">
                                                            <i class="icon"></i>
                                                        </span>
                                                    </div>
                                                    <input placeholder="Digite aqui o valor" pattern="[0-9]" id="alterar_com" name="alterar_com" type="number" class="form-control">
                                                </div><label>Intervalo:</label>
                                                <div class="mb-4 input-group">
                                                    <div class="input-group-prepend no-marginright">
                                                        <span class="input-group-text">
                                                            <i class="icon" title="Mostrar/Esconder Senha"></i>
                                                        </span>
                                                    </div>
                                                    <input placeholder="Digite aqui o intervalo" pattern="[0-9]" id="intervalo" name="intervalo" type="number" class="form-control">
                                                </div>
                                                <div class="mb-4 input-group justify-content-end">
                                                    <button type="submit" class="float-right btn btn-primary">Salvar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <?php
                                    if (isset($_GET['msg'])) {
                                        switch ($_GET['msg']) {
                                            case "success":
                                                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">O cadastro foi feito <strong>com successo!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                                                break;
                                            case "error":
                                                $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Alguma coisa deu <strong>errado!</strong> Por favor, tente novamente.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                                                break;
                                            case "nomsg":
                                                $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Os dados não podem estar <strong>vazios...</strong> Por favor, tente novamente.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                                                break;
                                            default:
                                                $msg = '';
                                                break;
                                        }
                                        echo $msg;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">TIPS</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>Alertar com:</h5>
                    <p>Quando esse valor for alcançado (R$), você receberá o primeiro email de aviso sobre reativos</p>
                    <hr>
                    <h5>Intervalo</h5>
                    <p>Toda vez que aumentar esse valor, você recebe um novo email.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>
    <!-- FIM DO CONTEUDO DO SITE -->
    <!-- FOOTER -->
    <?php include "../recursos/footer.php"; ?>
