<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    var checkCookie = function() {
        var lastCookie = document.cookie;
        var last_id_unidades = $("#idUnidades").find(':selected').attr('id');
        var last_calendario_unidade = "01-" + $("#calendarioRelatorios").val();
        return function() {
            var currentCookie = document.cookie;
            //Codigo para mudanca de select unidades:
            var current_id_unidades = $("#idUnidades").find(':selected').attr('id');
            if (current_id_unidades != last_id_unidades) {
                last_id_unidades = current_id_unidades;
                //var anoNovo = last_calendario_unidade.substr(6, 10);
                //var mesNovo = last_calendario_unidade.substr(3, 2);
                //var finalNovo = new Date(anoNovo, mesNovo, 0);
                //var ultimoDia = finalNovo.getDate();
                //var dataFinalFormatada = anoNovo + '-' + mesNovo + '-' + ultimoDia;
                console.log("NOVO ID TESTE: " + last_id_unidades);
                console.log("Novo valor de calendario: " + last_calendario_unidade);

                $("#conteudoRelatorio").html("");
                $.ajax({
                    type: 'GET',
                    url: 'div/updateConteudoRelatorio.php',
                    data: {
                        'id_unidades': last_id_unidades,
                        'data': last_calendario_unidade
                    },
                    beforeSend: function(carrega) {
                        $("#conteudoRelatorio").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                    },
                    success: function(r) {
                        $("#conteudoRelatorio").html(r);
                    }
                });
            }

            //Codigo para mudanca de calendario no dashboard:
            var current_calendario_unidade = "01-" + $("#calendarioRelatorios").val();
            if (current_calendario_unidade != last_calendario_unidade) {
                last_calendario_unidade = current_calendario_unidade;
                //var anoNovo = last_calendario_unidade.substr(6, 10);
                //var mesNovo = last_calendario_unidade.substr(3, 2);
                //var finalNovo = new Date(anoNovo, mesNovo, 0);
                //var ultimoDia = finalNovo.getDate();
                //var dataFinalFormatada = anoNovo + '-' + mesNovo + '-' + ultimoDia;
                console.log("Novo valor de calendario: " + last_calendario_unidade);
                console.log("NOVO ID TESTE: " + last_id_unidades);
                //console.log("Novo valor final de calendario: " + dataFinalFormatada);
                //console.log(mesNovo);

                $("#conteudoRelatorio").html("");
                $.ajax({
                    type: 'GET',
                    url: 'div/updateConteudoRelatorio.php',
                    data: {
                        'data': last_calendario_unidade,
                        'id_unidades': last_id_unidades
                    },
                    beforeSend: function(carrega) {
                        $("#conteudoRelatorio").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 380px;'><span class='loader'></span></div>");
                    },
                    success: function(r) {
                        $("#conteudoRelatorio").html(r);
                    }
                });
            }
        };
    }();
    window.setInterval(checkCookie, 100);
</script>