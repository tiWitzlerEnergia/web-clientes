<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<!-- HEAD DA PAGINA -->
<?php
header('Access-Control-Allow-Origin: https://clientes.witzler.com.br/');
include "../recursos/head.php";
?>
<!-- BODY DA PAGINA -->

<body class="c-app">
    <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
    <?php
    include "../recursos/menu.php";
    //Variavel que mudara o config do select
    //$meus_arquivos = 1;
    ?>

    <div class="c-subheader px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Relatórios</li>
        </ol>
    </div>
    <!-- Fim do cabeçalho -->
    <!--Començo do conteudo do site-->
    <br />
    <div class="container-fluid">

        <div>
            <?php //require_once "../../backend/Classes/autoload.php"; 
            ?>
            <!--<div class="alert alert-danger" role="alert">Página de relatórios em manutenção.</div>-->
            <div class="CardFilters hideInPrint card" style="max-width: 100%;">
                <div class="text-value card-header">
                    <div class="row">
                        <div class="col">
                            <div class="mb-0 card-title text-center">Relatórios </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-group justify-content-center">
                        <div class="input-group col-sm-12 col-lg-3 arquivoInput">
                            <div class="dropdown2 dropdown input-group-prepend no-marginright">
                                <label class="text-value input-group-text" style="width: 83px;">Datas: </label>
                            </div>

                            <?php
                            // Teste para ver datas:
                            $diaAtual = date("d");
                            if ($diaAtual >= 15) {
                                // -1 mes
                                $dataAtual = date("m-Y", strtotime("-1 month"));
                            } else {
                                // -2 mes
                                $dataAtual = date("m-Y", strtotime("-2 month"));
                            }
                            //echo $dataAtual;

                            //$calendario = new ListaDatas("calendarioRelatorios", "mm-yyyy", "months", "data_relatorio", date("m-Y", strtotime('-1 month')));
                            $calendario = new ListaDatas("calendarioRelatorios", "mm-yyyy", "months", "data_relatorio", $dataAtual);
                            $calendario->criarVisual();
                            ?>
                        </div>

                        <div class="input-group col-sm-12 col-lg-3 arquivoInput">
                            <div class="dropdown2 dropdown input-group-prepend no-marginright">
                                <label class="text-value input-group-text" style="width: 83px;">Unidades: </label>
                            </div>
                            <?php
                            $lista_unidades = new ListaUnidades();
                            $lista_unidades->criarVisual("idUnidades", "id_unidades");
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php
        // Incluir div caso exista conteudo:
        include "div/conteudoRelatorio.php";
        ?>

    </div>

    <!-- FOOTER -->
    <?php include "div/configAjax.php"; ?>
    <?php include "../recursos/footer.php"; ?>
    <!-- FIM -->