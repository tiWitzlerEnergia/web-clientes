<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<!-- HEAD DA PAGINA -->
<?php
header('Access-Control-Allow-Origin: https://clientes.witzler.com.br/');
include "../recursos/head.php";
?>
<!-- BODY DA PAGINA -->

<body class="c-app">
    <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
    <?php
    include "../recursos/menu.php";
    //Variavel que mudara o config do select
    //$meus_arquivos = 1;
    ?>

    <div class="c-subheader px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Relatórios</li>
        </ol>
    </div>
    <!-- Fim do cabeçalho -->
    <!--Començo do conteudo do site-->
    <br />
    <div class="container-fluid">

        <div>
            <div class="CardFilters hideInPrint card" style="max-width: 100%;">
                <div class="text-value card-header">
                    <div class="row">
                        <div class="col">
                            <div class="mb-0 card-title text-center">Relatórios </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-group justify-content-center">
                        <div class="input-group col-sm-12 col-lg-3 arquivoInput">
                            <div class="dropdown2 dropdown input-group-prepend no-marginright">
                                <label class="text-value input-group-text" style="width: 83px;">Datas: </label>
                            </div>

                            <?php
                            // Teste para ver datas:
                            $diaAtual = date("d");
                            if ($diaAtual >= 15) {
                                // -1 mes
                                $dataAtual = date("m-Y", strtotime("-1 month"));
                            } else {
                                // -2 mes
                                $dataAtual = date("m-Y", strtotime("-2 month"));
                            }

                            $calendario = new ListaDatas("calendarioRelatorios", "mm-yyyy", "months", "data_relatorio", $dataAtual);
                            $calendario->criarVisual();
                            ?>
                        </div>

                        <div class="input-group col-sm-12 col-lg-3 arquivoInput">
                            <div class="dropdown2 dropdown input-group-prepend no-marginright">
                                <label class="text-value input-group-text" style="width: 83px;">Unidades: </label>
                            </div>
                            <?php
                            $lista_unidades = new ListaUnidades();
                            $lista_unidades->criarVisual("idUnidades", "id_unidades");
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
        // Cria classe que vai pegar dados do relatorio:
        $relatorio = new ResultadosRelatorio(0, date("Y-m-d", strtotime('first day of last month')));

        $pdfDownload = new DonwloadArquivo(0, date("Y-m-d", strtotime('first day of last month')));
        if (!$relatorio->getIdRelatorio()) {
        ?>

            <div id="conteudoRelatorio" class="col-12 noPadding">
                <div class="row col-12 noPadding noMargin">
                    <div class="col-lg-9 col-12 separarDiv">
                        <h4 class="linha_colorida">
                            RESULTADOS DE HOJE
                        </h4>
                    </div>
                    <div class="col-lg-3 col-12 separarDiv">
                        <div class="input-group">
                            <select class="custom-select" id="inputGroupSelect04" style="height: 100%; min-height: 45px;">
                                <option selected>Choose...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                            <div class="input-group-append" style="margin: 0;">
                                <a rel="nofollow" href="" class="download" download target="_blank" style="padding: 5px; font-size: 13px;"> BAIXAR ARQUIVO </a>
                                <!--<button class="btn btn-outline-secondary" type="button">Button</button>-->
                            </div>
                        </div>
                        <!--<a rel="nofollow" href="" class="download" download target="_blank"> BAIXAR ARQUIVO </a>-->
                    </div>
                </div>

                <div class="row col-12 noMargin noPadding">
                    <div class="col-12 col-lg-6 separarDiv">
                        <div class="card justify-content-center col-12" style="min-height: 100%;">
                            <div class="row">
                                <div class="col-lg-2 col-3">
                                    <img src="" height="60px" class="redonda">
                                </div>
                                <div class="col-lg-10 col-9 noPadding" style="align-self: center;">
                                    <p class="atendente noMargin">ATENDENTE
                                        <br>0
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class=" col-1" style=" padding:0;">
                                </div>

                                <div class="col-lg-8 col-11 linha " style=" padding:0;">
                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b>VALOR ACR (R$): </b></p>
                                        <p class="col-6 resultados">0</p>
                                    </div>

                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b>VALOR ACL (R$): </b></p>
                                        <p class="col-6 resultados">0</p>
                                    </div>

                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b>ECONOMIA (R$): </b></p>
                                        <p class="col-6 resultados">0</p>
                                    </div>

                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b>CONSUMO CONSIDERADO (MWh): </b></p>
                                        <p class="col-6 resultados">0</p>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-12 mt-4">
                                    <h2 class="porcentagem"><b>0%</b></h2>
                                    <p class="porcentagem">valor</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-12 col-lg-6 espaco">
                        <div class="row" style="min-height: 100%;">
                            <div class="col-lg-6 col-12 separarDiv">
                                <div class="card justify-content-center" style="min-height: 100%;">
                                    <div class="row noMargin" style="min-height: 100%; align-content: center;">
                                        <div class="col-4" style="align-self: center; padding-right: 0;">
                                            <img src="../bibliotecas/icones/iconeDinheiro.png" class="centro">
                                        </div>

                                        <div class="col-8" style="align-self: center;">
                                            <p class="atendente mb-0">CUSTO UNITÁRIO EFETIVO (R$/MWH.)</p>
                                            <H3><b>R$ 0</b></H3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-12 separarDiv">
                                <div class="card justify-content-center" style="min-height: 100%;">
                                    <div class="row noMargin" style="min-height: 100%; align-content: center;">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row col-12 noMargin noPadding">
                    <div class=" col-12 col-lg-6 ">
                        <div class="card col-12" style="min-height: 220px;">
                            <p class="mt-2"><b>COMPOSIÇÃO ACL</b></p>
                            <div class="row">
                                <div class="col-lg-3 col-12">
                                    <canvas class="mx-auto chartjs-render-monitor fade-in col-12 noPadding" id="chartComposicaoAcl" style="display: block;"></canvas>
                                    <script>
                                        var charComposicaoAcl = {
                                            labels: ['Energia', 'Fatura distribuidora', 'CCEE'],
                                            datasets: [{
                                                label: 'Grafico ACL',
                                                backgroundColor: [
                                                    "#113759",
                                                    "#34aee4",
                                                    "#e98e29"
                                                ],
                                                data: [
                                                    0, 0, 0
                                                ]
                                            }]
                                        };
                                        var ctxComposicaoAcl = document.getElementById('chartComposicaoAcl').getContext('2d');
                                        window.grafico = new Chart(ctxComposicaoAcl, {
                                            type: 'doughnut',
                                            data: charComposicaoAcl,
                                            options: {
                                                responsive: true,
                                                maintainAspectRatio: false,
                                                legend: {
                                                    display: false
                                                },
                                                title: {
                                                    display: false,
                                                },
                                                animation: {
                                                    animateScale: true,
                                                    animateRotate: true,
                                                    duration: 2000,
                                                }
                                            }
                                        });
                                    </script>
                                </div>
                                <div class="col-lg-9 col-12">
                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b> <span style="color:#113759">0%</span> ENERGIA </b></p>
                                        <p class="col-6 resultados">0</p>
                                    </div>
                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b><span style="color:#34aee4">0%</span> FATURA DISTRIBUIDORA </b></p>
                                        <p class="col-6 resultados">0</p>
                                    </div>
                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b><span style="color:#e98e29">0%</span> CCEE </b></p>
                                        <p class="col-6 resultados">0</p>
                                    </div>
                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b> Imposto </b></p>
                                        <p class="col-6 resultados"><b>0</b></p>
                                    </div>
                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b> Total </b></p>
                                        <p class="col-6 resultados"><b>0</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 espaco">
                        <div class="card col-12" style="min-height: 220px;">
                            <p class="mt-2"><b>COMPOSIÇÃO ACR</b></p>
                            <div class="row">
                                <div class="col-lg-3 col-12">
                                    <canvas class="mx-auto chartjs-render-monitor fade-in col-12 noPadding" id="chartComposicaoAcr" style="display: block;"></canvas>
                                    <script>
                                        var charComposicaoAcr = {
                                            labels: ['TUSD', 'Outros', 'Energia'],
                                            datasets: [{
                                                label: 'Composição ACR',
                                                backgroundColor: [
                                                    "#113759",
                                                    "#34aee4",
                                                    "#e98e29"
                                                ],
                                                data: [
                                                    0, 0, 0
                                                ]
                                            }]
                                        };
                                        var ctxComposicaoAcr = document.getElementById('chartComposicaoAcr').getContext('2d');
                                        window.grafico = new Chart(ctxComposicaoAcr, {
                                            type: 'doughnut',
                                            data: charComposicaoAcr,
                                            options: {
                                                responsive: true,
                                                maintainAspectRatio: false,
                                                legend: {
                                                    display: false
                                                },
                                                title: {
                                                    display: false,
                                                },
                                                animation: {
                                                    animateScale: true,
                                                    animateRotate: true,
                                                    duration: 2000,
                                                }
                                            }
                                        });
                                    </script>
                                </div>
                                <div class="col-lg-9 col-12">
                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b> <span style="color:#113759">0%</span> TUSD </b></p>
                                        <p class="col-6 resultados">0</p>
                                    </div>
                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b><span style="color:#34aee4">0%</span> Outros </b></p>
                                        <p class="col-6 resultados">0</p>
                                    </div>
                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b><span style="color:#e98e29">0%</span> ENERGIA </b></p>
                                        <p class="col-6 resultados">0</p>
                                    </div>
                                    <div class="row atendente">
                                        <p class="col-6 resultados"><b> TOTAL </b></p>
                                        <p class="col-6 resultados"><b>0</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div id="conteudoRelatorio" class="row col-12 noMargin noPadding text-center justify-content-center">
                <h4>Sem relatório disponível, tente com outra data e unidade.</h4>;
            </div>
        <?php } ?>

    </div>

    <!-- FOOTER -->
    <?php include "div/configAjax.php"; ?>
    <?php include "../recursos/footer.php"; ?>
    <!-- FIM -->