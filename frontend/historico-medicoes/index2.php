<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<!-- HEAD DA PAGINA -->
<?php include "../recursos/head.php"; ?>
<!-- BODY DA PAGINA -->

<body class="c-app">
    <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
    <?php include "../recursos/menu.php"; ?>

    <div class="c-subheader px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Histórico de Medições</li>
        </ol>
    </div>
    <!-- Fim do cabeçalho -->
    <!-- conteudo da pagina-->
    <br />

    <div class="container-fluid">
        <div id="mearusementhistory" class="animated fadeIn">
            <div class="card">
                <div class="text-value card-header">
                    <div class="row">
                        <div class="col">Escolha a unidade de referência</div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row d-flex">
                        <div class="col-lg-4 col-md-3 col-sm-3"></div>
                        <div class="text-center justify-content-center card-header-row col-sm-6 col-md-6 col-lg-4 input-group">
                            <div class="dropdown2 dropdown input-group-prepend no-marginright">
                                <label class="text-value input-group-text">Unidade: </label>
                            </div>
                            <!-- Aqui carrega o arquivo que tem as opcoes de unidade -->
                            <?php include "../../backend/dadosUnidades/dadosUnidades.php"; ?>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-3"></div>
                    </div>
                </div>
            </div>
            <div id="corpoHistoricoMedicoes">
                <div class="row">
                    <!-- GRAFICOS DO PRIMEIRO ROW: CONSUMO POR DIA E CONSUMO POR MES -->
                    <?php include "div/divConsumoDia.php"; ?>
                    <?php include "div/divConsumoMes.php"; ?>
                </div>
                <div class="card">
                    <!-- GRAFICO SEGUNDO ROW: CONSUMO POSTO TARIFARIO -->
                    <?php include "div/divConsumoPostoTarifario.php"; ?>
                </div>
                <div class="card">
                    <!-- GRAFICO TERCEIRO ROW: CAPACITIVO E INDUTIVO -->
                    <?php include "div/divCapacitivoIndutivo.php"; ?>
                </div>
                <div class="card" id="historicoDemanda">
                    <?php include "div/divHistoricoDemanda.php"; ?>
                </div>
                <div class="card">
                    <?php include "div/divHistoricoDemandaTabela.php"; ?>
                </div>
                <div class="card">
                    <!-- GRAFICO QUARTO ROW: FATOR POTENCIA -->
                    <?php include "div/divConsumoFatorPotencia.php"; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- INCLUDE DAS REQUISCOES AJAX -->
    <?php include "div/configAjax.php"; ?>
    <!-- FIM CONTEUDO DO SITE -->
    <!-- FOOTER -->
    <?php include "../recursos/footer.php"; ?>