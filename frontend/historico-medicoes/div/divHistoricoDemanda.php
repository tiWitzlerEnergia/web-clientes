<div class="text-value card-header">
    <div class="row">
        <div class="card-header-row col-12 col-sm-3 col-lg-2 text-center d-flex justify-content-center align-items-center">HíSTORICO DE DEMANDAS</div>
    </div>
</div>
<div class="card-body">
    <div class="chartWrapper" style="position: relative;">
        <div class="chartAreaWrapper" id="updateHistoricoDemadas" style="overflow-x: auto; height: 400px;">
            <script>
                $("#updateHistoricoDemadas").html("<div id='loadHistoricoDemadas' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                setTimeout(function() {
                    document.getElementById('loadHistoricoDemadas').remove();
                    document.getElementById('chartBarHistoricoDemadas').style.display = 'block';
                }, 2500);
            </script>

            <canvas id="chartBarHistoricoDemadas" class="fade-in" width="1540" height="380" style="display: none; position: relative;"></canvas>
            <?php
                $hdemandas = new Demanda("chartBarHistoricoDemadas", $dataInicial, $id_unidade);
            ?>
            <script>
                <?php
                    $hdemandas->varChartData("chartHistoricoDemadas");
                    $hdemandas->configChartData("chartHistoricoDemadas", "bar", "Historico de Demandas");
                ?>
            </script>
        </div>
    </div>
</div>