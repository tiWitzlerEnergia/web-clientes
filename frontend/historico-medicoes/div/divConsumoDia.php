<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php include "../../backend/graficos/consumoDia/pegarUltimoRegistro/recolhe.php"; ?>

<div class="col-12 col-lg-6">
    <div class="card">
        <div class="text-value card-header">
            <div class="row">
                <div class="card-header-row col-12 col-sm-12 col-lg-3 text-center d-flex justify-content-center">Consumo por Dia</div>
                <div class="card-header-row pt-2 centerInMobile col-lg-3"></div>
                <div class="card-header-row arquivoInput col-12 col-sm-12 col-lg-6" style="text-align: right;">
                    <div class="card-header-date">
                        <!-- CALENDARIO -->
                        <?php include "../../backend/datas/datasConsumoDia/calendarioConsumoDia.php"; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="chartWrapper" style="position: relative;">
                <div class="chartAreaWrapper" id="updateConsumoDia" style="overflow-x: auto; height: 378px;">
                    <script>
                        $("#updateConsumoDia").html("<div id='loadConsumodia' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                        setTimeout(function() {
                            document.getElementById('loadConsumodia').remove();
                            document.getElementById('chartConsumoDia').style.display = 'block';
                        }, 2500);
                    </script>

                    <canvas class="mx-auto fade-in" id="chartConsumoDia" width="750" height="350" style="display: none; position: relative;"></canvas>
                    <?php
                    //include "../../backend/graficos/consumoDia/chartConsumoDia.php";
                    $dataDia = isset($_COOKIE['data_consumo_dia']) ? $_COOKIE['data_consumo_dia'] : $ultimaData;
                    $id_unidade_dia = isset($_COOKIE['id_unidades']) ? $_COOKIE['id_unidades'] : $arrayIdOption[0];
                    $dia = new ConsumoDia("chartConsumoDia", $dataDia, $id_unidade_dia);
                    ?>
                    <script>
                        <?php
                        $dia->varChartData("chartCDia", array("Consumo geral (KWh)"), array("ativo_c"), array($_SESSION['cor-custom-7']), array($_SESSION['cor-custom-7']));
                        $dia->configChartData("chartCDia", "line", "Consumo por Dia");
                        ?>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>