<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    var checkCookie = function() {
        var lastCookie = document.cookie;
        //Aqui separamos as id de unidade para cada uma das buscas:
        var last_id_unidades_dia = $("#idUnidades").find(':selected').attr('id');
        var last_id_unidades_mes = $("#idUnidades").find(':selected').attr('id');
        var last_id_unidades_posto_tarifario = $("#idUnidades").find(':selected').attr('id');
        var last_id_unidades_fator_potencia = $("#idUnidades").find(':selected').attr('id');
        var last_id_unidades = $("#idUnidades").find(':selected').attr('id');
        //Aqui separamos as datas de buscas por input:
        var last_calendario_consumo_dia = $("#datasCalendarioConsumoDia").val();
        var last_calendario_unidade = $("#datasCalendario").val();
        var last_calendario_posto_tarifario = $("#datasCalendarioConsumoPostoTarifario").val();
        var last_calendario_fator_potencia = $("#datasCalendarioFatorPotencia").val();
        return function() {
            var currentCookie = document.cookie;

            // Codigo mundanca de demanda
            var current_id_unidades = $("#idUnidades").find(':selected').attr('id');
            if(current_id_unidades != last_id_unidades) {
                last_id_unidades = current_id_unidades;
                //Remover div:
                $("#updateHistoricoDemadas").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/demanda/updateHistoricoDemanda.php',
                    data: {
                        'id_unidades': last_id_unidades
                    },
                    beforeSend: function(carrega) {
                        //alert("teste");
                        $("#updateHistoricoDemadas").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 380px;'><span class='loader'></span></div>");
                    },
                    success: function(r) {
                        $("#updateHistoricoDemadas").html(r);
                        //console.log(r);
                    }
                });

                //Remover div:
                $("#updateHistoricoDemandaTable").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/demanda/updateHistoricoDemandaTable.php',
                    data: {
                        'id_unidades': last_id_unidades
                    },
                    beforeSend: function(carrega) {
                        //alert("teste");
                        $("#updateHistoricoDemandaTable").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 380px;'><span class='loader'></span></div>");
                    },
                    success: function(r) {
                        $("#updateHistoricoDemandaTable").html(r);
                        //console.log(r);
                    }
                });
            }

            //Codigo para mudanca de consumo dia:
            var current_id_unidades_dia = $("#idUnidades").find(':selected').attr('id');
            var current_calendario_consumo_dia = $("#datasCalendarioConsumoDia").val();
            if(current_id_unidades_dia != last_id_unidades_dia || current_calendario_consumo_dia != last_calendario_consumo_dia){
                last_id_unidades_dia = current_id_unidades_dia;
                last_calendario_consumo_dia = current_calendario_consumo_dia;
                //Editando o grafico consumo por dia
                //Remover div:
                $("#updateConsumoDia").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/consumoDia/updateConsumoDia.php',
                    data: {
                        'data_consumo_dia':  last_calendario_consumo_dia,
                        'id_unidades': last_id_unidades_dia
                    },
                    beforeSend: function(carrega){
                        $("#updateConsumoDia").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                    },
                    success: function(r){
                        $("#updateConsumoDia").html(r);
                    }
                });
            }

            //Codigo para mudanca de consumo mes:
            var current_id_unidades_mes = $("#idUnidades").find(':selected').attr('id');
            var current_calendario_unidade = "01-"+$("#datasCalendario").val();
            if(current_id_unidades_mes != last_id_unidades_mes || current_calendario_unidade != last_calendario_unidade){
                //Atualiza variaveis:
                last_id_unidades_mes = current_id_unidades_mes;
                last_calendario_unidade = current_calendario_unidade;
                //Formateia para pegar ate o ultimo mes
                /*var anoNovo = last_calendario_unidade.substr(0, 4);
                var mesNovo = last_calendario_unidade.substr(5, 2);*/
                var anoNovo = last_calendario_unidade.substr(6, 10);
                var mesNovo = last_calendario_unidade.substr(3, 2);
                var finalNovo = new Date(anoNovo, mesNovo, 0);
                var ultimoDia = finalNovo.getDate();
                var dataFinalFormatada = anoNovo+'-'+mesNovo+'-'+ultimoDia;
                //Editando o grafico consumo por mes
                //Remover div:
                $("#updateConsumoMensal").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/consumoMes/updateConsumoMes.php',
                    data: {
                        'calendario_unidade':  last_calendario_unidade,
                        'final_mes': dataFinalFormatada,
                        'id_unidades': last_id_unidades_mes
                    },
                    beforeSend: function(carrega){
                        $("#updateConsumoMensal").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                    },
                    success: function(r){
                        $("#updateConsumoMensal").html(r);
                    }
                });
            }

            //Codigo para mudanca de posto tarifario:
            var current_id_unidades_posto_tarifario = $("#idUnidades").find(':selected').attr('id');
            var current_calendario_posto_tarifario = "01-"+$("#datasCalendarioConsumoPostoTarifario").val();
            if(current_id_unidades_posto_tarifario != last_id_unidades_posto_tarifario || current_calendario_posto_tarifario != last_calendario_posto_tarifario){
                //Atualiza valores:
                last_id_unidades_posto_tarifario = current_id_unidades_posto_tarifario;
                last_calendario_posto_tarifario = current_calendario_posto_tarifario;
                //Formateia para pegar ate o ultimo mes:
                /*var anoNovoPostoTarifario = last_calendario_posto_tarifario.substr(0, 4);
                var mesNovoPostoTarifario = last_calendario_posto_tarifario.substr(5, 2);*/
                var anoNovoPostoTarifario = last_calendario_posto_tarifario.substr(6, 10);
                var mesNovoPostoTarifario = last_calendario_posto_tarifario.substr(3, 2);
                var finalNovoPostoTarifario = new Date(anoNovoPostoTarifario, mesNovoPostoTarifario, 0);
                var ultimoDiaPostoTarifario = finalNovoPostoTarifario.getDate();
                var dataFinalFormatadaPostoTarifario = anoNovoPostoTarifario+'-'+mesNovoPostoTarifario+'-'+ultimoDiaPostoTarifario;
                //Editando o grafico consumo posto tarifario:
                //Remove div e insere os dados recolhidos do ajax:
                $("#updateConsumoPostoTarifario").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/consumoPostoTarifario/updateConsumoPostoTarifario.php',
                    data: {
                        'data_consumo_posto_tarifario':  last_calendario_posto_tarifario,
                        'final_mes_consumo_posto_tarifario': dataFinalFormatadaPostoTarifario,
                        'id_unidades': last_id_unidades_posto_tarifario
                    },
                    beforeSend: function(carrega){
                        $("#updateConsumoPostoTarifario").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                    },
                    success: function(p){
                        $("#updateConsumoPostoTarifario").html(p);
                    }
                });
            }

            //Codigo para mudanca de fator potencia:
            var current_id_unidades_fator_potencia = $("#idUnidades").find(':selected').attr('id');
            var current_calendario_fator_potencia = $("#datasCalendarioFatorPotencia").val();
            if(current_id_unidades_fator_potencia != last_id_unidades_fator_potencia || current_calendario_fator_potencia != last_calendario_fator_potencia){
                //Atualiza valores:
                last_id_unidades_fator_potencia = current_id_unidades_fator_potencia;
                last_calendario_fator_potencia = current_calendario_fator_potencia;
                //Editando o grafico consumo posto tarifario:
                //Remove div e insere os dados recolhidos do ajax:
                $("#updateFatorPotencia").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/fatorPotencia/updateFatorPotencia.php',
                    data: {
                        'data_fator_potencia':  last_calendario_fator_potencia,
                        'id_unidades': last_id_unidades_fator_potencia
                    },
                    beforeSend: function(carrega){
                        $("#updateFatorPotencia").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                    },
                    success: function(f){
                        $("#updateFatorPotencia").html(f);
                    }
                });




                //===============================================
                $.ajax({
                    type: 'GET',
                    url: '../../backend/cardsDashboard/updates/updateCapacitivoIndutivoFatorPotencia.php',
                    data: {
                        'data_fator_potencia':  last_calendario_fator_potencia,
                        'id_unidades': last_id_unidades_fator_potencia
                    },
                    success: function(newSend){
                        /*console.log(newSend);*/
                        var arr = $.parseJSON(newSend);
                        $.each(arr, function(index, val) {
                            console.log("Indice: " + index + " Valor: " + val);
                            if(index == 0){
                                $("#input_capacitivo").val(val);
                            }else{
                                $("#input_indutivo").val(val);
                            }
                        });
                    }
                });

                //===============================================



            }
        };
    }();
    window.setInterval(checkCookie, 100);
</script>