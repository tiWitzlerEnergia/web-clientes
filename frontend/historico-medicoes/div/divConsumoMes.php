<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php include "../../backend/graficos/consumoMes/pegarUltimoRegistro/recolhe.php"; ?>

<div class="col-12 col-lg-6">
    <div class="card">
        <div class="text-value card-header">
            <div class="row">
                <div class="card-header-row col-12 col-sm-12 col-lg-3 text-center d-flex justify-content-center">Consumo por Mês</div>
                <div class="card-header-row pt-2 centerInMobile col-lg-3"></div>
                <div class="card-header-row arquivoInput col-12 col-sm-12 col-lg-6" style="text-align: right;">
                    <div class="card-header-date">
                        <!-- CALENDARIO -->
                        <?php include "../../backend/datas/datasPonta/calendarioDashboard.php"; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="chartWrapper" style="position: relative;">
                <!--<div class="chartAreaWrapper" id="graficoPrevia" style="overflow-x: auto;">-->
                <div class="chartAreaWrapper" id="updateConsumoMensal" style="overflow-x: auto; height: 378px;">
                    <script>
                        $("#updateConsumoMensal").html("<div id='loadConsumomes' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                        setTimeout(function() {
                            document.getElementById('loadConsumomes').remove();
                            document.getElementById('medicao_mensal_de_consumo').style.display = 'block';
                        }, 2500);
                    </script>
                    <canvas id="medicao_mensal_de_consumo" class="fade-in" width="750" height="350" style="display: none; position: relative;"></canvas>
                    <?php
                    $dataInicial = $ultimaData;
                    $dataFinal = date("Y-m-t", strtotime($ultimaData));
                    $id_unidade = isset($_COOKIE['id_unidades']) ? $_COOKIE['id_unidades'] : $arrayIdOption[0];
                    $finalMesAtual = date("t");
                    //include "../../backend/graficos/ponta/graficoPontas.php";
                    //include "../../backend/graficos/ponta/chartPonta.php";
                    $bar = new MedicaoMensalConsumo("medicao_mensal_de_consumo", "medidas/periodo/fora/ponta/diario?dataFinal=$dataFinal&dataInicial=$dataInicial&unidade_id=$id_unidade", array("data_medicao" => array(), "foraPonta" => array(), "ponta" => array()), $dataInicial);
                    ?>
                    <script>
                        <?php
                            // $bar->varChartData("medicaoMensalConsumo", array("Fora Ponta", "Ponta"), array("data_medicao", "foraPonta", "ponta"), array("rgba(29, 107, 170, 0.7)", "rgba(76, 142, 173, 0.7)"), array("rgba(29, 107, 170, 0.7)", "rgba(76, 142, 173, 0.7)"), 1);
                            $bar->varChartData("medicaoMensalConsumo", array("Fora Ponta", "Ponta"), array("data_medicao", "foraPonta", "ponta"), array($_SESSION['cor-custom-1'], $_SESSION['cor-custom-2']), array($_SESSION['cor-custom-1'], $_SESSION['cor-custom-2']), 1);
                            $bar->configChartData("medicaoMensalConsumo", "bar", "teste bar");
                        ?>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>