<?php
//$error = "";
//verificador da região do usuário:
function getUserIP()
{
  $client  = @$_SERVER['HTTP_CLIENT_IP'];
  $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
  $remote  = $_SERVER['REMOTE_ADDR'];
  if (filter_var($client, FILTER_VALIDATE_IP)) {
    $ip = $client;
  } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
    $ip = $forward;
  } else {
    $ip = $remote;
  }
  return $ip;
}
$user_ip = getUserIP();

/*
$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
$ipgeo = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
$pais = $ipgeo->geoplugin_countryName;
echo '<script>console.log("' . $pais . '");</script>';
if ($pais != "Brazil") {
  $exterior = 1;
}
*/
session_start();

if (isset($_SESSION['token'])) {
  header("Location: ../dashboard/index.php");
}

header("X-Frame-Options: SAMEORIGIN");
?>
<!DOCTYPE html>
<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<html lang="en">

<head>
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <title>Witzler Energia | Mercado Livre de Energia</title>
  <link rel="apple-touch-icon" sizes="57x57" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="60x60" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="72x72" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="76x76" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="114x114" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="120x120" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="144x144" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="152x152" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="180x180" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="icon" type="image/png" sizes="192x192" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="icon" href="../bibliotecas/icones/FAVICON/FAVICON_WITZLER_ENERGIA.ico">
  <!--<link rel="manifest" href="../bibliotecas/assets/favicon/manifest.json">-->

  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="../bibliotecas/icones/FAVICON/FAVICON_WITZLER_ENERGIA.ico">
  <meta name="theme-color" content="#ffffff">
  <!-- Main styles for this application-->
  <link href="../bibliotecas/css/style.css" rel="stylesheet">

  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css'>
  <meta name="robots" content="noindex">
  <script>
    /*window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');*/
  </script>
</head>

<body class="c-app flex-row align-items-center" style="background-image: url(../bibliotecas/assets/img/background-3.9902c0af.jpg); background-repeat: no-repeat; background-attachment: fixed; background-position: center; ">
  <div class="wrapper">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-5">
          <div class="login animated slideInDown card-group">
            <div class="card p-4" style="background-color: rgba(255, 255, 255, 0.8);">
              <div class="card-body">
                <!--<h1>Login</h1>
                <p class="text-muted">Entre na sua conta</p>-->
                <div style="display: flex; justify-content: center; align-items: center; margin-top: -10px; padding-bottom: inherit; width:80%;">
                  <img src="../bibliotecas/assets/img/logo-witzler.svg" style="padding-left: 65px;">
                </div>
                <form method="post" id="formulario-login" id="formulario-login" action="login.php">
                  <div class="input-group mb-3" style="padding-left: 25px; padding-right: 40px;">
                    <div class="input-group-prepend"><span class="input-group-text">
                        <!--<svg class="c-icon">
                          <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-user"></use>
                        </svg>--><img src="../bibliotecas/icones/ÍCONES_USUÁRIO_18 px CIANO.svg" height=16 width=16></span></div>
                    <input class="form-control" type="text" placeholder="Usuario" name="usuario" id="usuario" autocomplete="off">
                  </div>


                  <div class="input-group mb-4" style="padding-left: 25px; padding-right: 40px;">
                    <div class="input-group-prepend"><span class="input-group-text">
                        <!--<svg class="c-icon">
                          <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-lock-locked"></use>
                        </svg>--><img src="../bibliotecas/icones/ÍCONES_SENHA_18 px CIANO .svg" height=16 width=16></span></div>
                    <input class="form-control" type="password" placeholder="Senha" name="senha" id="senha" required>
                  </div>
                  <input type="hidden" name="ip" value="" id="ipusuario"/>
                  <div class="row">
                    <div class="col-6 " style="padding-left: 40px;">
                      <button class="btn btn-link px-0" type="button" onclick="window.location.href='../esqueci-senha/index.php'">Esqueceu sua senha?</button>
                    </div>
                    <div class="col-6 text-right" style="padding-right: 55px;">
                      <button class="login-btn btn btn-primary px-4" type="submit" name="login" id="login" value="login" <?php /*if (($exterior)) {
                                                                                                                  echo "disabled";
                                                                                                                }*/ ?>>Login</button>
                    </div>
                  </div>
                </form>

                <?php
                if (isset($_GET['erro']) && $_GET['erro'] == "login") {
                  echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Essa conta não existe<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                } else if (isset($_GET['erro']) && $_GET['erro'] == "nologin") {
                  echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Voce nao esta logado<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                } else if (isset($_GET['erro']) && $_GET['erro'] == "sessaoexpirou") {
                  echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">A sessão expirou<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                } else if (isset($_GET['erro']) && $_GET['erro'] == "containvalida") {
                  echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">A conta logada não é válida.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                } else if (isset($exterior)) {
                  echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Somente são permitidos usuarios do Brasil.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                }
                ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- CoreUI and necessary plugins-->
  <script src="../bibliotecas/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
  <!--[if IE]><!-->
  <script src="../bibliotecas/vendors/@coreui/icons/js/svgxuse.min.js"></script>
  <!--<![endif]-->

  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
  <script src="./script.js"></script>
  <script>
  $.getJSON('https://api.ipify.org?format=jsonp&callback=?', function(data) {
    //console.log(JSON.stringify(data, null, 2));
    //console.log(data.ip);
    document.getElementById('ipusuario').value = data.ip;
  });
  </script>
</body>

</html>
