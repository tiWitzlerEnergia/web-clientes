<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
$usuario = strtolower($_POST['usuario']);
$u_password = $_POST['senha'];



$url = "https://api.develop.clientes.witzler.com.br/api/usuarios/auth";
$curl = curl_init();

//Cria requisiÃ§Ã£o em json:
$request = '{
    "u_password": "'.$_POST['senha'].'",
    "username": "'.$usuario.'"
}';


curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json']);
curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);
$err = curl_error($curl);
if($err){
    echo '<br/><div class="alert alert-danger" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Curl Error: '.$err.'</div>';
}else{
    $response = json_decode($result, true);
    if(isset($response['token'])){
        $token = $response['token'];    
        if($token == $response['token']){
            //echo $token;
            session_cache_expire(25);
            session_start();
            $_SESSION['usuario'] = $usuario;
            $_SESSION['senha'] = $_POST['senha'];
            $_SESSION['token'] = $response['token'];
            $_SESSION['CREATED'] = time();
            //Pega o cliente ID e armazena em SESSION::
            $urlUnidades = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=".$_SESSION['usuario'];
            $context = stream_context_create(array(
                'http' => array(
                    'header' => "Authorization: Bearer ".$_SESSION['token'],
                ),
            ));
            $recolheUnidades = file_get_contents($urlUnidades, false, $context);
            $resultUnidades = json_decode($recolheUnidades);
            foreach($resultUnidades as $unidades){
                $cliente_id = $unidades->cliente_id;
            }
            $_SESSION['cliente_id'] = $cliente_id;

            /*
            //Pega o usuario ID e armazena em sessao:
            $urlUserId = "https://api.develop.clientes.witzler.com.br/api/usuarios/info/usuario/".$_SESSION['usuario'];
            $contextUserId = stream_context_create(array(
                'http' => array(
                    'header' => "Authorization: Bearer ".$_SESSION['token'],
                ),
            ));
            $recolheUserId = file_get_contents($urlUserId, false, $contextUserId);
            $resultUserId = json_decode($recolheUserId);
            foreach($resultUserId as $userid){
                $user_id = $userid->id_login;
            }
            $_SESSION['user_id'] = $user_id;
            */

            // DEFININDO O FUSO HORARIO COMO O HORARIO DE BRASILIA
            //date_default_timezone_set('America/Sao_Paulo');

            // CRIA UMA VARIAVEL E ARMAZENA AHORA ATUAL DO FUSO-HORÁRIO DEFINIDO (BRASÍLIA)
            $timezone = new DateTimeZone('America/Sao_Paulo');
            $dataLogin = date('d/m/Y H:i:s', time());

            $h = "3"; //HORAS DO FUSO ((BRASÍLIA = -3) COLOCA-SE SEM O SINAL -).
            $hm = $h * 60;
            $ms = $hm * 60;
            //COLOCA-SE O SINAL DO FUSO ((BRASÍLIA = -3) SINAL -) ANTES DO ($ms). DATA
            $gmdata = gmdate("d/m/Y", time()-($ms)); 
            //COLOCA-SE O SINAL DO FUSO ((BRASÍLIA = -3) SINAL -) ANTES DO ($ms). HORA
            $gmhora = gmdate("g:i", time()-($ms)); 

            $dataLogin = $gmdata . " " . $gmhora;


            
            // PEGANDO O ID DO CLIENTE
            $urlUsuarioCliente = "https://api.develop.clientes.witzler.com.br/api/informacaoCliente?id_cliente=".$_SESSION['cliente_id'];
            $context = stream_context_create(array(
                'http' => array(
                    'header' => "Authorization: Bearer ".$_SESSION['token'],
                ),
            ));
            @$recolheUsuarioCliente = file_get_contents($urlUsuarioCliente, false, $context);
            @$resultUsuarioCliente = json_decode($recolheUsuarioCliente);
            if(isset($resultUsuarioCliente)){
                foreach($resultUsuarioCliente as $usuarioCliente){
                    $nome_usuario_fantasia = $usuarioCliente->nome_fantasia;
                }

                $nome = $nome_usuario_fantasia;

                // ENVIANDO OS DADOS PARA FAZER INSERT DO LOGIN
                //$urlRegistraLogin = "https://api.develop.clientes.witzler.com.br/api/loginacesso/registraacesso";
                $urlRegistraLogin = "https://api.develop.clientes.witzler.com.br/api/loginacesso/registraacesso";
                $curl = curl_init();


                $request = '{
                
                    "nome":"'.$nome.'",
                    "data_login":"'.$dataLogin.'"
                
                }';
                
                curl_setopt($curl, CURLOPT_URL, $urlRegistraLogin);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, ['content-type: application/json']);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $request);

                $result = curl_exec($curl);
                $err = curl_error($curl);

                if($err) {
                    echo "ERROR";
                }

                
                if($_SESSION['usuario'] == "adminwitzler" && $_POST['senha'] == "9c=Pv#!=sxEtQBeQ") {
                    header("Location: ../dashboard-admin/index.php");
                    //echo $_SESSION['usuario'];
                    exit();
                } else {
                    header("Location: ../dashboard/index.php");
                    exit();
                }
            
            
            }
        }else{
            //Mensagem de erro de login
            header("Location: index.php?erro=login");
            exit();
        }
    }else{
        //Mensagem de erro de login:
        header("Location: index.php?erro=login");
        exit();
        $error = '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Essa conta nÃ£o existe<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
        echo $error;
    }
        

}