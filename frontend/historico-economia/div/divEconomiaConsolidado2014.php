<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php include "../../backend/graficos/economiaConsolidado/graficoEconomiaConsolidado.php"; ?>

<div class="card">
   <div class="card-header">
      <div class="row">
         <div class="col-lg-3">
            <div class="header-chart Col card-title text-center d-flex justify-content-center">
               <label class="text-value">DETALHES</label>
            </div>
         </div>
         <div class="card-header-row pt-2 centerInMobile col-lg-1"></div>
         <!--div class="text-center d-flex justify-content-center col-lg-4">
            <input id="txtTotalCliente" type="text" class=" align-middle form-control" value="R$ <?php echo $valorEconomizadoTotalFormatado; ?>" style="text-align: center;">
         </div-->
         <div class="text-right card-header-row pt-2 centerInMobile col-lg-2"></div>
         <!--div class="text-center d-flex justify-content-center card-header-row col-lg-2">
            <span>• Desde Fevereiro de 2014 •</span>
         </div-->
      </div>
   </div>
   <div class="card-body" style="">
      <div class="chartWrapper" style="position: relative;">
         <div class="chartAreaWrapper" id="charthistorico_de_economia_consolidado_acl_x_acr" style="overflow-x: auto; height: 400px;">
            <script>
               /*$("#charthistorico_de_economia_consolidado_acl_x_acr").html("<div id='loadhistorico_de_economia_consolidado_acl_x_acr' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
               setTimeout(function() {
                  document.getElementById('loadhistorico_de_economia_consolidado_acl_x_acr').remove();
                  document.getElementById('historico_de_economia_consolidado_acl_x_acr').style.display = 'block';
               }, 2500);*/
            </script>

            <table class="table">
            <thead>
               <tr>
                  <th scope="col"><strong>UNIDADE</strong></th>
                  <th scope="col">ECONOMIA Jan/2021</th>
                  <th scope="col">ECONOMIA Fev/2021</th>
                  <th scope="col">ECONOMIA até Jan/2021</th>
                  <th scope="col">ECONOMIA TOTAL</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <th scope="row">1</th>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>@mdo</td>
                  <td>1</td\>
               </tr>
               <tr>
                  <th scope="row">2</th>
                  <td>Jacob</td>
                  <td>Thornton</td>
                  <td>@fat</td>
                  <td>1</td\>
               </tr>
               <tr>
                  <th scope="row">3</th>
                  <td>Larry</td>
                  <td>the Bird</td>
                  <td>@twitter</td>
                  <td>1</td\>
               </tr>
            </tbody>
            </table>

            <canvas id="historico_de_economia_consolidado_acl_x_acr" class="fade-in" width="1570" height="380" style="display: none; position: relative;"></canvas>
            <?php //include "../../backend/graficos/economiaConsolidado/chartEconomiaConsolidado.php"; ?>
         </div>
      </div>
   </div>
</div>


<!--div class="card">
   <div class="card-header">
      <div class="row">
         <div class="col-lg-3">
            <div class="header-chart Col card-title text-center d-flex justify-content-center">
               <label class="text-value">Histórico de Economia Consolidado: ACL X ACR</label>
            </div>
         </div>
         <div class="card-header-row pt-2 centerInMobile col-lg-1"></div>
         <div class="text-center d-flex justify-content-center col-lg-4">
            <input id="txtTotalCliente" type="text" class=" align-middle form-control" value="R$ <?php //echo $valorEconomizadoTotalFormatado; ?>" style="text-align: center;">
         </div>
         <div class="text-right card-header-row pt-2 centerInMobile col-lg-2"></div>
         <div class="text-center d-flex justify-content-center card-header-row col-lg-2">
            <span>• Desde Fevereiro de 2014 •</span>
         </div>
      </div>
   </div>
   <div class="card-body" style="">
      <div class="chartWrapper" style="position: relative;">
         <div class="chartAreaWrapper" id="charthistorico_de_economia_consolidado_acl_x_acr" style="overflow-x: auto; height: 400px;">
            <script>
               $("#charthistorico_de_economia_consolidado_acl_x_acr").html("<div id='loadhistorico_de_economia_consolidado_acl_x_acr' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
               setTimeout(function() {
                  document.getElementById('loadhistorico_de_economia_consolidado_acl_x_acr').remove();
                  document.getElementById('historico_de_economia_consolidado_acl_x_acr').style.display = 'block';
               }, 2500);
            </script>

            <canvas id="historico_de_economia_consolidado_acl_x_acr" class="fade-in" width="1570" height="380" style="display: none; position: relative;"></canvas>
            <?php //include "../../backend/graficos/economiaConsolidado/chartEconomiaConsolidado.php"; ?>
         </div>
      </div>
   </div>
</div>