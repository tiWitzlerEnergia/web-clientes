<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<?php
include "../../backend/detalhesUnidade/detalhesUnidade.php";
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
//Pegar datas:
/*$arrDataFormatada = [];
if (isset($arrDataEconomizado) && count($arrDataEconomizado) >= 1) {
    for ($i = 0; $i < 3; $i++) {
        $data[$i] =  explode("/", ucfirst(strftime('%B/%Y', strtotime($arrDataEconomizado[0][$i]))));
        $pegarInicial = substr($data[$i][0], 0, 3);

        array_push($arrDataFormatada, $pegarInicial . "/" . $data[$i][1]);
    }
} else {
    //Pega 3 ultimos meses (com o atual):
    $dt = new DateTime('first day of this month');
    $datas = array($dt->format('Y-m-d'));
    for ($i = 0; $i < 3; $i++) {
        $dt->modify('-1 month');

        $datas[] = $dt->format('Y-m-d');
    }
    //print_r($datas);
    for ($i = 0; $i = count($datas); $i++) {
        $transformar[$i] = explode("/", ucfirst(strftime('%B/%Y', strtotime($datas[$i]))));
        $pegarInicial = substr($transformar[$i], 0, 3);

        array_push($arrDataFormatada, $pegarInicial . "/" . $transformar[$i][1]);
    }
}*/



/*echo "<pre>";
for ($i = 0; $i < count($arrayIdOption); $i++) {
    //print_r($arrValorEconomizado[$i]);
    //print_r($arrPorcentagemEconomia[$i]);   
}
//print_r($arrMedPerEconomiaUnidade);
echo "</pre>";
for ($i = 0; $i < count($arrayIdOption); $i++) {
    $valorTotal12Meses = 0;
    for ($j = 0; $j < 12; $j++) {
        //$valorTotal12Meses += isset($arrValorEconomizado[$i][$j]) ? $arrValorEconomizado[$i][$j] : 0;
        //echo "Valor economia para o mes $j: " . isset($arrValorEconomizado[$i][$j]) && !(empty($arrValorEconomizado[$i][$j])) ? $arrValorEconomizado[$i][$j] : 0;
    }
}*/
?>


<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-lg-3">
                <div class="mb-0 header-chart Col card-title text-center text-lg-right text-sm-left text-xl-right">
                    <!-- <img class="c-icon" src="../bibliotecas/icones/ECONOMIA.svg" style="height: 65px; width: 65px;"> -->
                    <?php echo("<img class='c-icon' src='".$_SESSION['path-imagem-default']."ECONOMIA.svg' style='height: 65px; width: 65px;' /> ")?>
                    <!--<i class="c-icon c-icon-2xl mb-2 cil-cash"></i>-->
                    <label class="text-value align-middle" style="margin-top: 10px;">
                        <h4 style="margin: 0;"><b>MINHAS ECONOMIAS</b></h4>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body" style="padding-top: 10px; padding-bottom: 0; padding-right: 0; padding-left: 0;">
        <div class="chartWrapper" style="position: relative;">
            <div class="chartAreaWrapper" id="charthistorico_de_economia_consolidado_acl_x_acr" style="overflow-x: auto; /*height: 400px;*/">
                <script>
                    /*$("#charthistorico_de_economia_consolidado_acl_x_acr").html("<div id='loadhistorico_de_economia_consolidado_acl_x_acr' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                    setTimeout(function() {
                        document.getElementById('loadhistorico_de_economia_consolidado_acl_x_acr').remove();
                        document.getElementById('historico_de_economia_consolidado_acl_x_acr').style.display = 'block';
                    }, 2500);*/
                </script>

                <table class="table" style="padding: 0.6rem; margin-bottom: 0;">
                    <thead class="text-center">
                        <tr>
                            <th scope="col" style="border-top: 0;"><strong>UNIDADE</strong></th>
                            <th scope="col" style="border-top: 0;">Últimos 12 Meses (R$)</th>
                            <th scope="col" style="border-top: 0;">Percentual (%)</th>
                            <th scope="col" style="border-top: 0;">Economia Total (R$)</th>
                            <th scope="col" style="border-top: 0;">Economia Percentual Med (%)</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        <?php
                        if (isset($arrayIdOption) &&  count($arrayIdOption) >= 1) {
                            for ($i = 0; $i < count($arrayIdOption); $i++) {
                        ?>
                                <tr>
                                    <th scope="row"><?php echo $arrayNomeUnidade[$i]; ?></th>
                                    <td><?php echo !is_null($arrValorEconomizado12Meses[$i]) ? number_format($arrValorEconomizado12Meses[$i], 2, ',', '.') : 0; ?></td>
                                    <td><?php echo !is_null($arrMedPerEconomizado12Meses[$i]) ? round(($arrMedPerEconomizado12Meses[$i] * 100)) . "%" : "0%"; ?></td>
                                    <td><?php echo !is_null($arrValorEconomizado[$i]) ? number_format(array_sum($arrValorEconomizado[$i]), 2, ',', '.') : 0; ?></td>
                                    <td><?php echo !is_null($arrMedPerEconomiaUnidade[$i]) ? round(($arrMedPerEconomiaUnidade[$i] * 100)) . "%" : "0%"; ?></td>
                                </tr>
                            <?php
                            }

                            ?>
                            <tr style="background-color: #d8d8d8;">
                                <th scope="row"><strong>Total</strong></th>
                                <td><strong><?php echo isset($valorTotalEconomizado12Meses) ? number_format($valorTotalEconomizado12Meses, 2, ',', '.') : 0; ?></strong></td>
                                <td><strong><?php echo isset($medTotalPerEconomia12Meses) ? round(($medTotalPerEconomia12Meses * 100)) . "%" : "0%"; ?></strong></td>
                                <td><strong><?php echo isset($somaValorEconomizadoTodasUnidades) ? number_format($somaValorEconomizadoTodasUnidades, 2, ',', '.') : 0; ?></strong></td>
                                <td><strong><?php echo isset($medTotalPerEconomia) ? round(($medTotalPerEconomia * 100)) . "%" : "0%"; ?></strong></td>
                            </tr>
                        <?php
                        } else {
                        ?>
                            <div class="row col-12 text-center align-items-center justify-content-center" style="height: 120px;">
                                <h3>Não tem nenhuma unidade cadastrada.</h3>
                            </div>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>