<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    var checkCookie = function() {
        var lastCookie = document.cookie;
        //Aqui separamos as id de unidade para cada uma das buscas:
        var last_id_unidades_historico_economia = $("#idUnidades").find(':selected').attr('id');
        return function() {
            var currentCookie = document.cookie;

            //Codigo para mudanca de consumo dia:
            var current_id_unidades_historico_economia = $("#idUnidades").find(':selected').attr('id');
            if(current_id_unidades_historico_economia != last_id_unidades_historico_economia){
                last_id_unidades_historico_economia = current_id_unidades_historico_economia;
                //Editando o grafico consumo por dia
                //Remover div:
                $("#updateHistoricoEconomia").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/historicoEconomiaUnidade/updateHistoricoEconomiaUnidade.php',
                    data: {
                        'id_unidades': last_id_unidades_historico_economia
                    },
                    beforeSend: function(carrega){
                        $("#updateHistoricoEconomia").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                    },
                    success: function(r){
                        $("#updateHistoricoEconomia").html(r);
                    }
                });
            }
        };
    }();
    window.setInterval(checkCookie, 100);
</script>