  <!-- HEAD DA PAGINA -->
  <?php include "../recursos/head.php"; ?>
  <!-- BODY DA PAGINA -->
  <body class="c-app">
        <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
        <?php include "../recursos/menu.php"; ?>

        <div class="c-subheader px-3">
          <!-- Breadcrumb-->
          <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Histórico de Economia</li>
          </ol>
        </div>
      </header>
      <!-- Fim do cabeçalho -->
      <!-- Conteudo da pagina -->
    <br/>
    <div class="container-fluid">
        <div id="economyhistory" class="animated fadeIn">
            <div class="row">
                <div class="col">
                    <!-- CODIGO DO GRAFICO HISTORICO ECONOMIA UNIDADE -->
                    <?php include "div/divHistoricoEconomiaUnidade.php"; ?>
                </div>
            </div>
            <!-- CODIGO DO GRAFICO ECONOMIA CONSOLIDADO -->
            <?php include "div/divDetalhes.php"; ?>
        </div>
    </div>
    <!-- INCLUDE DO AJAX -->
    <?php include "div/configAjax.php"; ?>
    <!-- Fim do conteudo da pagina -->
    <!-- FOOTER -->
    <?php include "../recursos/footer.php"; ?>
    <!-- FIM -->