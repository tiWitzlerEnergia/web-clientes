<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    var checkCookie = function() {
        var lastCookie = document.cookie;
        //Aqui separamos as id de unidade para cada uma das buscas:
        var last_id_unidades_contratos = $("#idUnidades").find(':selected').attr('id');
        return function() {
            var currentCookie = document.cookie;

            //Codigo para mudanca de consumo dia:
            var current_id_unidades_contratos = $("#idUnidades").find(':selected').attr('id');
            if(current_id_unidades_contratos != last_id_unidades_contratos){
                last_id_unidades_contratos = current_id_unidades_contratos;
                //Editando o grafico consumo por dia
                //Remover div:
                $("#updateContratos").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/contratos/updateContratos.php',
                    data: {
                        'id_unidades': last_id_unidades_contratos
                    },
                    beforeSend: function(carrega){
                        $("#updateContratos").html("<div id='loadContratos' class='col-12 d-flex align-items-center text-center justify-content-center' style='height: 550px; background: white;'><span class='loader'></span></div>");
                    },
                    success: function(r){
                        setTimeout(function(){
                            $("#updateContratos").html(r);
                        }, 2500);
                    }
                });
            }
        };
    }();
    window.setInterval(checkCookie, 100);
</script>