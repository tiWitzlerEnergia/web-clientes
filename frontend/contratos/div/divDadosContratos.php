<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="row" id="updateContratos" style="margin: 0;">
    <?php
    $id_unidades_contrato =  isset($arrayIdOption[0]) ? $arrayIdOption[0] : $_COOKIE['id_unidades'];
    $data = date("Y");
    $cards = new CardContratos($data, $id_unidades_contrato);
    if (empty($cards->getIndicesAnoAtual())) {
    ?>
        <script>
            $("#updateContratos").html("<div id='loadContratos' class='col-12 d-flex align-items-center text-center justify-content-center' style='height: 550px; background: white;'><span class='loader'></span></div>");
            setTimeout(function() {
                document.getElementById('loadContratos').remove();
                document.getElementById('contrato').style.display = 'block';

            }, 2500);
        </script>
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 fade-in" id="contrato">
            <div class="card-accent-secondary card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="border-top: 0;">
                                        <div class="col-12 container row noPadding noMargin">
                                            <div class="col-12 noPadding text-center">
                                                <h1>Sem contratos</h1>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php
    } else {
        $tamanhoContratos = count($cards->getIndicesAnoAtual());
    ?>
        <!-- ANIMACAO JAVASCRIPT AO CARREGAR CONTRATOS -->
        <script>
            $("#updateContratos").html("<div id='loadContratos' class='col-12 d-flex align-items-center text-center justify-content-center' style='height: 550px; background: white;'><span class='loader'></span></div>");
            setTimeout(function() {
                document.getElementById('loadContratos').remove();
                for (var i = 0; i < <?php echo $tamanhoContratos; ?>; i++) {
                    document.getElementById('contrato' + i).style.display = 'block';
                }
            }, 2500);
        </script>
    <?php
        for ($i = 0; $i < $tamanhoContratos; $i++) {
            $cards->gerarCard($i);
        }
    }
    ?>
</div>