<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="text-value card-header">
    <div class="row">
        <div class="col">
            <div class="mb-0 card-title">Filtros</div>
        </div>
    </div>
</div>
<!-- style="background: rgba(200, 200, 200, 1);" -->
<div class="card-body">
    <div class="row d-flex">
        <div class="col-lg-3 col-md-12 col-sm-12 centerInMobile text-center justify-content-center" style="padding: 20px 0px;">
            <div class="d-flex col-12 align-items-end" style="height: 50%;">
                <div class="dropdown2 dropdown input-group-prepend no-marginright">
                    <label class="text-value input-group-text" style="min-width: 90px; max-width: 90px; place-content: center;">Unidade: </label>
                </div>
                <!-- Aqui carrega o arquivo que tem as opcoes de unidade -->
                <?php include "../../backend/dadosUnidades/dadosUnidades.php"; ?>
            </div>
            <br />
            <div class="d-flex col-12 align-items-start" style="height: 50%;">
                <div class="input-group-prepend no-marginright">
                    <label class="text-value input-group-text" style="min-width: 90px; max-width: 90px; place-content: center;">Ano: </label>
                </div>
                <?php include "../../backend/datas/datasContratos/calendarioContratos.php"; ?>

            </div>
        </div>
        <?php
        //Carrega contratos
        include "../../backend/contratos/contratos.php";
        include "../../backend/contratos/contratos2.php";

        $novoArrTeste = array_unique($armazenaNome);
        $novooArr = array_values($novoArrTeste);
        for ($i = 0; $i < count($novooArr); $i++) {
            echo "<script>console.log('$novooArr[$i]')</script>";
        }
        ?>
        <!--<div class="d-flex col-sm-8 col-lg-4 col-md-6 col-xs-12">-->
        <div class="col-lg-4 col-md-12 col-sm-12 centerInMobile text-center justify-content-center scrollbar" style="padding: 20px 0px;">
            <div class="chartWrapper scrollbar" style="position: relative;">
                <div class="chartAreaWrapper scrollbar" id="graficoComercializadoras" style="overflow-x: auto;">
                    <script>
                        $("#graficoComercializadoras").html("<div id='giftcarregarComercializadoras' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 280px;'><span class='loader'></span></div>");
                        setTimeout(function() {
                            document.getElementById('giftcarregarComercializadoras').remove();
                            document.getElementById('chartDoughnutComercializadora').style.display = 'block';
                        }, 2500);
                    </script>
                    <canvas id="chartDoughnutComercializadora" class="mx-auto fade-in" width="288" height="230" style="display: none; width: 288px !important; height: 230px !important"></canvas>
                    <?php //include "../../backend/graficos/contratos/comercializadoras.php"; 
                    ?>
                    <?php
                    //Montar variaveis de busqueda:
                    $id_unidades_contrato =  isset($arrayIdOption[0]) ? $arrayIdOption[0] : $_COOKIE['id_unidades'];
                    $data = date("Y");
                    $comercializadora = new Comercializadoras($data, "chartDoughnutComercializadora", $id_unidades_contrato);
                    ?>
                    <script>
                        <?php
                        $comercializadora->varChartData("chartComercializadoras", array("Distribuidoras"), array("nomeComercializadora", "values"));
                        $comercializadora->configChartData("chartComercializadoras", "doughnut", "Comercializadoras");
                        ?>
                    </script>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12" style="padding: 20px 0px;">
            <div class="chartWrappe" style="position: relative;">
                <div class="chartAreaWrapper scrollbar" id="graficoMontantes" style="overflow-x: auto;">
                    <script>
                        $("#graficoMontantes").html("<div id='giftcarregarMontantes' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 280px;'><span class='loader'></span></div>");
                        setTimeout(function() {
                            document.getElementById('giftcarregarMontantes').remove();
                            document.getElementById('chartLineMontantes').style.display = 'block';
                        }, 2500);
                    </script>
                    <canvas id="chartLineMontantes" class="mx-auto fade-in" width="510" height="280" style="display: none;"></canvas>
                    <?php
                    //include "../../backend/graficos/contratos/montantes.php"; 
                    $montantes = new Montantes($data, "chartLineMontantes", $id_unidades_contrato);
                    ?>
                    <script>
                        <?php
                        $montantes->varChartData("charMontantes", array("Flex máx", "Valor médio", "Flex min"), array(), array("#c8c8c8", "#E98E29", "#c8c8c8"), array("#c8c8c8", "#E98E29", "#c8c8c8"));
                        $montantes->configChartData("charMontantes", "", "Montante");
                        ?>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>