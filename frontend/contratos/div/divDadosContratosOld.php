<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="row" id="updateContratos" style="margin: 0;">
    <?php
    $contarChart = 1;
    //include "../../backend/contratos/contratos.php";

    if (empty($armazenaNome)) {
    ?>
        <script>
            $("#updateContratos").html("<div id='loadContratos' class='col-12 d-flex align-items-center text-center justify-content-center' style='height: 550px; background: white;'><span class='loader'></span></div>");
            setTimeout(function() {
                document.getElementById('loadContratos').remove();
                for (var i = 0; i < <?php echo $tamanhoContratos; ?>; i++) {
                    document.getElementById('contrato').style.display = 'block';
                }
            }, 2500);
        </script>
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 fade-in" id="contrato">
            <div class="card-accent-secondary card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="border-top: 0;">
                                        <div class="col-12 container row noPadding noMargin">
                                            <div class="col-12 noPadding text-center">
                                                <h1>Sem contratos</h1>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php
    } else {
        //echo "<script>console.log('".count($arrId_distribuidora)."')</script>";
        //Tratamento de dados de contratos2:
        $totalContratos2 = count($arrId);
        /*for($i = 0; $i < $totalContratos2; $i++){

        }*/

        $tamanhoContratos = count($armazenaNome);
    ?>
        <!-- ANIMACAO JAVASCRIPT AO CARREGAR CONTRATOS -->
        <script>
            $("#updateContratos").html("<div id='loadContratos' class='col-12 d-flex align-items-center text-center justify-content-center' style='height: 550px; background: white;'><span class='loader'></span></div>");
            setTimeout(function() {
                document.getElementById('loadContratos').remove();
                for (var i = 0; i < <?php echo $tamanhoContratos; ?>; i++) {
                    document.getElementById('contrato' + i).style.display = 'block';
                }
            }, 2500);
        </script>
        <?php
        for ($i = 0; $i < $totalAnos; $i++) {
        ?>
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 fade-in" id="contrato<?php echo $i; ?>" style="display: none;">
                <div class="card-accent-secondary card">
                    <div class="text-center h-25 card-header">
                        <div class="row">
                            <div class="col">
                                <div class="mb-0 pt-3 card-title">
                                    <img height="50" src="https://clientes.witzler.com.br/backend/assets/img/distributor/<?php echo $arrPath[$i]; ?>">
                                    <hr>
                                    <h5 class="mt-3"><?php echo "Janeiro/" . $arrAno[$arrVAno[$i]] . " A " . "Dezembro/" . $arrAno[$arrVAno[$i]]; ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-6 noPadding text-left">
                                                    Comercializadora:
                                                </div>
                                                <div class="col-6 noPadding text-right">
                                                    <?php echo $arrNomeComercializadora[$i]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-6 noPadding text-left">
                                                    Número do contrato:
                                                </div>
                                                <div class="col-6 noPadding text-right">
                                                    <?php echo $arrN_contract[$arrVAno[$i]]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-6 noPadding text-left">
                                                    Preço Contratado [R$/MWh]:
                                                </div>
                                                <div class="col-6 noPadding text-right">
                                                    <?php echo round($arrPreco[$arrVAno[$i]], 2); ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-6 noPadding text-left">
                                                    Flexibilidade:
                                                </div>
                                                <div class="col-6 noPadding text-right">
                                                    <?php echo $arrFlex_min[$arrVAno[$i]] . "/" . $arrFlex_max[$arrVAno[$i]]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="col-12 container row noPadding noMargin">
                                                <div class="col-6 noPadding text-left">
                                                    Sazonalidade:
                                                </div>
                                                <div class="col-6 noPadding text-right">
                                                    <?php echo $arrSazo_min[$arrVAno[$i]] . "/" . $arrSazo_max[$arrVAno[$i]]; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="height: 250px; ">

                                                <div class="chartWrapper" style="position: relative;">
                                                    <div class="chartAreaWrapper" id="graficoContrato<?php echo $contarChart; ?>" style="overflow-x: auto;">
                                                        <canvas id="chartDoughnutContrato<?php echo $contarChart; ?>" height="250" width="400" class="mx-auto" style="display: block;"></canvas>
                                                    </div>
                                                </div>
                                                <script>
                                                    <?php
                                                    $idGrafico = "chartDoughnutContrato" . $contarChart;
                                                    $varChartGrafico = "charMontantes" . $contarChart;
                                                    $idUnidade =  isset($arrayIdOption[0]) ? $arrayIdOption[0] : $_COOKIE['id_unidades'];
                                                    $contrato[$contarChart] = new Montantes($arrAno[$arrVAno[$i]], $idGrafico, $idUnidade);
                                                    $contrato[$contarChart]->varChartData($varChartGrafico, array("Flex máx", "Valor médio", "Flex min"), array(), array("#c8c8c8", "#E98E29", "#c8c8c8"), array("#c8c8c8", "#E98E29", "#c8c8c8"));
                                                    $contrato[$contarChart]->configChartData($varChartGrafico, "", "Montante");
                                                    ?>
                                                </script>
                                                <!--<script>
                                                    var barChartContrato<?php echo $contarChart; ?> = {
                                                        labels: labelsChart,
                                                        datasets: [
                                                            {
                                                                label: 'Flex máx',
                                                                data: [
                                                                    <?php
                                                                    for ($j = 0; $j < 12; $j++) {
                                                                        echo round(($arrV[$arrVAno[$i]][$j]) * (1 + ($arrFlex_max[$arrVAno[$i]] / 100)), 2) .
                                                                            ", ";
                                                                    }
                                                                    ?>
                                                                ],
                                                                type: "line",
                                                                fill: false,
                                                                backgroundColor: "#c8c8c8",
                                                                borderColor: "#c8c8c8",
                                                                borderWidth: 2,
                                                                lineTension: 0,
                                                                borderDash: [5, 5],
                                                            },
                                                            {
                                                                label: 'Valor médio',
                                                                data: [
                                                                    <?php
                                                                    for ($j = 0; $j < 12; $j++) {
                                                                        echo round(($arrV[$arrVAno[$i]][$j]), 2) .
                                                                            ", ";
                                                                    }
                                                                    ?>
                                                                ],
                                                                type: "line",
                                                                fill: false,
                                                                backgroundColor: "#E98E29",
                                                                borderColor: "#E98E29",
                                                                borderWidth: 2,
                                                                lineTension: 0
                                                            },
                                                            {
                                                                label: 'Flex min',
                                                                data: [
                                                                    <?php
                                                                    for ($j = 0; $j < 12; $j++) {
                                                                        echo round(($arrV[$arrVAno[$i]][$j]) * (1 - ($arrFlex_min[$arrVAno[$i]] / 100)), 2) . ", ";
                                                                    }
                                                                    ?>
                                                                ],
                                                                type: "line",
                                                                fill: false,
                                                                backgroundColor: "#c8c8c8",
                                                                borderColor: "#c8c8c8",
                                                                borderWidth: 2,
                                                                lineTension: 0,
                                                                borderDash: [5, 5],
                                                            }
                                                        ]
                                                    };

                                                    var ctxContrato<?php echo $contarChart; ?> = document.getElementById('chartDoughnutContrato<?php echo $contarChart; ?>').getContext('2d');
                                                    window.myBar = new Chart(ctxContrato<?php echo $contarChart; ?>, {
                                                        type: 'line',
                                                        data: barChartContrato<?php echo $contarChart; ?>,
                                                        options: {
                                                            responsive: false,
                                                            maintainAspectRatio: true,
                                                            legend: {
                                                                position: 'top',
                                                            },
                                                            title: {
                                                                display: false,
                                                                text: 'Comercializadoras'
                                                            },
                                                            animation: {
                                                                animateScale: true,
                                                                animateRotate: true,
                                                                duration: 2000,
                                                            },
                                                            scales: {
                                                                yAxes: [{
                                                                    ticks: {
                                                                        min: 0,
                                                                    },
                                                                }, ]
                                                            }
                                                        }
                                                    });
                                                </script>-->
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    <?php
            $contarChart++;
        }
    }
    ?>
</div>