<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<!-- HEAD DA PAGINA -->
<?php include "../recursos/head.php"; ?>
<!-- BODY DA PAGINA -->

<body class="c-app">
    <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
    <?php
    include "../recursos/menu.php";
    //Variavel que mudara o config do select
    //$meus_arquivos = 1;
    ?>

    <div class="c-subheader px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Meus arquivos</li>
        </ol>
    </div>
    <!-- Fim do cabeçalho -->
    <!--Començo do conteudo do site-->
    <br />
    <div class="container-fluid">
        <div>

            <div class="CardFilters hideInPrint card" style="max-width: 100%;">
                <div class="text-value card-header">
                    <div class="row">
                        <div class="col">
                            <div class="mb-0 card-title">MEUS ARQUIVOS</div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-group">

                        <div style="display:none">
                            <?php include "../../backend/dadosUnidades/dadosUnidades.php"; ?>
                        </div>

                        <!--div class="input-group col-sm-12 col-lg-3 arquivoInput" >
                                <div class="dropdown2 dropdown input-group-prepend">
                                    <label class="text-value input-group-text" style="width: 83px;">Unidade: </label>
                                </div>
                                <!-- Aqui carrega o arquivo que tem as opcoes de unidade -->
                        <?php //include "../../backend/dadosUnidades/dadosUnidades.php"; 
                        ?>
                        <!--/div-->

                        <div class="input-group col-sm-12 col-lg-3 arquivoInput">
                            <div class="dropdown2 dropdown input-group-prepend no-marginright">
                                <label class="text-value input-group-text" style="width: 83px;">Datas: </label>
                            </div>
                            <?php //include "../../backend/datas/datasPonta/datasUnidade.php"; 
                            ?>
                            <!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


                            <input type="text" id="datasUnidade" class="custom-select dropdown-menu-right dropdown2 dropdown" onChange="updateDatas();" />

                            <?php
                            if (isset($_COOKIE['id_unidades'])) {
                                $id_unidades = $_COOKIE['id_unidades'];
                                $urlDatas = "https://api.develop.clientes.witzler.com.br/api/datas/arquivos/unidade?unidade_id=" . $id_unidades;
                                $context = stream_context_create(array(
                                    'http' => array(
                                        'header' => "Authorization: Bearer " . $_SESSION['token'],
                                    ),
                                ));
                                @$recolheDatas = file_get_contents($urlDatas, false, $context);
                                @$resultDatas = json_decode($recolheDatas);
                                if (isset($resultDatas)) {
                                    foreach ($resultDatas as $datas) {
                            ?>

                            <?php
                                    }
                                }
                            }
                            ?>

                            <?php
                            // Teste para ver datas:
                            $diaAtual = date("d");
                            if ($diaAtual >= 15) {
                                // -1 mes
                                $dataAtual = date("m-Y", strtotime("-1 month"));
                            } else {
                                // -2 mes
                                $dataAtual = date("m-Y", strtotime("-2 month"));
                            }
                            ?>
                            <script type="text/javascript">
                                var d = new Date();
                                var dia = d.getDate();
                                var mes = d.getMonth() + 1;
                                var ano = d.getFullYear();
                                //var dataCompleta = ano + "-" + mes + "-" + 01;
                                //var dataCompleta = mes + "-" + ano;
                                var dataCompleta = "<?php echo $dataAtual; ?>";

                                function updateDatas() {
                                    //Configurando tempo de vida da cookie ao executar funçao:
                                    var tempoCookie = new Date();
                                    tempoCookie.setTime(tempoCookie.getTime() + 1 * 3600 * 1000);

                                    //Definindo valores da funçao:
                                    var selectData = document.getElementById('datasUnidade');
                                    var recojeData = "01-" + selectData.value;
                                    console.log(recojeData);
                                    //console.log('TESTANDO')
                                    document.cookie = "data_unidade = " + recojeData + "; expires=" + tempoCookie.toUTCString();

                                }

                                updateDatas();


                                $('#datasUnidade').datepicker({
                                    format: "mm-yyyy",
                                    viewMod: "months",
                                    minViewMode: "months",
                                    language: "pt-BR",
                                    zIndexOffset: 10000,
                                    //defaultDate: new Date()
                                }).datepicker('update', dataCompleta);
                            </script>
                        </div>

                        <div class="input-group col-sm-12 col-lg-3 arquivoInput">
                            <div class="dropdown2 dropdown input-group-prepend no-marginright">
                                <label class="text-value input-group-text" style="width: 83px;">Tipo: </label>
                            </div>
                            <?php include "../../backend/tipoArquivoPdf/tipoArquivoPdf.php"; ?>
                        </div>

                        <div class="input-group col-sm-12 col-lg-3 arquivoInput">
                            <div class="dropdown2 dropdown input-group-prepend no-marginright">
                                <label class="text-value input-group-text" style="width: 83px;">Arquivo: </label>
                            </div>
                            <?php include "../../backend/arquivosPdf/arquivosPdf.php"; ?>
                        </div>

                    </div>
                </div>
            </div>


        </div>

        <br />

        <div class="card">
            <div class="card-header">
                <center>
                    ARQUIVO PDF
                </center>

            </div>
            <div class="card-body" id="atualizapdf" style="height: 800px;">
                <iframe id="abrir" name="abrir" src="" width=100% height=800></iframe>
            </div>
        </div>
    </div>

    <!-- CODIGO TESTE PARA PUXAR PDF -->
    <?php
    /*$arquivo_origem = "https://clientes.witzler.com.br/backend/assets/files/clientes/226/ARQUIVOS/FATURA_ENERGIA/21-06//35210711482752000152550010000011901571502537-12021-07-09115941.228600.pdf";
    $arquivo_destino = "pdfcopias/meupdf.pdf";
    if (copy($arquivo_origem, $arquivo_destino)) {
        echo "Arquivo copiado com Sucesso.";
    } else {
        echo "Erro ao copiar arquivo.";
    }*/
    ?>
    <!-- --->

    <!-- INCLUDE DO CODIGO DO AJAX -->
    <?php include "div/configAjax.php"; ?>
    <!-- FIM DO CONTEUDO DO SITE -->
    <!-- FOOTER -->
    <?php include "../recursos/footer.php"; ?>