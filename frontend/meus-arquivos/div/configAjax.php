<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    var checkCookie = function() {
        var lastCookie = document.cookie;
        //Declarando variaveis de FILTROS UNIDADE:
        var last_id_unidades = $("#idUnidades").find(':selected').attr('id');
        var last_calendario_meus_arquivos = "01-"+$("#datasUnidade").val();
        var last_tipo_arquivo = $("#tipoArquivoPdf").find(':selected').attr('id');
        //Declarando variaveis de FILTROS CLIENTE:
        var last_id_tipo_unidades_cliente = $("#tipoArquivoCliente").find(':selected').attr('id');
        var last_calendario_cliente = $("#dataTipoArquivoCliente").val();
        return function() {
            var currentCookie = document.cookie;
            //====================================== FILTROS UNIDADE ==========================================================
            //Codigo para mudanca de select unidades atualizando select de tipo e pdf:
            var current_id_unidades = $("#idUnidades").find(':selected').attr('id');
            var current_calendario_meus_arquivos = "01-"+$("#datasUnidade").val();
            var current_tipo_arquivo = $("#tipoArquivoPdf").find(':selected').attr('id');
            if(current_id_unidades != last_id_unidades){
                last_id_unidades = current_id_unidades;
                last_calendario_meus_arquivos = current_calendario_meus_arquivos;
                last_tipo_arquivo = current_tipo_arquivo;

                //Editando tipo arquivo:
                $("#tipoArquivoPdf").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/tipoArquivoPdf/updateTipoArquivoPdf.php',
                    data: {
                        'id_unidades': last_id_unidades
                    },
                    success: function(r){
                        $("#tipoArquivoPdf").html(r);
                    }
                });

                //Editando PDF:
                $("#diretorioArquivo").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/arquivosPdf/updateArquivosPdf.php',
                    data: {
                        'data_unidade':  last_calendario_meus_arquivos,
                        'id_unidades': last_id_unidades,
                        'tipo_arquivo': last_tipo_arquivo
                    },
                    success: function(s){
                        $("#diretorioArquivo").html(s);
                    }
                });

                //Editando tipo unidades cliente:
                $("#tipoArquivoCliente").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/tipoArquivoCliente/update/updateTipoArquivoCliente.php',
                    data: {
                        'id_unidades': last_id_unidades
                    },
                    success: function(s){
                        $("#tipoArquivoCliente").html(s);
                    }
                });
            }

            //Codigo para mudanca de pdf usando tipo arquivo:
            if(current_tipo_arquivo != last_tipo_arquivo){
                last_id_unidades = current_id_unidades;
                last_calendario_meus_arquivos = current_calendario_meus_arquivos;
                last_tipo_arquivo = current_tipo_arquivo;
                //Editando PDF:
                $("#diretorioArquivo").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/arquivosPdf/updateArquivosPdf.php',
                    data: {
                        'data_unidade':  last_calendario_meus_arquivos,
                        'id_unidades': last_id_unidades,
                        'tipo_arquivo': last_tipo_arquivo
                    },
                    success: function(s){
                        $("#diretorioArquivo").html(s);
                    }
                });
            }

            //Codigo para mudanca de pdf usando tipo arquivo:
            if(current_calendario_meus_arquivos != last_calendario_meus_arquivos){
                last_id_unidades = current_id_unidades;
                last_calendario_meus_arquivos = current_calendario_meus_arquivos;
                last_tipo_arquivo = current_tipo_arquivo;
                //Editando PDF:
                $("#diretorioArquivo").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/arquivosPdf/updateArquivosPdf.php',
                    data: {
                        'data_unidade':  last_calendario_meus_arquivos,
                        'id_unidades': last_id_unidades,
                        'tipo_arquivo': last_tipo_arquivo
                    },
                    success: function(s){
                        $("#diretorioArquivo").html(s);
                    }
                });
            }
            //================================================================================================
            //=================================== FILTROS CLIENTE ============================================
            var current_id_tipo_unidades_cliente = $("#tipoArquivoCliente").find(':selected').attr('id');
            var current_calendario_cliente = $("#dataTipoArquivoCliente").val();
            if(current_id_tipo_unidades_cliente != last_id_tipo_unidades_cliente){
                last_id_tipo_unidades_cliente = current_id_tipo_unidades_cliente;
                last_calendario_cliente = current_calendario_cliente;
                last_id_unidades = current_id_unidades;
                //Editando PDF por tipo filtro cliente:
                $("#diretorioTipoArquivoCliente").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/tipoArquivoCliente/update/updatePdfTipoArquivoCliente.php',
                    data: {
                        'data_tipo_arquivo_cliente':  last_calendario_cliente,
                        'id_unidades': last_id_unidades,
                        'tipo_arquivo_arquivo_cliente': last_id_tipo_unidades_cliente
                    },
                    success: function(s){
                        $("#diretorioTipoArquivoCliente").html(s);
                    }
                });
            }
            //Editando pela data:
            if(current_calendario_cliente != last_calendario_cliente){
                last_id_tipo_unidades_cliente = current_id_tipo_unidades_cliente;
                last_calendario_cliente = current_calendario_cliente;
                last_id_unidades = current_id_unidades;
                //Editando PDF por tipo filtro cliente:
                $("#diretorioTipoArquivoCliente").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/tipoArquivoCliente/update/updatePdfTipoArquivoCliente.php',
                    data: {
                        'data_tipo_arquivo_cliente':  last_calendario_cliente,
                        'id_unidades': last_id_unidades,
                        'tipo_arquivo_arquivo_cliente': last_id_tipo_unidades_cliente
                    },
                    success: function(s){
                        $("#diretorioTipoArquivoCliente").html(s);
                    }
                });
            }
            //================================================================================================
        };
    }();
    window.setInterval(checkCookie, 100);
</script>