<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<script>
    var checkCookie = function() {
        var lastCookie = document.cookie;
        var last_id_unidades = $("#idUnidades").find(':selected').attr('id');
        var last_calendario_unidade = $("#datasCalendario").val();
        return function() {
            var currentCookie = document.cookie;
            /* codigo pra mudanca de cookie (qualquer coisa dentro dela)
            if (currentCookie != lastCookie) {
                lastCookie = currentCookie;
                console.log(lastCookie);
                //document.location.reload(true)
            }
            */
            //Codigo para mudanca de select unidades:
            var current_id_unidades = $("#idUnidades").find(':selected').attr('id');
            if(current_id_unidades != last_id_unidades){
                last_id_unidades = current_id_unidades;
                var anoNovo = last_calendario_unidade.substr(0, 4);
                var mesNovo = last_calendario_unidade.substr(5, 2);
                var finalNovo = new Date(anoNovo, mesNovo, 0);
                var ultimoDia = finalNovo.getDate();
                var dataFinalFormatada = anoNovo+'-'+mesNovo+'-'+ultimoDia;
                console.log("NOVO ID TESTE: "+last_id_unidades);

                //Remover o valor do card Acumulado Mensal (kWh)
                $(".valorKWh").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/cardsDashboard/updates/valores/updateAcumuladoMensal.php',
                    data: {
                        'calendario_unidade':  last_calendario_unidade,
                        'final_mes': dataFinalFormatada,
                        'id_unidades': last_id_unidades
                    },
                    success: function(s){
                        $(".valorKWh").html(s);
                    }
                });
                //Remover o valor do card Fator Potencia Capacitativo
                $("#valorCapacitivo").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/cardsDashboard/updates/valores/updateFatorPotenciaCapacitativo.php',
                    data: {
                        'calendario_unidade':  last_calendario_unidade,
                        'final_mes': dataFinalFormatada,
                        'id_unidades': last_id_unidades
                    },
                    success: function(s){
                        $("#valorCapacitivo").html(s);
                    }
                });
                //Remover o valor do card Fator Potencia Indutivo
                $("#valorIndutivo").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/cardsDashboard/updates/valores/updateFatorPotenciaIndutivo.php',
                    data: {
                        'calendario_unidade':  last_calendario_unidade,
                        'final_mes': dataFinalFormatada,
                        'id_unidades': last_id_unidades
                    },
                    success: function(s){
                        $("#valorIndutivo").html(s);
                    }
                });

                //Editando o grafico
                //Remover div:
                $("#graficoMedicaoConsumo").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/ponta/updateChartPonta.php',
                    data: {
                        'calendario_unidade':  last_calendario_unidade,
                        'final_mes': dataFinalFormatada,
                        'id_unidades': last_id_unidades
                    },
                    success: function(r){
                        $("#graficoMedicaoConsumo").html(r);
                    }
                });

                //Editando o grafico
                //Remover div:
                //Function p
                
                $("#graficoPrevia").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/webPrevia/updatePrevia.php',
                    data: {
                        'id_unidades': last_id_unidades
                    },
                    success: function(p){
                        $("#graficoPrevia").html(p);
                        
                    }
                });
            }
            
            //Codigo para mudanca de calendario no dashboard:
            var current_calendario_unidade = $("#datasCalendario").val();
            if(current_calendario_unidade != last_calendario_unidade){
                //Atualizando variaveis para possivel busca:
                last_calendario_unidade = current_calendario_unidade;
                var anoNovo = last_calendario_unidade.substr(0, 4);
                var mesNovo = last_calendario_unidade.substr(5, 2);
                var finalNovo = new Date(anoNovo, mesNovo, 0);
                var ultimoDia = finalNovo.getDate();
                var dataFinalFormatada = anoNovo+'-'+mesNovo+'-'+ultimoDia;
                console.log("Novo valor de calendario: "+last_calendario_unidade);
                console.log("Novo valor final de calendario: "+dataFinalFormatada);

                //Editando as cards:
                //Remover div de datas das cards
                $(".dataCard").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/cardsDashboard/updates/datas/updateDataCard.php',
                    data: {
                        'calendario_unidade':  last_calendario_unidade,
                    },
                    success: function(s){
                        $(".dataCard").html(s);
                    }
                });
                


                //Remover o valor do card Acumulado Mensal (kWh)
                $(".valorKWh").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/cardsDashboard/updates/valores/updateAcumuladoMensal.php',
                    data: {
                        'calendario_unidade':  last_calendario_unidade,
                        'final_mes': dataFinalFormatada,
                        'id_unidades': last_id_unidades
                    },
                    success: function(s){
                        $(".valorKWh").html(s);
                    }
                });
                //Remover o valor do card Fator Potencia Capacitativo
                $(".valorFatorPotenciaCapacitativo").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/cardsDashboard/updates/valores/updateFatorPotenciaCapacitativo.php',
                    data: {
                        'calendario_unidade':  last_calendario_unidade,
                        'final_mes': dataFinalFormatada,
                        'id_unidades': last_id_unidades
                    },
                    success: function(s){
                        $(".valorFatorPotenciaCapacitativo").html(s);
                    }
                });
                //Remover o valor do card Fator Potencia Indutivo
                $(".valorFatorPotenciaIndutivo").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/cardsDashboard/updates/valores/updateFatorPotenciaIndutivo.php',
                    data: {
                        'calendario_unidade':  last_calendario_unidade,
                        'final_mes': dataFinalFormatada,
                        'id_unidades': last_id_unidades
                    },
                    success: function(s){
                        $(".valorFatorPotenciaIndutivo").html(s);
                    }
                });

                //Editando grafico 
                //Remover div do grafico:
                $("#graficoMedicaoConsumo").html("");
                $.ajax({
                    type: 'GET',
                    url: '../../backend/graficos/ponta/updateChartPonta.php',
                    data: {
                        'calendario_unidade':  last_calendario_unidade,
                        'final_mes': dataFinalFormatada,
                        'id_unidades': last_id_unidades
                    },
                    success: function(r){
                        $("#graficoMedicaoConsumo").html(r);
                    }
                });
            }
        };
    }();
    window.setInterval(checkCookie, 100);
</script>