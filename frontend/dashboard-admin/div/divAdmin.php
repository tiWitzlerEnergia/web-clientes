<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php

// PRIMEIRO PRECISAMOS LISTAR TODOS OS USUÁRIOS, ÚNICOS, QUE SE LOGARAM
$cliente_id = $_SESSION['cliente_id'];

$conn_string = "host=192.168.11.160 port=5432 dbname=postgres user=apirole password=W1tzl3r3n3rg1@!#";
$conn = pg_connect($conn_string);
//$result = pg_query("select * from pc.web_login_acesso order by data_login desc limit 20");
$result = pg_query("select nome from pc.web_login_acesso order by id desc limit 20");
$result2 = pg_query("select data_login from pc.web_login_acesso order by id desc limit 20");
$result3 = pg_query("select count(*) from pc.web_login_acesso limit 20");

if (!$result || $result2 || $result3)
{
    echo "ERROR AO EXECUTAR A PESQUISA";
}


?>


<style>

    #controle_de_logs{
        text-align: left;
    }

</style>


<div class="card" id="medicoMensalConsumo" style="margin-top: -50px; margin-bottom: 35px;">
    <div class="card-header">
        <div class="row d-flex">
            <div class="col-sm-12 col-lg-4 col-md-12 col-xs-12 arquivoInput centerInMobile">
                <div class="mb-0 header-chart Col card-title col-12 text-center text-lg-right text-xl-right"><label class="text-value" id="controle_de_logs" style="margin-top: 10px;">CONTROLE DE LOGS</label></div>
            </div>
            <!--<div class="col-lg-1"></div>-->
        </div>
    </div>
    <!-- PRIMEIRO QUADRO DE GRÁFICO -->
    <div class="card-body" style="height: 1350px; ">
        <div class="chartWrapper" style="position: relative;">
            <div class="chartAreaWrapper" id="graficoMedicaoConsumo" style="overflow-x: auto;">
            <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">Cliente</th>
                        <th scope="col">Data do último login</th>
                        <th scope="col">Login nos últimos 3 meses</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $dados = array();
                            $dados2 = array();
                            $dados3 = array();
                            $nomes = array();
                            $datas = array();
                            while ($row = pg_fetch_array($result)) //&& $row2 = pg_fetch_array($result2) && $row3 = pg_fetch_array($result3))
                            {
                                array_push($dados, $row);
                                array_push($nomes, $row['nome']);
                            }

                            while ($row2 = pg_fetch_array($result2))
                            {
                                array_push($dados2, $row2);
                            }

                            while ($row3 = pg_fetch_array($result3))
                            {
                                array_push($dados3, $row3);
                            }


                        ?>
                        <?php
                            for($i=0; $i<count($dados); $i++)
                            { 
                        ?>
                        <tr>
                            <td><?php print_r($dados[$i]['nome']); ?></td>
                            <td><?php print_r($dados2[$i]['data_login']); ?></td>
                            <td><?php 
                                
                                $result4 = pg_query("select data_login from pc.web_login_acesso where nome = '$nomes[$i]'");
                                if (!$result4) 
                                {
                                    echo "ERROR AO PESQUISAR";
                                }

                                while($row4 = pg_fetch_array($result4))
                                {
                                    array_push($datas, $row4);
                                }
                            

                                //echo "---"; 
                                //print_r($datas[$i]['data_login']);

                                $tratandoData = explode('/',$datas[$i]['data_login']);
                                $mesAtual = date("m");
                                $mesAnterior = $mesAtual-1;
                                $mesAnterior2 = $mesAnterior-1;
                                $mesAnterior3 = $mesAnterior2-1;

                                //(int)$mesAtual;

                                //var_dump($mesAtual);
                                //echo $mesAtual;

                                $result5 = pg_query("select count(*) from pc.web_login_acesso where nome = '$nomes[$i]' AND mes = '$mesAtual'");
                                $result6 = pg_query("select count(*) from pc.web_login_acesso where nome = '$nomes[$i]' AND mes = '$mesAnterior'");
                                $result7 = pg_query("select count(*) from pc.web_login_acesso where nome = '$nomes[$i]' AND mes = '$mesAnterior2'");
                                $result8 = pg_query("select count(*) from pc.web_login_acesso where nome = '$nomes[$i]' AND mes = '$mesAnterior3'");

                                $acessosMesAtual = array();
                                $acessosMesAnterior = array();
                                $acessosMesAnterior2 = array();
                                $acessosMesAnterior3 = array();

                                //var_dump($result5);

                                $resultCount = pg_fetch_assoc($result5) + pg_fetch_assoc($result6) + pg_fetch_assoc($result7) + pg_fetch_assoc($result8);
                                echo $resultCount["count"];

                                
                            
                            ?></td>
                            <?php } ?>
                            
                        </tr>
                            
                        
                        
                    </tbody>
            </table>
            </div>
        </div>
    </div>
</div>




