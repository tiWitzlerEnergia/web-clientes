<?php
  $error = "";
?>
<!DOCTYPE html>
<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Witzler Energia | Mercado Livre de Energia</title>
    <link rel="apple-touch-icon" sizes="57x57" href="../bibliotecas/assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../bibliotecas/assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../bibliotecas/assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../bibliotecas/assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../bibliotecas/assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../bibliotecas/assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../bibliotecas/assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../bibliotecas/assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../bibliotecas/assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../bibliotecas/assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../bibliotecas/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../bibliotecas/assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../bibliotecas/assets/favicon/favicon-16x16.png">
    <link rel="icon" href="../bibliotecas/assets/favicon/favicon-witzler-mercado-livre-de-energia.ico">
    <link rel="manifest" href="../bibliotecas/assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Main styles for this application-->
    <link href="../bibliotecas/css/style.css" rel="stylesheet">
    <meta name="robots" content="noindex">
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script>
  </head>
  <body class="c-app flex-row align-items-center" style="background-image: url(../bibliotecas/assets/img/background-3.9902c0af.jpg); background-repeat: no-repeat; background-attachment: fixed; background-position: center; ">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-5">
          <div class="card-group">
            <div class="card p-4" style="background-color: rgba(255, 255, 255, 0.8);">
              <div class="card-body">
                <!--<h1>Login</h1>
                <p class="text-muted">Entre na sua conta</p>-->
                  <div style="display: flex; justify-content: center; align-items: center; margin-top: -10px; padding-bottom: inherit; width:80%;">
                    <img src="../bibliotecas/assets/img/logo-witzler.svg" style="padding-left: 65px;">
                    
                  </div>
                  <p class="text-muted text-center pb-3">Esqueceu sua senha? Não se preocupe! <br> Cuidamos disso também para você! :)</p>
                  <form method="post" id="formulario-email" id="formulario-email" action="recupera.php">
                  
                  <div class="input-group mb-3" style="padding-left: 25px; padding-right: 40px;">
                       <div class="input-group-prepend"><span class="input-group-text"><img src="../bibliotecas/icones/ÍCONES_USUÁRIO_18 px CIANO.svg" height="16" width="16"></span></div>
                       <input placeholder="Digite seu usuário" autocomplete="username" id="username" name="username" type="text" class="form-control" />
                    </div>

                    
                    <div class="input-group mb-3" style="padding-left: 25px; padding-right: 40px;">
                      <div class="input-group-prepend">
                      <span class="input-group-text">
                          <svg class="c-icon">
                            <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-at"></use>
                            
                          </svg></span></div>
                      <input class="form-control" type="email" placeholder="Seu email" name="email" id="email">
                    </div>
                    
                    <div class="row">
                      <div class="col-12 text-right" style="padding-right: 55px; padding-left: 40px;">
                        <button class="btn btn-primary col-12" type="submit" class="form-control" name="recupera" id="recupera" value="Enviar e-mail de recuperação">Enviar e-mail de recuperação</button>
                        
                      </div>              
                    </div>
                    <div class="row">
                      <div class="col-6 " style="padding-left: 40px; padding-top: 10px;">
                        <button class="btn btn-link px-0" type="button" onclick="window.location.href='../login/index.php'">Voltar para login!</button>
                      </div>
                    </div>
                </form>
                <?php
                  /*if(isset($_GET['erro']) && $_GET['erro'] == "ok"){
                    echo '<br/><div class="alert alert-primary alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Um email foi enviado aos administradores do sistema, por favor, espere até receber uma resposta.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                  }else if(isset($_GET['erro']) && $_GET['erro'] == "no-email"){
                    echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Nao foi inserido nenhum email.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                  }else if(isset($_GET['erro']) && $_GET['erro'] == "no-valid"){
                    echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">O email inserido nao é valido..<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                  }else if(isset($_GET['erro']) && $_GET['erro'] == "inexistente"){
                    echo '<br/><div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">Não encontramos um email para recuperação, por favor falar com a atendente!<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                  }*/
                  if(isset($_GET['msg'])){
                    $msg = strip_tags($_GET['msg']);
                    $msg = stripcslashes($msg);
                    switch($msg){
                      case "ok":
                        echo '<br/><div class="alert alert-primary alert-dismissible fade show" role="alert" style="margin-left: 25px;margin-right: 39px;margin-bottom: -15px;">A nova senha foi enviada no seu email.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div>';
                      break;
                      default:
                        echo '';
                      break;
                    }
                  }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="../bibliotecas/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
    <!--[if IE]><!-->
    <script src="../bibliotecas/vendors/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->
  </body>
</html>