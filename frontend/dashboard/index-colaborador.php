<?php
session_start();
require_once("../../backend/Classes/autoload.php");

$colaboradores = new ColaboradoresController();
$usuario = new Usuario();

$usuario->definirUsuarioColaborador();
$id_usuario = $usuario->getId_usuario();
$nome = $usuario->getUsuario();
$adminColaborador = $usuario->getAdminColaborador();
//var_dump($_SESSION['admin_colaborador']);
//$lista = $colaboradores->listaClientesUsuario($id_usuario);
if(isset($_SESSION['admin_colaborador']) && isset($_SESSION['id_colaborador'])) {
	$lista = isset($adminColaborador) && $adminColaborador != true ? $colaboradores->listaClientesUsuario($id_usuario) : $colaboradores->listaClientesAdmin();
	$infoCol = $colaboradores->infoUsuarioColaborador($nome);
	//var_dump($infoCol);
	//var_dump($adminColaborador);
} else {
	$lista  = array();
	$infoCol = "";
}

?>

<!DOCTYPE html>
<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<html lang="en">

<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Witzler Energia | Mercado Livre de Energia</title>
    <link rel="apple-touch-icon" sizes="57x57" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="60x60" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="72x72" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="76x76" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="114x114" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="120x120" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="144x144" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="152x152" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="apple-touch-icon" sizes="180x180" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="icon" type="image/png" sizes="192x192" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
    <link rel="icon" href="../bibliotecas/icones/FAVICON/FAVICON_WITZLER_ENERGIA.ico">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../bibliotecas/icones/FAVICON/FAVICON_WITZLER_ENERGIA.ico">
    <meta name="theme-color" content="#ffffff">
    <!-- Main styles for this application-->
    <link href="../bibliotecas/css/style.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css'>
    <meta name="robots" content="noindex">

    <style>
        .list-colaborador h3 {
            color: #fff;
            font-size: 1.2rem;
            font-weight: 700;
        }

        .list-item-colaborador {
            position: relative;
            display: flex;
            align-items: center;
            margin: 10px 0;
            cursor: pointer;
        }

        .list-item-colaborador:hover::before {
            width: 100%;
        }

        .list-item-colaborador .rank-colaborador {
            position: relative;
            min-width: 45px;
            height: 45px;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            margin-right: 10px;
        }

        .list-item-colaborador .rank-colaborador::before {
            content: '';
            position: absolute;
            width: 30px;
            height: 30px;
            background: #2f363e;
            border-radius: 30px;
            transform: scale(0);
            transition: 0.5s;
        }

        .list-item-colaborador:hover .rank-colaborador::before {
            transform: scale(1);
        }

        .list-item-colaborador .rank-colaborador span {
            position: relative;
            font-size: 1rem;
            font-weight: 600;
            color: #fff;
            transition: 0.5s;
        }

        .list-item-colaborador .name-colaborador {
            position: relative;
            line-height: 1.15rem;
        }

        .list-item-colaborador .name-colaborador h4 {
            font-weight: 600;
            color: #6952db;
            transition: 0.5s;
        }

        .list-item-colaborador .name-colaborador p {
            font-size: 0.8rem;
            font-weight: 400;
            color: #113759;
            transition: 0.5s;
        }

        .list-item-colaborador:hover .name-colaborador h4,
        .list-item-colaborador:hover .name-colaborador p {
            color: black;
        }

        .scroll-list-colaborador {
            height: 186px !important;
            overflow-y: auto;
        }
    </style>
</head>

<body class="c-app flex-row align-items-center" style="background-image: url(../bibliotecas/assets/img/background-3.9902c0af.jpg); background-repeat: no-repeat; background-attachment: fixed; background-position: center; ">
    <div class="wrapper">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-7">
                    <div class="login animated slideInDown card-group">
                        <div class="card p-4" style="background-color: rgba(255, 255, 255, 0.8);">
                            <div class="card-body">
                                <div style="display: flex; justify-content: center; align-items: center; margin-top: -10px; padding-bottom: inherit; width:80%;">
                                    <img src="../bibliotecas/assets/img/logo-witzler.svg" style="padding-left: 65px;">
                                </div>
                                <form id="formulario-login" id="formulario-login">
                                    <div class="input-group mb-2" style="padding-left: 25px; padding-right: 25px;">
                                        <div class="input-group-prepend"><span class="input-group-text">
                                                <img src="../bibliotecas/icones/ÍCONES_USUÁRIO_18 px CIANO.svg" height=16 width=16></span></div>
                                        <input class="form-control" type="text" placeholder="Digite o nome de uma empresa pra procurar" name="search" id="search" autocomplete="off">
                                        <!--<div class="text-right">
                                            <button class="login-btn btn btn-primary px-3" type="submit" name="login" id="login" value="login">Procurar</button>
                                        </div>-->
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-center" style="padding-left: 40px;">
                                            <button class="btn btn-link px-0" type="button" onclick="window.location.href='../../backend/logout.php'">Sair</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-center" style="padding-left: 40px;">
                                            <p class="px-0"><?php echo isset($_SESSION["admin_colaborador"]) && $_SESSION["admin_colaborador"] == true ? "É admin" : "Não é admin"; ?></p>
                                        </div>
                                    </div>
                                </form>
                                <div class="scroll-list-colaborador">
                                    <div class="list-colaborador" id="result">
                                        <!--<div class="list-item-colaborador">
                                            <div class="name-colaborador">
                                                <h4>Alex Jason</h4>
                                                <p class="mb-1">Musician</p>
                                                <p class="mb-1">Musician 2</p>
                                            </div>
                                        </div>
                                        <div class="list-item-colaborador">
                                            <div class="name-colaborador">
                                                <h4>Alex Jason</h4>
                                                <p class="mb-1">Musician</p>
                                                <p class="mb-1">Musician 2</p>
                                            </div>
                                        </div>
                                        <div class="list-item-colaborador">
                                            <div class="name-colaborador">
                                                <h4>Alex Jason</h4>
                                                <p class="mb-1">Musician</p>
                                                <p class="mb-1">Musician 2</p>
                                            </div>
                                        </div>-->
                                        <?php
                                        $totLista = count($lista) ? count($lista) : 0;
                                        if ($totLista > 0) {
                                            // For que vai listar a lista
                                            for ($i = 0; $i < $totLista; $i++) {
                                                $id_cliente = isset($lista[$i]["id_cliente"]) && is_numeric($lista[$i]["id_cliente"]) ? $lista[$i]["id_cliente"] : 0;
                                                $nome = isset($lista[$i]["nome"]) && $lista[$i]["nome"] != "" ? $lista[$i]["nome"] : "Empresa sem nome";
                                                $nome_fantasia = isset($lista[$i]["nome_fantasia"]) && $lista[$i]["nome_fantasia"] != "" ? $lista[$i]["nome_fantasia"] : "Empresa sem nome";
                                                $colaborador_tec_id = isset($lista[$i]["colaborador_tec_id"]) && is_numeric($lista[$i]["colaborador_tec_id"]) ? $lista[$i]["colaborador_tec_id"] : 0;
                                                $colaborador_comercial_id = isset($lista[$i]["colaborador_comercial_id"]) && is_numeric($lista[$i]["colaborador_comercial_id"]) ? $lista[$i]["colaborador_comercial_id"] : 0;
                                                $cow_client_id = isset($lista[$i]["cow_client_id"]) && is_numeric($lista[$i]["cow_client_id"]) ? $lista[$i]["cow_client_id"] : 0;
                                                $email = isset($lista[$i]["email"]) && $lista[$i]["email"] != "" ? $lista[$i]["email"] : "Email não definido";

                                                echo "
                                                    <div class='list-item-colaborador linhas-colaborador'>
                                                        <div class='name-colaborador col-12'>
                                                            <a href='../../backend/Classes/redirect-dashboard-colaborador.php?id_cliente_solicitado=$id_cliente'><h4>$nome</h4></a>
                                                            <p class='mb-1'>$nome_fantasia</p>
                                                            <p class='mb-1'>$email</p>
                                                        </div>
                                                    </div>
                                                ";
                                            }
                                        } else {
                                            // Mensagem onde não tem valores na conta
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="../bibliotecas/vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
    <!--[if IE]><!-->
    <script src="../bibliotecas/vendors/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->

    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
    <!--<script src="./script.js"></script>-->
    <script>
        /*$(document).ready(function() {
            $("#search").keyup(function() {
                $("#result").html('');
                var searchField = $("#search").val();
                var expression = new RegExp(searchField, "i");
                $.getJSON('../../backend/Config/search-empresas-colaborador.php', function(data) {
                    $.each(data, function(key, value) {
                        if (value.nome.search(expression) != -1 || value.nome_fantasia.search(expression) != -1 || value.email.search(expression) != -1) {
                            $("#result").append(`<div class='list-item-colaborador'><div class='name-colaborador'><a href='../../backend/Classes/redirect-dashboard-colaborador.php?id_cliente_solicitado=${value.id_cliente}'><h4>${value.nome}</h4></a><p class='mb-1'>${value.nome_fantasia}</p><p class='mb-1'>${value.email}</p></div></div>`);
                        }
                    });
                });
            });
        });*/
        
        function busca(value, targetSelector) {
            $(targetSelector).show();
            $(targetSelector + ':not(:contains("' + value + '"))').hide();
        }


        $('#search').keyup(function() {
            busca($(this).val(), '.linhas-colaborador');
        })
        
        $('#formulario-login').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    </script>
</body>

</html>
