<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<div class="row noMargin">
    <?php
    if ($totalEconomiaUnidade && $totalEconomiaUnidade != 0) {
    ?>
        <div class="col-lg-6 col-12" style="background: #154169; margin-top: 10px; margin-bottom: 74px; padding: 40px; box-shadow: 0 0.5rem 1rem rgba(0, 0, 21, 0.15) !important;">
            <div class="col=lg-12 col-12">
                <h5 class="text-white">Parabéns!</h5>
                <h5 class="text-white"><small> Você já economizou no Mercado Livre de Energia</small></h5>
                <h4 class="display-4 text-white" style="font-size: 2rem; font-weight: bold;"><strong><span class="count"><?php echo isset($arrSomaValorEconomiaUnidade) ? array_sum($arrSomaValorEconomiaUnidade) : 0; ?></span></strong></h4>
                <script>
                    // Closure
                    (function() {

                        /**
                         * Decimal adjustment of a number.
                         *
                         * @param	{String}	type	The type of adjustment.
                         * @param	{Number}	value	The number.
                         * @param	{Integer}	exp		The exponent (the 10 logarithm of the adjustment base).
                         * @returns	{Number}			The adjusted value.
                         */
                        function decimalAdjust(type, value, exp) {
                            // If the exp is undefined or zero...
                            if (typeof exp === 'undefined' || +exp === 0) {
                                return Math[type](value);
                            }
                            value = +value;
                            exp = +exp;
                            // If the value is not a number or the exp is not an integer...
                            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                                return NaN;
                            }
                            // Shift
                            value = value.toString().split('e');
                            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                            // Shift back
                            value = value.toString().split('e');
                            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
                        }

                        // Decimal round
                        if (!Math.round10) {
                            Math.round10 = function(value, exp) {
                                return decimalAdjust('round', value, exp);
                            };
                        }
                        // Decimal floor
                        if (!Math.floor10) {
                            Math.floor10 = function(value, exp) {
                                return decimalAdjust('floor', value, exp);
                            };
                        }
                        // Decimal ceil
                        if (!Math.ceil10) {
                            Math.ceil10 = function(value, exp) {
                                return decimalAdjust('ceil', value, exp);
                            };
                        }

                    })();
                    $('.count').each(function() {
                        $(this).prop('Counter', 0).animate({
                            Counter: $(this).text()
                        }, {
                            duration: 4000,
                            easing: 'swing',
                            step: function(now) {
                                $(this).text(Math.round10(now, -2).toLocaleString('pt-br', {
                                    style: 'currency',
                                    currency: 'BRL'
                                }));
                            }
                        });
                    });
                </script>
            </div>
        </div>
        <div class="col-lg-6 col-12" style="background: #d8dbe0; margin-top: 10px; margin-bottom: 74px; padding: 0; box-shadow: 0 0.5rem 1rem rgba(0, 0, 21, 0.15) !important;">
            <div class="col-lg-12 col-12">
                <div class="row col-12 noMargin noPadding">
                    <div class="col-lg-3 col-12 text-center" style="padding: 0;">
                        <img src="../bibliotecas/icones/nuvem2.gif" alt="this slowpoke moves" class="col-lg-12 col-6 noPadding" />
                    </div>
                    <div class="col-lg-9 col-12" style="padding: 30px; align-self: center;" id="carregarCo2">
                        <h6 style="color: #0c2338; font-weight: bold;" class="loadCo2 text-center">CARREGANDO...</h6>
                        <script>
                            // Wrap every letter in a span
                            var textWrapper = document.querySelector('.loadCo2');
                            textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

                            anime.timeline({
                                    loop: true
                                })
                                .add({
                                    targets: '.loadCo2 .letter',
                                    translateX: [40, 0],
                                    translateZ: 0,
                                    opacity: [0, 1],
                                    easing: "easeOutExpo",
                                    duration: 1200,
                                    delay: (el, i) => 500 + 30 * i
                                }).add({
                                    targets: '.loadCo2 .letter',
                                    translateX: [0, -30],
                                    opacity: [1, 0],
                                    easing: "easeInExpo",
                                    duration: 1100,
                                    delay: (el, i) => 100 + 30 * i
                                });
                        </script>
                    </div>
                </div>
            </div>
            <script>
                window.onload = function() {
                    // aqui vai fazer a requisicao do co2:
                    $.ajax({
                        type: 'GET',
                        url: '../../backend/economiaUnidade/co2Emitido.php',
                        beforeSend: function(carrega) {
                            //$("#updateHistoricoEconomia").html("<div class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                        },
                        success: function(r) {
                            //$("#updateHistoricoEconomia").html(r);
                            $("#carregarCo2").html(r);
                        }
                    });
                }
            </script>
        </div>
    <?php
    } else {
    ?>
        <div class="col-lg-6 col-12" style="background: #154169; margin-top: 10px; margin-bottom: 74px; padding: 40px; box-shadow: 0 0.5rem 1rem rgba(0, 0, 21, 0.15) !important;">
            <h1 class="text-white" style="margin-top: 0; margin-bottom: 0; font-size: 2.5rem;"><strong>Bem-vindo</strong></h1>
            <h1 class="display-4 text-white" style="font-size: 1.8rem; margin-bottom: 0;"><?php echo ucfirst($_SESSION['usuario']); ?>!</h1>
        </div>
        <div class="col-lg-6 col-12" style="background: #d8dbe0; margin-top: 10px; margin-bottom: 74px; padding: 0; box-shadow: 0 0.5rem 1rem rgba(0, 0, 21, 0.15) !important;">
            <div class="row col-12 noMargin noPadding">
                <div class="col-lg-3 col-12 text-center" style="padding: 0;">
                    <img src="../bibliotecas/icones/nuvem2.gif" alt="this slowpoke moves" class="col-lg-12 col-6 noPadding" />
                </div>
                <div class="col-lg-9 col-12" style="padding: 30px; align-self: center;">
                    <h6 style="color: #0c2338; font-weight: bold;">30 toneladas de CO2 não foram emitidas graças a nossa parceria!</h6>
                    <h6 style="color: #0c2338; font-weight: bold;">Isso equivale a 20 campos de futebol!</h6>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>