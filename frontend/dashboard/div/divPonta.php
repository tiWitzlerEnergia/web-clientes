<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<div class="card" id="medicoMensalConsumo" style="/*margin-top: -50px;*/ margin-bottom: 35px;">
    <div class="card-header">
        <div class="row d-flex">
            <div class="col-sm-12 col-lg-4 col-md-12 col-xs-12 arquivoInput centerInMobile">
                <div class="mb-0 header-chart Col card-title col-12 text-center text-lg-right text-xl-right"><label class="text-value" style="margin-top: 10px;">MEDIÇÃO MENSAL</label></div>
            </div>
            <!--<div class="col-lg-1"></div>-->
            <div class="col-sm-12 col-lg-2 col-md-12 col-xs-12 arquivoInput d-flex text-center justify-content-center">
                <!-- CALENDARIO -->
                <div class="col-12 justify-content-center text-center">
                    <?php include "../../backend/datas/datasPonta/calendarioDashboard.php"; ?>
                </div>
            </div>
            <!--<div class="col-lg-1"></div>-->
            <div class="col-md-12 col-sm-12 col-lg-4 col-xs-12 input-group arquivoInput">
                <div class="dropdown2 dropdown input-group-prepend col-12 col-lg-7 text-center text-lg-left text-xl-left">
                    <label class="text-value input-group-text" style="height: 35px;">Unidade: </label>
                    <!-- Aqui carrega o arquivo que tem as opcoes de unidade -->
                    <?php include "../../backend/dadosUnidades/dadosUnidades.php"; ?>
                </div>
            </div>
            <?php
            $pageName = basename($_SERVER['PHP_SELF']);
            //echo $pageName;
            if ($pageName == "index2.php") {
            ?>
                <div class="col-md-12 col-sm-12 col-lg-2 col-xs-12 arquivoInput d-flex text-center justify-content-end">
                    <button type="button" id="downloadDadosPonta" class="btn btn-primary">Baixar dados</button>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <!-- PRIMEIRO QUADRO DE GRÁFICO -->
    <div class="card-body" style="height: 400px; ">
        <div class="chartWrapper" style="position: relative;">
            <div class="chartAreaWrapper" id="graficoMedicaoConsumo" style="overflow-x: auto;">
                <script>
                    $("#graficoMedicaoConsumo").html("<div id='giftcarregar' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 380px;'><span class='loader'></span></div>");
                    setTimeout(function() {
                        document.getElementById('giftcarregar').remove();
                        document.getElementById('medicao_mensal_de_consumo').style.display = 'block';
                    }, 2500);
                </script>
                <canvas id="medicao_mensal_de_consumo" class="fade-in" width="1540" height="380" style="position: relative; display: none;"></canvas>
                <div id="testeAjax">
                    <?php
                    //Pega variaveis para gerar grafico e instancia objeto:
                    if (isset($ultimaData) && isset($arrayIdOption[0])) {
                        $dataInicial = $ultimaData;
                        $dataFinal = date("Y-m-t", strtotime($ultimaData));
                        if (isset($_COOKIE['id_unidades'])) {
                            $id_unidade = $_COOKIE['id_unidades'];
                        } else {
                            $id_unidade = $arrayIdOption[0];
                        }
                    }
                    $bar = new MedicaoMensalConsumo("medicao_mensal_de_consumo", "medidas/periodo/fora/ponta/diario?dataFinal=$dataFinal&dataInicial=$dataInicial&unidade_id=$id_unidade", array("data_medicao" => array(), "foraPonta" => array(), "ponta" => array()), $dataInicial);

                    ?>
                    <script>
                        function formatadorDeNumeroInglesParaNumeroBrasileiro(valor) {
                            if (valor == 0 || valor == null) { // CASO VAZIO OU NULO
                                return "0,0";
                            } else { // VALIDAÇÃO DA STRING
                                let numeroFinal = valor.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "###").replace(".", ",").replace("###", ".");
                                if (numeroFinal.includes(",")) {
                                    let array = numeroFinal.split(",");
                                    if (array[1].length == 1) {
                                        return numeroFinal + "0"; // SE FOR UM NÚMERO COM APENAS UMA CASA DEPOIS DA VÍRGULA, ADICIONA UM 0 NO FINAL
                                    } else { // 
                                        return numeroFinal;
                                    }
                                } else { // SE ACABAR SEM ,00 O JS VAI ACRESCENTAR NO FINAL O ,00
                                    return numeroFinal + ",00";
                                }
                            }
                        }
                        <?php
                            $bar->varChartData("medicaoMensalConsumo", array("Fora Ponta", "Ponta"), array("data_medicao", "foraPonta", "ponta"), array($_SESSION['cor-custom-1'], $_SESSION['cor-custom-2']), array($_SESSION['cor-custom-1'], $_SESSION['cor-custom-2']), 1);
                            $bar->configChartData("medicaoMensalConsumo", "bar", "teste bar");
                        ?>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- GERAR TABELA COM CSS DISPLAY NONE PARA BAIXAR ARQUIVOS -->
<?php
$mesDataInicial = date("m", strtotime($dataFinal));
$anoDataInicial = date("Y", strtotime($dataFinal));
?>
<div id="dadosPontaExcel" style="display:none;">

    <?php
    $bar->gerarValoresDownload($mesDataInicial, $anoDataInicial);
    ?>
</div>

<script>
    $("#downloadDadosPonta").click(function(e) {
        window.open('data:application/vnd.ms-excel,' + $('#dadosPontaExcel').html());
        e.preventDefault();
    });
</script>