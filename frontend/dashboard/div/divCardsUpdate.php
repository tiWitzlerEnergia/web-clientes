<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<div>
    <div class="row" style="padding: 0;">
        <?php
        include "../../backend/cardsDashboard/acumuladoMensal.php";
        ?>
        <div class="col-sm-12 col-lg-3 col-12">
            <div class="card shadow" id="consumoMensal">
                <div class="card-body text-center" id="containerConsumoMensal" style="height: 195px; align-content: center;">
                    <script>
                        $("#containerConsumoMensal").html("<div id='giftConsumoMensal' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                        setTimeout(function() {
                            document.getElementById('giftConsumoMensal').remove();
                            document.getElementById('carregarConsumoMensal').style.display = 'block';
                        }, 2500);
                    </script>

                    <div class="row fade-in" id="carregarConsumoMensal" style="display: none; height: 100%">
                        <div class="col-12 d-flex" style="height: 100%;">
                            <div class="col-lg-2 col-2 centralizar" style="padding: 0 30px !important;">
                                <!--<img src="../bibliotecas/icons/Ativo_3.svg" height=100>-->
                                <?php echo("<img src='".$_SESSION['path-imagem-default']."Ativo_3.svg' height='100' /> ")?>
                            </div>
                            <div class="col-lg-10 col-10 centralizar" style="justify-content: inherit; padding-right: 0;">
                                <div class="col-12" style="text-align: left; padding: 0;">
                                    <h6><b>Consumo</b></h6>
                                </div>
                                <div class="text-value-xl valorKWh" id="valorKWh" style="height: 36px; text-align: left; font-size: 20px;">
                                    <?php
                                    if (isset($totalAcumuladoMensal)) {
                                        echo $totalAcumuladoMensal . "<span class=\"text-muted small\" style=\"font-size: 14px;\">kWh</span>";
                                    } else {
                                        echo "--";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-lg-3 col-12">
            <div class="card shadow" id="demandaMensal">
                <div class="card-body text-center" id="containerDemandaMensal" style="height: 195px; align-content: center;">
                    <script>
                        $("#containerDemandaMensal").html("<div id='giftDemandaMensal' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                        setTimeout(function() {
                            document.getElementById('giftDemandaMensal').remove();
                            document.getElementById('carregarDemandaMensal').style.display = 'block';
                        }, 2500);
                    </script>

                    <?php
                    $mes_ref = date("Y-m-01");
                    $mes_demanda = date("Y-m-01", strtotime("-1 months"));
                    //echo $mes_demanda;
                    $demanda = new CardDemandas("cardDemanda", $mes_ref, $id_unidade);
                    //$demanda->varChartData("teste");
                    $demanda->setValuesCard();

                    $v_dfponta = is_numeric($demanda->getDemandaFponta()) ? $demanda->getDemandaFponta() : 0;
                    $v_dponta = is_numeric($demanda->getDemandaPonta()) ? $demanda->getDemandaPonta() : 0;
                    $v_cfponta = is_numeric($demanda->getCDemandaFponta()) ? $demanda->getCDemandaFponta() : 0;
                    $v_cponta = is_numeric($demanda->getCDemandaPonta()) ? $demanda->getCDemandaPonta() : 0;
                    ?>
                    <div class="row fade-in" id="carregarDemandaMensal" style="display: none; height: 100%;">
                        <div class="col-12 row justify-content-center" style="height: 100%; padding-right: 0;">
                            <div class="col-12" style="padding-right: 0;">
                                <div class="col-lg-12 col-12 linha centralizar" style="padding:0; width: 100%; justify-content: inherit;">
                                    <div class="col-12 mb-2" style="text-align: left;">
                                        <h6><b>Demanda</b></h6>
                                    </div>

                                    <div class="col-12 d-flex" id="updateDemandasCard">
                                        <div class="col-6 noPadding">
                                            <div class="col-12 noPadding mb-2" id="valorDemandaFPonta">
                                                <h5 style="margin: 0;">
                                                    <?php echo number_format($v_dfponta,2,",","."); ?>
                                                    <span class="text-muted small" style="font-size: 10px;">kW</span>
                                                </h5>
                                                <span class="text-muted small" style="font-size: 10px;" id="alteraPonta1">
                                                    Demanda Registrada Ponta
                                                </span>
                                            </div>
                                            <div class="col-12 noPadding" id="valorDemandaPonta">
                                                <h5 style="margin: 0;">
                                                    <?php echo number_format($v_dponta,2,",","."); ?>
                                                    <span class="text-muted small" style="font-size: 10px;">kW</span>
                                                </h5>
                                                <span class="text-muted small" style="font-size: 10px;">
                                                    Demanda Registrada Fora Ponta
                                                </span>
                                            </div>
                                        </div>

                                        <?php
                                        /*if ($demanda->getDemandaFPonta() == 0 || is_null($demanda->getDemandaFPonta()) || !is_numeric($demanda->getDemandaFPonta())) {
                                            echo "
                                                <script>
                                                document.getElementById('valorDemandaFPonta').style.display = 'none';
                                                </script>
                                            ";
                                        }

                                        if ($demanda->getDemandaPonta() == 0 || is_null($demanda->getDemandaPonta()) || !is_numeric($demanda->getDemandaPonta())) {
                                            
                                        }*/
                                        ?>


                                        <div class="col-6 noPadding">
                                            <div class="col-12 noPadding mb-2" id="valorCDemandaFPonta">
                                                <h5 style="margin: 0;">
                                                    <?php echo number_format($v_cfponta,2,",","."); ?>
                                                    <span class="text-muted small" style="font-size: 10px;">kW</span>
                                                </h5>
                                                <span class="text-muted small" style="font-size: 10px;" id="alteraPonta2">
                                                    Demanda Contratada Ponta
                                                </span>
                                            </div>
                                            <div class="col-12 noPadding" id="valorCDemandaPonta">
                                                <h5 style="margin: 0;">
                                                    <?php echo number_format($v_cponta,2,",","."); ?>
                                                    <span class="text-muted small" style="font-size: 10px;">kW</span>
                                                </h5>
                                                <span class="text-muted small" style="font-size: 10px;">
                                                    Demanda Contratada Fora Ponta
                                                </span>
                                            </div>
                                        </div>

                                        <?php
                                        /*if ($demanda->getCDemandaFPonta() == 0 || is_null($demanda->getCDemandaFPonta()) || !is_numeric($demanda->getCDemandaFPonta())) {
                                            echo "
                                                <script>
                                                document.getElementById('valorCDemandaFPonta').style.display = 'none';
                                                </script>
                                            ";
                                        }

                                        if ($demanda->getCDemandaPonta() == 0 || is_null($demanda->getCDemandaPonta()) || !is_numeric($demanda->getCDemandaPonta())) {
                                            
                                        }*/

                                        // atualiza textos:
                                        if (
                                            ($demanda->getCDemandaPonta() == 0 || is_null($demanda->getCDemandaPonta()) || !is_numeric($demanda->getCDemandaPonta()))
                                            && ($demanda->getDemandaPonta() == 0 || is_null($demanda->getDemandaPonta()) || !is_numeric($demanda->getDemandaPonta()))
                                        ) {
                                            echo "
                                                <script>
                                                $('#alteraPonta1').html('Demanda Registrada');
                                                $('#alteraPonta2').html('Demanda Contratada');
                                                </script>
                                            ";

                                            echo "
                                                <script>
                                                document.getElementById('valorDemandaPonta').style.display = 'none';
                                                </script>
                                            ";
                                            
                                            echo "
                                                <script>
                                                document.getElementById('valorCDemandaPonta').style.display = 'none';
                                                </script>
                                            ";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-lg-3 col-12">
            <div class="card shadow" id="fatorPotencia">
                <div class="card-body text-center" id="containerFatorPotencia" style="height: 195px; align-content: center;">
                    <script>
                        $("#containerFatorPotencia").html("<div id='giftFatorPotencia' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                        setTimeout(function() {
                            document.getElementById('giftFatorPotencia').remove();
                            document.getElementById('carregarFatorPotencia').style.display = 'block';
                        }, 2500);
                    </script>

                    <div class="row fade-in" id="carregarFatorPotencia" style="display: none; height: 100%;">
                        <div class="col-12 row justify-content-center" style="height: 100%; padding-right: 0;">
                            <div class="col-12" style="padding-right: 0;">
                                <div class="col-lg-12 col-12 linha centralizar" style="padding:0; width: 100%; justify-content: inherit;">
                                    <div class="col-12 mb-3" style="text-align: left;">
                                        <h6><b>FATOR DE POTENCIA</b></h6>
                                    </div>

                                    <div class="col-12 d-flex">
                                        <div class="col-6">
                                            <h4 id="valorCapacitivo" style="margin: 0;">
                                                <?php
                                                if (isset($mediaFormatada)) {
                                                    if (is_nan($media)) {
                                                        echo "--";
                                                    } else {
                                                        echo $mediaFormatada;
                                                    }
                                                } else {
                                                    echo "--";
                                                }
                                                ?>
                                            </h4>
                                            <span style="font-size: 10px;">
                                                Capacitivo
                                            </span>
                                        </div>

                                        <div class="col-6">
                                            <h4 id="valorIndutivo" style="margin: 0;">
                                                <?php
                                                if (isset($mediaFormatadaIndutiva)) {
                                                    if (is_nan($mediaIndutiva)) {
                                                        echo "--";
                                                    } else {
                                                        echo $mediaFormatadaIndutiva;
                                                    }
                                                } else {
                                                    echo "--";
                                                }
                                                ?>
                                            </h4>
                                            <span style="font-size: 10px;">
                                                Indutivo
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-lg-3 col-12">
            <div class="card shadow" id="bandeiraTpo">
                <div class="card-body text-center" id="containerBandeiraTipo" style="height: 195px; align-content: center;">
                    <script>
                        $("#containerBandeiraTipo").html("<div id='giftBandeiraTipo' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                        setTimeout(function() {
                            document.getElementById('giftBandeiraTipo').remove();
                            document.getElementById('carregarBandeiraTipo').style.display = 'block';
                        }, 2500);
                    </script>

                    <div class="row fade-in" id="carregarBandeiraTipo" style="display: none; height: 100%;">
                        <div class="col-12 row justify-content-center noPadding" style="height: 100%; margin: 0;">
                            <div class="col-lg-12 col-12" style="padding: 0; height: 100%;">
                                <div class="text-value-xl d-flex" id="bandeiraTarifariaView" style="height: 100%; padding-top: 7px;">
                                    <?php
                                    $mesBandeira = date("m", strtotime($dataInicial)) != "" ? date("m", strtotime($dataInicial)) : date("m");
                                    $anoBandeira = date("Y", strtotime($dataInicial)) != "" ? date("Y", strtotime($dataInicial)) : date("Y");
                                    $bandeira = new TipoBandeira($mesBandeira, $anoBandeira);
                                    if ($bandeira->getTipoBandeira() == 0) {
                                        //echo '<span style="font-size: 19px; color: green; font-weight: 900;">VER. <img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERDE/GREEN/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERDE_24 px BLUE.svg" height=100></span>';
                                        echo  '<div class="col-6 centralizar" style="padding: 0;"><img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERDE/GREEN/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERDE_24 px BLUE.svg" height=100></div><div class="col-6 centralizar" style="padding: 0; text-align: left; justify-content: inherit;"><h6 style="margin: 0; color: #113759;"><b>Bandeira Tarifária</b></h6><span style="font-size: 16px; color: green; font-weight: 900;">VERDE</span></div>';
                                    } elseif ($bandeira->getTipoBandeira() == 1) {
                                        //echo '<span style="font-size: 19px; color: rgb(233, 142, 41); font-weight: 900;">AMA. <img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_AMARELA/YELLOW/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_AMARELA_24 px BLUE.svg" height=100></span>';
                                        echo  '<div class="col-6 centralizar" style="padding: 0;"><img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_AMARELA/YELLOW/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_AMARELA_24 px BLUE.svg" height=100></div><div class="col-6 centralizar" style="padding: 0; text-align: left; justify-content: inherit;"><h6 style="margin: 0; color: #113759;"><b>Bandeira Tarifária</b></h6><span style="font-size: 16px; color: rgb(233, 142, 41); font-weight: 900;">AMARELA</span></div>';
                                    } elseif ($bandeira->getTipoBandeira() == 2) {
                                        //echo '<span style="font-size: 19px; color: red; font-weight: 900;">VERMELHA 2<img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERMELHA/RED/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS VERMELHA_24 px BLUE.svg" height=100></span>';
                                        echo  '<div class="col-6 centralizar" style="padding: 0;"><img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERMELHA/RED/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS VERMELHA_24 px BLUE.svg" height=100></div><div class="col-6 centralizar" style="padding: 0; text-align: left; justify-content: inherit;"><h6 style="margin: 0; color: #113759;"><b>Bandeira Tarifária</b></h6><span style="font-size: 16px; color: red; font-weight: 900;">VERMELHA 2</span></div>';
                                    } else if ($bandeira->getTipoBandeira() == 3) {
                                        //echo '<span style="font-size: 19px; color: red; font-weight: 900;">VERMELHA 2<img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERMELHA/RED/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS VERMELHA_24 px BLUE.svg" height=100></span>';
                                        echo  '<div class="col-6 centralizar" style="padding: 0;"><img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERMELHA/RED/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS VERMELHA_24 px BLUE.svg" height=100></div><div class="col-6 centralizar" style="padding: 0; text-align: left; justify-content: inherit;"><h6 style="margin: 0; color: #113759;"><b>Bandeira Tarifária</b></h6><span style="font-size: 16px; color: red; font-weight: 900;">VERMELHA 2</span></div>';
                                    } else if ($bandeira->getTipoBandeira() == 4) {
                                        //echo '<span style="font-size: 19px; color: black; font-weight: 900;">VERMELHA 2<img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERMELHA/RED/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS VERMELHA_24 px BLUE.svg" height=100></span>';
                                        echo  '<div class="col-6 centralizar" style="padding: 0;"><img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/bandeira_escassez_hidriva.svg" height=80 width=80></div><div class="col-6 centralizar" style="padding: 0; text-align: left; justify-content: inherit;"><h6 style="margin: 0; color: #113759;"><b>Bandeira Tarifária</b></h6><span style="font-size: 16px; color: black; font-weight: 900;">Escassez Hidrica</span></div>';
                                    } else {
                                        //echo '<span style="font-size: 19px; color: red; font-weight: 900;">VERMELHA 2<img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERMELHA/RED/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS VERMELHA_24 px BLUE.svg" height=100></span>';
                                        echo  '<div class="col-6 centralizar" style="padding: 0;"><img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERMELHA/RED/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS VERMELHA_24 px BLUE.svg" height=100></div><div class="col-6 centralizar" style="padding: 0; text-align: left; justify-content: inherit;"><h6 style="margin: 0; color: #113759;"><b>Bandeira Tarifária</b></h6><span style="font-size: 16px; color: red; font-weight: 900;">VERMELHA 2</span></div>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <?php 
    
        if($_SESSION['qualServicoEstou'] != 2){
            ?>
            <div class="row">
                <div class="col-sm-12 col-lg-12" id="cardsAcumulados" style="padding: 0;">
                    <div class="col-sm-12 col-lg-12">
                        <div class="card" id="fatorPotencia">
                            <div class="card-header d-flex text-center" style="background: #3c4b64; color: white; /*height: 106px;*/ font-weight: bold; font-size: 18px;">
                                <div class="col-12" style="align-self: center;">
                                    PLD
                                </div>
                            </div>

                            <div class="card-body row text-center col-12 noMargin" id="containPLD" style="padding: 0px;">
                                <div class="row d-flex col-12 noMargin">
                                    <div class="col-sm-12 col-lg-6 col-md-12 col-xs-12 arquivoInput d-flex text-center justify-content-center" style="padding-right: 0;">
                                        <!-- CALENDARIO -->
                                        <div class="col-12 justify-content-center text-center" style="padding-right: 0;">
                                            <?php include "../../backend/dadosRegiao/dadosRegiao.php"; ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-6 col-md-12 col-xs-12 arquivoInput d-flex text-center justify-content-center" style="padding-left: 0;">
                                        <!-- CALENDARIO -->
                                        <div class="col-12 justify-content-center text-center" style="padding-left: 0;">
                                            <?php include "../../backend/datas/datasPLD/calendarioPLD.php"; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="chartAreaWrapper col-12 noPadding" id="updatePLD" style="height: 296px;">
                                    <script>
                                        $("#updatePLD").html("<div id='giftPLD' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                                        setTimeout(function() {
                                            document.getElementById('giftPLD').remove();
                                            document.getElementById('valores_pld').style.display = 'block';
                                        }, 2500);
                                    </script>
                                    <canvas id="valores_pld" class="mx-auto fade-in col-12" height="296" style="display: none; position: relative;"></canvas>
                                    <?php
                                    $dataDia = date('Y-m-d');
                                    $pld = new Pld("valores_pld", "pldhorario?date_ref=$dataDia&region=1", array("id" => array(), "value" => array()));
                                    ?>
                                    <script>
                                        <?php
                                            $pld->varChartData("graficoPld", array("Consumo PLD"), array("value"), array($_SESSION['cor-custom-3']), array($_SESSION['cor-custom-3']));
                                            $pld->configChartData("graficoPld", "line", "Grafico de PDL");
                                        ?>
                                    </script>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }

    ?>
</div>