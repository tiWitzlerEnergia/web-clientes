<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="card">
    <div class="card-header">
        <div class="row d-flex">
            <div class="col-lg-5 col-sm-6 ">
                <div class="mb-0 header-chart Col card-title text-center text-lg-right text-sm-right"><label class="text-value align-middle" style="margin-top: 10px;">HISTÓRICO DE ECONOMIA</label></div>
            </div>
            <div class="text-right card-header-row col-sm-6 input-group col-lg-6 d-flex col-12">
                <div class="text-center text-lg-left text-xl-left col-lg-7 d-flex">
                    <input id="txtTotalCliente" readonly type="text" class="col-lg-6 align-middle form-control" value="R$ 12312312412" style="text-align: center; background-color: white; font-weight: bolder;">
                </div>
            </div>
        </div>
    </div>
    <div class="card-body" style="height: 400px; ">
        <div class="chartWrapper" style="width: 100; overflow-x: auto; overflow-y: hidden">
            <div class="chartAreaWrapper" id="carregaConsolidacao">
                <script>
                    //$("#graficoMedicaoConsumo").html("<div id='giftcarregar' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 380px;'><div class='square'></div></div>");
                    $("#carregaConsolidacao").html("<div id='loadConsolidacao' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 380px;'><span class='loader'></span></div>");
                    setTimeout(function() {
                        document.getElementById('loadConsolidacao').remove();
                        document.getElementById('historico_de_economia_consolidado_acl_x_acr').style.display = 'block';
                    }, 2500);
                </script>

                <canvas id="historico_de_economia_consolidado_acl_x_acr" class="mx-auto fade-in" width="1500" height="310" style="display: none;"></canvas>
                <?php include "../../backend/graficos/economiaConsolidado/graficoEconomiaConsolidado.php"; ?>
                <?php include "../../backend/graficos/economiaConsolidado/chartEconomiaConsolidado.php"; ?>
            </div>
        </div>
    </div>
</div>