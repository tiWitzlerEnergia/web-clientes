<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<?php include "../../backend/graficos/economiaConsolidado/graficoEconomiaConsolidado.php"; ?>
<div class="card">
   <div class="card-header">
      <div class="row d-flex align-items-center">
         <div class="col-lg-5">
            <!--<div sm="8" class="mb-0 header-chart Col card-title text-center d-flex justify-content-center"><label class="text-value">Histórico de Economia Consolidado: ACL X ACR</label></div>-->
            <div sm="8" class="mb-0 header-chart Col card-title text-center text-lg-right text-xl-right"><label class="text-value align-middle" style="margin-top: 10px;">ECONOMIA</label></div>
         </div>
         <!--<div class="text-right card-header-row pt-2 centerInMobile col-lg-2"></div>-->
         <div class="text-center text-lg-left text-xl-left col-lg-5"><input id="txtTotalCliente" readonly type="text" class="col-lg-6 align-middle form-control" value="R$ <?php echo $valorEconomizadoTotalFormatado; ?>" style="text-align: center; background-color: white; font-weight: bolder;"></div>
         <!--<div class="text-right card-header-row pt-2 centerInMobile col-lg-4"></div>-->
      </div>
   </div>
   <!-- grafico do segundo quadro -->
   <div class="card-body" style="height: 400px; ">
      <div class="chartWrapper" style="width: 100; overflow-x: auto; overflow-y: hidden">
         <div class="chartAreaWrapper" id="carregaConsolidacao">
            <script>
               //$("#graficoMedicaoConsumo").html("<div id='giftcarregar' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 380px;'><div class='square'></div></div>");
               $("#carregaConsolidacao").html("<div id='loadConsolidacao' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 380px;'><span class='loader'></span></div>");
               setTimeout(function() {
                  document.getElementById('loadConsolidacao').remove();
                  document.getElementById('historico_de_economia_consolidado_acl_x_acr').style.display = 'block';
               }, 2500);
            </script>
            <!--<canvas id="historico_de_economia_consolidado_acl_x_acr" class="mx-auto" width="1500" height="310" style="display: block;"></canvas>-->
            <canvas id="historico_de_economia_consolidado_acl_x_acr" class="mx-auto fade-in" width="1500" height="310" style="display: none;"></canvas>
         </div>
      </div>
   </div>
</div>

<?php include "../../backend/graficos/economiaConsolidado/chartEconomiaConsolidado.php"; ?>