<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="card">
   <div class="card-header">
      <div class="row d-flex">
         <div class="col-lg-2 col-sm-6 ">
            <div class="mb-0 header-chart Col card-title text-left text-lg-left text-sm-left"><label class="text-value align-middle" style="margin-top: 10px;">HISTÓRICO DE ECONOMIA</label></div>
         </div>
         <div class="text-right card-header-row col-sm-6 input-group col-lg-10 d-flex col-12">
            <div class="text-center text-lg-left text-xl-left col-lg-7 d-flex">
               <?php //include "../../backend/economiaUnidade/listarEconomiaunidade.php"; 
               ?>
               <input id="txtTotalCliente" readonly type="text" class="col-lg-6 align-middle form-control" value="R$ <?php echo isset($arrSomaValorEconomiaUnidade) ? number_format(array_sum($arrSomaValorEconomiaUnidade), 2, ',', '.') : 0; ?>" style="text-align: center; background-color: white; font-weight: bolder;">
            </div>
         </div>
      </div>
   </div>
   <div class="card-body" style="">
      <div class="chartWrapper" style="position: relative;">
         <div class="chartAreaWrapper" id="updateHistoricoEconomia" style="overflow-x: auto; height: 400px;">
            <script>
               $("#updateHistoricoEconomia").html("<div id='loadHistoricoEconomia' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
               setTimeout(function() {
                  document.getElementById('loadHistoricoEconomia').remove();
                  document.getElementById('historicoEconomia').style.display = 'block';
               }, 2500);
            </script>

            <canvas id="historicoEconomia" class="fade-in" width="1530" height="380" style="display: none; position: relative;"></canvas>
            <?php //include "../../backend/graficos/historicoEconomiaUnidade/chartHistoricoEconomiaUnidade.php"; 
            ?>
            <?php
            //recolhendo variaveis uteis:
            if (isset($_COOKIE['id_unidades'])) {
               $id_unidade = $_COOKIE['id_unidades'];
            } else {
               $id_unidade = $arrayIdOption[0];
            }
            $historicoEconomia = new HistoricoEconomia("historicoEconomia", "economia/ultimos13Meses?id_unidade=$id_unidade");
            $historicoEconomia->montarValores();
            ?>
            <script>
               <?php
                  $historicoEconomia->varChartData("dataHistorico", array("Valor ACR", "Valor ACL", "Economia [R$]", "Economia [%]"), array("dataReferencia", "valorACR", "valorACL", "valorEconomizado", "porcentagemEconomia"), array($_SESSION['cor-custom-4'], $_SESSION['cor-custom-5'], $_SESSION['cor-custom-6'], $_SESSION['cor-custom-3']), array($_SESSION['cor-custom-4'], $_SESSION['cor-custom-5'], $_SESSION['cor-custom-6'], $_SESSION['cor-custom-3']));
                  $historicoEconomia->configChartData("dataHistorico", "bar", "Historico Economia Unidade");
               ?>
            </script>
         </div>
      </div>
   </div>
</div>