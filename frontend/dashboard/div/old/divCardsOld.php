<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<div class="row">
   <div class="col-sm-12 col-lg-6" id="cardsAcumulados" style="padding: 0;">
      <?php
      include "../../backend/cardsDashboard/acumuladoMensal.php";
      ?>
      <div class="col-sm-12 col-lg-12">
         <div class="card shadow" id="acumuladoMensal">
            <div class="card-header d-flex text-center" style="background: #3c4b64; color: white; height: 106px; font-weight: bold; font-size: 18px;">
               <div class="col-12" style="align-self: center;">
                  ACUMULADO MENSAL BANDEIRA TARIFÁRIA <br /> E FATOR DE POTÊNCIA
                  <div class="dataCard" style="text-transform: uppercase;">

                     <?php
                     if (isset($dataInicial)) {
                        setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                        date_default_timezone_get('America/Sao_Paulo');
                        $dataCard = strftime('%B de %Y', strtotime($dataInicial));
                        echo htmlentities(strtoupper($dataCard));
                     } else {
                        setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                        date_default_timezone_get('America/Sao_Paulo');
                        $dataCard = strftime('%B de %Y', strtotime('today'));
                        echo htmlentities(strtoupper($dataCard));
                        /*strtolower($string);
                        mb_strtolower($string, "utf-8");*/
                     }
                     ?>
                  </div>
               </div>
            </div>



            <div class="card-body text-center" id="containerAculuadoFator" style="height: 195px; align-content: center;">
               <script>
                  $("#containerAculuadoFator").html("<div id='giftAcumuladoFator' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                  setTimeout(function() {
                     document.getElementById('giftAcumuladoFator').remove();
                     document.getElementById('carregarAcumuladoFator').style.display = 'block';
                  }, 2500);
               </script>

               <div class="row fade-in" id="carregarAcumuladoFator" style="display: none;">
                  <div class="col-12 row justify-content-center">
                     <div class="col-lg-6 col-12">
                        <div></div>
                        <div class="text-value-xl valorKWh" id="valorKWh" style="height: 36px;">

                           <?php
                           if (isset($totalAcumuladoMensal)) {
                              echo $totalAcumuladoMensal;
                           } else {
                              echo "--";
                           }
                           ?>
                        </div>
                        <div class="text-uppercase text-muted small">KWh</div>

                        <div></div>
                     </div>

                     <div class="col-lg-6 col-12">
                        <div class="text-value-xl" id="bandeiraTarifariaView" style="height: 36px; padding-top: 7px;">
                           <?php
                           $mesBandeira = date("m", strtotime($dataInicial)) != "" ? date("m", strtotime($dataInicial)) : date("m");
                           $anoBandeira = date("Y", strtotime($dataInicial)) != "" ? date("Y", strtotime($dataInicial)) : date("Y");
                           $bandeira = new TipoBandeira($mesBandeira, $anoBandeira);
                           if ($bandeira->getTipoBandeira() == 0) {
                              echo '<span style="font-size: 19px; color: green; font-weight: 900;">VER. <img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERDE/GREEN/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERDE_24 px BLUE.svg" height=40></span>';
                           } elseif ($bandeira->getTipoBandeira() == 1) {
                              echo '<span style="font-size: 19px; color: rgb(233, 142, 41); font-weight: 900;">AMA. <img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_AMARELA/YELLOW/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_AMARELA_24 px BLUE.svg" height=40></span>';
                           } elseif ($bandeira->getTipoBandeira() == 2) {
                              echo '<span style="font-size: 19px; color: red; font-weight: 900;">VERM. I<img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERMELHA/RED/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS VERMELHA_24 px BLUE.svg" height=40></span>';
                           } else {
                              echo '<span style="font-size: 19px; color: red; font-weight: 900;">VERM II<img src="../bibliotecas/icones/BANDEIRAS_TARIFÁRIAS/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS_VERMELHA/RED/ÍCONES_SITE_BANDEIRAS_TARIFÁRIAS VERMELHA_24 px BLUE.svg" height=40></span>';
                           }
                           ?>

                        </div>
                     </div>
                  </div>

                  <div class="col-12 row justify-content-center" style="padding-top: 15px;">
                     <div class="col-12 justify-content-center">
                        <div class="text-uppercase table-responsive justify-content-center">
                           <table class="table table-sm">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th><b>CAPACITIVO</b></th>
                                    <th><b>INDUTIVO</b></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <th><b></b></th>
                                    <th id="valorCapacitivo">
                                       <?php
                                       if (isset($mediaFormatada)) {
                                          if (is_nan($media)) {
                                             echo "--";
                                          } else {
                                             echo $mediaFormatada;
                                          }
                                       } else {
                                          echo "--";
                                       }
                                       ?>
                                    </th>
                                    <th id="valorIndutivo">
                                       <?php
                                       if (isset($mediaFormatadaIndutiva)) {
                                          if (is_nan($mediaIndutiva)) {
                                             echo "--";
                                          } else {
                                             echo $mediaFormatadaIndutiva;
                                          }
                                       } else {
                                          echo "--";
                                       }
                                       ?>
                                    </th>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-12 col-lg-12">
         <div class="card" id="fatorPotencia">
            <div class="card-header d-flex text-center" style="background: #3c4b64; color: white; /*height: 106px;*/ font-weight: bold; font-size: 18px;">
               <div class="col-12" style="align-self: center;">
                  PLD
                  <!--<div>
                     <?php
                     echo $dataInicialPld . " a " . $dataFinalPld;
                     ?>
                  </div>
                  (R$/MWh)-->
               </div>
            </div>

            <div class="card-body row text-center col-12 noMargin" id="containPLD" style="padding: 0px;">
               <!--<div class="chartAreaWrapper" id="updatePLD" style="overflow-x: auto; overflow-y: hidden">-->
               <div class="row d-flex col-12 noMargin">
                  <div class="col-sm-12 col-lg-6 col-md-12 col-xs-12 arquivoInput d-flex text-center justify-content-center" style="padding-right: 0;">
                     <!-- CALENDARIO -->
                     <div class="col-12 justify-content-center text-center" style="padding-right: 0;">
                        <?php include "../../backend/dadosRegiao/dadosRegiao.php"; ?>
                     </div>
                  </div>
                  <div class="col-sm-12 col-lg-6 col-md-12 col-xs-12 arquivoInput d-flex text-center justify-content-center" style="padding-left: 0;">
                     <!-- CALENDARIO -->
                     <div class="col-12 justify-content-center text-center" style="padding-left: 0;">
                        <?php include "../../backend/datas/datasPLD/calendarioPLD.php"; ?>
                     </div>
                  </div>
               </div>
               <div class="chartAreaWrapper col-12 noPadding" id="updatePLD" style="height: 296px;">
                  <script>
                     $("#updatePLD").html("<div id='giftPLD' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                     setTimeout(function() {
                        document.getElementById('giftPLD').remove();
                        document.getElementById('valores_pld').style.display = 'block';
                     }, 2500);
                  </script>
                  <canvas id="valores_pld" class="mx-auto fade-in col-12" height="296" style="display: none; position: relative;"></canvas>
                  <?php //include "../../backend/graficos/pld/graficoPLD.php"; 
                  ?>
                  <?php //include "../../backend/graficos/pld/chartPLD.php"; 
                  ?>
                  <?php
                  $dataDia = date('Y-m-d');
                  $pld = new Pld("valores_pld", "pldhorario?date_ref=$dataDia&region=1", array("id" => array(), "value" => array()));
                  ?>
                  <script>
                     <?php
                     $pld->varChartData("graficoPld", array("Consumo PLD"), array("value"), array("#34aee4"), array("#34aee9"));
                     $pld->configChartData("graficoPld", "line", "Grafico de PDL");
                     ?>
                  </script>
               </div>

            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-12 col-lg-6" style="padding: 0;">
      <div class="col-sm-12 col-lg-12">
         <div class="card" id="fatorPotenciaIndutivo">
            <div class="card-header d-flex text-center justify-content-center" style="background: #3c4b64; color: white; height: 106px; font-weight: bold; font-size: 18px;">
               <div class="col-12" style="align-self: center;">
                  <!--PRÉVIA <div class="dataCard" style="text-transform: uppercase;"><?php echo htmlentities(strtoupper($dataCard)); ?></div>-->
                  PRÉVIA <div style="text-transform: uppercase;"><?php echo htmlentities(strtoupper($dataCard)); ?></div>
               </div>
            </div>
            <div class="card-body row text-center" style="height: 622px; align-content: center; justify-content:center;">
               <div class="chartAreaWrapper" id="updatePrevia" style="overflow-x: auto; overflow-y: hidden">
                  <script>
                     $("#updatePrevia").html("<div id='carregarPrevia' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%; padding: 0; margin: 0; overflow-x: hidden; overflow-y: hidden;'><span class='loader'></span></div>");
                     setTimeout(function() {
                        document.getElementById('carregarPrevia').remove();
                        document.getElementById('valores_previa').style.display = 'block';
                     }, 2500);
                  </script>
                  <!--<canvas id="medicao_mensal_de_consumo" width="750" height="350" style="display: block; position: relative;"></canvas>-->
                  <canvas id="valores_previa" width="750" class="mx-auto fade-in" height="350" style="display: none; position: relative;"></canvas>
                  <?php include "../../backend/graficos/webPrevia/graficoEconomiaPrevia.php"; ?>
                  <?php include "../../backend/graficos/webPrevia/chartPrevia.php"; ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>