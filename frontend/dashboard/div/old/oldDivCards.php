<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="row" id="cardsAcumulados">
<?php
    include "../../backend/cardsDashboard/acumuladoMensal.php";
?>
   <div class="col-sm-12 col-lg-4">
      <div class="card" id="acumuladoMensal">
         <div class="card-header d-flex text-center" style="background: #3c4b64; color: white; height: 106px; font-weight: bold; font-size: 18px;">
            <div class="col-12" style="align-self: center;">
               ACUMULADO MENSAL 
               <div class="dataCard">
                  <?php 
                  if(isset($dataInicial)){
                     setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                     date_default_timezone_get('America/Sao_Paulo');
                     $dataCard = strftime('%B de %Y', strtotime($dataInicial));
                     echo $dataCard;
                  }else{
                     setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                     date_default_timezone_get('America/Sao_Paulo');
                     $dataCard = strftime('%B de %Y', strtotime('today'));
                     echo $dataCard;
                  }
                  ?>
               </div>
            </div>
         </div>
         <div class="card-body row text-center" style="height: 128px; align-content: center;">
            <div class="col">
               <div class="text-value-xl valorKWh" id="valorKWh"  style="height: 36px;">
                  <?php
                     if(isset($totalAcumuladoMensal)){ 
                        echo $totalAcumuladoMensal; 
                     } else{
                        echo "--";
                     }
                     ?>
               </div>
               <div class="text-uppercase text-muted small">KWh</div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-12 col-lg-4">
      <div class="card" id="fatorPotencia">
         <div class="card-header d-flex text-center" style="background: #3c4b64; color: white; height: 106px; font-weight: bold; font-size: 18px;">
            <div class="col-12" style="align-self: center;">
               FATOR DE POT�NCIA 
               <div class="dataCard">
                  <?php 
                     echo $dataCard; 
                  ?>
               </div>
            </div>
         </div>
         <div class="card-body row text-center" style="height: 128px; align-content: center;">
            <div class="col">
               <div class="text-value-xl valorFatorPotenciaCapacitativo" id="valorFatorPotenciaCapacitativo" style="height: 36px;">
                  <?php
                  if(isset($mediaFormatada)){ 
                     if(is_nan($media)){ 
                        echo "--"; 
                     }else{ 
                        echo $mediaFormatada; 
                     } 
                  }else{
                     echo "--";
                  }
                     ?>
               </div>
               <div class="text-uppercase text-muted small">Capacitivo</div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-12 col-lg-4">
      <div class="card" id="fatorPotenciaIndutivo">
         <div class="card-header d-flex text-center" style="background: #3c4b64; color: white; height: 106px; font-weight: bold; font-size: 18px;">
            <div class="col-12" style="align-self: center;">
               FATOR DE POT�NCIA <div class="dataCard"><?php echo strtoupper($dataCard); ?></div>
            </div>
         </div>
         <div class="card-body row text-center" style="height: 128px; align-content: center;">
            <div class="col">
               <div class="text-value-xl valorFatorPotenciaIndutivo" id="valorFatorPotenciaIndutivo" style="height: 36px;">
                  <?php 
                  if(isset($mediaFormatadaIndutiva)){
                     if(is_nan($mediaIndutiva)){
                        echo "--"; 
                     }else{ 
                        echo $mediaFormatadaIndutiva; 
                     } 
                  }else{
                     echo "--";
                  }
                  ?>   
               </div>
               <div class="text-uppercase text-muted small">Indutivo</div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<div class="row" id="cardsAcumulados">
   <div class="col-sm-12 col-lg-4">
      <div class="card" id="acumuladoMensal">
         <div class="card-header d-flex text-center" style="background: #3c4b64; color: white; height: 106px; font-weight: bold; font-size: 18px;">
            <div class="col-12" style="align-self: center;">
               ACUMULADO MENSAL BANDEIRA TARIFARIA 
               <div class="dataCard">
                  <?php 
                  if(isset($dataInicial)){
                     setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                     date_default_timezone_get('America/Sao_Paulo');
                     $dataCard = strftime('%B de %Y', strtotime($dataInicial));
                     echo $dataCard;
                  }else{
                     setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                     date_default_timezone_get('America/Sao_Paulo');
                     $dataCard = strftime('%B de %Y', strtotime('today'));
                     echo $dataCard;
                  }
                  ?>
               </div>
            </div>
         </div>
         <div class="card-body row text-center" style="height: 128px; align-content: center;">
            <div class="col-6">
               <div class="text-value-xl valorKWh" id="valorKWh"  style="height: 36px;">
                  <?php
                     if(isset($totalAcumuladoMensal)){ 
                        echo $totalAcumuladoMensal; 
                     } else{
                        echo "--";
                     }
                     ?>
               </div>
               <div class="text-uppercase text-muted small">KWh</div>
            </div>

            <div class="col-6">
               <div class="text-value-xl" style="height: 36px; padding-top: 7px;">
                  <span style="font-size: 19px; color: green; font-weight: 900;">VERDE</span>
               </div>
               
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-12 col-lg-4">
      <div class="card" id="fatorPotencia">
         <div class="card-header d-flex text-center" style="background: #3c4b64; color: white; height: 106px; font-weight: bold; font-size: 18px;">
            <div class="col-12" style="align-self: center;">
                  PLD 
               <div class="dataCard">
                  <?php 
                     echo $dataCard; 
                  ?>
               </div>
            </div>
         </div>

         <div class="card-body row text-center" style="height: 128px; padding: 0px;">
            <div class="col">
               <div class="text-uppercase table-responsive" style="">
                  <table class="table table-sm">
                     <thead>
                        <tr>
                           <th></th>
                           <th><b>SE/CO</b></th>
                           <th><b>S</b></th>
                           <th><b>NE</b></th>
                           <th><b>N</b></th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <th><b>PESADA</b></th>
                           <th>95,21</th>
                           <th>95,21</th>
                           <th>76,13</th>
                           <th>95,21</th>
                        </tr>
                        <tr>
                           <th><b>M�dio</b></th>
                           <th>95,01</th>
                           <th>95,01</th>
                           <th>73,14</th>
                           <th>95,01</th>
                        </tr>
                        <tr>
                           <th><b>LEVE</b></th>
                           <th>92,94</th>
                           <th>92,94</th>
                           <th>72,37</th>
                           <th>92,94</th>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-12 col-lg-4">
      <div class="card" id="fatorPotenciaIndutivo">
         <div class="card-header d-flex text-center justify-content-center" style="background: #3c4b64; color: white; height: 106px; font-weight: bold; font-size: 18px;">
            <div class="col-12" style="align-self: center;">
               FATOR DE POT�NCIA <div class="dataCard"><?php echo $dataCard; ?></div>
            </div>
         </div>
         <div class="card-body row text-center" style="height: 128px; align-content: center;">
            <div class="col-6">
               <div class="text-value-xl valorFatorPotenciaCapacitativo" id="valorFatorPotenciaCapacitativo" style="height: 36px;">
                  <?php
                  if(isset($mediaFormatada)){ 
                     if(is_nan($media)){ 
                        echo "--"; 
                     }else{ 
                        echo $mediaFormatada; 
                     } 
                  }else{
                     echo "--";
                  }
                     ?>
               </div>
               <div class="text-uppercase text-muted small">Capacitivo</div>
            </div>

            <div class="col-6">
               <div class="text-value-xl valorFatorPotenciaIndutivo" id="valorFatorPotenciaIndutivo" style="height: 36px;">
                  <?php 
                  if(isset($mediaFormatadaIndutiva)){
                     if(is_nan($mediaIndutiva)){
                        echo "--"; 
                     }else{ 
                        echo $mediaFormatadaIndutiva; 
                     } 
                  }else{
                     echo "--";
                  }
                  ?>   
               </div>
               <div class="text-uppercase text-muted small">Indutivo</div>
            </div>
         </div>
      </div>
   </div>
</div>