<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<!-- HEAD DA PAGINA -->
<?php include "../recursos/head.php"; ?>
<!-- BODY DA PAGINA -->

<body class="c-app">
  <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
  <?php include "../recursos/menu.php"; ?>

  <div class="c-subheader px-3">
    <!-- Breadcrumb-->
    <ol class="breadcrumb border-0 m-0">
      <li class="breadcrumb-item">Atualizações</li>
    </ol>
  </div>
  <!-- Fim do cabeçalho -->
  <div class="c-body">
    <main class="c-main">
      <div class="container-fluid">
        <div class="fade-in">
          <!-- SE NAO QUEREMOS QUE ARMAZENE OS VALORES AO RECARREGAR, SO TIRAR ESTE COMENTARIO COM O INCLUDE -->
          <?php include "div/pegarUltimoRegistro.php"; ?>
          <!-- ============================================================================================= -->
          <!-- NOVO -->
          <?php include "../../backend/economiaUnidade/listarEconomiaunidade.php"; ?>
          <?php include "div/divCardEconomiaMercadoLivre.php"; ?>
          <!---------->
          <!-- CARD DE MEDICAO MENSAL DE CONSUMO -->
          <?php include "div/divPonta.php"; ?>
          <!-- GRUPO DE CARD: acumulado mensal, fatores potencia -->
          <?php include "div/divCardsUpdate.php"; ?>
          <!-- Segundo quadro -->
          <?php //include "div/divEconomiaConsolidado.php"; 
          ?>
          <?php //include "div/divHistoricoEconomiaUnidade.php"; 
          ?>
          <?php include "div/divHistoricoEconomiaUnidade.php"; ?>
        </div>
      </div>
    </main>
  </div>
  <!-- INCLUDE DE CONFIG DO AJAX -->
  <?php include "div/configAjax.php"; ?>
  <!-- FOOTER -->
  <?php include "../recursos/footer.php"; ?>
