<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
  
  
  <!-- HEAD DA PAGINA -->
  <?php include "../recursos/head.php"; ?>
  <!-- BODY DA PAGINA -->
  <body class="c-app">

        <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
        <?php include "../recursos/menu.php" ?>
        
        <div class="c-subheader px-3">
          <!-- Breadcrumb-->
          <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Unidade</li>
          </ol>
        </div>
        <!-- Fim do cabeçalho -->
        <!--Començo do conteudo do site-->    
        <div class="app-body" style="padding-top: 40px;">
            
            <main class="main">
                <div class="container-fluid">
                    <div id="contract" class="animated fadeIn">
                        <div class="CardFilters hideInPrint card"></div>
                            <!-- INCLUDE DOS ROW DE CONTRATOS -->
                            <?php include "div/divDadosUnidades.php"; ?>
                        </div>
                    </div>
            </main>
            <aside class="aside-menu"></aside>
        </div>
    <!-- INCLUDE DO AJAX -->
    <?php //include "div/configAjax.php"; ?>
    <!--Fim conteudo do site-->
    <!-- FOOTER -->
    <?php include "../recursos/footer.php"; ?>