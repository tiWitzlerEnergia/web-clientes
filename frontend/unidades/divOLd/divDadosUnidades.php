<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="row" id="updateDadosUnidades">
   <?php

   include "../../backend/unidades/unidades.php";
   if (empty($arrayNomeUnidade)) {
   } else {
      $tamanhoNomeUnidades = count($arrayNomeUnidade);
      for ($i = 0; $i < $tamanhoNomeUnidades; $i++) {

   ?>
         <div class="col-12 col-sm-6 col-md-6 col-lg-4">
            <div class="card-accent-secondary card">
               <div class="card-header">
                  <div class="row">
                     <div class="col-12">
                        <div class="pt-3 card-title">
                           <h5><b><?php echo $arrayNomeUnidade[$i]; ?></b></h5>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card-body" id="containerUnidade<?php echo $i; ?>" style="height: 343px;">
                  <script>
                     $("#containerUnidade<?php echo $i; ?>").html("<div id='loadUnidade<?php echo $i; ?>' class='col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                     setTimeout(function() {
                        document.getElementById('loadUnidade<?php echo $i; ?>').remove();
                        document.getElementById('conteudoUnidade<?php echo $i; ?>').style.display = 'block';
                     }, 2500);
                  </script>

                  <div class="table-responsive fade-in" id="conteudoUnidade<?php echo $i; ?>" style="display: none;">
                     <table class="table" style="margin-top: -10px">
                        <thead>
                           <tr>
                              <th style=" border-top:0px;"></th>
                              <th class="text-center" style="border-top:0px;">ULTIMO</th>
                              <th class="text-center" style="border-top:0px;">|</th>
                              <th class="text-center" style="border-top:0px;">ATUAL</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <th style=""><b>Consumo:</b></th>
                              <td class="text-center"><?php echo $arrayTotalAcumuladoMensalAnterior[$i]; ?></td>
                              <td class="text-center" style="">|</td>
                              <td class="text-center"><?php echo $arrayTotalAcumuladoMensalAtual[$i]; ?></td>
                           </tr>
                           <tr>
                              <th style=""><b>Demanda:</b></th>
                              <td class="text-center"><?php echo $arrayDemandaForaPonta[$i][1]; ?> <?php echo $arrayDemandaPonta[$i][1]; ?></td>
                              <td class="text-center" style="">|</td>
                              <td class="text-center"><?php echo $arrayDemandaForaPonta[$i][0]; ?> <?php echo $arrayDemandaPonta[$i][0]; ?></td>
                           </tr>
                           <tr>
                              <th style=""><b>UC:</b></th>
                              <td class="text-center"></td>
                              <td class="text-center" style=""><?php echo $arrayUcUnidade[$i]; ?></td>
                              <td class="text-center"></td>
                           </tr>
                           <tr>
                              <th style=""><b>Distribuidora:</b></th>
                              <td class="text-center"></td>
                              <td class="text-center"><?php echo $arrayNomeDistribuidora[$i]; ?></td>
                              <td class="text-center"></td>
                           </tr>
                           <tr>
                              <th style=""><b>Medidor:</b></th>
                              <td class="text-center"></td>
                              <td class="text-center" style=""><?php echo $arrayMedidorUnidade[$i]; ?></td>
                              <td class="text-center"></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>




   <?php
      }
   }
   ?>

</div>