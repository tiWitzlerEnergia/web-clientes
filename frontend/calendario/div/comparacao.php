<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="row">
   <div class="col">
      <div class="card">
         <div class="text-value card-header">
            <div class="row d-flex">
               <div class="col-12 col-lg-6" style="align-self: center;">Comparação dos dias selecionados</div>
               <div class="col-12 col-lg-6 justify-content-right text-right">
                  <button type="button" class="btn btn-primary" onclick="resetarGrafico()">Reiniciar gráfico</button>
               </div>
            </div>
         </div>
         <div class="card-body" style="">
            <div class="chartWrapper" style="position: relative;">
               <div class="chartAreaWrapper" id="card-line-chart-all-days" style="overflow-x: auto; height: 400px;">
                  <!-- Config animacao -->
                  <?php $comeca = true; ?>
                  <script>
                     $("#card-line-chart-all-days").html("<div id='loadCard-line-chart-all-days' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 100%;'><span class='loader'></span></div>");
                     setTimeout(function() {
                        document.getElementById('loadCard-line-chart-all-days').remove();
                        document.getElementById('comparacao').style.display = 'block';
                     }, 2500);
                  </script>

                  <?php include "../../backend/graficos/calendario/graficoComparacao.php"; ?>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
   function resetarGrafico() {
      //Pegando datas:
      var calendario = $("#datasCalendario").val();
      var anoNovo = calendario.substring(3, 7);
      var mesNovo = calendario.substring(0, 2);
      //console.log(finalNovaData);

      var finalFechas = [];
      var finalValues = [];

      // Retira as cores claras da tabela
      $("#calendar-body").find("td").removeClass("bg-light");

      // Envia form de dados vazios para resetar grafico
      $("#card-line-chart-all-days").html("");
      $.ajax({
         type: 'POST',
         url: '../../backend/graficos/calendario/updateComparacao.php',
         data: {
            'listaItensEscolhidos': finalFechas,
            'mes': mesNovo,
            'ano': anoNovo,
            'dataSet': finalValues
         },
         success: function(r) {
            $("#card-line-chart-all-days").html(r);
            //alert(r);
         }
      });
   }
</script>