<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->

<!--
<?php
/*
//Instancia classe que vai gerar o grafico dos calendarios:
$id_unidade = isset($_COOKIE['id_unidades']) ? $_COOKIE['id_unidades'] : $arrayIdOption[0];
$dataAtual = isset($ultimaData) ? $ultimaData : date("Y-m-01");
$calendario = new Calendario("calendariochart", $id_unidade, $dataAtual);

//if(isset($_COOKIE['ultimo_dia_calendario']) && isset($_COOKIE['mes_novo_calendario']) && isset($_COOKIE['ano_novo_calendario'])){
//if(isset($ultimaData)){
if ($calendario->getDataUrl()) {
    $dataAtual = $calendario->getDataUrl();
    $limiteData = date("t", strtotime($dataAtual));
    $mes = date("m", strtotime($dataAtual));
    $ano = date("Y", strtotime($dataAtual));
    $finalDoMes = date("t", strtotime($dataAtual));
    //$tamanhoArray = count($listaMsg);
} else {
    //Pegar mes atual com php para evitar erros de variaveis
    $atual = date("Y-m-d");
    //Ultimo dia do mes atual:
    $limiteData = date("t");
    $finalDoMes = date("t");
    //Mes e ano:
    $mes = date("m");
    $ano = date("Y");
}
for ($i = 0; $i < $finalDoMes; $i++) {
    $number = $i;
    $diaTd = $i + 01;
    if ($diaTd < 10) {
        $diaTd = "0" . $diaTd;
    }
    if ($number == 0 || $number % 7 == 0) {
        echo "<tr>";
    }
?>
    <td id="<?php echo $diaTd . "-" . $mes . "-" . $ano; ?>" style="cursor: pointer; height: 160px; min-width: 223px !important;">

        <div class="chartjs-size-monitor">
            <div class="chartjs-size-monitor-expand">
                <div class=""></div>
            </div>
            <div class="chartjs-size-monitor-shrink">
                <div class=""></div>
            </div>
        </div>
        <canvas id="chart<?php echo $diaTd . "-" . $mes . "-" . $ano; ?>" resize="true" class="chartjs-render-monitor fade-in" width="223" height="160" style="display: block;"></canvas>
    </td>
<?php
    if ($number == 6) {

        echo "</tr>";
    }
}
if ($diaTd == $finalDoMes) {
    $calendario->gerarGrafico();
}*/
?>
-->
