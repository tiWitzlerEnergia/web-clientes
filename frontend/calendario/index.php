<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<!-- HEAD DA PAGINA -->
<?php include "../recursos/head.php"; ?>
<!-- BODY DA PAGINA -->

<body class="c-app">
    <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
    <?php include "../recursos/menu.php"; ?>

    <div class="c-subheader px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Calendario</li>
        </ol>
    </div>
    <!-- Fim do cabeçalho -->
    <!--Començo do conteudo do site-->
    <?php include "../../backend/graficos/calendario/pegarUltimoRegistro/recolhe.php"; ?>

    <div class="container-fluid">
        <div id="calendar" class="animated fadeIn">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <div class="row d-flex">
                                <div class="text-value col-12 col-lg-1 text-center d-flex justify-content-center">Consumo</div>
                                <div class="card-header-row pt-2 centerInMobile col-lg-3 col-md-3 col-sm-2"></div>
                                <div class="centerInMobile text-center d-flex justify-content-center col-12 col-lg-4 col-md-6 col-sm-8 arquivoInput">
                                    <div class="dropdown2 dropdown input-group-prepend no-marginright">
                                        <label class="text-value input-group-text">Unidade: </label>
                                    </div>
                                    <!-- Aqui carrega o arquivo que tem as opcoes de unidade -->
                                    <?php include "../../backend/dadosUnidades/dadosUnidades.php"; ?>
                                </div>
                                <div class="card-header-row pt-2 centerInMobile col-lg-5 col-md-3 col-sm-2"></div>
                            </div>
                        </div>
                        <div class="card-body" style="height: 933px;">
                            <div class="mb-3 row">
                                <div class="col-3 col-sm-4 col-lg-4"><button class="btn btn-outline-secondary px-3 align-middle" id="previous"><i class="c-icon c-icon-2xl cil-arrow-left"></i></button></div>
                                <div class="text-center d-flex justify-content-center col-6 col-sm-4 col-lg-4">
                                    <!-- INCLUDE DO CALENDARIO -->
                                    <?php include "../../backend/datas/datasCalendario/datasCalendario.php"; ?>
                                </div>
                                <div class="text-right col-3 col-sm-4 col-lg-4"><button class="btn btn-outline-secondary px-3 align-middle" id="next"><i class="c-icon c-icon-2xl cil-arrow-right"></i></button></div>
                                <!-- INCLUDE DOS CONFIG QUE MUDAM APENAS 1 MES -->
                                <?php include "../../backend/datas/datasCalendario/mudarMes.php"; ?>
                            </div>
                            <table class="table table-bordered table-responsive" id="calendar">
                                <!--<thead>
                                        <tr class="text-center">
                                        <th>Domingo</th>
                                        <th>Segunda</th>
                                        <th>Terça</th>
                                        <th>Quarta</th>
                                        <th>Quinta</th>
                                        <th>Sexta</th>
                                        <th>Sábado</th>
                                        </tr>
                                    </thead>-->
                                <tbody id="calendar-body">

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <?php
            //include "../../backend/graficos/calendario/graficoCalendario.php"; 
            //include "div/conteudoCalendario.php";
            //include "../../backend/graficos/calendario/chartCalendario.php"; 
            include "../../backend/graficos/calendario/comparacao/funcaoMuda.php";
            ?>
            <?php include "div/comparacao.php"; ?>
        </div>
    </div>
    <!-- INCLUDE CONFIG AJAX -->
    <?php include "div/configAjax.php"; ?>

    <!--Fim conteudo do site-->
    <!-- FOOTER -->
    <?php include "../recursos/footer.php"; ?>

    <!-- Fim -->