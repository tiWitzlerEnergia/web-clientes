<?php
require_once "../../backend/verificador.php";
header('content-type:text/html;charset=utf-8');
header("X-Frame-Options: SAMEORIGIN");
header('Access-Control-Allow-Origin: https://clientes.witzler.com.br/');
?>
<!DOCTYPE html>
<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<html lang="pt-BR">

<head>
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <script src="../bibliotecas/js/general.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <title>Witzler Energia | Mercado Livre de Energia</title>
  <!--<link rel="apple-touch-icon" sizes="57x57" href="../bibliotecas/assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../bibliotecas/assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../bibliotecas/assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../bibliotecas/assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../bibliotecas/assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../bibliotecas/assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../bibliotecas/assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../bibliotecas/assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../bibliotecas/assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../bibliotecas/assets/favicon/android-icon-192x192.png">
    <link rel="icon" href="../bibliotecas/assets/favicon/favicon-witzler-mercado-livre-de-energia.ico">
    <link rel="manifest" href="../bibliotecas/assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../bibliotecas/assets/favicon/ms-icon-144x144.png">-->

  <link rel="apple-touch-icon" sizes="57x57" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="60x60" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="72x72" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="76x76" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="114x114" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="120x120" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="144x144" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="152x152" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="apple-touch-icon" sizes="180x180" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="icon" type="image/png" sizes="192x192" href="../bibliotecas/icones/WITZLER/BLUE/ÍCONES_SITE_WITZLER_96 px_BLUE.svg">
  <link rel="icon" href="../bibliotecas/icones/FAVICON/FAVICON_WITZLER_ENERGIA.ico">
  <link rel="manifest" href="../bibliotecas/assets/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="../bibliotecas/icones/FAVICON/FAVICON_WITZLER_ENERGIA.ico">

  <meta name="theme-color" content="#ffffff">
  <!-- CALENDARIO -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.pt-BR.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.standalone.min.css">
  <script src="https://momentjs.com/downloads/moment.min.js"></script>
  <!-- Main styles for this application-->
  <link href="../bibliotecas/css/style.css" rel="stylesheet">
  <link href="../bibliotecas/css/datas.css" rel="stylesheet">
  <style>
    .c-icon:not(.c-icon-c-s):not(.c-icon-custom-size).c-icon-lg{
      width: 1.6rem;
      height: 3rem;
      font-size: 1.25rem;
    }
  </style>
  <?php
      if(isset($_SESSION['type_client'])){
        $_SESSION['cor-primaria-produto'] = "#154169";
        $_SESSION['cor-icones-topo'] = "#154169";
        $_SESSION['cor-custom-1'] = "rgba(29, 107, 170, 0.7)";
        $_SESSION['cor-custom-2'] = "rgba(76, 142, 173, 0.7)";
        $_SESSION['cor-custom-3'] = "#34aee4";
        $_SESSION['cor-custom-4'] = "rgba(134, 134, 134, 0.8)";
        $_SESSION['cor-custom-5'] = "rgba(17, 55, 89, 0.8)";
        $_SESSION['cor-custom-6'] = "rgba(135, 187, 45, 0.8)";
        $_SESSION['cor-custom-7'] = "#34aee9";
        $_SESSION['cor-custom-8'] = "#113759";
        $_SESSION['cor-custom-9'] = "#ace2e2ab";
        $_SESSION['cor-custom-10'] = "rgba(255, 159, 64, 0.8)";
        $_SESSION['path-imagem-default'] = "../bibliotecas/icones/";
        $_SESSION['qualServicoEstou'] = 0;
        if($_SESSION['type_client'] == 2){ // ECO EXP // 2 É O CERTO
          echo("<link href='../bibliotecas/css/estilos-eco-exp.css' rel='stylesheet'>");
          $_SESSION['qualServicoEstou'] = 2;
          $_SESSION['cor-primaria-produto'] = "#47b8b2";
          $_SESSION['cor-icones-topo'] = "#47b8b2";
          $_SESSION['cor-custom-1'] = "rgba(8, 99, 117,0.7)";
          $_SESSION['cor-custom-2'] = "rgba(38, 207, 240, 0.7)";
          $_SESSION['cor-custom-3'] = "#47b8b2";
          $_SESSION['cor-custom-4'] = "rgba(240, 101, 67, 0.8)";
          $_SESSION['cor-custom-5'] = "rgba(252, 163, 17, 0.7)";
          $_SESSION['cor-custom-6'] = "rgba(27, 81, 45, 0.8)";
          $_SESSION['cor-custom-7'] = "rgba(75, 36, 74, 0.8)";
          $_SESSION['cor-custom-8'] = "#086375";
          $_SESSION['cor-custom-9'] = "#AA78A6";
          $_SESSION['cor-custom-10'] = "rgba(240, 101, 67, 0.8)";
          $_SESSION['path-imagem-default'] = $_SESSION['path-imagem-default']."ICONES_TIPOS_CLIENTES_DIFERENTES/2/";
        }
      }
  ?>

  <!-- STYLES DROPDOWN -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.min.css'>
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css'>

  <!-- Global site tag (gtag.js) - Google Analytics
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>-->
  <!--script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script-->
  <link href="../bibliotecas/vendors/@coreui/chartjs/css/coreui-chartjs.css" rel="stylesheet">
  <link href="../bibliotecas/vendors/@coreui/icons/css/free.min.css" rel="stylesheet">




  <!-- ESTILO PARA O CANVAS -->
  <style>
    canvas {
      -moz-user-select: none;
      -webkit-user-select: none;
      -ms-user-select: none;
    }
  </style>


  <!-- BIBLIOTECAS PARA O FUNCIONAMENTO DO ChartJS -->
  <script src="../bibliotecas/chartjs/Chart.min.js"></script>
  <script src="../bibliotecas/chartjs/utils.js"></script>

  <!-- BIBLIOTECA DE ANIMACOES -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>


  <!-- BIBLIOTECAS PARA O FUNCIONAMENTO DO MAPA -->
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" />
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>

  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>

  <meta name="robots" content="noindex">

  <script>
    function formatadorDeNumeroInglesParaNumeroBrasileiro(valor) {
      if (valor == 0 || valor == null) { // CASO VAZIO OU NULO
        return "0,0";
      } else { // VALIDAÇÃO DA STRING
        let numeroFinal = valor.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "###").replace(".", ",").replace("###", ".");
        if (numeroFinal.includes(",")) {
          let array = numeroFinal.split(",");
          if (array[1].length == 1) {
            return numeroFinal + "0"; // SE FOR UM NÚMERO COM APENAS UMA CASA DEPOIS DA VÍRGULA, ADICIONA UM 0 NO FINAL
          } else { // 
            return numeroFinal;
          }
        } else { // SE ACABAR SEM ,00 O JS VAI ACRESCENTAR NO FINAL O ,00
          return numeroFinal + ",00";
        }
      }
    }
  </script>
</head>

<?php
require_once "../../backend/Classes/autoload.php";

// Faz registro para análise de dados:
$registro = new RegistrosController();
//$registro->salvarInfoRotasRegistroPg();
?>