<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//Recolher diretorios da URL atual:
$dir = $_SERVER["REQUEST_URI"];
$menu = new MenuDashboard();
?>
<script>
   function nomeDoMes() {
      const months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
      var now = new Date();
      return months[now.getMonth()] + "/" + now.getFullYear();
   }
</script>
<script>
   console.log(document.cookie);
</script>
<!-- começo do cabeçalho -->
<!--<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">-->
<div class="c-sidebar c-sidebar-dark c-sidebar-fixed" id="sidebar">
   <div class="c-sidebar-brand d-lg-down-none">
      <h4>Menu</h4>
      <!--<<div class="c-sidebar-brand d-lg-down-none" style="background: #fff;">
      <!--<img src="../bibliotecas/assets/img/logo-witzler.svg" width="200" height="46" class="c-sidebar-brand-full">-->
   </div>
   <ul class="c-sidebar-nav">
      <?php
      //Gerar menu:
      if (!$menu->getUsuarioNovo()) {
         //Se usuario tiver unidades:
         $menu->criarElementoMenu($dir, "../dashboard/index.php", $_SESSION['path-imagem-default']."ÍCONES_HOME_18 px CIANO.svg", "titulo1", "Dashboard");
         echo '<li class="c-sidebar-nav-title">GUIAS</li>';
         if($_SESSION['qualServicoEstou'] != 2){
            $menu->criarElementoMenu($dir, "../contratos/index.php", $_SESSION['path-imagem-default']."ÍCONES_CONTRATOS_18 px CIANO.svg", "titulo2", "Contratos");
            $menu->criarElementoMenu($dir, "../calendario/index.php", $_SESSION['path-imagem-default']."ÍCONES_CALENDÁRIO_18 px CIANO.svg", "titulo3", "Calendário");
            $menu->criarElementoMenu($dir, "../historico-medicoes/index.php", $_SESSION['path-imagem-default']."ÍCONES_HISTÓRICO DE CONSUMO_18 px CIANO.svg", "titulo4", "Histórico de Medições");
            $menu->criarElementoMenu($dir, "../historico-economia/index.php", $_SESSION['path-imagem-default']."ÍCONES_HISTÓRICO DE ECONOMIA_18 px CIANO.svg", "titulo5", "Histórico de Economia");
            $menu->criarElementoMenu($dir, "../meus-arquivos/index.php", $_SESSION['path-imagem-default']."ÍCONES_MEUS ARQUIVOS_18 px CIANO.svg", "titulo6", "Meus Arquivos ");
            $menu->criarElementoMenu($dir, "../alerta-reativos/index.php", $_SESSION['path-imagem-default']."ALERTA DE REATIVOS.svg", "titulo7", "Alerta de Reativos");
         }else{
            $menu->criarElementoMenu($dir, "../calendario/index.php", $_SESSION['path-imagem-default']."ÍCONES_CALENDÁRIO_18 px CIANO.svg", "titulo2", "Calendário");
            $menu->criarElementoMenu($dir, "../historico-medicoes/index.php", $_SESSION['path-imagem-default']."ÍCONES_HISTÓRICO DE CONSUMO_18 px CIANO.svg", "titulo3", "Histórico de Medições");
            $menu->criarElementoMenu($dir, "../historico-economia/index.php", $_SESSION['path-imagem-default']."ÍCONES_HISTÓRICO DE ECONOMIA_18 px CIANO.svg", "titulo4", "Histórico de Economia");
            $menu->criarElementoMenu($dir, "../meus-arquivos/index.php", $_SESSION['path-imagem-default']."ÍCONES_MEUS ARQUIVOS_18 px CIANO.svg", "titulo5", "Meus Arquivos ");
         }
         //$menu->criarElementoMenu($dir, "../unidades/index.php", "../bibliotecas/icones/ÍCONES_UNIDADES_18 px CIANO.svg", "titulo8", "Unidades");
         //$menu->criarElementoMenu($dir, "../relatorio/index.php", "../bibliotecas/icones/ÍCONES_UNIDADES_18 px CIANO.svg", "titulo9", "Relatórios");
      } else {
         //Se usuario nao tiver unidades:
         $menu->criarElementoMenu($dir, "../migracao/index.php", "../bibliotecas/icones/ÍCONES_HOME_18 px CIANO.svg", "titulo1", "Migração");
      }
      ?>

      <ul class="c-sidebar-nav-dropdown-items">
         <li class="c-sidebar-nav-item">
            <a rel="nofollow" class="c-sidebar-nav-link" href="../login/index.php" target="_top">
               <svg class="c-sidebar-nav-icon">
                  <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-account-logout"></use>
               </svg>
               Login
            </a>
         </li>
         <li class="c-sidebar-nav-item">
            <a rel="nofollow" class="c-sidebar-nav-link" href="../recursos/register.php" target="_top">
               <svg class="c-sidebar-nav-icon">
                  <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-account-logout"></use>
               </svg>
               Register
            </a>
         </li>
         <li class="c-sidebar-nav-item">
            <a rel="nofollow" class="c-sidebar-nav-link" href="../404/404.php" target="_top">
               <svg class="c-sidebar-nav-icon">
                  <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-bug"></use>
               </svg>
               Error 404
            </a>
         </li>
         <li class="c-sidebar-nav-item">
            <a rel="nofollow" class="c-sidebar-nav-link" href="../500/500.php" target="_top">
               <svg class="c-sidebar-nav-icon">
                  <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-bug"></use>
               </svg>
               Error 500
            </a>
         </li>
      </ul>
      </li>
</div>
<div class="c-wrapper c-fixed-components">
   <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
      <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
         <?php echo("<svg class='c-icon c-icon-lg' style='fill: ".$_SESSION['cor-icones-topo']."'>") ?>
            <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-menu"></use>
         </svg>
      </button>
      <a rel="nofollow" class="c-header-brand d-lg-none" href="#">
         <!-- <img src="../bibliotecas/assets/img/logo-witzler.svg" width="200" height="46" class="c-sidebar-brand-full">-->
         <?php echo("<img src='".$_SESSION['path-imagem-default']."logo-witzler.svg' width='200' height='46' class='c-sidebar-brand-full'>") ?>
      </a>
      <div class="c-sidebar-brand d-lg-down-none ocultarImg" id="imgNavBar" style="background: #fff; padding-left: 15px;">
         <!-- <img src="../bibliotecas/assets/img/logo-witzler.svg" width="200" height="46">-->
         <?php echo("<img src='".$_SESSION['path-imagem-default']."logo-witzler.svg' width='200' height='46'>") ?>
      </div>
      <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true" onclick="showImg()">
         <?php echo("<svg class='c-icon c-icon-lg' style='fill: ".$_SESSION['cor-icones-topo']."'>") ?>
            <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-menu"></use>
         </svg>
      </button>
      <ul class="c-header-nav d-md-down-none ml-auto mr-auto">
         <!-- <img src="../bibliotecas/assets/img/logo-witzler.svg" width="200" height="46" id="imgNavBarCenter"> -->
         <?php echo("<img src='".$_SESSION['path-imagem-default']."logo-witzler.svg' width='200' height='46' id='imgNavBarCenter'>") ?>
      </ul>
      <ul class="c-header-nav mr-4">
         <?php
         if (isset($_SESSION["id_colaborador"]) && is_numeric($_SESSION["id_colaborador"])) {
         ?>
            <li class="c-header-nav-item">
               <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalListaEmpresasColaborador">Lista de empresas</button>
            </li>
         <?php
         }
         ?>
         <li class="c-header-nav-item d-md-down-none mx-2">
            <a rel="nofollow" class="c-header-nav-link" href="../map/index.php">
               <!-- <img src="../bibliotecas/icones/" width=18 height=18> -->
               <?php echo("<img src='".$_SESSION['path-imagem-default']."ÍCONES_LOCALIZAÇÃO_18 px CIANO.svg' height='18' width='18' /> ")?>
            </a>
         </li>
         <li class="c-header-nav-item dropdown">
            <a rel="nofollow" class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
               <div class="c-avatar">
                  <!--<img class="c-avatar-img" src="<?php if (isset($_SESSION['cliente_id'])) {
                                                         echo "https://clientes.witzler.com.br/backend/assets/img/client/" . $_SESSION['cliente_id'] . ".jpg";
                                                      } ?>">-->
                  <!-- <img src="../bibliotecas/icones/ÍCONES_USUÁRIO_18 px CIANO.svg" width=18 height=18>-->
                  <?php echo("<img src='".$_SESSION['path-imagem-default']."ÍCONES_USUÁRIO_18 px CIANO.svg' height='18' width='18' /> ")?>
               </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0" style="overflow:hidden;">
               <div class="dropdown-header bg-light py-2"><strong><?php  ?></strong></div>
               <!--a rel="nofollow" class="dropdown-item" href="../perfil/index.php">
                  <img src="../bibliotecas/icones/ÍCONES_USUÁRIO_18 px CIANO.svg" class="c-icon mr-2">
                  Perfil
               </a-->
               <a rel="nofollow" class="dropdown-item" href="../configuracoes/index.php">
                  <!-- <img src="../bibliotecas/icones/ÍCONES_CONFIGURAÇÃO_18 px CIANO.svg" class="c-icon mr-2"> -->
                  <?php echo("<img src='".$_SESSION['path-imagem-default']."ÍCONES_CONFIGURAÇÃO_18 px CIANO.svg' class='c-icon mr-2' /> ")?>
                  Configurações
               </a>
               <a rel="nofollow" class="dropdown-item" href="../../backend/logout.php">
                  <!-- <img src="../bibliotecas/icones/ÍCONES_LOGOUT_18 px CIANO.svg" class="c-icon mr-2"> -->
                  <?php echo("<img src='".$_SESSION['path-imagem-default']."ÍCONES_LOGOUT_18 px CIANO.svg' class='c-icon mr-2' /> ")?>
                  Sair
               </a>
            </div>
         </li>
      </ul>
   </header>



   <!-- DIA 08/01/2021 - Config de animacao para menu -->
   <script>
      setTimeout(function() {
         const menu = document.getElementById('sidebar');
         if (menu.classList) {
            menu.classList.add('c-sidebar-lg-show');
         } else {
            menu.className += ' c-sidebar-lg-show';
         }

         <?php
         //gerar animacoes do menu:
         if (!$menu->getUsuarioNovo()) {
            $menu->criarAnimacaoElemento("titulo1");
            $menu->criarAnimacaoElemento("titulo2");
            $menu->criarAnimacaoElemento("titulo3");
            $menu->criarAnimacaoElemento("titulo4");
            $menu->criarAnimacaoElemento("titulo5");
            $menu->criarAnimacaoElemento("titulo6");
            $menu->criarAnimacaoElemento("titulo7");
            //$menu->criarAnimacaoElemento("titulo8");
            //$menu->criarAnimacaoElemento("titulo9");
         } else {
            $menu->criarAnimacaoElemento("titulo1");
         }
         ?>
      }, 700);
   </script>

   <!-- MODAL LISTA EMPRESAS EM CASO DE SER COLABORADOR -->
   <?php
   if (isset($_SESSION["id_colaborador"]) && is_numeric($_SESSION["id_colaborador"])) {
      $colaboradores = new ColaboradoresController();
      $usuario = new Usuario();

      $id_usuario = $_SESSION["id_colaborador"];
      $adminColaborador = $usuario->getAdminColaborador();
      $lista = isset($adminColaborador) && $adminColaborador != true ? $colaboradores->listaClientesUsuario($id_usuario) : $colaboradores->listaClientesAdmin();
   ?>
      <div class="modal fade" id="modalListaEmpresasColaborador" tabindex="-1" aria-labelledby="modalListaEmpresasColaboradorTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <!--<h5 class="modal-title" id="modalListaEmpresasColaboradorTitle">Modal title</h5>-->
                  <form  id="formulario-login" class="col-12 p-0"> 
	                  <input class="form-control col-12" type="text" placeholder="Digite o nome de uma empresa pra procurar" name="search" id="search" autocomplete="off">
	           </form>
               </div>
               <div class="modal-body">
                  <div class="scroll-list-colaborador">
                     <div class="list-colaborador" id="result">
                        <?php
                        $totLista = count($lista) ? count($lista) : 0;
                        if ($totLista > 0) {
                           // For que vai listar a lista
                           for ($i = 0; $i < $totLista; $i++) {
                              $id_cliente = isset($lista[$i]["id_cliente"]) && is_numeric($lista[$i]["id_cliente"]) ? $lista[$i]["id_cliente"] : 0;
                              $nome = isset($lista[$i]["nome"]) && $lista[$i]["nome"] != "" ? $lista[$i]["nome"] : "Empresa sem nome";
                              $nome_fantasia = isset($lista[$i]["nome_fantasia"]) && $lista[$i]["nome_fantasia"] != "" ? $lista[$i]["nome_fantasia"] : "Empresa sem nome";
                              $colaborador_tec_id = isset($lista[$i]["colaborador_tec_id"]) && is_numeric($lista[$i]["colaborador_tec_id"]) ? $lista[$i]["colaborador_tec_id"] : 0;
                              $colaborador_comercial_id = isset($lista[$i]["colaborador_comercial_id"]) && is_numeric($lista[$i]["colaborador_comercial_id"]) ? $lista[$i]["colaborador_comercial_id"] : 0;
                              $cow_client_id = isset($lista[$i]["cow_client_id"]) && is_numeric($lista[$i]["cow_client_id"]) ? $lista[$i]["cow_client_id"] : 0;
                              $email = isset($lista[$i]["email"]) && $lista[$i]["email"] != "" ? $lista[$i]["email"] : "Email não definido";

                              echo "
                                 <div class='list-item-colaborador linhas-colaborador'>
                                    <div class='name-colaborador col-12'>
                                       <a href='../../backend/Classes/redirect-dashboard-colaborador.php?id_cliente_solicitado=$id_cliente'><h4>$nome</h4></a>
                                       <p class='mb-1'>$nome_fantasia</p>
                                       <p class='mb-1'>$email</p>
                                    </div>
                                 </div>
                              ";
                           }
                        } else {
                           // Mensagem onde não tem valores na conta
                        }
                        ?>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button class="btn btn-primary" type="button" data-dismiss="modal">Fechar</button>
               </div>
            </div>
         </div>
      </div>

      <script>
         /*$(document).ready(function() {
            $("#search").keyup(function() {
               $("#result").html('');
               var searchField = $("#search").val();
               var expression = new RegExp(searchField, "i");
               $.getJSON('../../backend/Config/search-empresas-colaborador.php', function(data) {
                  $.each(data, function(key, value) {
                     if (value.nome.search(expression) != -1 || value.nome_fantasia.search(expression) != -1 || value.email.search(expression) != -1) {
                        $("#result").append(`<div class='list-item-colaborador'><div class='name-colaborador'><a href='../../backend/Classes/redirect-dashboard-colaborador.php?id_cliente_solicitado=${value.id_cliente}'><h4>${value.nome}</h4></a><p class='mb-1'>${value.nome_fantasia}</p><p class='mb-1'>${value.email}</p></div></div>`);
                     }
                  });
               });
            });
         });*/
         
        function busca(value, targetSelector) {
            $(targetSelector).show();
            $(targetSelector + ':not(:contains("' + value + '"))').hide();
        }


        $('#search').keyup(function() {
            busca($(this).val(), '.linhas-colaborador');
        })

        $('#formulario-login').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
      </script>
   <?php
   }
   ?>
