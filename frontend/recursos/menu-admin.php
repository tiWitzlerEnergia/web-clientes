<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//Recolher diretorios da URL atual:
$pegarDiretorios = $_SERVER["REQUEST_URI"];
?>
<script>
   function nomeDoMes() {
     const months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
     var now = new Date();
     return months[now.getMonth()] + "/" + now.getFullYear();
   }
</script>
<script>
   console.log(document.cookie);
</script>
<!-- começo do cabeçalho -->
<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
   <div class="c-sidebar-brand d-lg-down-none" style="background: #fff;">
      <img src="../bibliotecas/assets/img/logo-witzler.svg" width="200" height="46" class="c-sidebar-brand-full">
   </div>
   <ul class="c-sidebar-nav">
    <!-- ITEMS DE MENU COM IF PARA VER SE USUARIO ESTA NESSE LUGAR -->
   <li class="c-sidebar-nav-item"><a rel="nofollow" class="c-sidebar-nav-link <?php if(file_exists($pegarDiretorios)){ echo "c-active"; } ?>" href="../dashboard/index.php"><img src="../bibliotecas/icones/ÍCONES_HOME_18 px CIANO.svg" class="c-icon">Dashboard</a></li>

   
   <ul class="c-sidebar-nav-dropdown-items">
      <li class="c-sidebar-nav-item">
         <a rel="nofollow" class="c-sidebar-nav-link" href="../login/index.php" target="_top">
            <svg class="c-sidebar-nav-icon">
               <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-account-logout"></use>
            </svg>
            Login
         </a>
      </li>
      <li class="c-sidebar-nav-item">
         <a rel="nofollow" class="c-sidebar-nav-link" href="../recursos/register.php" target="_top">
            <svg class="c-sidebar-nav-icon">
               <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-account-logout"></use>
            </svg>
            Register
         </a>
      </li>
      <li class="c-sidebar-nav-item">
         <a rel="nofollow" class="c-sidebar-nav-link" href="../404/404.php" target="_top">
            <svg class="c-sidebar-nav-icon">
               <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-bug"></use>
            </svg>
            Error 404
         </a>
      </li>
      <li class="c-sidebar-nav-item">
         <a rel="nofollow" class="c-sidebar-nav-link" href="../500/500.php" target="_top">
            <svg class="c-sidebar-nav-icon">
               <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-bug"></use>
            </svg>
            Error 500
         </a>
      </li>
   </ul>
   </li>
</div>
<div class="c-wrapper c-fixed-components">
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
   <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
      <svg class="c-icon c-icon-lg">
         <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-menu"></use>
      </svg>
   </button>
   <a rel="nofollow" class="c-header-brand d-lg-none" href="#">
      <img src="../bibliotecas/assets/img/logo-witzler.svg" width="200" height="46" class="c-sidebar-brand-full">
   </a>
   <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
      <svg class="c-icon c-icon-lg">
         <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-menu"></use>
      </svg>
   </button>
   <ul class="c-header-nav d-md-down-none">
   </ul>
   <ul class="c-header-nav ml-auto mr-4">
      <li class="c-header-nav-item d-md-down-none mx-2">
         <a rel="nofollow" class="c-header-nav-link" href="../map/index.php">
            <!--<svg class="c-icon">
               <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/pin.svg"></use>
            </svg>
            <img src="../bibliotecas/vendors/@coreui/icons/svg/pin.svg" width=18 height=18>-->
            <img src="../bibliotecas/icones/ÍCONES_LOCALIZAÇÃO_18 px CIANO.svg" width=18 height=18>
         </a>
      </li>
      <li class="c-header-nav-item dropdown">
         <a rel="nofollow" class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <div class="c-avatar"><img class="c-avatar-img" src="<?php if(isset($_SESSION['cliente_id'])){ echo "https://clientes.witzler.com.br/backend/assets/img/client/".$_SESSION['cliente_id'].".jpg"; } ?>" ></div>
         </a>
         <div class="dropdown-menu dropdown-menu-right pt-0">
            <div class="dropdown-header bg-light py-2"><strong><?php  ?></strong></div>
            <!--a rel="nofollow" class="dropdown-item" href="../perfil/index.php">
               <img src="../bibliotecas/icones/ÍCONES_USUÁRIO_18 px CIANO.svg" class="c-icon mr-2">
               Perfil
            </a-->
            <a rel="nofollow" class="dropdown-item" href="../configuracoes/index.php">
               <!--<svg class="c-icon mr-2">
                  <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-settings"></use>
               </svg>-->
               <img src="../bibliotecas/icones/ÍCONES_CONFIGURAÇÃO_18 px CIANO.svg" class="c-icon mr-2">
               Configurações
            </a>
            <a rel="nofollow" class="dropdown-item" href="../../backend/logout.php">
               <!--<svg class="c-icon mr-2">
                  <use xlink:href="../bibliotecas/vendors/@coreui/icons/svg/free.svg#cil-account-logout"></use>
               </svg>-->
               <img src="../bibliotecas/icones/ÍCONES_LOGOUT_18 px CIANO.svg" class="c-icon mr-2">
               Sair
            </a>
         </div>
      </li>
   </ul>
</header>
