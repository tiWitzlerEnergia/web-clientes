<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


  <!-- HEAD DA PAGINA -->
  <?php include "../recursos/head.php"; ?>
  <!-- BODY DA PAGINA -->
  <body class="c-app">
        <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
        <?php include "../recursos/menu.php"; ?>
        
        <div class="c-subheader px-3">
          <!-- Breadcrumb-->
          <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Mapa</li>
          </ol>
        </div>
        <!-- Fim do cabeçalho -->
        <!--Començo do conteudo do site-->    
        
        <main class="main">
            <div class="container-fluid">
                <div id="mapsunits" class="animated fadeIn">
                    <div class="row">
                        <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                    <div class="mb-0 card-title"><label class="text-value">Mapa</label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body noPadding" id="containerMap">
                                <!--<script>
                                    $("#containerMap").html("<div id='loadMap' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 580px;'><span class='loader'></span></div>");
                                    setTimeout(function() {
                                        document.getElementById('loadMap').remove();
                                        document.getElementById('mapid').style.display = 'block';
                                    }, 2500);
                                </script>-->
                                <div id="mapid" class="fade-in" style="width: 100%; height: 580px; display: block;"></div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- FIM CONTEUDO -->

    <!-- FOOTER -->
    <?php include "../recursos/footer.php"; ?>
    <!-- CONFIG JAVASCRIPT PARA MAPA -->
    <?php include "map.php"; ?>