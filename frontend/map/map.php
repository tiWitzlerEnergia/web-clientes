<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<?php
//session_start();
//Recolher id_unidades de listaUnidadesCliestes e armazenar em array($lista_id_unidades):
$urlUnidades = "https://api.develop.clientes.witzler.com.br/api/unidades/cliente?username=".$_SESSION['usuario'];
$context = stream_context_create(array(
    'http' => array(
        'header' => "Authorization: Bearer ".$_SESSION['token'],
    ),
));
$recolheUnidades = file_get_contents($urlUnidades, false, $context);
$resultUnidades = json_decode($recolheUnidades);
$lista_id_unidades = [];
foreach($resultUnidades as $unidades){
    array_push($lista_id_unidades, $unidades->id_unidades);
}
//print_r($lista_id_unidades);
//echo "<br/>";
//Recolher dados de listaUnidadesNome armazenar em array comparar array $lista_id_unidades, verificar se existe e começar a listar no javascript de index.php
$urlListaNome = "https://api.develop.clientes.witzler.com.br/api/unidadesNome";
$context = stream_context_create(array(
    'http' => array(
        'header' => "Authorization: Bearer ".$_SESSION['token'],
    ),
));
$recolheListaNome = file_get_contents($urlListaNome, false, $context);
$resultListaNome = json_decode($recolheListaNome);
$lista_unidades_nome = [];
$lista_unidades_nome_id = [];
$lista_unidades_latitude = [];
$lista_unidades_longitude = [];
foreach($resultListaNome as $lista){
    if(in_array($lista->id_unidades, $lista_id_unidades)){
        array_push($lista_unidades_nome_id, $lista->id_unidades);
        array_push($lista_unidades_nome, $lista->nome);
        array_push($lista_unidades_latitude, $lista->latitude);
        array_push($lista_unidades_longitude, $lista->longitude);
    }
}
$tamanhoLista = count($lista_id_unidades);
/*echo $tamanhoLista;
print_r($lista_unidades_latitude);
for($i = 0; $i<$tamanhoLista; $i++){
    echo $lista_unidades_latitude[$i];
    echo "<br/>";
}*/

?>


<script>
        var mymap = L.map('mapid').setView([0, 0], 10);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiY2F0YWNseXNtaW5jIiwiYSI6ImNsanEybXAzbTAyOXkzcGxxYnBreGRmaHgifQ.cp2fE-8IhTULYCBUUSoc0g', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1
        }).addTo(mymap);

        <?php
        for($i = 0; $i<$tamanhoLista; $i++){
        ?>
            L.marker([<?php echo $lista_unidades_latitude[$i]; ?>, <?php echo $lista_unidades_longitude[$i]; ?>]).addTo(mymap).bindPopup("<strong><?php echo $lista_unidades_nome[$i]; ?></strong><br />Vocês estão aqui!").openPopup();
        <?php
        }
        ?>
            L.marker([-22.328347, -49.065359]).addTo(mymap).bindPopup("<strong>WITZLER</strong><br />Nós estamos aqui!").openPopup();
        var popup = L.popup();

        function onMapClick(e) {
            popup
                .setLatLng(e.latlng)
                .setContent("You clicked the map at " + e.latlng.toString())
                .openOn(mymap);
        }

        mymap.on('click', onMapClick);
        //-22.328347, -49.065359
    </script>
