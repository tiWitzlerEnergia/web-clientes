<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="container-fluid">
   <div id="mudarSenha" class="animated fadeIn">
      <div class="justify-content-center row">
         <div class="col"></div>
      </div>
      <br/>
      <div class="row">
         <div class="col">
            <div class="card">
               <div class="card-header">
                  <div class="row">
                     <div class="col">
                        <div sm="8" class="mb-0 card-title"><label class="text-value">Minha Conta</label></div>
                     </div>
                  </div>
               </div>
               <div class="card-body">
                  <div class="row">
                     <div class="mx-auto col-12 col-sm-12 col-md-6 col-lg-4" id="containerConfig">
                        <script>
                           $("#containerConfig").html("<div id='giftConfig' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 452px;'><span class='loader'></span></div>");
                           setTimeout(function(){
                              document.getElementById('giftConfig').remove();
                              document.getElementById('cardConfig').style.display = 'block';
                           }, 2500);
                        </script>

                        <div class="card-accent-secondary card fade-in" id="cardConfig" style="display: none;">
                           <div class="text-center h-25 card-header">
                              <div class="row">
                                 <div class="col">
                                    <div class="mb-0 pt-1 card-title"><label class="text-value">Alterar Senha</label></div>
                                 </div>
                              </div>
                           </div>
                           <div class="card-body">
                              <form method="post" action="minhaContaNova.php">
                                 <div class="mb-3 input-group">
                                    <div class="input-group-prepend">
                                       <span class="input-group-text">
                                          <!-- <img src="../bibliotecas/icones/ÍCONES_USUÁRIO_18 px CIANO.svg" height=16 width=16> -->
                                          <?php echo("<img src='".$_SESSION['path-imagem-default']."ÍCONES_USUÁRIO_18 px CIANO.svg' height='16' width='16' /> ")?>
                                       </span>
                                    </div>
                                    <input placeholder="Usuário" autocomplete="username" id="usuario" name="usuario" type="text" class="form-control" readonly value="<?php echo $_SESSION['usuario']; ?>">
                                 </div>
                                 <div class="mb-4 input-group">
                                    <div class="input-group-prepend">
                                       <span class="input-group-text">
                                          <!-- <img src="../bibliotecas/icones/ÍCONES_SENHA_18 px CIANO .svg" height=16 width=16> -->
                                          <?php echo("<img src='".$_SESSION['path-imagem-default']."ÍCONES_SENHA_18 px CIANO .svg' height='16' width='16' /> ")?>
                                       </span>
                                    </div>
                                    <input placeholder="Senha atual" autocomplete="current-password" id="senha" name="senha" type="password" class="senha form-control">
                                    <span class="password-icon input-group-text">
                                        <i title="Mostrar/Esconder Senha" id="senha_icon" style="cursor: pointer;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                                                <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"></path>
                                            </svg>
                                        </i>
                                    </span>
                                 </div>
                                 <div class="mb-4 input-group">
                                    <div class="input-group-prepend">
                                       <span class="input-group-text">
                                          <!-- <img src="../bibliotecas/icones/ÍCONES_SENHA_18 px CIANO .svg" height=16 width=16> -->
                                          <?php echo("<img src='".$_SESSION['path-imagem-default']."ÍCONES_SENHA_18 px CIANO .svg' height='16' width='16' /> ")?>
                                       </span>
                                    </div>
                                    <input placeholder="Nova senha " autocomplete="current-password" id="novaSenha" name="novaSenha" type="password" class="senha form-control">
                                    <span class="password-icon input-group-text">
                                        <i title="Mostrar/Esconder Senha" id="novaSenha_icon" style="cursor: pointer;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                                                <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"></path>
                                            </svg>
                                        </i>
                                    </span>
                                 </div>
                                 <div class="mb-4 input-group">
                                    <div class="input-group-prepend">
                                       <span class="input-group-text">
                                          <!-- <img src="../bibliotecas/icones/ÍCONES_SENHA_18 px CIANO .svg" height=16 width=16> -->
                                          <?php echo("<img src='".$_SESSION['path-imagem-default']."ÍCONES_SENHA_18 px CIANO .svg' height='16' width='16' /> ")?>
                                       </span>
                                    </div>
                                    <input placeholder="Confirme a senha" autocomplete="current-password" id="confirmarSenha" name="confirmarSenha" type="password" class="senha form-control">
                                        <span class="password-icon input-group-text">
                                            <i title="Mostrar/Esconder Senha" id="confirmarSenha_icon" style="cursor: pointer;">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"></path>
                                                </svg>
                                            </i>
                                        </span>
                                 </div>
                                 <div class="mb-4 input-group text-center d-flex justify-content-center">
                                    <button type="submit" name="salvar" id="salvar" class="float-right btn btn-primary">Salvar</button>
                                 </div>
                              </form>
                              <?php
                                 if(isset($_GET['msg'])){
                                    $msg = strip_tags($_GET['msg']);
                                    $msg = stripcslashes($msg);
                                    switch($msg){
                                       case "ok":
                                          echo '<div class="alert alert-primary alert-dismissible fade show" role="alert" style="margin-bottom: -15px;">A senha foi redefinida.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div><br/>';
                                       break;
                                       case "senhadiferente":
                                          echo '<div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-bottom: -15px;">As senhas são diferentes.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.65rem 1.25rem;"><span aria-hidden="true">&times;</span></button></div><br/>';
                                       break;
                                       default:
                                          echo '';
                                       break;
                                    }
                                 }
                              ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script src="div/configInputs.js"></script>