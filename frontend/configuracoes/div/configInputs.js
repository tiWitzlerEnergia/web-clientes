/* Clientes
 * @version v1.1
 * Witzler Energia (c) 2020 Equipe de Desenvolvimento.
 */

//variavel global:
var visivel = false;
//Para senha atual:
var inputSenha = document.querySelector('#senha');
var iconSenha = document.querySelector('#senha_icon');
iconSenha.addEventListener('mousedown', function() {
    visivel = true;
    inputSenha.type = 'text';
});
window.addEventListener('mouseup', function(e) {
    if (visivel) visivel = !visivel;
    inputSenha.type = "password";
});

//Para nova senha:
var novaSenha = document.querySelector('#novaSenha');
var iconNovaSenha = document.querySelector('#novaSenha_icon');
iconNovaSenha.addEventListener('mousedown', function() {
    visivel = true;
    novaSenha.type = 'text';
});
window.addEventListener('mouseup', function(e) {
    if (visivel) visivel = !visivel;
    novaSenha.type = 'password';
});

//Para confirmar nova senha:
var confirmarSenha = document.querySelector('#confirmarSenha');
var iconConfirmarSenha = document.querySelector('#confirmarSenha_icon');
iconConfirmarSenha.addEventListener('mousedown', function() {
    visivel = true;
    confirmarSenha.type = 'text';
});
window.addEventListener('mouseup', function(e) {
    if (visivel) visivel = !visivel;
    confirmarSenha.type = 'password';
});