<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
  
  
  <!-- HEAD DA PAGINA -->
  <?php include "../recursos/head.php"; ?>
  <!-- BODY DA PAGINA -->
  <body class="c-app">
        <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
        <?php include "../recursos/menu.php"; ?>

        <div class="c-subheader px-3">
          <!-- Breadcrumb-->
          <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Configurações</li>
          </ol>
        </div>
        <!-- Fim do cabeçalho -->
        <!--Començo do conteudo do site-->    
        <main class="main">
            <div class="">
                <div id="mapsunits" class="animated fadeIn">
                  <!-- INCLUDE DO CARD DAS INFOS DE PERFIL -->
                    <?php include "div/divMinhaConta.php"; ?>
                </div>
            </div>
        </main>
    <!-- FIM CONTEUDO DO SITE -->
    <!-- FOOTER -->
    <?php include "../recursos/footer.php"; ?>