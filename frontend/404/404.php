<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<!-- HEAD DA PAGINA -->
<?php include "../recursos/head.php"; ?>
  <body class="c-app flex-row align-items-center">
    <!--<div class="container">
      <div class="row justify-content-center">
        <div class="col-md-6">
          <div class="clearfix flex-row align-items-center">
            <h1 class="display-3 mr-4">404</h1><br/>
            <h4 class="pt-3">Oops! You're lost.</h4><br/>
            <p class="text-muted">The page you are looking for was not found.</p>
          </div>
          <div class="input-prepend input-group">
            <div class="input-group-prepend"><span class="input-group-text">
                <svg class="c-icon">
                  <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-magnifying-glass"></use>
                </svg></span></div>
            <input class="form-control" id="prependedInput" size="16" type="text" placeholder="What are you looking for?"><span class="input-group-append">
              <button class="btn btn-info" type="button">Search</button></span>
          </div>
        </div>
      </div>
    </div>-->
    
      <div class="container">
          <div class="justify-content-center row">
            <div class="col-md-12">
                <h1 class="text-center mt-5">Ops!</h1>
                <p class="text-center mt-5">Página não encontrada! Sentimos muito...</p>
                <hr class="mt-5 mb-5">
                <a rel="nofollow" href="../dashboard/" class="mt-2"><i class="fa fa-reply mr-2 mt-1"></i>Retorne a Home</a>
            </div>
          </div>
      </div>
    <!-- CoreUI and necessary plugins-->
    <script src="vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
    <!--[if IE]><!-->
    <script src="vendors/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->

  </body>
</html>