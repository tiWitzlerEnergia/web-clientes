<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->


<div class="row">
   <div class="col">
      <div class="card text-center">

         <div class="card-body" id="containerInfosUser">
            <script>
               $("#containerInfosUser").html("<div id='giftInfosUser' class='row col-12 d-flex align-items-center text-center justify-content-center' style='height: 600px !important;'><span class='loader'></span></div>");
               setTimeout(function() {
                  document.getElementById('giftInfosUser').remove();
                  document.getElementById('cardInfosUser').style.display = 'block';
               }, 2500);
            </script>
            <div id="cardInfosUser" class="fade-in" style="display: none;">
               <!-- INCLUDE DOS DADOS DO USUARIO DE INFORMACAO CLIENTE -->
               <?php include "../../backend/perfil/dadosUsuarioCliente.php"; ?>
               <!-- IMAGEM DO PERFIL DO USUARIO -->
               <div class="row">
                  <div class="mx-auto contentCustomerLogo" style="border-radius: 100%;border: 1px solid gray;margin: 0 auto;width: 200px;line-height: 200px;text-align: center;">
                     <img class="imgCustomerLogo" src="<?php echo $urlLogoUsuario; ?>" style="width: 150px;display: inline-block;vertical-align: middle;line-height: normal;">
                  </div>
               </div>
               <!-- NOME DO USUARIO -->
               <div class="row">
                  <span class="mt-4 mr-auto ml-auto">
                     <h1 class="text-capitalize text-center font-weight-light"><?php echo $nome_usuario_fantasia; ?></h1>
                  </span>
               </div>
               <!-- SUBTEXTO DO NOME: DESDE... -->
               <div class="row">
                  <span class="mr-auto mb-3 ml-auto font-weight-light">
                     • Desde Novembro de 2015 •
                  </span>
               </div>
               <hr>
               <?php include "../../backend/perfil/dadosAtendenteComercial.php"; ?>
               <!-- PERFIL DO GERENTE COMERCIAL -->
               <div class="row">
                  <div class="col-sm-12 col-lg-5 row">
                     <div class="col-lg-8"></div>
                     <div class="col-lg-4 text-center d-flex justify-content-center arquivoInput">
                        <p class=" labelInfo centerInMobile">Meu gerente comercial:</p>
                     </div>
                  </div>
                  <div class="col-sm-12 col-lg-2">
                     <div class="centerInMobile contentCenter arquivoInput">
                        <div class="collaborator"><img src="<?php echo $urlImagemAtendenteComercial; ?>" style="width: 55px;" class="imgCollaborator"></div>
                        <div class="collaborator"><span class="nameCollaborator"><?php echo $nomeAtendenteComercial; ?></span></div>
                     </div>
                  </div>
                  <div class="col-sm-12 col-lg-5 text-left">
                     <div class="col-lg-4 text-center d-flex justify-content-center arquivoInput">
                        <p class=" labelInfo centerInMobile">Telefone: <?php echo $telefoneAtendenteComercial . " e " . $telefoneAtendenteComercial2; ?></p>
                     </div>
                     <div class="col-lg-8"></div>
                  </div>
               </div>
               <hr>
               <?php include "../../backend/perfil/dadosAtendenteTecnico.php"; ?>
               <!-- PERFIL DO GERENTE TECNICO -->
               <div class="row">
                  <div class="col-sm-12 col-lg-5 row">
                     <div class="col-lg-8"></div>
                     <div class="col-lg-4 text-center d-flex justify-content-center arquivoInput">
                        <p class=" labelInfo centerInMobile">Meu gerente técnico:</p>
                     </div>
                  </div>
                  <div class="col-sm-12 col-lg-2">
                     <div class="centerInMobile contentCenter arquivoInput">
                        <div class="collaborator"><img src="<?php echo $urlImagemAtendenteTecnico; ?>" style="width: 55px;" class="imgCollaborator"></div>
                        <div class="collaborator"><span class="nameCollaborator"><?php echo $nomeAtendenteTecnico; ?></span></div>
                     </div>
                  </div>
                  <div class="col-sm-12 col-lg-5 text-left">
                     <div class="col-lg-4 text-center d-flex justify-content-center arquivoInput">
                        <p class=" labelInfo centerInMobile">Telefone: <?php echo $telefoneAtendenteTecnico . " e " . $telefoneAtendenteTecnico2; ?></p>
                     </div>
                     <div class="col-lg-8"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>