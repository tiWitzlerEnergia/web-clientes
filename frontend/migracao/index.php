<!--
* Clientes
* @version v1.1
* Witzler Energia (c) 2020 Equipe de Desenvolvimento.
-->
<!-- HEAD DA PAGINA -->
<?php
include "../recursos/head.php";
?>
<!-- BODY DA PAGINA -->

<body class="c-app">
    <!-- INCLUDE DO MENU E COMECO DO CABECALHO -->
    <?php
    include "../recursos/menu.php";
    //Variavel que mudara o config do select
    //$meus_arquivos = 1;
    ?>

    <div class="c-subheader px-3">
        <!-- Breadcrumb-->
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Migração</li>
        </ol>
    </div>
    <!-- Fim do cabeçalho -->
    <!--Començo do conteudo do site-->
    <br />
    <div class="container-fluid">
        <?php //require_once "../../backend/Classes/autoload.php"; ?>
        
        
        <?php include "div/divTimeLine.php"; ?>
    </div>

    <!-- FOOTER -->
    <?php include "../recursos/footer.php"; ?>
    <!-- FIM -->