<?php
$passo = new PassosMigracao();
?>
<div id="content" class="col-12 content-center">
    <ul class="timeline noMargin">
        <?php
            $passo->gerarPasso(1, "Etapa 1", "Registration", "Get here on time, it's first come first serve. Be late, get turned away.");
            $passo->gerarPasso(2, "Etapa 2", "Opening Ceremony", "Get ready for an exciting event, this will kick off in amazing fashion with MOP and Busta Rhymes as an opening show.");
            $passo->gerarPasso(3, "Etapa 3", "Main Event", "This is where it all goes down. You will compete head to head with your friends and rivals. Get ready");
            $passo->gerarPasso(4, "Etapa 4", "Closing Ceremony", "See how is the victor and who are the losers. The big stage is where the winners bask in their own glory.");
        ?>
    </ul>
</div>